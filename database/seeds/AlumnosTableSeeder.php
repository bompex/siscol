<?php

use Illuminate\Database\Seeder;
use App\Models\User;
use App\Models\Alumno;

class AlumnosTableSeeder extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$data = [
        	['firstname' =>  'JUAN CRUZ', 'lastname' => 'BORRE'   , 'dni' => '47833341'  , 'email' => 'juanchetitiborre@gmail.com'],
            ['firstname' =>  'TOBÍAS ESTEBAN', 'lastname' => 'DOMINGUEZ'   , 'dni' => '48058517'  , 'email' => 'woshi_17@hotmail.com'  ],
            ['firstname' =>  'JUAN BAUTISTA', 'lastname' => 'EMERI'   , 'dni' => '47453594'  , 'email' => 'jbauemeri@gmail.com'],
            ['firstname' =>  'MANUEL', 'lastname' => 'GARCÍA BARBERIO' , 'dni' => '47739303'  , 'email' => 'silviabarberio@hotmail.com'],
            ['firstname' =>  'MÁXIMO MANUEL', 'lastname' => 'KALASNICOI'  , 'dni' => '47516481'  , 'email' => 'jeromax0306@gmail.com' ],
            ['firstname' =>  'FAUSTO SEBASTIÁN', 'lastname' => 'PUJADO'  , 'dni' => '48019931'  , 'email' => 'faustosebpujado@gmail.com' ],
            ['firstname' =>  'BENJAMÍN ANDRÉS', 'lastname' => 'RINCÓN'  , 'dni' => '47739456'  , 'email' => 'zipon06@gmail.com' ],
            ['firstname' =>  'SOFÍA', 'lastname' => 'ACOSTA'  , 'dni' => '47480443'  , 'email' => 'acostasofia2701@gmail.com' ],
            ['firstname' =>  'PILAR', 'lastname' => 'ALBIZÚA' , 'dni' => '47739230'  , 'email' => 'pilialbizua@gmail.com' ],
            ['firstname' =>  'LOURDES', 'lastname' => 'ALLEN'   , 'dni' => '47477809'  , 'email' => 'lulu2006allen@gmail.com'],
            ['firstname' =>  'MARÍA VICTORIA', 'lastname' => 'ALONSO'  , 'dni' => '47739447'  , 'email' => 'mbdamico.cel2@gmail.com'],
            ['firstname' =>  'MAGALÍ AYLÉN', 'lastname' => 'AYMONINO'    , 'dni' => '47477805'  , 'email' => 'maguitaa2006@gmail.com'],
            ['firstname' =>  'JUANA', 'lastname' => 'BIANCHI FANTINI' , 'dni' => '48022523'  , 'email' => 'juanabianchi13@gmail.com'  ],
            ['firstname' =>  'TOMASA', 'lastname' => 'BONAFINE'    , 'dni' => '47833281'  , 'email' => 'tomasajuana0507@gmail.com' ],
            ['firstname' =>  'ELEONORA', 'lastname' => 'CABRERA' , 'dni' => '47638290'  , 'email' => 'ele54469@gmail.com'],
            ['firstname' =>  'MACARENA', 'lastname' => 'EMERI'   , 'dni' => '47453593'  , 'email' => 'macarenaemeri@gmail.com'],
            ['firstname' =>  'GIULIANA ABRIL', 'lastname' => 'FERRERO GONZALEZ'    , 'dni' => '47480448'  , 'email' => 'dylannvalloni@gmail.com'],
            ['firstname' =>  'JULIETA LUCÍA', 'lastname' => 'GONZALEZ YEZZI'  , 'dni' => '48022269'  , 'email' => '_marceyezzi@yahoo.com.ar'],
            ['firstname' =>  'GIANELLA MILAGROS', 'lastname' => 'HAAG NAVARRO'    , 'dni' => '48021062'  , 'email' => 'queonfa0@gmail.com'],
            ['firstname' =>  'LUCÍA ARACELI', 'lastname' => 'HIEBAUM' , 'dni' => '47638046'  , 'email' => 'luchibaum@outlook.com' ],
            ['firstname' =>  'CATALINA', 'lastname' => 'MÉNDEZ'  , 'dni' => '47739224'  , 'email' => 'catalinamendez321@gmail.com'],
            ['firstname' =>  'ANA', 'lastname' => 'NAGELSMIT'   , 'dni' => '47685605'  , 'email' => 'anitanagelsmit@gmail.com'  ],
            ['firstname' =>  'PRISCILA TATIANA', 'lastname' => 'NUÑEZ'   , 'dni' => '47673086'  , 'email' => 'mndavidmn@gmail.com'],
            ['firstname' =>  'CANDELA', 'lastname' => 'PIERANTONI'  , 'dni' => '47638374'  , 'email' => 'candelapierantoni@gmail.com'],
            ['firstname' =>  'ANA', 'lastname' => 'PLUNKETT'    , 'dni' => '47638319'  , 'email' => 'anitaplunkett2810@gmail.com'],
            ['firstname' =>  'MARÍA JUANA', 'lastname' => 'SOTO'    , 'dni' => '47312272'  , 'email' => 'maju.soto.06@gmail.com'],
            ['firstname' =>  'CATALINA', 'lastname' => 'TISSBERGER'  , 'dni' => '47883216'  , 'email' => 'catatiss@gmail.com'],
            ['firstname' =>  'IGNACIO', 'lastname' => 'AGUIRRE' , 'dni' => '47186181'  , 'email' => 'igbaaguirre@gmail.com' ],
            ['firstname' =>  'FELIX CARLOS', 'lastname' => 'ALMIRÓN' , 'dni' => '47013920'  , 'email' => 'tumamafelagg@gmail.com'],
            ['firstname' =>  'JUAN AGUSTÍN', 'lastname' => 'ÁLVAREZ' , 'dni' => '46827136'  , 'email' => 'bingmec9@gmail.com'],
            ['firstname' =>  'RODRIGO', 'lastname' => 'FRASSE'  , 'dni' => '47274843'  , 'email' => 'ro.frasse@gmail.com'],
            ['firstname' =>  'AGUSTÍN EZEQUIEL', 'lastname' => 'GONZALEZ'    , 'dni' => '46435232'  , 'email' => 'paravosmg@gmail.com'],
            ['firstname' =>  'FRANCO', 'lastname' => 'GRUNEWALD'   , 'dni' => '47186140'  , 'email' => 'frangrunewald2006@gmail.com'],
            ['firstname' =>  'IVAN', 'lastname' => 'GRUNEWALD'   , 'dni' => '47186141'  , 'email' => 'ivangrunewald@gmail.com'],
            ['firstname' =>  'SANTINO', 'lastname' => 'MARTURANO'   , 'dni' => '47090801'  , 'email' => 'santinomarturano@gmail.com'],
            ['firstname' =>  'TOMÁS AGUSTÍN', 'lastname' => 'ROMERO'  , 'dni' => '46697606'  , 'email' => 'romerotomas242@gmail.com'  ],
            ['firstname' =>  'MANUEL', 'lastname' => 'VIZ' , 'dni' => '47013701'  , 'email' => 'vizmanuel50@gmail.com' ],
            ['firstname' =>  'SANTIAGO', 'lastname' => 'WEIMAN'  , 'dni' => '47279319'  , 'email' => 'santiweiman01@gmail.com'],
            ['firstname' =>  'LUCÍA', 'lastname' => 'ALBIZÚA' , 'dni' => '46198232'  , 'email' => 'susanaeabt@yahoo.com'  ],
            ['firstname' =>  'JAZMÍN', 'lastname' => 'BUSTOS'  , 'dni' => '46825185'  , 'email' => 'jzmnbustos@gmail.com'  ],
            ['firstname' =>  'TIARA NOEL', 'lastname' => 'CARRERA' , 'dni' => '47274965'  , 'email' => 'tiaracarrera2006@gmail.com'],
            ['firstname' =>  'JULIETA', 'lastname' => 'de ROSA' , 'dni' => '46645601'  , 'email' => 'julietar2014@gmail.com'],
            ['firstname' =>  'SOFÍA AILÉN', 'lastname' => 'DÍAZ SAUYI'  , 'dni' => '46261745'  , 'email' => 'sofidiaz2628@gmail.com'],
            ['firstname' =>  'MARTINA CANDELA', 'lastname' => 'GALLARDÓN BÉCARES'   , 'dni' => '46901655'  , 'email' => 'marticgal@gmail.com'],
            ['firstname' =>  'SOFÍA', 'lastname' => 'GOICOCHEA DAILOFF'   , 'dni' => '46901626'  , 'email' => 'sofia_goicoechea@yahoo.com'],
            ['firstname' =>  'CANDELA', 'lastname' => 'GONZALEZ YEZZI'  , 'dni' => '47117618'  , 'email' => 'marceyezzi2@yahoo.com.ar'],
            ['firstname' =>  'MARTINA', 'lastname' => 'MAJKA'   , 'dni' => '46813598'  , 'email' => 'martinamajka2@gmail.com'],
            ['firstname' =>  'ORNELLA', 'lastname' => 'MARTINI' , 'dni' => '47090751'  , 'email' => 'ornemartbb@gmail.com'  ],
            ['firstname' =>  'ALMA', 'lastname' => 'STIEB'   , 'dni' => '47274994'  , 'email' => 'almastieb12@gmail.com' ],
            ['firstname' =>  'DELFINA', 'lastname' => 'TOURN'   , 'dni' => '47186292'  , 'email' => 'delfitourn@gmail.com'  ],
            ['firstname' =>  'IÑAKI', 'lastname' => 'CARCERENY'   , 'dni' => '46697629'  , 'email' => 'inakicarce@hotmail.com.ar' ],
            ['firstname' =>  'STÉFANO', 'lastname' => 'FILIPPI' , 'dni' => '46102889'  , 'email' => 'fanofilippi07@gmail.com'],
            ['firstname' =>  'LUCA DAMIÁN', 'lastname' => 'KERN ARELLANO'   , 'dni' => '46339790'  , 'email' => 'lucakern200@gmail.com' ],
            ['firstname' =>  'GINO LEONEL DANTE', 'lastname' => 'MORRESI' , 'dni' => '46339816'  , 'email' => 'ginomorresi@hotmail.com.ar'],
            ['firstname' =>  'JERÓNIMO JAVIER', 'lastname' => 'OCAMPO'  , 'dni' => '46750572'  , 'email' => 'jerojavier2019@gmail.com'  ],
            ['firstname' =>  'FRANCISCO', 'lastname' => 'RESTA RUPPEL'    , 'dni' => '46002044'  , 'email' => 'franciscoresta@outlook.com'],
            ['firstname' =>  'JOAQUÍN', 'lastname' => 'STARKLOFF'   , 'dni' => '46102809'  , 'email' => 'joaquistar26@gmail.com'],
            ['firstname' =>  'MAITE', 'lastname' => 'ARRARAS' , 'dni' => '46339795'  , 'email' => 'maitearraras0403@gmail.com'],
            ['firstname' =>  'BIANCA VICTORIA', 'lastname' => 'CABRERA' , 'dni' => '46435400'  , 'email' => 'bianca.cabreraaa@gmail.com'],
            ['firstname' =>  'AGOSTINA LUDMILA', 'lastname' => 'CORONEL' , 'dni' => '45908230'  , 'email' => 'coronel.agos@gmail.com'],
            ['firstname' =>  'ABRIL', 'lastname' => 'FILIPOW' , 'dni' => '46001707'  , 'email' => 'abrilfilipow@gmail.com'],
            ['firstname' =>  'MILAGROS JAZMÍN', 'lastname' => 'GONZALEZ'    , 'dni' => '47091135'  , 'email' => 'jgbahiablanca@gmail.com'],
            ['firstname' =>  'LOURDES SAMIRA', 'lastname' => 'IGARZABAL VOGEL' , 'dni' => '46339525'  , 'email' => 'samira.igarzabal@gmail.com'],
            ['firstname' =>  'CELESTE', 'lastname' => 'MACAZAGA'    , 'dni' => '46016368'  , 'email' => 'celeste.macazaga@gmail.com'],
            ['firstname' =>  'RAFAELA ANTONIA', 'lastname' => 'MOLTENI' , 'dni' => '45907871'  , 'email' => 'rafaelamolten@gmail.com'],
            ['firstname' =>  'AMPARO PILAR', 'lastname' => 'MORALES' , 'dni' => '46102837'  , 'email' => 'amparopilarmorales@gmail.com'  ],
            ['firstname' =>  'SOFÍA', 'lastname' => 'PALLEROS'    , 'dni' => '46692804'  , 'email' => 'sofipalleros@gmail.com'],
            ['firstname' =>  'MILAGROS', 'lastname' => 'QUARTARONE'  , 'dni' => '46270987'  , 'email' => 'quartaronemilagros@gmail.com'  ],
            ['firstname' =>  'FIORELLA', 'lastname' => 'REALI'   , 'dni' => '46270965'  , 'email' => 'fiorela.reali@gmail.com'],
            ['firstname' =>  'PILAR', 'lastname' => 'RODRIGUEZ DI FRANCO' , 'dni' => '46339729'  , 'email' => 'pilarrodriguez353@gmail.com'],
            ['firstname' =>  'JUAN JOSÉ', 'lastname' => 'ARIEU'   , 'dni' => '45783320'  , 'email' => 'juanjooarieu04@gmail.com'  ],
            ['firstname' =>  'LAUTARO', 'lastname' => 'BORRE'   , 'dni' => '45423212'  , 'email' => 'borrech2003@gmail.com' ],
            ['firstname' =>  'GENARO', 'lastname' => 'CERATO'  , 'dni' => '45740438'  , 'email' => 'geni.cerato@hotmail.com'],
            ['firstname' =>  'LUCIO', 'lastname' => 'DOMINICI'    , 'dni' => '45501058'  , 'email' => 'luciol.d03@hotmail.com'],
            ['firstname' =>  'ALEJO LEONEL', 'lastname' => 'FERNANDEZ'   , 'dni' => '45423018'  , 'email' => 'falejo173@gmail.com'],
            ['firstname' =>  'GEONZALO MAARTÍN', 'lastname' => 'FRASSE'  , 'dni' => '45423288'  , 'email' => 'gonfrasse@gmail.com'],
            ['firstname' =>  'JUSTO', 'lastname' => 'GARCIA BARBERIO' , 'dni' => '45783395'  , 'email' => 'jusgarciabar@gmail.com'],
            ['firstname' =>  'PEDRO FRANCISCO', 'lastname' => 'HORRACH HEREDIA' , 'dni' => '45567819'  , 'email' => 'horrach.pedro03@gmail.com' ],
            ['firstname' =>  'SEBASTIÁN', 'lastname' => 'MARTURANO'   , 'dni' => '45740486'  , 'email' => 'sebastianmarturano@gmail.com'  ],
            ['firstname' =>  'NICOLÁS', 'lastname' => 'MASSON'  , 'dni' => '45740495'  , 'email' => 'nicolasmasson03@gmail.com' ],
            ['firstname' =>  'VALENTÍN', 'lastname' => 'MÉNDEZ'  , 'dni' => '44837186'  , 'email' => 'valentinmendez24@hotmail.com'  ],
            ['firstname' =>  'LUCAS GABRIEL', 'lastname' => 'ROMANO ACOSTA'   , 'dni' => '45811870'  , 'email' => 'xxlucaspxxx@gmail.com' ],
            ['firstname' =>  'ALEJO LEONEL', 'lastname' => 'SCHROEDER'   , 'dni' => '45811879'  , 'email' => 'alejoschroeder10@hotmail.com'  ],
            ['firstname' =>  'IAN', 'lastname' => 'STEMPELS'    , 'dni' => '45033184'  , 'email' => 'ianstmpls@gmail.com'],
            ['firstname' =>  'JUAN MARTÍN', 'lastname' => 'VALVERDE BLASCO' , 'dni' => '45106055'  , 'email' => 'tintinvalverde@gmail.com'  ],
            ['firstname' =>  'CAMILA STELLA M', 'lastname' => 'ACOSTA'  , 'dni' => '45811852'  , 'email' => 'camism2405@gmail.com'  ],
            ['firstname' =>  'PALOMA BELÉN', 'lastname' => 'CANGI'   , 'dni' => '44850248'  , 'email' => 'cangipaloma@gmail.com' ],
            ['firstname' =>  'SOFÍA BELÉN', 'lastname' => 'CIOCIA'  , 'dni' => '45825045'  , 'email' => 'sofiaciocia26@gmail.com'],
            ['firstname' =>  'ZOE', 'lastname' => 'CONIL'   , 'dni' => '45501883'  , 'email' => 'zoeconil5@gmail.com'],
            ['firstname' =>  'VALENTINA', 'lastname' => 'de ROSA' , 'dni' => '45580679'  , 'email' => 'valutini466@gmail.com' ],
            ['firstname' =>  'YASMIN NAHIARA', 'lastname' => 'GABO'    , 'dni' => '44490609'  , 'email' => 'yassofiagabo@gmail.com'],
            ['firstname' =>  'ABIGAIL SOLANA', 'lastname' => 'LEIVA'   , 'dni' => '45423479'  , 'email' => 'abileiva2017@@gmail.com'],
            ['firstname' =>  'NEREA VICTORIA', 'lastname' => 'MORAN'   , 'dni' => '45781999'  , 'email' => 'nereamoran11@hotmail.com'  ],
            ['firstname' =>  'NAHIARA ARIADNA ZOE', 'lastname' => 'SOSA'    , 'dni' => '45205580'  , 'email' => 'nahiara_ariadna_zoe@hotmail.com.ar'],
            ['firstname' =>  'MARTINA LUZ', 'lastname' => 'TREJO'   , 'dni' => '45501818'  , 'email' => 'matutrejo12@gmail.com' ],
            ['firstname' =>  'IARA LEFTALI', 'lastname' => 'VIGANÓ'  , 'dni' => '44958361'  , 'email' => 'viganoiara28@gmail.com'],
            ['firstname' =>  'ROSALÍA', 'lastname' => 'VINCIULLO'   , 'dni' => '44881001'  , 'email' => 'rosaliandeah@gmail.com'],
            ['firstname' =>  'NICOLÁS', 'lastname' => 'ALOMAR'  , 'dni' => '44881812'  , 'email' => 'nicolasalomar15@gmail.com' ],
            ['firstname' =>  'NICOLÁS', 'lastname' => 'de BEISTEGUI'    , 'dni' => '44321456'  , 'email' => 'NicolasdeBeistegui@gmail.com'  ],
            ['firstname' =>  'NAHUEL ALEJANDRO', 'lastname' => 'FERNANDEZ'   , 'dni' => '44453963'  , 'email' => 'fnatigre@gmail.com'],
            ['firstname' =>  'JOAQUÍN', 'lastname' => 'IBARRONDO'   , 'dni' => '43463616'  , 'email' => 'ibarrondojoaquin@gmail.com'],
            ['firstname' =>  'JERÓNIMO NAHUEL', 'lastname' => 'KALASNICOI'  , 'dni' => '44881432'  , 'email' => 'jerojuan10@gmail.com'  ],
            ['firstname' =>  'TOMÁS', 'lastname' => 'LEDO'    , 'dni' => '44490516'  , 'email' => 'tomyledo02@gmail.com'  ],
            ['firstname' =>  'JUAN PEDRO', 'lastname' => 'POLLA'   , 'dni' => '44881193'  , 'email' => 'juanpe2003p@gmail.com' ],
            ['firstname' =>  'GENARO', 'lastname' => 'SOLDINI' , 'dni' => '44881647'  , 'email' => 'soldinigenaro2003@gmail.com'],
            ['firstname' =>  'LAUTARO', 'lastname' => 'STARKLOFF'   , 'dni' => '44562090'  , 'email' => 'lautarostark@gmail.com'],
            ['firstname' =>  'JUAN IGNACIO', 'lastname' => 'TRACOGNA'    , 'dni' => '44461875'  , 'email' => 'juaniitracogna@gmail.com'  ],
            ['firstname' =>  'ABRIL', 'lastname' => 'CALVO VEGA'  , 'dni' => '44487413'  , 'email' => 'Abril.Vega02@gmail.com'],
            ['firstname' =>  'GIULIANA', 'lastname' => 'CHIRINO' , 'dni' => '44587195'  , 'email' => 'giulianachirino1@gmail.com'],
            ['firstname' =>  'VALENTINA', 'lastname' => 'DIEZ'    , 'dni' => '44881103'  , 'email' => 'diez.valentina@icloud.com' ],
            ['firstname' =>  'VALENTINA LUJÁN', 'lastname' => 'EMERI'   , 'dni' => '44490590'  , 'email' => 'valujan2002@gmail.com' ],
            ['firstname' =>  'MARIÁ VICTORIA', 'lastname' => 'GINESI'  , 'dni' => '44425901'  , 'email' => 'mvictoriaginesi@gmail.com' ],
            ['firstname' =>  'CATALINA', 'lastname' => 'HERRERO' , 'dni' => '44837184'  , 'email' => 'catanava26@gmail.com'  ],
            ['firstname' =>  'SOFÍA MALENA', 'lastname' => 'IGARZABAL VOGEL' , 'dni' => '44881797'  , 'email' => 'malena_igarzabal@hotmail.com'  ],
            ['firstname' =>  'ALMA NIEVES', 'lastname' => 'KRASER'  , 'dni' => '44561892'  , 'email' => 'almakraser@gmail.com'  ],
            ['firstname' =>  'NAIARA LUJÁN', 'lastname' => 'MENGHINI'    , 'dni' => '44340005'  , 'email' => 'naiaramenghini@gmail.com'  ],
            ['firstname' =>  'GIANNA', 'lastname' => 'MOZZONI' , 'dni' => '44881681'  , 'email' => 'gimozzoni1@gmail.com'  ],
            ['firstname' =>  'AGUSTINA', 'lastname' => 'NAGELSMIT'   , 'dni' => '44241042'  , 'email' => 'agusnagelsmit@gmail.com'],
            ['firstname' =>  'LUNA MICHELLE', 'lastname' => 'STIEB'   , 'dni' => '44417961'  , 'email' => 'lunastieb88@gmail.com' ],
            ['firstname' =>  'JUANA', 'lastname' => 'VIDELA GARCIA'   , 'dni' => '44417737'  , 'email' => 'juanavidela417@gmail.com'  ],
            ['firstname' =>  'MATÍAS', 'lastname' => 'CHIRAMBERRO' , 'dni' => '0'  , 'email' => 'machiramberro@gmail.com'],
// ['firstname' =>  'MILAGROS', 'lastname' => 'MONTIVERO'   , 'dni' => '0'  , 'email' => 'vaneecastro31@gmail.com'],
// ['firstname' =>  'ARIADNA BELÉN', 'lastname' => 'VALENCIA'    , 'dni' => '0'  , 'email' => 'ariivalencia24@gmail.com'  ],
// ['firstname' =>  'LOURDES SELENE', 'lastname' => 'VITALE'  , 'dni' => '0'  , 'email' => 'lourdes.s.vitale@gmail.com'],
// ['firstname' =>  'MÁXIMO HERNAN', 'lastname' => 'PALUMBO' , 'dni' => '0'  , 'email' => 'Maxipalumbo4@gmail.com'],
// ['firstname' =>  'LEONEL', 'lastname' => 'ARCE CRUZ'   , 'dni' => '0'  , 'email' => 'leoarce555@gmail.com'  ],
// ['firstname' =>  'FRANCISCO DIEGO', 'lastname' => 'BAIER'   , 'dni' => '0'  , 'email' => 'franciscobaier64@gmail.com'],
// ['firstname' =>  'RAMIRO', 'lastname' => 'CANOSA'  , 'dni' => '0'  , 'email' => 'ericacastro_9@hotmail.com' ],
// ['firstname' =>  'JOAQUÍN', 'lastname' => 'DUARTE'  , 'dni' => '0'  , 'email' => 'joacoduarte@gmail.com' ],
// ['firstname' =>  'MARTINIANO NADIR', 'lastname' => 'ELIAS'   , 'dni' => '0'  , 'email' => 'martieliasng741@gmail.com' ],
// ['firstname' =>  'ALEJO', 'lastname' => 'FERNANDEZ GAITAN'    , 'dni' => '0'  , 'email' => 'alejofg2018@gmail.com' ],
// ['firstname' =>  'JEREMÍAS', 'lastname' => 'GARCIA RODRIGUEZ'    , 'dni' => '0'  , 'email' => 'jeregr18@gmail.com'],
// ['firstname' =>  'PEDRO', 'lastname' => 'GINESI'  , 'dni' => '0'  , 'email' => 'pedroginesi08@gmail.com'   ],
// ['firstname' =>  'FRANCISCO', 'lastname' => 'MATTIOLI'    , 'dni' => '0'  , 'email' => 'franciscomattioli.a@gmail.com' ],
// ['firstname' =>  'BRUNO GABRIEL', 'lastname' => 'PACI'    , 'dni' => '0'  , 'email' => 'brunopaci80@gmail.com' ],
// ['firstname' =>  'GONZALO', 'lastname' => 'SABATTINI'   , 'dni' => '0'  , 'email' => 'eugi_loxi@hotmail.com' ],
// ['firstname' =>  'AMINA', 'lastname' => 'ALMEIDA ARANEDA' , 'dni' => '0'  , 'email' => 'almeida07amina@gmail.com'  ],
// ['firstname' =>  'GIULIANA', 'lastname' => 'FILIPPI' , 'dni' => '0'  , 'email' => 'giufilippi49@gmail.com'],
// ['firstname' =>  'EMMA', 'lastname' => 'FUSCONI KRÖGER'  , 'dni' => '0'  , 'email' => 'emmufusconi@gmail.com' ],
// ['firstname' =>  'MILAGROS', 'lastname' => 'GENOVESE'    , 'dni' => '0'  , 'email' => 'miligenovese@gmail.com'],
// ['firstname' =>  'EMMA', 'lastname' => 'OJEDA KIEHR' , 'dni' => '0'  , 'email' => 'emma.okiehr@gmail.com' ],
// ['firstname' =>  'ORIANA CAMIL', 'lastname' => 'VILLAGRAN'   , 'dni' => '0'  , 'email' => 'oriana17v@gmail.com']
        ];

        foreach($data as $user_data) {

            $user_data['firstname'] = ucwords(mb_strtolower($user_data['firstname']));
            $user_data['lastname'] = ucwords(mb_strtolower($user_data['lastname']));

            $user = ['name'=> str_replace(' ', '', $user_data['firstname']).'_'.str_replace(' ', '', $user_data['lastname']), 'email'=>$user_data['email'], 'password' => 'alumn#2020'];

        	$u = User::firstOrCreate($user);
        	$u->roles()->sync([2]);
        	$u->save();


            $alum = [
                'user_id' => $u->id,
                'firstname' => $user_data['firstname'],
                'lastname' => $user_data['lastname'],
                'dni' => $user_data['dni'],
                'birthdate' => '1990-01-01',
                'telefono' => '0',
                'gender' => 'm',
            ];
            $a = Alumno::firstOrCreate($alum);

        }
    }

}       
            
            

