<?php

use Illuminate\Database\Seeder;
use App\Models\User;
use App\Models\Administrativo;

class UsersTableSeeder extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$data = [
        	['firstname' => 'Juan', 'lastname' => 'Baena', 'dni' => '30959338', 'email' => 'ebompy@gmail.com', 'password' => 'admin#2020', 'role'=>'1'],
            ['firstname' => 'Enzo', 'lastname' => 'Ostrowski', 'dni' => '11111111', 'email' => 'enzo@weare.com.ar', 'password' => 'admin#2020', 'role'=>'1'],
            ['firstname' => 'Emma Susana', 'lastname' =>  'Esposito', 'dni' =>    '17511565', 'email' => 'emmaesposito@hotmail.com', 'password' => 'admin#2020', 'role'=>'4'],
            ['firstname' => 'Carolina', 'lastname' => 'Pelaez', 'dni' =>  '18347201', 'email' => 'calvo421@yahoo.com.ar', 'password' => 'admin#2020', 'role'=>'4'],
            ['firstname' => 'Julieta', 'lastname' =>  'Arias', 'dni' =>   '31499983', 'email' => 'julieta_arias_26@hotmail.com', 'password' => 'admin#2020', 'role'=>'4'],
            ['firstname' => 'Agustina', 'lastname' => 'Dello Russo', 'dni' => '36329130', 'email' => 'agustinadr91@hotmail.com', 'password' => 'admin#2020', 'role'=>'4'],
            ['firstname' => 'Nicolas Matias', 'lastname' =>   'Bottalico Salva', 'dni' => '28823763', 'email' => 'matibotta@hotmail.com', 'password' => 'admin#2020', 'role'=>'4']
        ];

        foreach($data as $user_data) {

            $user_data['firstname'] = ucwords(mb_strtolower($user_data['firstname']));
            $user_data['lastname'] = ucwords(mb_strtolower($user_data['lastname']));

            $user = ['name'=> str_replace(' ', '', $user_data['firstname']).'_'.str_replace(' ', '', $user_data['lastname']), 'email'=>$user_data['email'], 'password' => $user_data['password']];

        	$u = User::firstOrCreate($user);
        	$u->roles()->sync([$user_data['role']]);
        	$u->save();


            $admin = [
                'user_id' => $u->id,
                'firstname' => $user_data['firstname'],
                'lastname' => $user_data['lastname'],
                'dni' => $user_data['dni'],
                'birthdate' => null,
                'telefono' => null,
            ];
            $a = Administrativo::firstOrCreate($admin);

        }
    }

}


            
            
            
