<?php

use Illuminate\Database\Seeder;
use App\Models\Role;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
        $data = [
        	['name'=>'admin', 'display_name'=>'Usuario Super Administrador'],
            ['name'=>'alumno', 'display_name'=>'Usuario Alumno'],
            ['name'=>'profesor', 'display_name'=>'Usuario Profesor'],
            ['name'=>'administrativo', 'display_name'=>'Usuario Administrativo'],
            ['name'=>'director', 'display_name'=>'Usuario Director'],
            ['name'=>'secretario', 'display_name'=>'Usuario Secretario'],
            ['name'=>'preceptor', 'display_name'=>'Usuario Preceptor'],
        ];

        foreach($data as $role) {
        	Role::firstOrCreate($role);
        }
        
    }
}
