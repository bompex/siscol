<?php

use Illuminate\Database\Seeder;
use App\Models\User;
use App\Models\Profesor;

class ProfesoresTableSeeder extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$data = [

             ['firstname'=>'PATRICIA MÓNICA'  , 'lastname' =>  'PEREZ', 'dni'   =>  '18398902',  'phone'  =>  '2914262198', 'email'  =>  'patperez2003@hotmail.com', 'roles' => [3]],
        	 ['firstname'=>'MARIANO'  , 'lastname' =>  'CIUCCIO', 'dni' =>  '26456853',  'phone'  =>  '2914668948', 'email'  =>  'marianoc14@yahoo.com', 'roles' => [3]],
             ['firstname'=>'RITA ARACELI' , 'lastname' =>  'MONTANGIE', 'dni'   =>  '24209121',  'phone'  =>  '2915038292', 'email'  =>  'rmontangie@yahoo.com.ar', 'roles' => [3]],
             ['firstname'=>'ALBANA ANDREA', 'lastname' =>  'ALVAREZ', 'dni' =>  '30673742',  'phone'  =>  '2914221028', 'email'  =>  'albana.a_@hotmail.com', 'roles' => [3]],
             ['firstname'=>'MIRIAM ANDREA', 'lastname' =>  'TABOADA', 'dni' =>  '23289366',  'phone'  =>  '2915768301', 'email'  =>  'miritaboada@yahoo.com.ar', 'roles' => [3]],
             ['firstname'=>'HUGO ORLANDO' , 'lastname' =>  'ALDERETE', 'dni'    =>  '31134243',  'phone'  =>  '2915046584', 'email'  =>  'alderetehugoo@hotmail.com', 'roles' => [3]],
             ['firstname'=>'SILVANA MICAELA'  , 'lastname' =>  'MENDOZA', 'dni' =>  '26922210',  'phone'  =>  '2914755127', 'email'  =>  'nerina_mendoza79@hotmail.com', 'roles' => [3]],
             ['firstname'=>'PEDRO IGNACIO', 'lastname' =>  'BARRIONUEVO', 'dni' =>  '28032161',  'phone'  =>  '2914425715', 'email'  =>  'pedrign@gmail.com', 'roles' => [3]],
             ['firstname'=>'CAROLINA' , 'lastname' =>  'TAMBORINDEGUI', 'dni'   =>  '29554421',  'phone'  =>  '2914297060', 'email'  =>  'ctamborindegui@yahoo.com.ar', 'roles' => [3]],
             ['firstname'=>'SILVINA PAULA', 'lastname' =>  'GREGORIO', 'dni'    =>  '21559659',  'phone'  =>  '2914472215', 'email'  =>  'silvina.gregorio@gmail.com', 'roles' => [3]],
             ['firstname'=>'JAVIER IGNACIO'   , 'lastname' =>  'TACCARI', 'dni' =>  '28724359',  'phone'  =>  '2914327672', 'email'  =>  'javiertaccari@gmail.com', 'roles' => [3]],
             ['firstname'=>'MARIA PAULA'  , 'lastname' =>  'PELAEZ', 'dni'  =>  '32253581',  'phone'  =>  '2915766271', 'email'  =>  'pelaezmariapaula@hotmail.com', 'roles' => [3]],
             ['firstname'=>'MARÍA CELESTE', 'lastname' =>  'CARDINAUX', 'dni'   =>  '29647152',  'phone'  =>  '2915766539', 'email'  =>  'celestecx@hotmail.com', 'roles' => [3]],
             ['firstname'=>'MARISA EDITH' , 'lastname' =>  'PIVIDORI', 'dni'    =>  '26334689',  'phone'  =>  '2914748669', 'email'  =>  'marisapividori@yahoo.com.ar', 'roles' => [3]],
             ['firstname'=>'SILVIA SUSANA', 'lastname' =>  'BÉCARES', 'dni' =>  '17403806',  'phone'  =>  '2914138303', 'email'  =>  's.becares@yahoo.com.ar', 'roles' => [3]],
             ['firstname'=>'PABLO EMANUEL', 'lastname' =>  'MACCHI', 'dni'  =>  '28501958',  'phone'  =>  '2915778583', 'email'  =>  'macchipablo@hotmail.com', 'roles' => [3]],
             ['firstname'=>'JULIETA'  , 'lastname' =>  'DIMAYO', 'dni'  =>  '37235966',  'phone'  =>  '2914605863', 'email'  =>  'julicaux@gmail.com', 'roles' => [3]],
             ['firstname'=>'MARÍA BELEN'  , 'lastname' =>  'SCHELL', 'dni'  =>  '28946484',  'phone'  =>  '2916410722', 'email'  =>  'beluchi_81@hotmail.com', 'roles' => [3]],
             ['firstname'=>'ADRIANA DEL MILAGRO'  , 'lastname' =>  'ALANIS', 'dni'  =>  '17889794',  'phone'  =>  '2915709694', 'email'  =>  'adrianam.alanis@gmail.com', 'roles' => [3]],
             ['firstname'=>'DIEGO FABIÁN' , 'lastname' =>  'CASTILLO', 'dni'    =>  '26333238',  'phone'  =>  '2915037236', 'email'  =>  'diefcastillo@gmail.com', 'roles' => [3]],
             ['firstname'=>'MARCELA VIVIANA'  , 'lastname' =>  'ALVAREZ', 'dni' =>  '23966337',  'phone'  =>  '2914311315', 'email'  =>  'marcelavalvarez@hotmail.com', 'roles' => [3]],
             ['firstname'=>'CECILIA'  , 'lastname' =>  'SILENZI', 'dni' =>  '26958986',  'phone'  =>  '2916431585', 'email'  =>  'silenzicecilia@hotmail.com', 'roles' => [3]],
             ['firstname'=>'IGNACIO'  , 'lastname' =>  'BERMUDEZ', 'dni'    =>  '25576989',  'phone'  =>  '2914363938', 'email'  =>  'nbermudez@hotmail.com', 'roles' => [3]],
             ['firstname'=>'IVANA LEONOR' , 'lastname' =>  'PEREZ', 'dni'   =>  '14595785',  'phone'  =>  '2915716433', 'email'  =>  'ivanapannelli@hotmail.com', 'roles' => [3]],
             ['firstname'=>'NILO ABEL', 'lastname' =>  'NAVAS', 'dni'   =>  '16206256',  'phone'  =>  '2914075123', 'email'  =>  'nilonavas@gmail.com', 'roles' => [3]],
             ['firstname'=>'MARTINA INES' , 'lastname' =>  'GATTI', 'dni'   =>  '25215786',  'phone'  =>  '2914387069', 'email'  =>  'gattimartina@yahoo.com.ar', 'roles' => [3]],
             ['firstname'=>'ADRIANA'  , 'lastname' =>  'LAMOSO', 'dni'  =>  '24219598',  'phone'  =>  '2916437843', 'email'  =>  'adrilamoso@yahoo.com', 'roles' => [3]],
             ['firstname'=>'MARÍA CAROLINA'   , 'lastname' =>  'MENDEZ', 'dni'  =>  '22053156',  'phone'  =>  '2914714976', 'email'  =>  'carolinamendez8@hotmail.com', 'roles' => [3]],
             ['firstname'=>'MARÍA LAURA'  , 'lastname' =>  'MEDINA', 'dni'  =>  '28665340',  'phone'  =>  '2914196760', 'email'  =>  'malaumedina@hotmail.com', 'roles' => [3]],
             ['firstname'=>'MARIANA PAOLA', 'lastname' =>  'GARTNER', 'dni' =>  '23867970',  'phone'  =>  '2915340833', 'email'  =>  'marianapgartner@hotmail.com', 'roles' => [3]],
        ];

        foreach($data as $user_data) {

            $user_data['firstname'] = ucwords(mb_strtolower($user_data['firstname']));
            $user_data['lastname'] = ucwords(mb_strtolower($user_data['lastname']));
            

            $user = ['name'=> str_replace(' ', '', $user_data['firstname']).'_'.str_replace(' ', '', $user_data['lastname']), 'email'=>$user_data['email'], 'password' => 'profesor#2020'];

        	$u = User::firstOrCreate($user);
        	$u->roles()->sync([3]);
        	$u->save();

            
            $prof = [
                'user_id' => $u->id,
                'firstname' => $user_data['firstname'],
                'lastname' => $user_data['lastname'],
                'dni' => $user_data['dni'],
                'birthdate' => null,
                'telefono' => $user_data['phone'],
            ];
            $p = Profesor::firstOrCreate($prof);

        }
    }

}       
            
            
