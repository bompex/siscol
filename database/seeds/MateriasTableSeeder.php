<?php

use Illuminate\Database\Seeder;
use App\Models\Materia;

class MateriasTableSeeder extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            ['name'=>'Ciencias Naturales'],
            ['name'=>'Ciencias Sociales'],
            ['name'=>'Educación Artística'],
            ['name'=>'Educación Física (mujeres)'],
            ['name'=>'Educación Física (varones)'],
            ['name'=>'Inglés'],
            ['name'=>'Matemática'],
            ['name'=>'Prácticas del Lenguaje'],
            ['name'=>'Construcción de la Ciudadania'],
            ['name'=>'Informática'],
            ['name'=>'Biología'],
            ['name'=>'Fisicoquímica'],
            ['name'=>'Geografia'],
            ['name'=>'Historia'],
            ['name'=>'Literatura'],
            ['name'=>'NTICx'],
            ['name'=>'Salud y Adolescencia'],
            ['name'=>'Introducción a la Física'],
            ['name'=>'Psicologia'],
            ['name'=>'Política y Ciudadania'],
            ['name'=>'Introducción a la Química'],
            ['name'=>'Comunicación, Cultura y Sociedad'],
            ['name'=>'Economía Política'],
            ['name'=>'Sociología'],
            ['name'=>'Trabajo y Ciudadanía'],
            ['name'=>'Filosofía'],
            ['name'=>'Arte'],
            ['name'=>'Geografía'],
            ['name'=>'Proyecto de Investigación']
        ];

        foreach($data as $materia) {
            $m = Materia::firstOrCreate($materia);
            $m->save();

        }
    }

}
