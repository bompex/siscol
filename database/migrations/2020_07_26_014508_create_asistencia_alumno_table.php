<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAsistenciaAlumnoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('asistencia_alumno', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('asistencia_id');
            $table->unsignedInteger('alumno_id');
            $table->unsignedInteger('status')->default('0');

            $table->auditable();                  
            $table->timestamps();
            $table->softDeletes('deleted_at');

            $table->foreign('asistencia_id')->references('id')->on('asistencias');
            $table->foreign('alumno_id')->references('id')->on('alumnos');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('asistencia_alumno');
    }
}
