<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCommentParentIdToTopicComment extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('topic_comment', function (Blueprint $table) {
            $table->unsignedBigInteger('comment_parent_id')->nullable()->default(null)->after('topic_id');

            $table->foreign('comment_parent_id')->references('id')->on('topic_comment');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('topic_comment', function (Blueprint $table) {
            //
        });
    }
}
