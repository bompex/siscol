<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateActivityLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('activity_logs', function (Blueprint $table) {
            
            $table->increments('id');
            $table->unsignedBigInteger('user_id')->nullable();
            $table->string('route',255);
            $table->string('method',10);
            $table->text('data')->nullable();
            $table->string('device_type',255)->nullable();
            $table->string('device',255)->nullable();
            $table->string('browser',255)->nullable();
            $table->string('platform',255)->nullable();
            
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('activity_logs');
    }
}
