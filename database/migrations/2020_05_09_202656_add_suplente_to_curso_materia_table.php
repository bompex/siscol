<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSuplenteToCursoMateriaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('curso_materia', function (Blueprint $table) {
            $table->integer('profesor_suplente_id')->unsigned()->nullable()->after('profesor_id');


            $table->foreign('profesor_suplente_id')->references('id')->on('profesores');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('curso_materia', function (Blueprint $table) {
            //
        });
    }
}
