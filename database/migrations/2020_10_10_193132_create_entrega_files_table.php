<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEntregaFilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('entrega_files', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('entrega_id');
            $table->unsignedBigInteger('file_id');
            $table->boolean('devolucion')->default(0);


            $table->auditable();                  
            $table->timestamps();
            $table->softDeletes('deleted_at');

            $table->foreign('entrega_id')->references('id')->on('entregas');
            $table->foreign('file_id')->references('id')->on('files');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('entrega_files');
    }
}


//SELECT id as entrega_id, file_id FROM entregas WHERE file_id is not null;
//SELECT id as entrega_id, profesor_file_id as file_id, 1 as devolucion FROM entregas WHERE profesor_file_id is not null;