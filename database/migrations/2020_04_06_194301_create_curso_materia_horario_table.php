<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCursoMateriaHorarioTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('curso_materia_horario', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('curso_materia_id')->unsigned();
            $table->integer('dia')->unsigned();
            $table->time('hora_inicio');
            $table->time('hora_fin');


            $table->auditable();                  
            $table->timestamps();
            $table->softDeletes('deleted_at');

            $table->foreign('curso_materia_id')->references('id')->on('curso_materia');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('curso_materia_horario');
    }
}
