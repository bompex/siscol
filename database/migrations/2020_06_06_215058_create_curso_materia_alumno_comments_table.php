<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCursoMateriaAlumnoCommentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('curso_materia_alumno_comments', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('curso_materia_id')->unsigned();
            $table->integer('profesor_id')->unsigned();
            $table->integer('alumno_id')->unsigned();

            $table->longText('body');
            $table->integer('status')->default(0);

            $table->auditable();                  
            $table->timestamps();
            $table->softDeletes('deleted_at');

            $table->foreign('curso_materia_id')->references('id')->on('curso_materia');
            $table->foreign('profesor_id')->references('id')->on('profesores');
            $table->foreign('alumno_id')->references('id')->on('alumnos');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('curso_materia_alumno_comments');
    }
}
