<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCalificacionesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('calificaciones', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->unsignedInteger('curso_materia_id');
            $table->unsignedInteger('alumno_id');
            $table->unsignedInteger('profesor_id');
            $table->unsignedBigInteger('evaluacion_id')->nullable();

            $table->unsignedInteger('status');
            $table->unsignedInteger('publica');

            $table->decimal('nota',5,2);

            $table->string('observacion')->nullable();


            $table->auditable();                  
            $table->timestamps();
            $table->softDeletes('deleted_at');

            $table->foreign('curso_materia_id')->references('id')->on('curso_materia');
            $table->foreign('profesor_id')->references('id')->on('profesores');
            $table->foreign('alumno_id')->references('id')->on('alumnos');
            $table->foreign('evaluacion_id')->references('id')->on('evaluaciones');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('calificaciones');
    }
}
