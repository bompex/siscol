<div>
    <div class="col col-md-3 col-xs-12 input-filtro filt-btn">
        <div class="col col-md-6">
            {!! Form::submit(trans('page.button.download_xls'), ['class' => 'btn btn-primary', 'name' => 'download-xls']) !!}
        </div>
    </div>
</div>

<div class="clearfix"></div>
