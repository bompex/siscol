
<!-- Firstname Field -->
<div class="form-group col-sm-4">
    {!! Form::label('firstname', trans('page.label.user.name')) !!}
    {!! Form::text('firstname', null, ['class' => 'form-control']) !!}
</div>
<div class="clearfix"></div>
<!-- Lastname Field -->
<div class="form-group col-sm-4">
    {!! Form::label('lastname', trans('page.label.user.lastname')) !!}
    {!! Form::text('lastname', null, ['class' => 'form-control']) !!}
</div>
<div class="clearfix"></div>
<!-- Email Field -->
<div class="form-group col-sm-4">
    {!! Form::label('email', trans('page.label.user.email')) !!}
    @if(!$profesor->id || $profesor->id == '')
    {!! Form::email('email', $profesor->user->email, ['class' => 'form-control']) !!}
    @else
    {!! Form::email('email', $profesor->user->email, ['class' => 'form-control', 'readonly'=>true]) !!}
    @endif
</div>

<div class="clearfix"></div>
<div class="form-group col-sm-6">
    <div class="form-check">
        {!! Form::checkbox('primario', '1', $profesor->primario) !!}
        <label class="form-check-label" for="defaultCheck1">
            Profesor de Nivel Primario
        </label>
    </div>
    <div class="form-check">
        {!! Form::checkbox('secundario', '1', $profesor->secundario) !!}
        <label class="form-check-label" for="secundario">
            Profesor de Nivel Secundario
        </label>
    </div>
</div>
<div class="clearfix"></div>
<!-- Dni Field -->
<div class="form-group col-sm-3">
    {!! Form::label('dni', trans('page.label.user.dni')) !!}
    {!! Form::text('dni', null, ['class' => 'form-control']) !!}
</div>
<div class="clearfix"></div>
<!-- Birthdate Field -->
<div class="form-group col-sm-3">
    {!! Form::label('birthdate', trans('page.label.user.birthdate')) !!}
    {!! Form::date('birthdate', ($profesor->birthdate!= '')? date('Y-m-d',strtotime($profesor->birthdate)) : null
        , ['class' => 'form-control','id'=>'birthdate']) !!}
</div>

@section('scripts')
    <script type="text/javascript">
        $('#birthdate').datetimepicker({
            format: 'YYYY-MM-DD HH:mm:ss',
            useCurrent: false
        })
    </script>
@endsection
<div class="clearfix"></div>
<!-- Telefono Field -->
<div class="form-group col-sm-4">
    {!! Form::label('telefono', trans('page.label.user.phone')) !!}
    {!! Form::text('telefono', null, ['class' => 'form-control']) !!}
</div>
<div class="clearfix"></div>

<!-- Password Field -->
<div class="form-group col-sm-4">
    {!! Form::label('password', trans('page.label.user.password')) !!}
    {{ Form::password('password', ['class' => 'form-control']) }}
</div>
<div class="clearfix"></div>
<!-- Password Field -->
<div class="form-group col-sm-4">
    {!! Form::label('password_confirmation', trans('page.label.user.password_confirm')) !!}
    {{ Form::password('password_confirmation', ['class' => 'form-control']) }}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit(trans('page.button.save'), ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('profesor.index') }}" class="btn btn-default">{!! trans('page.button.cancel') !!}</a>
</div>