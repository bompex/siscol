<!-- Firstname Field -->
<div class="form-group">
    {!! Form::label('firstname', trans('page.label.user.name')) !!}
    <p>{{ $profesor->firstname }}</p>
</div>

<!-- Lastname Field -->
<div class="form-group">
    {!! Form::label('lastname', trans('page.label.user.lastname')) !!}
    <p>{{ $profesor->lastname }}</p>
</div>

<!-- Dni Field -->
<div class="form-group">
    {!! Form::label('dni', trans('page.label.user.dni')) !!}
    <p>{{ $profesor->dni }}</p>
</div>

<!-- Birthdate Field -->
<div class="form-group">
    {!! Form::label('birthdate', trans('page.label.user.birthdate')) !!}
    <p>{{ date('d/m/Y',strtotime($profesor->birthdate)) }}</p>
</div>

<!-- Telefono Field -->
<div class="form-group">
    {!! Form::label('telefono', trans('page.label.user.phone')) !!}
    <p>{{ $profesor->telefono }}</p>
</div>

<!-- Email Field -->
<div class="form-group">
    {!! Form::label('email', trans('page.label.user.email')) !!}
    <p>{{ $profesor->user->email }}</p>
</div>

