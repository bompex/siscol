<div class="table-responsive">
    <table class="table datatable" id="profesor-table">
        <thead>
            <tr>
                <th>{!! trans('page.label.user.name') !!}</th>
                <th>{!! trans('page.label.user.lastname') !!}</th>
                <th>{!! trans('page.label.user.nivel') !!}</th>
                <th>{!! trans('page.label.user.dni') !!}</th>
                <th>{!! trans('page.label.user.email') !!}</th>
                <th class='no-sort'>&nbsp;</th>
            </tr>
        </thead>
        <tbody>
        @foreach($profesor as $profesor)
            <tr>
                <td>{{ $profesor->firstname }}</td>
                <td>{{ $profesor->lastname }}</td>
                <td>{{ ($profesor->primario == 1)? '-Primario' : '' }} {!! ($profesor->secundario == 1 && $profesor->primario == 1)? '</br>' : '' !!} {{ ($profesor->secundario == 1)? '-Secundario' : '' }}</td>
                <td>{{ $profesor->dni }}</td>
                <td>{{ $profesor->email }}</td>
                <td>
                    {!! Form::open(['route' => ['profesor.destroy', $profesor->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{{ route('profesor.show', [$profesor->id]) }}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                        <a href="{{ route('profesor.edit', [$profesor->id]) }}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>

                        @if(!$profesor->user->administrativo)

                            {!! Form::button('<i class="glyphicon glyphicon-home"></i>', ['type' => 'button', 'title' => 'Convertir en Administrativo', 'class' => 'btn btn-default btn-xs', 'onclick' => "if(confirm('Va darle el rol Administrativo a este usuario')){ window.location ='".route('profesor.store_admin', $profesor->id)."'}"]) !!}
                        
                        @endif

                        {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
