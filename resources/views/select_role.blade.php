@extends('layouts.app')
@section('content_header')
@endsection
@section('content')
    <div class="row">
        <div class="col-xs-12">
        <div class="clearfix"></div>

        @include('flash::message')

        <div class="clearfix"></div>
        <div class="box box-primary">
            <div class="box-body">
                <div class="row">
                    <div class="form-group col-sm-12">
                        <a href="{{ route('home.profesor') }}" class="btn btn-default">Ingresar como Profesor</a>
                    </div>
                    <div class="form-group col-sm-12">
                        <a href="{{ route('home.admin') }}" class="btn btn-default">Ingresar como Administrativo</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection