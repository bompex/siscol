@extends('layouts.app')
@section('content_header')
    <h1 class="pull-left">
        Supervisar Mensajes
    </h1>
@endsection
@section('content')
    <div class="row">
        <div class="col-xs-12">
        <div class="clearfix"></div>

        @include('flash::message')

        <div class="clearfix"></div>
        <div class="box box-primary">
            <div class="box-body">
                <div class="row">
                    <div class="form-group col-sm-6">
                        {!! Form::label('date_from', "Persona:") !!}
                        {{ Form::select('user1_id', $users, null, ['class' => 'form-control select2 user1_field']) }}
                    </div>
                    <div class="clearfix"></div>
                    <div class="form-group col-sm-6">
                        {!! Form::label('date_from', "Persona:") !!}
                        {{ Form::select('user2_id', $users, null, ['class' => 'form-control select2 user2_field']) }}
                    </div>
                    <div class="clearfix"></div>
                    <!-- Submit Field -->
                    <div class="form-group col-sm-12">
                        <button onclick="show_supervision_message()" class="btn btn-primary">Ver Mensajes</button>

                    </div>
                </div>
            </div>
        </div>
        <div class="text-center">
        
        </div>
    </div>
</div>
<script>
function show_supervision_message(){
    var show_url  = "{{ route('supervision_message.show', ['user1_id','user2_id']) }}";
    var u1 = $( ".user1_field option:selected" ).val();
    var u2 = $( ".user2_field option:selected" ).val();

    show_url = show_url.replace('user1_id',u1);
    show_url = show_url.replace('user2_id',u2);

    $.ajax({
        url: show_url,
        type : 'GET',
      success : function(data) {
        $('#ventana-modal').html(data);
        $('#ventana-modal').modal('show');   
        setTimeout(function(){
            var chat = $('.chat-wrapper');
            chat.scrollTop(chat[0].scrollHeight);  
          }, 500);            
      },

      // código a ejecutar si la petición falla;
       error : function(xhr, status) {
        console.log(status);
            alert('Disculpe, existió un problema');
        },
    });
}
</script>
@endsection