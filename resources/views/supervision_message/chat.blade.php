<div class="chat-wrapper col-xs-12 ">
	@if(isset($messages) && count($messages)>0)
	@foreach($messages as $message)
		<div class="{{ ($message->is_own($user1->id) )? 'own-message' : 'other-message' }}">
			
			<div class="message-body">
				<p style="font-weight: 600">{{ $message->sender_user->lastname }}, {{ $message->sender_user->firstname }} 
					@if($message->sender_user->isAlumno())
						(Alumno - {{ $message->sender_user->alumno->curso_actual->name }})
					@elseif($message->sender_user->isProfesor())
						(Profesor)
					@endif
				</p>
				{!! $message->body !!}

				@if(isset($message->files) && count($message->files)>0)
		    		@foreach($message->files as $file)
			            <div><a href='{!! $file->path !!}' target='blank'>{{ $file->name }}</a></div>
			        @endforeach
		    	@endif

			</div>
			
			<div class="message-foot">
				<span>{{ date('d/m H:i', strtotime($message->created_at)) }}</span>
				<span></span>
			</div>
		</div>
		<div class="clearfix"></div>
	@endforeach
	@else
		<div class="message-empty">
			<p>No hay ningun mensaje</p>
		</div>
	@endif
</div>
