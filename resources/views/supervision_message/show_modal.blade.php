@extends('layouts.modal')
@section('modal-title')
	<h4>Mensajes: {{ $user1->lastname }}, {{ $user1->firstname }}
		{!! ($user1->isAlumno())? " (Alumno - ".$user1->alumno->curso_actual->name.")" : "" !!}
	</h4>
@endsection
@section('modal-content')
	<div class="modal-body">
        @include('supervision_message.chat')
        <div class="clearfix"></div>
	</div>

@endsection


