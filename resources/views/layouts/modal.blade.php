<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <div class='modal-title'>
                @yield('modal-title')
            </div>
            <button type="button" class="close" data-dismiss="modal">
                <span>×</span>
            </button>
        </div>
        @yield('modal-content')
    </div>
</div>