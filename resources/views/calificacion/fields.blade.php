<div class="row">
	<div class="form-group col-sm-3">
	    {!! Form::label('nota', trans('page.label.calificacion.nota')) !!}
	    {!! Form::number('nota', $calificacion->nota, ['class' => 'form-control ', 'required'=>'true', 'step'=>'0.01']) !!}
	</div>
	<div class="clearfix"></div>
	<div class="form-group col-sm-12">
	    {!! Form::label('observacion', trans('page.label.calificacion.observacion')) !!}
	    {!! Form::text('observacion', $calificacion->observacion, ['class' => 'form-control']) !!}
	</div>
	<div class="clearfix"></div>
	<div class="form-group col-sm-3">
	    {!! Form::label('publica', trans('page.label.calificacion.publica')) !!}
	    {!! Form::select('publica', ['0'=>'No', '1'=>'Si'],$calificacion->publica, ['class' => 'form-control']) !!}
	</div>
</div>