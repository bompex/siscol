@extends('layouts.modal')
@section('modal-title')
	<h4>Agregar Calificación</h4>
@endsection
@section('modal-content')
	@if($evaluacion)
	{!! Form::open(['route' => ['calificacion.store', $evaluacion->id, $alumno->id]]) !!}
	@else
	{!! Form::open(['route' => ['calificacion.store_concepto', $cursoMateria->id, $alumno->id]]) !!}
	@endif
	<div class="modal-body">
		@include('calificacion.info')
		@include('calificacion.fields')
	</div>
	<div class="modal-footer">
	    <input type="submit" class="btn btn-primary" value="Guardar">
	    <a href="#" class="btn btn-default" data-dismiss="modal">{!! trans('page.button.cancel') !!}</a>
	</div>
	{!! Form::close() !!}
@endsection

