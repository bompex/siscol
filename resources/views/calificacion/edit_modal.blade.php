@extends('layouts.modal')
@section('modal-title')
	<h4>Modificar Calificación</h4>
@endsection
@section('modal-content')
	{!! Form::open(['route' => ['calificacion.update', $calificacion->id], 'method' => 'patch']) !!}
	<div class="modal-body">
		@include('calificacion.info')
		@include('calificacion.fields')
	</div>
	<div class="modal-footer">
	    <input type="submit" class="btn btn-primary" value="Guardar">
	    <a href="#" class="btn btn-default" data-dismiss="modal">{!! trans('page.button.cancel') !!}</a>
	</div>
	{!! Form::close() !!}
@endsection

