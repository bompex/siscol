<div class="row">
	<div class="col-md-6">
		<label>Curso:</label>	
		<p>{{ $cursoMateria->curso->name }}</p>
	</div>
	<div class="col-md-6">
		<label>Materia:</label>	
		<p>{{ $cursoMateria->materia->name }}</p>
	</div>
	@if($evaluacion)
	<div class="col-md-12">
		<label>Tema:</label>	
		<p>{{ $evaluacion->subject }} ({{ $evaluacion->type }})</p>
	</div>
	@endif
</div>
<div class="row">
	<div class="col-md-12">
		<label>Alumno:</label>	
		<p>{{ $alumno->lastname }}, {{ $alumno->firstname }}</p>
	</div>
</div>