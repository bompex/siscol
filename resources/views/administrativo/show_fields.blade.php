<!-- Firstname Field -->
<div class="form-group">
    {!! Form::label('firstname', trans('page.label.user.name')) !!}
    <p>{{ $administrativo->firstname }}</p>
</div>

<!-- Lastname Field -->
<div class="form-group">
    {!! Form::label('lastname', trans('page.label.user.lastname')) !!}
    <p>{{ $administrativo->lastname }}</p>
</div>

<!-- Dni Field -->
<div class="form-group">
    {!! Form::label('dni', trans('page.label.user.dni')) !!}
    <p>{{ $administrativo->dni }}</p>
</div>

<!-- Birthdate Field -->
<div class="form-group">
    {!! Form::label('birthdate', trans('page.label.user.birthdate')) !!}
    <p>{{ date('d/m/Y',strtotime($administrativo->birthdate)) }}</p>
</div>

<!-- Telefono Field -->
<div class="form-group">
    {!! Form::label('telefono', trans('page.label.user.phone')) !!}
    <p>{{ $administrativo->telefono }}</p>
</div>

<!-- Email Field -->
<div class="form-group">
    {!! Form::label('email', trans('page.label.user.email')) !!}
    <p>{{ $administrativo->user->email }}</p>
</div>

