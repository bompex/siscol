<div class="table-responsive">
    <table class="table  datatable" id="administrativo-table">
        <thead>
            <tr>
                <th>{!! trans('page.label.user.name') !!}</th>
                <th>{!! trans('page.label.user.rol') !!}</th>
                <th>{!! trans('page.label.user.dni') !!}</th>
                <th>{!! trans('page.label.user.email') !!}</th>
                <th class='no-sort'>&nbsp;</th>
            </tr>
        </thead>
        <tbody>
        @foreach($administrativo as $administrativo)
            <tr>
            <td>{{ $administrativo->lastname }}, {{ $administrativo->firstname }}</td>
            <td>{{ ucfirst($administrativo->user->roles[0]->name) }}</td>
            <td>{{ $administrativo->dni }}</td>
            <td>{{ $administrativo->user->email }}</td>
                <td>
                    {!! Form::open(['route' => ['administrativo.destroy', $administrativo->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{{ route('administrativo.show', [$administrativo->id]) }}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                        <a href="{{ route('administrativo.edit', [$administrativo->id]) }}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                        @if(!$administrativo->user->profesor)
                        {!! Form::button('<i class="glyphicon glyphicon-apple"></i>', ['type' => 'button', 'title' => 'Convertir en Profesor', 'class' => 'btn btn-default btn-xs', 'onclick' => "if(confirm('Va darle el rol Profesor a este usuario')){ window.location ='".route('administrativo.store_profesor', $administrativo->id)."'}"]) !!}
                        @endif
                        {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Seguro desea Eliminar?')"]) !!}
                        
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
