@extends('layouts.app')

@section('content_header')
        <h1 class="pull-left">{!! trans('page.model.plural.administrativo') !!}</h1>
        <h1 class="pull-right">
           <a class="btn btn-primary pull-right" style="margin-top: -10px;margin-bottom: 5px" href="{{ route('administrativo.create') }}">{!! trans('page.button.add') !!} {!! trans('page.button.new') !!}</a>
        </h1>
@endsection        

@section('content')
    
    <div class="row">
        <div class="col-xs-12">
        <div class="clearfix"></div>

        @include('flash::message')

        <div class="clearfix"></div>
        <div class="box box-primary">
            <div class="box-body">
                    @include('administrativo.table')
            </div>
        </div>
        <div class="text-center">
        
        </div>
    </div>
    </div>
@endsection

