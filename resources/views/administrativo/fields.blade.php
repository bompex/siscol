
<!-- Firstname Field -->
<div class="form-group col-sm-12">
    {!! Form::label('rol', trans('page.label.user.rol')) !!}
    {{ Form::select('rol', $roles, (isset($administrativo->user->roles[0]))? $administrativo->user->roles[0]->name : '', ['class' => 'form-control', 'id'=>'rol']) }}
</div>
<!-- Firstname Field -->
<div class="form-group col-sm-6">
    {!! Form::label('firstname', trans('page.label.user.name')) !!}
    {!! Form::text('firstname', null, ['class' => 'form-control']) !!}
</div>

<!-- Lastname Field -->
<div class="form-group col-sm-6">
    {!! Form::label('lastname', trans('page.label.user.lastname')) !!}
    {!! Form::text('lastname', null, ['class' => 'form-control']) !!}
</div>

<!-- Dni Field -->
<div class="form-group col-sm-6">
    {!! Form::label('dni', trans('page.label.user.dni')) !!}
    {!! Form::text('dni', null, ['class' => 'form-control']) !!}
</div>

<!-- Birthdate Field -->
<div class="form-group col-sm-6">
    {!! Form::label('birthdate', trans('page.label.user.birthdate')) !!}
    {!! Form::date('birthdate', ($administrativo->birthdate!= '')? date('Y-m-d',strtotime($administrativo->birthdate)) : null
        , ['class' => 'form-control','id'=>'birthdate']) !!}
</div>

@section('scripts')
    <script type="text/javascript">
        $('#birthdate').datetimepicker({
            format: 'YYYY-MM-DD HH:mm:ss',
            useCurrent: false
        })
    </script>
@endsection

<!-- Telefono Field -->
<div class="form-group col-sm-6">
    {!! Form::label('telefono', trans('page.label.user.phone')) !!}
    {!! Form::text('telefono', null, ['class' => 'form-control']) !!}
</div>

<!-- Email Field -->
<div class="form-group col-sm-6">
    {!! Form::label('email', trans('page.label.user.email')) !!}
    @if(!$administrativo->id || $administrativo->id == '')
    {!! Form::email('email', $administrativo->user->email, ['class' => 'form-control']) !!}
    @else
    {!! Form::email('email', $administrativo->user->email, ['class' => 'form-control', 'readonly'=>true]) !!}
    @endif
</div>

<!-- Password Field -->
<div class="form-group col-sm-6">
    {!! Form::label('password', trans('page.label.user.password')) !!}
    {{ Form::password('password', ['class' => 'form-control']) }}
</div>

<!-- Password Field -->
<div class="form-group col-sm-6">
    {!! Form::label('password_confirmation', trans('page.label.user.password_confirm')) !!}
    {{ Form::password('password_confirmation', ['class' => 'form-control']) }}
</div>


<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit(trans('page.button.save'), ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('administrativo.index') }}" class="btn btn-default">{!! trans('page.button.cancel') !!}</a>
</div>
