<div class="form-group col-sm-12">
<div id='curso_schedule'></div>
</div>

<script>
	
(function ($) {
      $("#curso_schedule").dayScheduleSelector({

        days: [1,2,3,4,5],
        stringDays  : ['Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes'],
        @if($curso->turno == 'mañana')
            structure : ['07:35','08:35','09:35','09:50','10:50','11:50','12:00','13:00'],
            turno : 'mañana'
        @else
            structure : ['13:00','13:50','14:00','14:50','15:40','15:55','16:45','17:30'],
            turno : 'tarde'
        @endif
        

      });
      $("#curso_schedule").data('artsy.dayScheduleSelector').deserialize(
      	{!! json_encode($curso->getFullSchedule()) !!}
      	);
    })($);
</script>