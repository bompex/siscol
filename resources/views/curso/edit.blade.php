@extends('layouts.app')
@section('content_header')
    <h1>
        @if($preset_nivel === false)
            {!! trans('page.model.curso') !!}
        @else
            {!! trans('page.model.curso'.$preset_nivel) !!}
        @endif
    </h1>
@endsection
@section('content')
  <div class="row">
        <div class="col-xs-12">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($curso, ['route' => ['curso.update', $curso->id], 'method' => 'patch']) !!}

                        @include('curso.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
 </div>
@endsection