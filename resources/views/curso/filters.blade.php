<div>
    
    
    <div class="col-r col-md-3 col-xs-12 input-filtro">
        <label>Periodo:</label>
        {{ Form::select('periodo', $periodos, isset($periodo) ? $periodo : null, [
            'id' => 'periodo',
            'class' => 'form-control'
        ]) }}
        {{ Form::hidden('nivel', $nivel) }}
    </div>
</div>

<div class="clearfix"></div>


<script type="text/javascript">
    $(function () {
        $("#periodo").change(function () {
            $('#filter-form').submit();
        });

    });

</script>