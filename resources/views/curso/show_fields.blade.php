<div class="form-group col-sm-3">
    {!! Form::label('nivel', trans('page.label.curso.nivel')) !!}
    <p>{{ $curso->nivel }}</p>
</div>
<div class="clearfix"></div>
<!-- Date From Field -->
<div class="form-group col-sm-6">
    {!! Form::label('date_from', trans('page.label.curso.date_from')) !!}
    <p>{{ $curso->date_from }}</p>
</div>

<!-- Date To Field -->
<div class="form-group col-sm-6">
    {!! Form::label('date_to', trans('page.label.curso.date_to')) !!}
    <p>{{ $curso->date_to }}</p>
</div>

<!-- Turno Field -->
<div class="form-group col-sm-3">
    {!! Form::label('turno', trans('page.label.curso.turno')) !!}
    <p>{{ $curso->turno }}</p>
</div>
<div class="clearfix"></div>
<!-- Active Field -->
<div class="form-group col-sm-3">
    {!! Form::label('active', trans('page.label.curso.active')) !!}
    <p>{{ ($curso->active)? 'Si' : 'No' }}</p>
</div>
<div class="clearfix"></div>





