<div class="table-responsive">
    <table class="table" id="alumno-table">
        <thead>
            <tr>
                <th class='no-sort'>&nbsp;</th>
                <th>{!! trans('page.label.user.lastname') !!}, {!! trans('page.label.user.name') !!}</th>
                <th>{!! trans('page.label.user.dni') !!}</th>
                <th>{!! trans('page.label.curso.nivel') !!}</th>
                <th>{!! trans('page.model.curso') !!}</th>
                
            </tr>
        </thead>
        <tbody>
        @foreach($alumnos as $alumno)
            <tr>
                <td>
                    {!! Form::checkbox('alumno_select[]', $alumno->id, true) !!}
                </td>
                <td>{{ $alumno->lastname }}, {{ $alumno->firstname }}</td>
                <td>{{ $alumno->dni }}</td>
                <td>{{ ($alumno->curso_actual)? $alumno->curso_actual->nivel : '--' }}</td>
                <td>{{ ($alumno->curso_actual)? $alumno->curso_actual->name : '--' }}</td>
                
            </tr>
        @endforeach
        </tbody>
    </table>
</div>