<div class="table-responsive">
    <table class="table datatable" id="curso-table">
        <thead>
            <tr>
                <th>{!! trans('page.label.curso.name') !!}</th>
                <th>{!! trans('page.label.curso.nivel') !!}</th>
                <th>{!! trans('page.label.curso.turno') !!}</th>
                <th>{!! trans('page.model.plural.materia'.$nivel) !!}</th>
                <th>{!! trans('page.label.curso.alumnos') !!}</th>
                <th>{!! trans('page.label.curso.active') !!}</th>
                <th class='no-sort'>&nbsp;</th>
            </tr>
        </thead>
        <tbody>
        @foreach($cursos as $curso)
            <tr>
                <td>{{ $curso->name }}</td>
                <td>{{ $curso->nivel }}</td>
                <td>{{ $curso->turno }}</td>
                <td>{{ count($curso->cursoMaterias) }}</td>
                <td>{{ count($curso->cursoAlumnos) }}</td>
                <td>{{ ($curso->active)? 'Si' : 'No' }}</td>
                <td>
                    {!! Form::open(['route' => ['curso.destroy', $curso->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{{ route('curso.show', [$curso->id]) }}" class='btn btn-default btn-xs' title="Ver Curso"><i class="glyphicon glyphicon-eye-open"></i></a>
                        <a href="{{ route('curso.edit', [$curso->id]) }}" class='btn btn-default btn-xs' title="Editar Curso"><i class="glyphicon glyphicon-edit"></i></a>
                        <a href="{{ route('curso.add_materias', [$curso->id]) }}" class='btn btn-default btn-xs' title="Administrar Materias"><i class="glyphicon glyphicon-list"></i></a>
                        <a href="{{ route('curso.add_materias_horario', [$curso->id]) }}" class='btn btn-default btn-xs' title="Administrar Horarios"><i class="glyphicon glyphicon-calendar"></i></a>
                        <a href="{{ route('curso.add_alumnos', [$curso->id]) }}" class='btn btn-default btn-xs' title="Administrar Alumnos"><i class="glyphicon glyphicon-user"></i></a>
                        @if(date('Y',strtotime($curso->date_from)) == date('Y',strtotime('-1 year')) )
                        <a href="{{ route('curso.promote', [$curso->id]) }}" class='btn btn-default btn-xs' title="Promover Alumnos"><i class="glyphicon glyphicon-star-empty"></i></a>
                        @endif
                        {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
