@extends('layouts.app')
@section('content_header')
    <h1>
        {!! trans('page.model.curso'.$curso->primario) !!} {{ $curso->name }}
    </h1>
@endsection
@section('content')
    <div class="row">
        <div class="col-xs-12">
        <div class="box box-primary">
            <div class="box-body">
                <div class="row" style="padding-left: 20px">
                    @include('curso.show_fields')
                </div>
                <div class="row" style="padding-left: 20px">
                    @include('curso.table_horario')
                </div>
                <div class="row" style="padding-left: 20px">
                    @include('curso.show_fields_materias_alumnos')
                </div>
                
                <div class="row" style="padding-left: 20px">
                    <a href="{{ route('curso.index',['nivel'=>$curso->nivel]) }}" class="btn btn-default">{!! trans('page.button.back') !!}</a>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
