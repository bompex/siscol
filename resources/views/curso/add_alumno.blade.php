@extends('layouts.app')

@section('content_header')
    <h1>
        {!! trans('page.model.curso'.$curso->primario) !!}: {!! $curso->name !!}
    </h1>
@endsection

@section('content')
    <div class="row">
        <div class="col-xs-12">
        @include('adminlte-templates::common.errors')

         

        <div class="box box-primary">
            <div class="box-body">
            	
            	<h2>Listado de {!! trans('page.label.curso.alumnos') !!}</h2>
            	{!! Form::open(['route' => ['curso.store_alumnos',$curso->id]]) !!}
                <div class="row">

                	{{ Form::hidden('alumnos_id', '-', ['class'=>'alumnos_id','id'=>'alumnos_id']) }}
                	<div class = 'form-group col-sm-6'>
                		
                		<ul id="enableList" class="connectedSortable">
						@foreach($curso->cursoAlumnos as $cursoAlumnos)
					        <li id='{{ $cursoAlumnos->alumno->id }}' class="ui-state-highlight">
					    			{{ $cursoAlumnos->alumno->lastname.", ".$cursoAlumnos->alumno->firstname }}
					    	</li>
					    @endforeach
						</ul>
                	</div>
                	<div class = 'form-group col-sm-6'>
                		{!! Form::text('search', null, ['class' => 'form-control', 'placeholder'=> 'Buscar Alumno ...', 'id' => 'search']) !!}
                		<ul id="disableList" class="connectedSortable">
	            	  	@foreach($alumnos as $alumno)
	            	  		@if(!in_array($alumno->id, $current_alumnos))
					        <li id='{{ $alumno->id }}' class="ui-state-default">
					    			{{ $alumno->lastname.", ".$alumno->firstname }}
					    	</li>
					    	@endif
					    @endforeach
						</ul>
                	</div>
                	
                </div>
                <div class='row'>
                	<!-- Submit Field -->
					<div class="form-group col-sm-12">
					    {!! Form::submit(trans('page.button.save'), ['class' => 'btn btn-primary']) !!}
					    <a href="{{ route('curso.index',['nivel'=>$curso->nivel]) }}" class="btn btn-default">{!! trans('page.button.cancel') !!}</a>
					</div>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
    </div>
@endsection

@section('css')

<style>
  #disableList, #enableList {
    border: 1px solid #eee;
    width: 90%;
    min-height: 20px;
    list-style-type: none;
    margin: 0;
    padding: 5px 0 0 0;
    float: left;
    margin-right: 10px;
    height: 300px;
    overflow-y: scroll;
  }
  #search{
  	width: 90%;
  }
  #disableList li, #enableList li {
    margin: 0 5px 5px 5px;
    padding: 5px;
    font-size: 1.2em;
  }
  </style>

@endsection


@section('js')
<script type="text/javascript">

	$( function() {
	    $( "#disableList, #enableList" ).sortable({
	      connectWith: ".connectedSortable",
	      update: function(event, ui) {
                update_alumno_list();
            }
	    }).disableSelection();



	    $("#search").on("keyup", function() {
		    var value = $(this).val().toLowerCase();
		    $("#disableList li").filter(function() {
		      $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
		    });
		 });

	    update_alumno_list();

	  } );

	function update_alumno_list()
	{
		var order = $("#enableList").sortable("toArray");
        $('#alumnos_id').val(order.join(","));
	}
	

	function delete_row(elem){
		var tr = $(elem).parents('tr');
		tr.hide();
		tr.find('.curso_materia_del_id').val(tr.find('.curso_materia_id').val());
	}

	function add_row(){
		var html = $('#row_sample tbody').html();
		$("#curso-materia-table tbody").append(html);
	}
</script>
@endsection
