@extends('layouts.app')

@section('content_header')
    <h1>
        @if($preset_nivel === false)
            {!! trans('page.model.curso') !!}
        @else
            {!! trans('page.model.curso'.$preset_nivel) !!}
        @endif
    </h1>
@endsection

@section('content')
    <div class="row">
        <div class="col-xs-12">
        @include('adminlte-templates::common.errors')
        {!! Form::open(['route' => 'curso.store']) !!}
        
        <div class="box box-primary">
            <div class="box-body">
                <p>Cofiguración:</p>
                <div class="form-group col-sm-6">
                {{ Form::select('accion', ['0'=>'Utilizar config. Nueva', '1'=>'Utilizar config. de curso del periodo anterior'], null, ['class' => 'form-control', 'id'=>'accion']) }}
                </div>
                <div class="clearfix"></div>
                <p class="config_exist hidden">Este curso copiará las Materias, Profesores y Horarios del curso que seleccione</p>
                <div class="form-group col-sm-6 config_exist hidden">                    
                    {!! Form::label('curso_exist', 'Curso:') !!}
                    <select id="curso_exist" name="curso_exist">
                        @foreach($cursos_last as $curso_exist)
                            @if($curso_exist->primario == $preset_nivel)
                            <option class="nivel-{{ $curso_exist->primario }}" value="{{ $curso_exist->id }}">{{ $curso_exist->name }}</option>
                            @endif
                        @endforeach
                    </select>
                </div>
            </div>
        </div>

        <div class="box box-primary">
            <div class="box-body">
                <div class="row">
                        @include('curso.fields')
                </div>
            </div>
        </div>

        

        {!! Form::close() !!}
    </div>
</div>

<script>
    
    $('#accion').change(function(){
        var val = $('#accion option:selected').val();
        if(val != 0){
            $('.config_exist').removeClass('hidden');
        }else{
            $('.config_exist').addClass('hidden');
        }
    });
</script>
@endsection
