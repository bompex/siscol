@extends('layouts.app')
@section('content_header')
    <h1>
        {!! trans('page.model.curso'.$curso->primario) !!}: {!! $curso->name !!}
    </h1>
@endsection
@section('content')
    <div class="row">
        <div class="col-xs-12">
        @include('adminlte-templates::common.errors')

         

        <div class="box box-primary">
            <div class="box-body">
            	<h2>{!! trans('page.model.plural.materia'.$curso->primario) !!}</h2>

                <div class="row">

                    {!! Form::open(['route' => ['curso.store_materias',$curso->id]]) !!}

                    <div class="col-md-8">
					    <div class="hidden-xs">
					        <div class="col-md-4">{!! trans('page.model.materia'.$curso->primario) !!}</div>
					        <div class="col-md-3">{!! trans('page.model.profesor') !!}</div>
					        <div class="col-md-3">{!! trans('page.model.profesor') !!} {!! trans('page.label.suplente') !!}</div>
					        <div class="col-md-2">&nbsp;</div>
					    </div>
					    <div class="materia-list">
				        @foreach($curso->cursoMaterias as $cursoMateria)
					        <div class="materia-list-row  row">
						        <div class="col-md-4 col-xs-12">{{ Form::select('materia_id[]', $materias, $cursoMateria->materia->id, ['class' => 'form-control select2 materia_field2']) }}</div>
						        <div class="col-md-3 col-xs-12">{{ Form::select('profesor_id[]', $profesores, $cursoMateria->profesor->id, ['class' => 'form-control select2 profesor_field']) }}</div>
						        <div class="col-md-3 col-xs-10">{{ Form::select('profesor_suplente_id[]', $profesores_sup, ($cursoMateria->profesorSuplente)? $cursoMateria->profesorSuplente->id : null, ['class' => 'form-control select2 profesorSuplente_field', 'placeholder' => trans('page.label.curso.sin_suplente')]) }}</div>
						        <div class="col-md-2 col-xs-2">
						        	{{ Form::hidden('curso_materia_id[]', $cursoMateria->id, ['class'=>'curso_materia_id']) }}
					            	{{ Form::hidden('curso_materia_del_id[]', '-', ['class'=>'curso_materia_del_id']) }}

					            	<a href="javascirpt:;" onclick="delete_row(this)" class='btn btn-danger btn-xs' title="Quitar"><i class="glyphicon glyphicon-remove"></i></a>
						        </div>
						    </div>
						    <div class="clearfix"></div>
				        @endforeach
				    	</div>
					</div>
					<!-- Submit Field -->
					<div class="form-group col-sm-12">
					    <a href="javascript:;" onclick="add_row()" class='btn btn-success btn-xs'>
			        		{!! trans('page.button.add') !!} {!! trans('page.model.materia'.$curso->primario) !!}
			        	</a>
					</div>

					<!-- Submit Field -->
					<div class="form-group col-sm-12">
					    {!! Form::submit(trans('page.button.save'), ['class' => 'btn btn-primary']) !!}
					    <a href="{{ route('curso.index',['nivel' => $curso->nivel]) }}" class="btn btn-default">{!! trans('page.button.cancel') !!}</a>
					</div>
                        

                    {!! Form::close() !!}

                    <div style="display:none;" id="row_sample">
                    	<div class="materia-list-row row">
					        <div class="col-md-4 col-xs-12">{{ Form::select('materia_id[]', $materias, null, ['class' => 'form-control sample_select2 materia_field2']) }}</div>
					        <div class="col-md-3 col-xs-12">{{ Form::select('profesor_id[]', $profesores, null, ['class' => 'form-control sample_select2 profesor_field']) }}</div>
					        <div class="col-md-3 col-xs-10">{{ Form::select('profesor_suplente_id[]', $profesores_sup, null, ['class' => 'form-control sample_select2 profesorSuplente_field', 'placeholder' => trans('page.label.curso.sin_suplente')]) }}</div>
					        <div class="col-md-2 col-xs-2">
					        	{{ Form::hidden('curso_materia_id[]', '-', ['class'=>'curso_materia_id']) }}
			            	{{ Form::hidden('curso_materia_del_id[]', '-', ['class'=>'curso_materia_del_id']) }}

			            	<a href="javascirpt:;" onclick="remove_row(this)" class='btn btn-danger btn-xs' title="Quitar"><i class="glyphicon glyphicon-remove"></i></a>
					        </div>
					    </div>
					    <div class="clearfix"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection


@section('js')
<script type="text/javascript">
	function delete_row(elem){
		var tr = $(elem).parent().parent();
		tr.hide();
		tr.find('.curso_materia_del_id').val(tr.find('.curso_materia_id').val());
	}

	function remove_row(elem){
		var tr = $(elem).parent().parent();
		tr.remove();
	}

	function add_row(){
		var html = $('#row_sample').html();
		html= html.replace('sample_select2','select2');
		html= html.replace('sample_select2','select2');
		html= html.replace('sample_select2','select2');
		$(".materia-list").append(html);
		$('.select2').select2();
	}
</script>
@endsection
