@extends('layouts.app')
@section('content_header')
    <h1 class="pull-left">
        @if($nivel === false)
            {!! trans('page.model.plural.curso') !!}
        @else
            {!! trans('page.model.plural.curso'.$nivel) !!}
        @endif
    </h1>
    <h1 class="pull-right">
       <a class="btn btn-primary pull-right" style="margin-top: -10px;margin-bottom: 5px" href="{{ route('curso.create',['nivel'=>$nivel]) }}">{!! trans('page.button.add') !!} {!! trans('page.button.new') !!}</a>
    </h1>
@endsection
@section('content')
    <div class="row">
        <div class="col-xs-12">
        <div class="clearfix"></div>

        @include('flash::message')

        {{ Form::open(['route' => ['curso.index','nivel='.$nivel ], 'method' => 'get', 'id'=>'filter-form']) }}
            <div class="box box-primary">
                <div class="box-body">
                    <div class="filters">
                        @include('curso.filters')               
                    </div>
                </div>
            </div>
            {{ Form::close() }}

        <div class="clearfix"></div>
        <div class="box box-primary">
            <div class="box-body">
                    @include('curso.table')
            </div>
        </div>
        <div class="text-center">
        
        </div>
    </div>
</div>
@endsection

