<div class="form-group col-sm-6">
	<h2>{!! trans('page.label.curso.alumnos') !!}</h2>
	<ul>
		@foreach($alumnos as $alumno)
	        <li>
	    			{{ $alumno->lastname.", ".$alumno->firstname }}
	    	</li>
	    @endforeach
		
	</ul>
</div>

<div class="form-group col-sm-6">
	<h2>{!! trans('page.label.curso.materias') !!}</h2>
	<ul>
		@foreach($curso->cursoMaterias as $cursoMateria)
	        <li>
	    			<strong>{{ $cursoMateria->materia->name }}</strong> ({{$cursoMateria->profesor->lastname.", ".$cursoMateria->profesor->firstname }})
	    	</li>
	    @endforeach
		
	</ul>
</div>