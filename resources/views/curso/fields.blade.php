<!-- Name Field -->
<div class="form-group col-sm-4">
    {!! Form::label('name', 'Name:') !!}
    {!! Form::text('name', null, ['class' => 'form-control']) !!}
</div>
<div class="clearfix"></div>

<!-- Date From Field -->
<div class="form-group col-sm-6">
    {!! Form::label('date_from', trans('page.label.curso.date_from')) !!}
    {!! Form::date('date_from', ($curso->date_from!= '')? date('Y-m-d',strtotime($curso->date_from)) : date('Y-01-01',time()), ['class' => 'form-control','id'=>'date_from']) !!}
</div>

@section('scripts')
    <script type="text/javascript">
        $('#date_from').datetimepicker({
            format: 'YYYY-MM-DD HH:mm:ss',
            useCurrent: false
        })
    </script>
@endsection

<!-- Date To Field -->
<div class="form-group col-sm-6">
    {!! Form::label('date_to', trans('page.label.curso.date_to')) !!}
    {!! Form::date('date_to', ($curso->date_to!= '')? date('Y-m-d',strtotime($curso->date_to)) : date('Y-12-31',time()), ['class' => 'form-control','id'=>'date_to']) !!}
</div>

@section('scripts')
    <script type="text/javascript">
        $('#date_to').datetimepicker({
            format: 'YYYY-MM-DD HH:mm:ss',
            useCurrent: false
        })
    </script>
@endsection
<div class="clearfix"></div>

@if($preset_nivel === false)
<!-- Primario Field -->
<div class="form-group col-sm-2">
    {!! Form::label('primario', 'Nivel:') !!}
    {{ Form::select('primario', $nivel, $curso->primario ? $curso->primario : '0', ['class' => 'form-control', 'id'=>'primario']) }}
</div>
<div class="clearfix"></div>
@else
    {{ Form::hidden('primario', $preset_nivel) }}
@endif
<!-- Turno Field -->
<div class="form-group col-sm-2">
    {!! Form::label('turno', 'Turno:') !!}
    {{ Form::select('turno', $turnos, $curso->turno ? $curso->turno : 'mañana', ['class' => 'form-control', 'id'=>'turno']) }}
</div>
<div class="clearfix"></div>
<!-- Active Field -->
<div class="form-group col-sm-2">
    {!! Form::label('active', trans('page.label.curso.active')) !!}
    {{ Form::select('active', ['0'=>'No', '1' => 'Si'], $curso->active ? $curso->active : '0', ['class' => 'form-control', 'id'=>'activo']) }}
</div>
<!-- Turno Field -->
<div class="clearfix"></div>



<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit(trans('page.button.save'), ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('curso.index',['nivel'=>$nivel[$preset_nivel]]) }}" class="btn btn-default">{!! trans('page.button.cancel') !!}</a>
</div>
