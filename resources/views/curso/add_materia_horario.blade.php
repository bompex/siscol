@extends('layouts.app')
@section('content_header')
    <h1>
            {!! trans('page.model.curso'.$curso->primario) !!}: {!! $curso->name !!}
        </h1>
@endsection

@section('content')
    <div class="row">
        <div class="col-xs-12">
        @include('adminlte-templates::common.errors')

         

        <div class="box box-primary">
            <div class="box-body">
            	
            	<h2>{!! trans('page.model.plural.materia'.$curso->primario) !!}</h2>
                <div class='row col-md-5'>
            	{!! Form::open(['route' => ['curso.store_materias_horario',$curso->id]]) !!}
                    @foreach($curso->cursoMaterias as $cursoMateria)
                    
                    <div class='row'>
                        <div class = 'form-group col-sm-12 mareia_row'>
                            <div class = 'row col-sm-12'>
                                <h4>{{ $cursoMateria->materia->name }}</h4>

                                {{ Form::hidden('materia_id['.$cursoMateria->id.'][]', $cursoMateria->materia_id, ['class'=>'materia_field']) }}
                                {{ Form::hidden('color['.$cursoMateria->id.'][]', $cursoMateria->materia->color, ['class'=>'materia_color_field']) }}
                            </div>
                            @foreach($cursoMateria->cursoMateriaHorarios as $cursoMateriaHorarios)
                                <div class = 'row col-sm-12 horario_row'>
                                    <div class = 'col-sm-4'>
                                        {{ Form::select('dia['.$cursoMateria->id.'][]', $dias, $cursoMateriaHorarios->dia, ['class' => 'form-control dia_field']) }}
                                    </div>
                                    <div class = 'col-sm-3'>
                                        {!! Form::text('hora_inicio['.$cursoMateria->id.'][]', date('H:i',strtotime($cursoMateriaHorarios->hora_inicio)), ['class' => 'form-control timepicker hora_inicio_field', 'placeholder' => trans('page.label.curso_horario.hora_inicio')]) !!}
                                        
                                    </div>
                                    <div class = 'col-sm-3'>
                                        {!! Form::text('hora_fin['.$cursoMateria->id.'][]', date('H:i',strtotime($cursoMateriaHorarios->hora_fin)), ['class' => 'form-control timepicker hora_fin_field', 'placeholder' => trans('page.label.curso_horario.hora_fin')]) !!}
                                    </div>
                                    <div class = 'col-sm-2'>
                                        <a href="javascirpt:;" onclick="delete_row(this)" class='btn btn-danger btn-xs'><i class="glyphicon glyphicon-remove"></i></a></td>
                                    </div>
                                    {{ Form::hidden('curso_materia_horario_id['.$cursoMateria->id.'][]', $cursoMateriaHorarios->id, ['class'=>'curso_materia_horario_id']) }}
                                    {{ Form::hidden('curso_materia_horario_del_id['.$cursoMateria->id.'][]', '-', ['class'=>'curso_materia_horario_del_id']) }}
                                </div>
                                <div class="clearfix"></div>
                            @endforeach     
                            <div class = 'row col-sm-12 text-center'>
                                <a href="javascript:;" onclick="add_row(this,{!! $cursoMateria->id !!})" class='btn btn-success btn-xs'>
                                    {!! trans('page.button.add') !!} {!! trans('page.label.curso_horario.horario') !!}
                                </a>
                            </div> 

                        </div>
                        
                        
                    </div>
                    <div class="clearfix"></div>

                    @endforeach

                    <!-- Submit Field -->
                    <div class="form-group col-sm-12">
                        {!! Form::submit(trans('page.button.save'), ['class' => 'btn btn-primary']) !!}
                        <a href="{{ route('curso.index',['nivel'=>$curso->nivel]) }}" class="btn btn-default">{!! trans('page.button.cancel') !!}</a>
                    </div>

                {!! Form::close() !!}

                    <div  style="display:none;" id="row_sample">
                        <div class="row col-sm-12 horario_row">
                            <div class = 'col-sm-4'>
                                {{ Form::select('sample_dia', $dias, null, ['class' => 'form-control dia_field']) }}
                            </div>
                            <div class = 'col-sm-3'>
                                {!! Form::text('sample_hora_inicio', '', ['class' => 'form-control sample_timepicker hora_inicio_field', 'placeholder' => trans('page.label.curso_horario.hora_inicio')]) !!}
                                
                            </div>
                            <div class = 'col-sm-3'>
                                {!! Form::text('sample_hora_fin', '', ['class' => 'form-control sample_timepicker hora_fin_field', 'placeholder' => trans('page.label.curso_horario.hora_fin')]) !!}
                            </div>
                            <div class = 'col-sm-2'>
                                <a href="javascirpt:;" onclick="remove_row(this)" class='btn btn-danger btn-xs'><i class="glyphicon glyphicon-remove"></i></a></td>
                            </div>
                            {{ Form::hidden('sample_curso_materia_horario_id', '-', ['class'=>'curso_materia_horario_id']) }}
                            {{ Form::hidden('sample_curso_materia_horario_del_id', '-', ['class'=>'curso_materia_horario_del_id']) }}
                        </div>
                        <div class="clearfix"></div>
                    </div>

                </div>
                <div class = 'form-group col-md-7'>
                    <div id='curso_schedule'></div>                        
                </div>
            </div>
        </div>
    </div>
</div>
@endsection


@section('js')
<script type="text/javascript">
    $( function() {
        $('.timepicker').timepicker({
            controlType: 'select',
            oneLine: true,
            stepMinute: 5,
            hourMin: {!! $horamin !!},
            hourMax: {!! $horamax !!},
        });

        $("#curso_schedule").dayScheduleSelector({

            days: [1,2,3,4,5],
            stringDays  : ['Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes'],
            @if($curso->turno == 'mañana')
                structure : ['07:35','08:35','09:35','09:50','10:50','11:50','12:00','13:00'],
                turno : 'mañana'
            @else
                structure : ['13:00','13:50','14:00','14:50','15:40','15:55','16:45','17:30'],
                turno : 'tarde'
            @endif
        });
        $("#curso_schedule").data('artsy.dayScheduleSelector').deserialize(
            {!! json_encode($curso->getFullSchedule()) !!}
        );
        $('.mareia_row').on('change', '.dia_field, .hora_inicio_field, .hora_fin_field', function() {
            refreshScheduleTable("#curso_schedule");            
        });

    });

    

    function delete_row(elem){
        var row = $(elem).parents('.horario_row');
        row.hide();
        row.find('.curso_materia_horario_del_id').val(row.find('.curso_materia_horario_id').val());
        refreshScheduleTable("#curso_schedule");
    }
    function remove_row(elem){
        var row = $(elem).parents('.horario_row');
        row.remove();
        refreshScheduleTable("#curso_schedule");
    }

    function add_row(elem, id){
        var row = $(elem).parent();
        var html = $('#row_sample').html();

        html= html.replace('sample_dia','dia['+id+'][]');
        html= html.replace('sample_hora_inicio','hora_inicio['+id+'][]');
        html= html.replace('sample_hora_fin','hora_fin['+id+'][]');
        html= html.replace('sample_curso_materia_horario_id','curso_materia_horario_id['+id+'][]');
        html= html.replace('sample_curso_materia_horario_del_id','curso_materia_horario_del_id['+id+'][]');

        html= html.replace('sample_timepicker','timepicker');
        html= html.replace('sample_timepicker','timepicker');



        row.before(html);

        $('.timepicker').timepicker({
            controlType: 'select',
            oneLine: true,
            stepMinute: 5,
            hourMin: {!! $horamin !!},
            hourMax: {!! $horamax !!},
        });

    }
    function refreshScheduleTable(table_selector){
        $(table_selector).find('.time-slot').removeClass().addClass('time-slot').removeAttr('data-selected').removeAttr('style').html('');
        $(table_selector).find('.over-schedule').html('');
        $(table_selector).data('artsy.dayScheduleSelector').deserialize(getDataObject());
    }

    function getDataObject(){
        var data = {};
        $('.mareia_row').each(function(index) {
            var materia_row = $(this);
            var name = materia_row.find('h4').html();
            var color = materia_row.find('.materia_color_field').val();
            var id = materia_row.find('.materia_field').val();
            var hora = {};
            materia_row.find('.horario_row').each(function(index) {
                var horario_row = $(this);
                
                var dia = horario_row.find('.dia_field').val();
                var hi = horario_row.find('.hora_inicio_field').val();
                var hf = horario_row.find('.hora_fin_field').val();
                var vhi = hi.replace(':','')*1;
                var vhf = hf.replace(':','')*1;
                if(hi!='' && hf!='' && vhi<vhf && horario_row.find('.curso_materia_horario_del_id').val() == '-'){
                    if (typeof hora[dia] == 'undefined') {
                      hora[dia] = [];
                    }
                    hora[dia].push([hi,hf]);
                }
            });
            data[id] = { 'name': name,'color': color,'schedule': hora}
        });
        return data;
    }
   
    
</script>
@endsection
