@extends('layouts.app')
@section('content_header')
    <h1 class="pull-left">
        Promover Curso {{ $curso_actual->name }}
    </h1>
@endsection
@section('content')
    <div class="row">
        <div class="col-xs-12">
        <div class="clearfix"></div>

        @include('flash::message')

        {{ Form::open(['route' => 'curso.store_promote', 'method' => 'post', 'id'=>'filter-form', 'onsubmit' => 'return confirm("Seguro que desea Guardar?")']) }}
        <div class="box box-primary">
            <div class="box-body">
                <p>Los alumnos que se marquen en el listado inferior seran promovidos al curso seleccionado. Los alumnos que no se seleccionen permaneceran en el curso en el que estan actualmente. Podra volver a utilizar esta opcion para promocionar los alumnos que queden en el curso a un curso diferente.</p>
                <p>Seleccione a que curso se desea promocionar a los alumnos.
                    <ul>
                        <li>-Crear Curso del Periodo Anterior: Se creara un curso nuevo para el periodo acutal en base a la configuracion del periodo anterior. Se mantendran las materias y los profesores. El curso podra ser modificado luego desde su correspondiente opción.</li>
                        <li>-Usar Curso Existente: Los alumnos seleccionados se promoveran a un curso existente del periodo actual.</li>
                        <li>-Crear Nuevo Curso: Los alumnos seleccionados se promoveran a un curso nuevo en el periodo actual. Este curso no tiene ninguna matera, profesor u horario asignado, se debera configurar eso posteriormente.</li>
                    </ul>
                </p>
                <div class="form-group col-sm-6">
                {{ Form::select('accion', $accion, null, ['class' => 'form-control', 'id'=>'accion']) }}
                </div>
                <div class="form-group col-sm-6">
                    <a href="{{ route('curso.index',['nivel'=>$nivel[$preset_nivel], 'periodo' => date('Y',strtotime('-1 year'))]) }}" class="btn btn-default">{!! trans('page.button.cancel') !!}</a>
                </div>
                <div class="clearfix"></div>
                {{ Form::hidden('old', $curso_actual->id) }}
            </div>
        </div>

        <div class="box box-primary promote_opt hidden" id='curso-new-form'>
            <div class="box-body">
                <h3>Nuevo Curso</h3>
                @include('curso.fields')
            </div>
        </div>

        <div class="box box-primary promote_opt hidden" id='curso-exist-form'>
            <div class="box-body">
                <h3>Cursos Periodo Actual ({{ date('Y',time()) }})</h3>
                <div class="form-group col-sm-4">
                    {!! Form::label('nivel_select_exist', 'Nivel:') !!}
                    <select id="nivel_select_exist">
                        <option value="nivel-1">Primario</option>
                        <option value="nivel-0" {{ ($curso_actual->primario == 0)? 'selected="selected"' : '' }}>Secundario</option>
                    </select>
                </div>
                <div class="clearfix"></div>
                <div class="form-group col-sm-6">
                    {!! Form::label('curso_exist', 'Curso:') !!}
                    <select id="curso_exist" name="curso_exist">
                        @foreach($cursos_current as $curso_exist)
                            <option class="nivel-{{ $curso_exist->primario }}" value="{{ $curso_exist->id }}">{{ $curso_exist->name }}</option>
                        @endforeach
                    </select>
                </div>
                <!-- Submit Field -->
                <div class="form-group col-sm-12">
                    {!! Form::submit(trans('page.button.save'), ['class' => 'btn btn-primary']) !!}
                    <a href="{{ route('curso.index',['nivel'=>$nivel[$preset_nivel]]) }}" class="btn btn-default">{!! trans('page.button.cancel') !!}</a>
                </div>
            </div>
        </div>


        <div class="box box-primary promote_opt hidden" id='curso-old-form'>
            <div class="box-body">
                <h3>Cursos Periodo Previo ({{ date('Y',strtotime('-1 year')) }})</h3>
                <div class="form-group col-sm-4">
                    {!! Form::label('nivel_select_old', 'Nivel:') !!}
                    <select id="nivel_select_old">
                        <option value="nivel-1">Primario</option>
                        <option value="nivel-0" {{ ($curso_actual->primario == 0)? 'selected="selected"' : '' }}>Secundario</option>
                    </select>
                </div>
                <div class="clearfix"></div>
                <div class="form-group col-sm-6">
                    {!! Form::label('curso_old', 'Curso:') !!}
                    <select id="curso_old" name="curso_old">
                        @foreach($cursos_last as $curso_exist)
                            <option class="nivel-{{ $curso_exist->primario }}" value="{{ $curso_exist->id }}">{{ $curso_exist->name }}</option>
                        @endforeach
                    </select>
                </div>
                <!-- Submit Field -->
                <div class="form-group col-sm-12">
                    {!! Form::submit(trans('page.button.save'), ['class' => 'btn btn-primary']) !!}
                    <a href="{{ route('curso.index',['nivel'=>$nivel[$preset_nivel]]) }}" class="btn btn-default">{!! trans('page.button.cancel') !!}</a>
                </div>
            </div>
        </div>

        <div class="clearfix"></div>
        <div class="box box-primary">
            <div class="box-body">
                    <h3>Alumnos</h3>
                    @include('curso.table_promote')
            </div>
        </div>
            {{ Form::close() }}

        
        <div class="text-center">
        
        </div>
    </div>
</div>

<script>
    
    $('#accion').change(function(){
        var val = $('#accion option:selected').val();
        
        if(val != 0){
            $('.promote_opt').addClass('hidden');
            $('#curso-'+val+'-form').removeClass('hidden');
        }else{
            $('.promote_opt').addClass('hidden');
        }
    });

    $('#nivel_select_old').change(function(){
        var val = $('#nivel_select_old option:selected').val();
        $('#curso-old-form select#curso_old option').hide();
        $('#curso-old-form select#curso_old option.'+val).show();
    });

    $('#nivel_select_exist').change(function(){
        var val = $('#nivel_select_exist option:selected').val();
        $('#curso-exist-form select#curso_exist option').hide();
        $('#curso-exist-form select#curso_exist option.'+val).show();
    });

    $('#curso-old-form select#curso_old option').hide();
    $('#curso-old-form select#curso_old option.nivel-{{ $curso_actual->primario }}').show();

    $('#curso-exist-form select#curso_exist option').hide();
    $('#curso-exist-form select#curso_exist option.nivel-{{ $curso_actual->primario }}').show();
</script>
@endsection


