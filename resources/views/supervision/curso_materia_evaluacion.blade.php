<div class="panel panel-info">
    <div class="panel-heading">Próximas {!! trans('page.model.plural.evaluacion'.$cursoMateria->curso->primario) !!}
      
    </div>
    <div class="panel-body">
      <ul class='foro-teaser'>
        @if(count($evaluaciones) >0)
          @foreach($evaluaciones as $evaluacion)
            <li class='row'>
              <div class="col-md-12 fr-subject"><a href="{{ route('supervision.show_evaluacion', $evaluacion->id) }}">{{ (!$cursoMateria->curso->primario)? $evaluacion_types[$evaluacion->type_id].":" : "" }} {{ $evaluacion->subject }}</a></div>
              <div class='col-md-12 fr-info row'>
                <div class="col-sm-6 col-xs-12 fr-author">Por: {{ $evaluacion->profesor->lastname }}, {{ $evaluacion->profesor->firstname }}</div>
                @if( date('Y-m-d', strtotime($evaluacion->fecha_inicio)) == date('Y-m-d', strtotime($evaluacion->fecha_fin)))
                  <div class="col-sm-6 fr-date">{{ date('d/m/Y', strtotime($evaluacion->fecha_inicio)) }} de {{ date('H:i', strtotime($evaluacion->fecha_inicio)) }} a {{ date('H:i', strtotime($evaluacion->fecha_fin)) }} </div>
                @else
                  <div class="col-sm-6 fr-date">{{ date('d/m/Y H:i', strtotime($evaluacion->fecha_inicio)) }} al {{ date('d/m/Y H:i', strtotime($evaluacion->fecha_fin)) }} </div>
                @endif                
                <div class="clearfix"></div>
              </div>
              <div class="clearfix"></div>
            </li>   
          @endforeach
        @else
        <li class='row'>
          <div class="col-md-12 fr-no-message">{!! trans('page.message.no_message') !!}</div>
          <div class="clearfix"></div>
        </li>  
        @endif    
      </ul>
      <div class='col-md-12  text-center'><a class="btn btn-primary btn-ssm text-center"  href="{{ route('supervision.index_evaluacion', $cursoMateria->id) }}">Ver todas las {!! trans('page.model.plural.evaluacion'.$cursoMateria->curso->primario) !!}</a></div>
    </div>
</div>


<div class="panel panel-info">
    <div class="panel-heading">{!! trans('page.model.plural.evaluacion'.$cursoMateria->curso->primario) !!} Realizadas</div>
    <div class="panel-body">
      <ul class='foro-teaser'>
        @if(count($evaluaciones_prev) >0)
          @foreach($evaluaciones_prev as $evaluacion)
            <li class='row'>
              <div class="col-md-12 fr-subject"><a href="{{ route('supervision.show_evaluacion', $evaluacion->id) }}">{{ (!$cursoMateria->curso->primario)? $evaluacion_types[$evaluacion->type_id].":" : "" }} {{ $evaluacion->subject }}</a></div>
              <div class='col-md-12 fr-info row'>
                <div class="col-sm-6 col-xs-12 fr-author">Por: {{ $evaluacion->profesor->lastname }}, {{ $evaluacion->profesor->firstname }}</div>
                @if( date('Y-m-d', strtotime($evaluacion->fecha_inicio)) == date('Y-m-d', strtotime($evaluacion->fecha_fin)))
                  <div class="col-sm-6 fr-date">{{ date('d/m/Y', strtotime($evaluacion->fecha_inicio)) }} de {{ date('H:i', strtotime($evaluacion->fecha_inicio)) }} a {{ date('H:i', strtotime($evaluacion->fecha_fin)) }} </div>
                @else
                  <div class="col-sm-6 fr-date">{{ date('d/m/Y H:i', strtotime($evaluacion->fecha_inicio)) }} al {{ date('d/m/Y H:i', strtotime($evaluacion->fecha_fin)) }} </div>
                @endif                
                <div class="clearfix"></div>
              </div>
              <div class="clearfix"></div>
            </li>   
          @endforeach
        @else
        <li class='row'>
          <div class="col-md-12 fr-no-message">{!! trans('page.message.no_message') !!}</div>
          <div class="clearfix"></div>
        </li>  
        @endif    
      </ul>
      <div class='col-md-12  text-center'><a class="btn btn-primary btn-ssm text-center"  href="{{ route('supervision.index_evaluacion', $cursoMateria->id) }}">Ver todas las {!! trans('page.model.plural.evaluacion'.$cursoMateria->curso->primario) !!}</a></div>
    </div>
</div>