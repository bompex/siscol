<div class="panel panel-info calendar-panel">
    <div class="panel-heading">Calendario: 
    </div>
    <div class="panel-body">
      <div class="calendar-wrapper">
        <div class="pull-left form-inline">
          <span class='calendar-title'></span>
        </div>
        <div class="pull-right form-inline">
          <div class="btn-group">
            <button class="btn btn-xs btn-primary" data-calendar-nav="prev"><< Anterior</button>
            <button class="btn btn-xs" data-calendar-nav="today">Hoy</button>
            <button class="btn btn-xs btn-primary" data-calendar-nav="next">Posterior >></button>
          </div>
          <div class="btn-group">
            <button class="btn btn-xs btn-warning active" data-calendar-view="month">Mes</button>
            <button class="btn btn-xs btn-warning" data-calendar-view="day">Día</button>
          </div>
        </div>

        <div class="clearfix"></div>
        <div id="alumno-calendar"></div>
      </div>
    </div>

</div>


<script>
(function ($) {
     
      

      var options = {
		    events_source:  function () { 
		      return {!! json_encode($calendar) !!}; 
		    },
		    view: 'month',
		    tmpl_path: "{{ asset('vendor/bootstrap-calendar-master/tmpls') }}/",
		    tmpl_cache: false,
		    language: 'es-MX',
		    onAfterEventsLoad: function(events) {
		      if(!events) {
		        return;
		      }
		      var list = $('#eventlist');
		      list.html('');

		      $.each(events, function(key, val) {
		        $(document.createElement('li'))
		          .html('<a href="' + val.url + '">' + val.title + '</a>')
		          .appendTo(list);
		      });
		    },
		    onAfterViewLoad: function(view) {
		      $('.calendar-panel .calendar-title').text(this.getTitle());
		      $('.btn-group button').removeClass('active');
		      $('button[data-calendar-view="' + view + '"]').addClass('active');
		    },
		    classes: {
		      months: {
		        general: 'label'
		      }
		    }
		  };



		  var calendar = $("#alumno-calendar").calendar(options); 


		  $('.btn-group button[data-calendar-nav]').each(function() {
		    var $this = $(this);
		    $this.click(function() {
		      calendar.navigate($this.data('calendar-nav'));
		    });
		  });

		  $('.btn-group button[data-calendar-view]').each(function() {
		    var $this = $(this);
		    $this.click(function() {
		      calendar.view($this.data('calendar-view'));
		    });
		  });


    })($);

</script> 