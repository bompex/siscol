@extends('layouts.modal')
@section('modal-title')
	<h4>{{ $cursoMateriaHorario->cursoMateria->materia->name }}: {{ $cursoMateriaHorario->dianame }} de {{ date('H:i',strtotime($cursoMateriaHorario->hora_inicio)) }} a {{ date('H:i',strtotime($cursoMateriaHorario->hora_fin)) }}</h4>
@endsection
@section('modal-content')
	<div class="modal-body">
		<div class="row">
			<div class="col-md-12">
			    {!! $cursoMateriaHorario->note !!}
			</div>
		</div>
	</div>
@endsection
