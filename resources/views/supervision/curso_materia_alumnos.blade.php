<div class="panel panel-info">
    <div class="panel-heading">{{ trans('page.model.plural.alumno') }}</div>
    <div class="panel-body">
    	<table  class="table" id="profesor-alumno-table">
        <tr>
          <th></th>
          @if($cursoMateria->curso->primario != 1)
          <th><span class='hidden-xs'>Examenes</span><span class='visible-xs'>Exam.</span></th>
          <th><span class='hidden-xs'>Prácticos</span><span class='visible-xs'>Prac.</span></th>
          @endif
          <th><span class='hidden-xs'>Concepto</span><span class='visible-xs'>Con.</span></th>
          @if($cursoMateria->curso->primario != 1)
          <th style='width: 77px;'><span class='hidden-xs'>Promedio</span></th>
          @endif
        </tr>
    		@foreach($alumnos as $Alumno)
  			<tr>
  				<td>
            
            <div class="dropdown boton-dropdown" style="display:inline">
              <a href="#" id="dropdownMenuButtonAlumno" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="glyphicon glyphicon-option-vertical"></i>
              </a>
              <div class="dropdown-menu" aria-labelledby="dropdownMenuButtonAlumno">
                <a title="Ver Observación" class="dropdown-item add_alumno_observation" href="javascript:;"  data-href="{{ route('supervision.alumno_nota_show', [$cursoMateria->id,$Alumno->id]) }}">Ver Observación</a>
                <a title="Enviar Mensaje" class="dropdown-item open_message_modal" href="javascript:;" data-href="{{ route('message.show', [$Alumno->user->id, 'mode' => 'modal']) }}">Enviar Mensaje</a>
              </div>
            </div>
              {{ $Alumno->lastname }}, {{ $Alumno->firstname }}
              @if($Alumno->cursoMateriaAlumnoComment($cursoMateria->id))
              <a title="Agregar Observación" href="javascript:;" class='add_alumno_observation'  data-href="{{ route('supervision.alumno_nota_show', [$cursoMateria->id,$Alumno->id]) }}">
                <i class="glyphicon glyphicon-tag"></i>
              </a>
              @endif
            
          </td>
          @if($cursoMateria->curso->primario != 1)
          <td>
            @if($exam = $Alumno->getCalificaciones($cursoMateria->id, 1))
              @foreach($exam as $calif)
                <a href="javascript:;" class="btn btn-warning btn-xs nota-alumno" >
                  {{ $calif->nota }}         
                </a>
              @endforeach
            @else
              -
            @endif
          </td>
          <td>
            @if($exam = $Alumno->getCalificaciones($cursoMateria->id, 2))
              @foreach($exam as $calif)
                <a href="javascript:;" class="btn btn-warning btn-xs nota-alumno" >
                  {{ $calif->nota }}         
                </a>
              @endforeach
            @else
              -
            @endif
          </td>
          @endif
          <td>
            @if($exam = $Alumno->getCalificaciones($cursoMateria->id, false))
              @if(count($exam)>0)
              @foreach($exam as $calif)
                <a href="javascript:;" class="btn btn-warning btn-xs nota-alumno"  >
                  {{ $calif->nota }}         
                </a>
              @endforeach
              @else
              -
              @endif
            @endif
            
          </td>
          @if($cursoMateria->curso->primario != 1)
          <td>
            <a href="javascript:;" class="btn btn-warning btn-xs nota-alumno" >
              {{ $Alumno->getPromedioPeriodo($cursoMateria->id) }}       
            </a>
          </td>
          @endif
  			</tr>
  			@endforeach
    	</table>

    </div>
</div>



<script>

  (function ($) {
      $('.add_alumno_observation').on( "click", function() {
          var href = $(this).data('href');
            $.ajax({
                url: href,
                type : 'GET',
              success : function(data) {
                $('#ventana-modal').html(data);
                $('#ventana-modal').modal('show');  
                CKEDITOR.replace('cmh_note',{plugins:'emoji,basicstyles,link,wysiwygarea,toolbar,image,colorbutton,list,resize',resize_dir: 'vertical'});                
              },

              // código a ejecutar si la petición falla;
               error : function(xhr, status) {
                console.log(status);
                    alert('Disculpe, existió un problema');
                },
            });
      })

      $('.nota-alumno').each(function( index ) {
        if( ($(this).html()*1) > 6 ){
          $(this).removeClass('btn-warning');
          $(this).addClass('btn-success');
        }
        if( ($(this).html()*1) < 5 ){
          $(this).removeClass('btn-warning');
          $(this).addClass('btn-danger');
        }
      });

      
      
    })($);


function add_alumno_observation(elem){


  var href = $(elem);
  console.log(href);
    $.ajax({
      url: href,
      type : 'GET',
    success : function(data) {
      $('#ventana-modal').html(data);
      $('#ventana-modal').modal('show');  
      CKEDITOR.replace('cmh_note',{plugins:'emoji,basicstyles,link,wysiwygarea,toolbar,image,colorbutton,list,resize',resize_dir: 'vertical'});               
    },

    // código a ejecutar si la petición falla;
     error : function(xhr, status) {
      console.log(status);
          alert('Disculpe, existió un problema');
      },
  });
}

function open_calificacion_form(elem){
    $.ajax({
        url: $(elem).data('url_redir'),
        type : 'GET',
      success : function(data) {
        $('#ventana-modal').html(data);
        $('#ventana-modal').modal('show');                
      },

      // código a ejecutar si la petición falla;
       error : function(xhr, status) {
        console.log(status);
            alert('Disculpe, existió un problema');
        },
    });
}



</script>