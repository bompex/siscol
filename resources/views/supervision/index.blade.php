@extends('layouts.app')
@section('content_header')
    <h1 class="pull-left">
        {!! trans('page.model.plural.curso') !!}
    </h1>
@endsection
@section('content')
    <div class="row">
        <div class="col-xs-12">
        <div class="clearfix"></div>

        @include('flash::message')
        {{ Form::open(['route' => 'supervision.index', 'method' => 'get', 'id'=>'filter-form']) }}
        <div class="box box-primary">
            <div class="box-body">
                <div class="filters">
                    <div>
                        <div class="col-r col-md-3 col-xs-12 input-filtro">
                            <label>Periodo:</label>
                            {{ Form::select('periodo', $periodos, isset($periodo) ? $periodo : null, [
                                'id' => 'periodo',
                                'class' => 'form-control'
                            ]) }}
                        </div>
                    </div>
                    <div class="clearfix"></div>        
                </div>
            </div>
        </div>
        {{ Form::close() }}

        <div class="clearfix"></div>
        <div class="box box-primary">
            <div class="box-body">
            <div class="table-responsive">
                <table class="table datatable" id="curso-table">
                    <thead>
                        <tr>
                            <th>{!! trans('page.label.curso.name') !!}</th>
                            <th>{!! trans('page.label.curso.nivel') !!}</th>
                            <th>{!! trans('page.label.curso.turno') !!}</th>
                            <th>{!! trans('page.model.plural.materia') !!}</th>
                            <th>{!! trans('page.label.curso.alumnos') !!}</th>
                            <th>{!! trans('page.label.curso.active') !!}</th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach($cursos as $curso)
                        
                            <tr>
                                <td><a href="{{ route('supervision.curso', [$curso->id]) }}">{{ $curso->name }}</a></td>
                                <td>{{ $curso->nivel }}</td>
                                <td>{{ $curso->turno }}</td>
                                <td>{{ count($curso->cursoMaterias) }}</td>
                                <td>{{ count($curso->cursoAlumnos) }}</td>
                                <td>{{ ($curso->active)? 'Si' : 'No' }}</td>

                            </tr>
                        
                    @endforeach
                    </tbody>
                </table>
            </div>
            </div>
        </div>
        <div class="text-center">
        
        </div>
    </div>
</div>

<script type="text/javascript">
    $(function () {
        $("#periodo").change(function () {
            $('#filter-form').submit();
        });

    });

</script>  
@endsection

