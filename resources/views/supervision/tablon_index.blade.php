@extends('layouts.app')
@section('content_header')
        <h1 class="pull-left">{!! trans('page.model.plural.topic') !!}</h1>
        <div class="pull-right">
            <a class="btn btn-primary btn-sm" href="{{ route('supervision.curso_materia',[$cursoMateria->id]) }}">{!! trans('page.button.back') !!}</a>
        </div>
@endsection
@section('content')
    <div class="row">
        <div class="col-xs-12">
        <div class="clearfix"></div>

        @include('flash::message')

        <div class="clearfix"></div>
        <div class="box box-primary">
            <div class="box-body">
                <div class="table-responsive">
                    <table class="table datatable topics-table" id="topic-table">
                        <thead>
                            <tr>
                                <th>{!! trans('page.label.topic.subject') !!}</th>
                                <th>{!! trans('page.label.topic.status') !!}</th>
                                <th>Permite Comentario</th>
                                <th>Fecha Creacion</th>
                                <th class='no-sort'>&nbsp;</th>
                            </tr>
                        </thead>
                        <tbody>
                        @foreach($topics as $topic)
                            <tr>
                                <td>{{ $topic->subject }}</td>
                                <td>{{ ($topic->active == 1)? 'Publicado' : 'Archivado' }}</td>
                                <td>{{ ($topic->allow_comments == 1)? 'Si' : 'No' }}</td>
                                <td>{{ date('d/m/Y H:i', strtotime($topic->created_at)) }}</td>
                                <td>
                                    <div class='btn-group'>
                                        <a href="{{ route('supervision.show_topic', $topic->id) }}" class='btn btn-default btn-xs' title='Ver'><i class="glyphicon glyphicon-eye-open"></i></a>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
