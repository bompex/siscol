@extends('layouts.app')

@section('content_header')

<h1 class="pull-left">{!! trans('page.model.curso') !!}: {{ $cursoMateria->curso->name }} - {{ $cursoMateria->materia->name }} - Prof.  {{ $cursoMateria->profesor->nombre }} 
    <a class="btn btn-success btn-sm open_message_modal" style="margin-top: -10px;margin-bottom: 5px" href="javascript:;" data-href="{{ route('message.show', [$cursoMateria->profesor->user->id, 'mode' => 'modal']) }}">Enviar Mensaje</a>
</h1>
<h1 class="pull-right">
   <a class="btn btn-primary btn-sm" style="margin-top: -10px;margin-bottom: 5px" href="{{ route('supervision.curso', [$cursoMateria->curso->id]) }}">{!! trans('page.button.back') !!}</a>
</h1>

@endsection

@section('content')
<div class="clearfix"></div>
    @include('flash::message')
<div class="row">
    <div class="col-md-6 col-xs-12">
        @include('supervision.curso_materia_foro')

        @include('supervision.curso_materia_evaluacion')
    </div>
    <div class="col-md-6 col-xs-12">
    	@include('supervision.curso_materia_horario')
        @include('supervision.curso_anuncio')

    	@include('supervision.curso_materia_alumnos')
    
    </div>
    <div class="clearfix"></div>
</div>
@endsection