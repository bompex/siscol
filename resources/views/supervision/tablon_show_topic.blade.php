@extends('layouts.app')


@section('content_header')

<h1 class="pull-left">{!! trans('page.model.curso') !!}: {{ $cursoMateria->curso->name }} - {{ $cursoMateria->materia->name }} </h1>
<h1 class="pull-right">
   		<a class="btn btn-primary btn-sm" href="{{ route('supervision.curso_materia',[$cursoMateria->id]) }}">{!! trans('page.button.back') !!}</a>
</h1>

@endsection

@section('content')

@include('flash::message')
<div class='row'>
  <div class="col-xs-12 foro">
    <div class="panel panel-default">
    <div class="panel-body">
      <div class="col-xs-12">
        <h3>{{ $topic->subject }} {{ ($topic->active == 0)? '(Archivado)' : '' }}</h3>
      </div>
      <div class="col-sm-2 fr-metadata row">
        <div class="col-sm-12 col-xs-12 fr-author">{{ $topic->user->lastname }}, {{ $topic->user->firstname }}</div>
      <div class="col-sm-12 col-xs-4 fr-date">{{ date('d/m/Y', strtotime($topic->created_at)) }}</div>
      <div class="col-sm-12 col-xs-4 fr-horu">{{ date('H:i:s', strtotime($topic->created_at)) }}</div>      
    </div>
    <div class="col-sm-10 topic-body">
        <div>
          {!! $topic->body !!}
        </div>
        @if(isset($topic->files) && count($topic->files)>0)
        <div>
          <strong>Archivos Adjuntos</strong>
          <ul>
            @foreach($topic->files as $file)
                  <li><div><a href='{!! $file->path !!}' target='blank'>{{ $file->name }}</a></div></li>
              @endforeach
          </ul>
        </div>
        @endif
    </div>
      
    </div>
</div>
  	

<div class="panel panel-default">
    <div class="panel-body">
      @if(isset($comments) && count($comments) > 0)
          @foreach($comments as $index => $comment)
              <div class="fr-comment row">
                <div class="col-sm-2 col-xs-12 fr-metadata  row">
                  <div class="col-sm-12 col-xs-2 fr-comment-number">#{{  count($comments) - $index }}</div>
                  <div class="col-sm-12 col-xs-10 fr-author">{{ $comment->user->lastname }}, {{ $comment->user->firstname }}</div>
                <div class="col-sm-12 col-xs-6 fr-date">{{ date('d/m/Y', strtotime($comment->created_at)) }}</div>
                <div class="col-sm-12 col-xs-6 fr-horu">{{ date('H:i:s', strtotime($comment->created_at)) }}</div>
                        <div class="col-sm-12  col-xs-4">
                            

                        </div>
                        
              </div>
              <div class="col-sm-10 col-xs-12 comment-body">
                    {!! $comment->body !!}
              </div>
              
                @if(isset($comment->childrens) && count($comment->childrens) > 0)
                <div class="clearfix"></div>
                <div class="toggle-subcomments">-Ver comentarios-</div>
                <div class="fr-subcomment" style="display:none">
                    @foreach($comment->childrens as $subindex => $subcomment)
                        <div class="fr-comment row">
                            <div class="col-sm-2 col-xs-12 fr-metadata  row">
                                <div class="col-sm-12 col-xs-2 fr-comment-number">#{{  $subindex+1 }}</div>
                                <div class="col-sm-12 col-xs-10 fr-author">{{ $subcomment->user->lastname }}, {{ $subcomment->user->firstname }}</div>
                                <div class="col-sm-12 col-xs-6 fr-date">{{ date('d/m/Y', strtotime($subcomment->created_at)) }}</div>
                                <div class="col-sm-12 col-xs-6 fr-horu">{{ date('H:i:s', strtotime($subcomment->created_at)) }}</div>
                                <div class="col-sm-12  col-xs-4">
                                    

                                </div>
                                
                            </div>
                            <div class="col-sm-10 col-xs-12 comment-body">
                                    {!! $subcomment->body !!}
                            </div>
                        </div>
                    @endforeach
                </div>
                @endif
                </div>
          @endforeach

      @else

          <div class='col-md-12 text-center'>
            No hay Comentarios
          </div>

      @endif



    </div>
</div>

<script>
$( function() {
    $( ".toggle-subcomments" ).click(function() {
        if($(this).html() == '-Ver comentarios-'){
            $(this).html('-Ocultar comentarios-');
        }else{
            $(this).html('-Ver comentarios-');
        }
        $(this).parent().find('.fr-subcomment').slideToggle( "slow" )
    });
} );
</script>


  </div>
</div>
@endsection
