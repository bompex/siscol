@extends('layouts.app')
@section('content')
    <div class="row">
        <div class="col-xs-12">
        <div class="box box-primary">
            <div class="box-body">
                <div class="row" style="padding-left: 20px">
                    
                    <div class="row">
                        <div class='col-md-6 evaluacion-subject'>
                            {!! Form::label('subject', 'Tema:') !!}
                            <p>{{ $evaluacion->subject }}</p>
                        </div>
                        <div class='col-md-6 evaluacion-fecha'>
                            {!! Form::label('date', trans('page.label.evaluacion.'.$evaluacion->type_id.'_date')) !!}:
                            <p>
                                @if( date('Y-m-d', strtotime($evaluacion->fecha_inicio)) == date('Y-m-d', strtotime($evaluacion->fecha_fin)))
                                    {{ date('d/m/Y', strtotime($evaluacion->fecha_inicio)) }} de {{ date('H:i', strtotime($evaluacion->fecha_inicio)) }} a {{ date('H:i', strtotime($evaluacion->fecha_fin)) }} 
                                @else
                                    {{ date('d/m/Y H:i', strtotime($evaluacion->fecha_inicio)) }} al {{ date('d/m/Y H:i', strtotime($evaluacion->fecha_fin)) }} 
                                @endif
                            </p>
                        </div> 

                        <div class="col-md-6">
                            {!! Form::label('type', trans('page.label.evaluacion.type')) !!}:
                            <p>{{ $evaluacion->type }}</p>              
                        </div>

                        @if($evaluacion->body != '')
                        <div class='col-md-12 evaluacion-body'>
                            {!! Form::label('body', trans('page.label.evaluacion.body')) !!}:
                            {!! $evaluacion->body !!}
                        </div>
                        @endif

                        @if(isset($evaluacion->files) && count($evaluacion->files)>0)
                        <div class='col-md-12'>
                            <strong>Archivos Adjuntos</strong>
                            <ul>
                                @foreach($evaluacion->files as $file)
                                    <li><div><a href='{!! $file->path !!}' target='blank'>{{ $file->name }}</a></div></li>
                                @endforeach
                            </ul>
                        </div>
                        @endif
                    </div>
                    
                    <div class='clearfix'></div>
                    <div class='col-md-6'>
                        <a href="{{ route('supervision.curso_materia', $evaluacion->curso_materia_id) }}" class="btn btn-default">{!! trans('page.button.back') !!}</a>
                    </div>
                    
                </div>
            </div>
        </div>

        
    </div>
    <div class="col-xs-12 col-md-6">
        <div class="box box-primary">
            <div class="box-body">
                <div class="row" style="padding-left: 20px">
                    <h4>{!! trans('page.model.plural.alumno') !!}</h4>
                    <div class="col-sm-12 table-responsive">
                        <table class="table datatable" id="alumno-evaluacion-table">
                            <thead>
                                <tr>
                                    <th>{!! trans('page.label.user.lastname') !!}, {!! trans('page.label.user.name') !!} </th>
                                    @if($evaluacion->type_id == 2)
                                    <th class='no-sort'>{!! trans('page.model.entrega') !!}</th>
                                    @endif
                                    <th class='no-sort'>{!! trans('page.model.calificacion') !!}</th>
                                </tr>
                            </thead>
                            <tbody>
                            @foreach($alumnos as $alumno)
                                <tr>
                                    <td>{{ $alumno->lastname }}, {{ $alumno->firstname }}</td>
                                    @if($evaluacion->type_id == 2)
                                        <td>
                                        @if($alumno->entrega($evaluacion->id))
                                            <a href="#" class='btn btn-default btn-xs' onclick='open_entrega_form(this)' data-url_redir='{{ route("supervision.show_entrega", [$evaluacion->id,$alumno->id]) }}'>Entregado <i class="glyphicon glyphicon-file"></i></a>
                                        @else
                                            {!! trans('page.label.evaluacion.no_entrega') !!}
                                        @endif
                                        </td>
                                    @endif
                                    <td>
                                    
                                    @if($alumno->calificacion($evaluacion->id))
                                        <a href="#" class='btn btn-default btn-xs'>{{ $alumno->calificacion($evaluacion->id)->nota }}</a>
                                    @else
                                        -
                                    @endif
                                        
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-xs-12 col-md-6 foro">
        <div class="panel panel-default">
            <div class="panel-body">
                @if(isset($comments) && count($comments) > 0)
                    @foreach($comments as $index => $comment)
                        <div class="fr-comment row">
                            <div class="col-sm-2 col-xs-12 fr-metadata  row">
                                <div class="col-sm-12 col-xs-2 fr-comment-number">#{{  count($comments) - $index }}</div>
                                <div class="col-sm-12 col-xs-10 fr-author">{{ $comment->user->lastname }}, {{ $comment->user->firstname }}</div>
                                <div class="col-sm-12 col-xs-6 fr-date">{{ date('d/m/Y', strtotime($comment->created_at)) }}</div>
                                <div class="col-sm-12 col-xs-6 fr-horu">{{ date('H:i:s', strtotime($comment->created_at)) }}</div>
                                
                                    <div class="col-sm-12  col-xs-4">
                                        
                                    </div>
                                
                            </div>
                            <div class="col-sm-10 col-xs-12 comment-body">
                                    {!! $comment->body !!}
                            </div>
                        </div>
                    @endforeach

                @else

                    <div class='col-md-12 text-center'>
                        No hay Comentarios
                    </div>

                @endif



            </div>
        </div>
    </div>
    </div>

    <script>
    function open_entrega_form(elem){
        $.ajax({
            url: $(elem).data('url_redir'),
            type : 'GET',
          success : function(data) {
            $('#ventana-modal').html(data);
            $('#ventana-modal').modal('show');                
          },

          // código a ejecutar si la petición falla;
           error : function(xhr, status) {
            console.log(status);
                alert('Disculpe, existió un problema');
            },
        });
    }
    </script>
@endsection