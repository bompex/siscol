@extends('adminlte::page_profesor')
@section('content_header')
    <h1 class="pull-left">
        Cursos de Años Anteriores
    </h1>
@endsection
@section('content')
    <div class="row">
        <div class="col-xs-12">
        <div class="clearfix"></div>

        @include('flash::message')
        {{ Form::open(['route' => 'profesor_curso_materia.history', 'method' => 'get', 'id'=>'filter-form']) }}
        <div class="box box-primary">
            <div class="box-body">
                <div class="filters">
                    <div>
                        <div class="col-r col-md-3 col-xs-12 input-filtro">
                            <label>Periodo:</label>
                            {{ Form::select('periodo', $periodos, isset($periodo) ? $periodo : null, [
                                'id' => 'periodo',
                                'class' => 'form-control'
                            ]) }}
                        </div>
                    </div>
                    <div class="clearfix"></div>        
                </div>
            </div>
        </div>
        {{ Form::close() }}

        <div class="clearfix"></div>
        <div class="box box-primary">
            <div class="box-body">
            <div class="table-responsive">
                <table class="table datatable" id="curso-table">
                    <thead>
                        <tr>

                            <th>{!! trans('page.model.materia') !!}</th>
                            <th>{!! trans('page.label.curso.name') !!}</th>
                            <th>{!! trans('page.label.curso.nivel') !!}</th>
                            <th>{!! trans('page.label.curso.turno') !!}</th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach($curso_materias as $cuMa)
                        
                            <tr>
                                <td><a href="{{ route('profesor_curso_materia.index',$cuMa->id) }}">{{ $cuMa->materia->name }}</a></td>
                                <td>{{ $cuMa->curso->name }}</td>
                                <td>{{ $cuMa->curso->nivel }}</td>
                                <td>{{ $cuMa->curso->turno }}</td>
                                
                                
                            </tr>
                        
                    @endforeach
                    </tbody>
                </table>
            </div>
            </div>
        </div>
        <div class="text-center">
        
        </div>
    </div>
</div>

<script type="text/javascript">
    $(function () {
        $("#periodo").change(function () {
            $('#filter-form').submit();
        });

    });

</script>  
@endsection

