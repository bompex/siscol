<div class="panel panel-info">
    <div class="panel-heading">{!! trans('page.model.plural.anuncio') !!}
      <a class="btn btn-danger btn-ssm pull-right"  href="{{ route('anuncio.create', $cursoMateria->id) }}">{!! trans('page.button.new') !!} {!! trans('page.model.anuncio') !!}</a>
    </div>
    <div class="panel-body">
      <ul class='foro-teaser'>
        @if(count($anuncios) >0)
          @foreach($anuncios as $anuncio)
            <li class='row'>

              <div class="col-md-12 fr-subject"><a class="open_message_modal"  href="javascript:;" data-href="{{ route('anuncio.show', [$anuncio->id]) }}">{{ $anuncio->subject }}</a></div>
              <div class='col-md-12 fr-info row'>
                <div class="col-sm-4 col-xs-6 fr-date">{{ date('d/m/Y H:i', strtotime($anuncio->fecha)) }}</div>
                <div class="clearfix"></div>
              </div>
              <div class="clearfix"></div>
            </li>   
          @endforeach
        @else
        <li  class='row'>
          <div class="col-md-12 fr-no-message">{!! trans('page.message.no_message') !!}</div>
          <div class="clearfix"></div>
        </li>  
        @endif    
      </ul>
      <div class='col-md-12  text-center'><a class="btn btn-primary btn-ssm text-center"  href="{{ route('anuncio.index', $cursoMateria->id) }}">Ver todas los {!! trans('page.model.plural.anuncio') !!}</a></div>
    </div>
</div>
