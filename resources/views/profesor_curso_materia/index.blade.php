@extends('adminlte::page_profesor')

@section('content_header')

<h1 class="pull-left">{!! trans('page.model.curso') !!}: {{ $cursoMateria->curso->name }} - {{ $cursoMateria->materia->name }} </h1>
<h1 class="pull-right">
   <a class="btn btn-primary btn-sm" style="margin-top: -10px;margin-bottom: 5px" href="{{ route('home.profesor') }}">{!! trans('page.button.back') !!}</a>
</h1>

@endsection

@section('content')
<div class="clearfix"></div>
    @include('flash::message')
<div class="row">
    <div class="col-md-6 col-xs-12">
        @include('profesor_curso_materia.foro')

        @include('profesor_curso_materia.evaluacion')
    </div>
    <div class="col-md-6 col-xs-12">
    	@include('profesor_curso_materia.horario')
        @include('profesor_curso_materia.anuncio')

    	@include('profesor_curso_materia.alumnos')
    
    </div>
    <div class="clearfix"></div>
</div>
@endsection