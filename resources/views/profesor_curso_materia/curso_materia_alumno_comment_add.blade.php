@extends('layouts.modal')
@section('modal-title')
	<h4>Agregar Observación</h4>
@endsection
@section('modal-content')
	{!! Form::open(['route' => ['profesor_curso_materia.store_alumno_note', $cursoMateria->id,$alumno->id]]) !!}
	<div class="modal-body">
		<div class="row">
			<div class="col-md-6">
				<label>Curso:</label>	
				<p>{{ $cursoMateria->curso->name }}</p>
			</div>
			<div class="col-md-6">
				<label>Materia:</label>	
				<p>{{ $cursoMateria->materia->name }}</p>
			</div>
			<div class="col-md-6">
				<label>Alumno:</label>	
				<p>{{ $alumno->lastname }}, {{ $alumno->firstname }}</p>
			</div>
		</div>
		<div class="row">
			<div class="form-group col-md-12">
			    {!! Form::label('body', "Comentario") !!}
			    {!! Form::textarea('body', $CursoMateriaAlumnoComment->body, ['class' => 'form-control', 'id'=>'cmh_note']) !!}
			</div>
		</div>
	</div>
	<div class="modal-footer">
	    <input type="submit" class="btn btn-primary" value="Guardar">
	    <a href="#" class="btn btn-default" data-dismiss="modal">{!! trans('page.button.cancel') !!}</a>
	</div>
	{!! Form::close() !!}


@endsection