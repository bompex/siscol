<div class="panel panel-info">
    <div class="panel-heading">{!! trans('page.model.plural.topic') !!}
    	<a class="btn btn-danger btn-ssm pull-right"  href="{{ route('profesor_curso_materia.create_topic', $cursoMateria->id) }}">{!! trans('page.button.new') !!} {!! trans('page.model.topic') !!}</a>
    </div>
    <div class="panel-body">
    	<ul class='foro-teaser'>
    		@if(count($topics) >0)
    			@foreach($topics as $topic)
		  			<li class='row'>
		  				<div class="col-md-12 fr-subject"><a href="{{ route('curso_materia_foro.show_topic', $topic->id) }}">{{ $topic->subject }}</a></div>
		  				<div class='col-md-12 fr-info row'>
                <div class="col-sm-4 col-xs-12 fr-author">Por: {{ $topic->user->lastname }}, {{ $topic->user->firstname }}</div>
		  					<div class="col-sm-4 col-xs-6 fr-comments"><span><i class="glyphicon glyphicon-comment"></i>&nbsp; {{ count($topic->comments)}}</span></div>
			  				<div class="col-sm-4 col-xs-6 fr-date">{{ date('d/m/Y H:i:s', strtotime($topic->created_at)) }}</div>
			  				<div class="clearfix"></div>
			  			</div>
		  				<div class="clearfix"></div>
		  			</li>  	
  				@endforeach
  			@else
  			<li  class='row'>
  				<div class="col-md-12 fr-no-message">{!! trans('page.message.no_message') !!}</div>
  				<div class="clearfix"></div>
  			</li>  
  			@endif		
    	</ul>
      <div class='col-md-12  text-center'><a class="btn btn-primary btn-ssm text-center"  href="{{ route('curso_materia_foro.index', $cursoMateria->id) }}">Ver todas los {!! trans('page.model.plural.topic') !!}</a></div>
    </div>
</div>



