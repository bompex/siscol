@extends('adminlte::page_profesor')
@section('content_header')
<h1 class="pull-left">{!! trans('page.model.curso') !!}: {{ $cursoMateria->curso->name }} - {{ $cursoMateria->materia->name }} </h1>
<h1 class="pull-right">
   <a class="btn btn-primary btn-sm"  href="{{ route('profesor_curso_materia.index',$cursoMateria->id) }}">{!! trans('page.button.back') !!}</a>
</h1>
@endsection
@section('content')
    <div class="row">
        <div class="col-xs-12">
        @include('adminlte-templates::common.errors')
        <div class="box box-primary">
            <div class="box-body">
                <div class="row">
                    {!! Form::open(['route' => ['profesor_curso_materia.update_topic',$cursoMateria->id, $topic->id] , 'method' => 'patch']) !!}

                        @include('profesor_curso_materia.topic_fields')

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection