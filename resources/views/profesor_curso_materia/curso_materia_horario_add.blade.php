@extends('layouts.modal')
@section('modal-title')
	<h4>Agregar Nota</h4>
@endsection
@section('modal-content')
	{!! Form::open(['route' => ['profesor_curso_materia.store_horario_note', $cursoMateriaHorario->id]]) !!}
	<div class="modal-body">
		<div class="row">
			<div class="col-md-6">
				<label>Curso:</label>	
				<p>{{ $cursoMateriaHorario->cursoMateria->curso->name }}</p>
			</div>
			<div class="col-md-6">
				<label>Materia:</label>	
				<p>{{ $cursoMateriaHorario->cursoMateria->materia->name }}</p>
			</div>
			<div class="col-md-6">
				<label>Dia:</label>	
				<p>{{ $cursoMateriaHorario->dianame }}</p>
			</div>
			<div class="col-md-6">
				<label>Horario:</label>	
				<p>de {{ date('H:i',strtotime($cursoMateriaHorario->hora_inicio)) }} a {{ date('H:i',strtotime($cursoMateriaHorario->hora_fin)) }}</p>
			</div>
		</div>
		<div class="row">
			<div class="form-group col-md-12">
			    {!! Form::label('note', "Nota") !!}
			    {!! Form::textarea('note', $cursoMateriaHorario->note, ['class' => 'form-control', 'id'=>'cmh_note']) !!}
			</div>
		</div>
	</div>
	<div class="modal-footer">
	    <input type="submit" class="btn btn-primary" value="Guardar">
	    <a href="#" class="btn btn-default" data-dismiss="modal">{!! trans('page.button.cancel') !!}</a>
	</div>
	{!! Form::close() !!}


@endsection

