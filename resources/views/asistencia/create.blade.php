@extends('layouts.app')
@section('content_header')
    <h1 class="pull-left">
            {!! trans('page.model.plural.asistencia') !!}: {!! $curso->name!!}
    </h1>
@endsection
@section('content')
    <div class="row">
        <div class="col-xs-12">
        <div class="clearfix"></div>

        @include('flash::message')

        <div class="clearfix"></div>
        <div class="box box-primary">
            <div class="box-body">
                {!! Form::open(['route' => ['asistencia.store', $curso->id]]) !!}

                    @include('asistencia.fields')

                {!! Form::close() !!}
                
            </div>
        </div>
        <div class="text-center">
        
        </div>
    </div>
</div>
@endsection