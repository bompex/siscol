@extends('layouts.app')
@section('content_header')
    <h1 class="pull-left">
            {!! trans('page.model.plural.asistencia') !!}
    </h1>
@endsection
@section('content')
    <div class="row">
        <div class="col-xs-12">
        <div class="clearfix"></div>

        @include('flash::message')

        <div class="clearfix"></div>
        <div class="box box-primary">
            <div class="box-body">
                @if($cursos)
                    @foreach($cursos as $curso)
                    <div class="col-md-6 {{ ($curso->asistenciaHoy)? 'has-asist' : '' }}">
                        {!! $curso->name !!}
                         <a class="btn btn-default pull-right" href="{{ route('asistencia.list',$curso->id) }}"><i class="glyphicon glyphicon-list"></i></a>
                         @if(!$curso->asistenciaHoy)
                         <a class="btn btn-default pull-right" href="{{ route('asistencia.create',$curso->id) }}"><i class="glyphicon glyphicon-edit"></i></a>
                         @else
                         <a class="btn btn-default pull-right" href="{{ route('asistencia.edit',$curso->asistenciaHoy->id) }}"><i class="glyphicon glyphicon-edit"></i></a>
                         @endif
                    </div>
                    @endforeach
                @endif
            </div>
        </div>
        <div class="text-center">
        
        </div>
    </div>
</div>
@endsection