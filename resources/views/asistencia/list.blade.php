@extends('layouts.app')
@section('content_header')
    <h1 class="pull-left">
            {!! trans('page.model.plural.asistencia') !!}: {!! $curso->name!!}
    </h1>
    <div class="pull-right">

            <a class="btn btn-primary btn-sm" href="{{ route('asistencia.index',['nivel'=>$curso->nivel]) }}">{!! trans('page.button.back') !!}</a>
          
        </div>
@endsection
@section('content')
    <div class="row">
        <div class="col-xs-12">
        <div class="clearfix"></div>

        @include('flash::message')

        <div class="clearfix"></div>
        <div class="box box-primary">
            <div class="box-body">
                @include('asistencia.table')
            </div>
        </div>
        <div class="text-center">
        
        </div>
    </div>
</div>
@endsection