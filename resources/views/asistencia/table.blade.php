<div class="table-responsive">
    <table class="table datatable" id="asistencia-table">
        <thead>
            <tr>
                <th>fecha</th>
                <th>día</th>
                <th class='no-sort'>&nbsp;</th>
            </tr>
        </thead>
        <tbody>
        @foreach($asistencias as $asistencia)
            <tr>
                <td>{{ date('d/m/Y H:i', strtotime($asistencia->fecha)) }}</td>
                <td>{{ date('l', strtotime($asistencia->fecha)) }}</td>
                <td>
                    <div class='btn-group'>
                        <a href="{{ route('asistencia.show', [$asistencia->id]) }}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                        <a href="{{ route('asistencia.edit', [$asistencia->id]) }}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    </div>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
