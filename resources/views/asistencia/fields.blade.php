<!-- Name Field -->
<div class="form-group col-sm-4 col-xs-12">
    {!! Form::label('fecha', 'Fecha:') !!}
    @if($asistencia->fecha)
    	<span>date('l d/m/Y',strtotime($asistencia->fecha))</span>
    	{{ Form::hidden('fecha', $asistencia->fecha) }}
    @else
    	{!! Form::date('fecha', date('Y-m-d',time()) , ['class' => 'form-control','id'=>'fecha']) !!}
    @endif
</div>
<div class="clearfix"></div>
<div class="form-group col-sm-12  col-xs-12">
    {!! Form::label('observacion', 'Observación:') !!}
    {!! Form::textarea('observacion', $asistencia->observacion, ['class' => 'form-control']) !!}
</div>
<div class="clearfix"></div>

<div class="form-group col-sm-12  col-xs-12">
<div class="table-responsive">
    <table class="table" id="asistencia-alumnos-table">
        <thead>
            <tr>
                <th>Nombre</th>
                <th class='no-sort'>Presente</th>
            </tr>
        </thead>
        <tbody>
        @foreach($alumnos as $alumno)
            <tr>
                <td>{{ $alumno->lastname }}, {{ $alumno->firstname }}</td>
                <td>
                    {!! Form::checkbox('presente['.$alumno->id.']', '1', false) !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
</div>

<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('asistencia.list',$curso->id) }}" class="btn btn-default">{!! trans('page.button.cancel') !!}</a>
</div>