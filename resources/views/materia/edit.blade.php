@extends('layouts.app')
@section('content_header')
    <h1>
        @if($preset_nivel === false)
            {!! trans('page.model.materia') !!}
        @else
            {!! trans('page.model.materia'.$preset_nivel) !!}
        @endif
    </h1>
@endsection
@section('content')
   <div class="row">
        <div class="col-xs-12">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($materia, ['route' => ['materia.update', $materia->id], 'method' => 'patch']) !!}

                        @include('materia.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
 </div>
@endsection