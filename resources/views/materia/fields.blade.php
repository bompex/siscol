<!-- Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('name', 'Name:') !!}
    {!! Form::text('name', null, ['class' => 'form-control']) !!}
</div>
<div class='clearfix'></div>
<div class="form-group col-sm-2">
    {!! Form::label('gender', trans('page.label.user.gender')) !!}
    {{ Form::select('gender', $gender, $materia->gender ? $materia->gender : 'A', ['class' => 'form-control', 'id'=>'gender']) }}
</div>
<div class='clearfix'></div>
<div class="form-group col-sm-2">
    {!! Form::label('color', 'Color:') !!}
    {!! Form::color('color', null, ['class' => 'form-control']) !!}
</div>

{{ Form::hidden('primario', $preset_nivel) }}

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('materia.index',['nivel'=>$nivel[$preset_nivel]]) }}" class="btn btn-default">Cancel</a>
</div>