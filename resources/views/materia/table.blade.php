<div class="table-responsive">
    <table class="table datatable" id="materia-table">
        <thead>
            <tr>
                <th>{!! trans('page.label.materia.name') !!}</th>
                <th>{!! trans('page.label.user.gender') !!}</th>
                <th>{!! trans('page.label.materia.color') !!}</th>
                <th class='no-sort'>&nbsp;</th>
            </tr>
        </thead>
        <tbody>
        @foreach($materias as $materia)
            <tr>
                <td>{{ $materia->name }}</td>
                <td>{{ $gender[$materia->gender] }}</td>
                <td ><span style="width:20px; height:20px; display:block; background-color:{{ $materia->color }}">&nbsp;</span></td>
                <td>
                    {!! Form::open(['route' => ['materia.destroy', $materia->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{{ route('materia.edit', [$materia->id]) }}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                        {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
