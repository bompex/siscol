@extends('layouts.app')
@section('content_header')
        <h1 class="pull-left">
        @if($nivel === false)
            {!! trans('page.model.plural.materia') !!}
        @else
            {!! trans('page.model.plural.materia'.$nivel) !!}
        @endif
        </h1>
        <h1 class="pull-right">
           <a class="btn btn-primary pull-right"  href="{{ route('materia.create',['nivel'=>$nivel]) }}">{!! trans('page.button.add') !!} {!! trans('page.button.new') !!}</a>
        </h1>
@endsection
@section('content')
    <div class="row">
        <div class="col-xs-12">
        <div class="clearfix"></div>

        @include('flash::message')

        <div class="clearfix"></div>
        <div class="box box-primary">
            <div class="box-body">
                    @include('materia.table')
            </div>
        </div>
        <div class="text-center">
        
        </div>
    </div>
</div>
@endsection

