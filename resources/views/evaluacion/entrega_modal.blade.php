@extends('layouts.modal')
@section('modal-title')
	<h4>Entrega de Trabajo Práctico</h4>
@endsection
@section('modal-content')
	{!! Form::open(['route' => ['evaluacion_alumno.store_entrega', $evaluacion->id], 'onsubmit' => '' ]) !!}
	<div class="modal-body">
		<div class="row">
			<div class="col-md-6">
				<label>Curso:</label>	
				<p>{{ $evaluacion->cursoMateria->curso->name }}</p>
			</div>
			<div class="col-md-6">
				<label>Materia:</label>	
				<p>{{ $evaluacion->cursoMateria->materia->name }}</p>
			</div>
			<div class="col-md-12">
				<label>Tema:</label>	
				<p>{{ $evaluacion->subject }} ({{ $evaluacion->type }})</p>
			</div>
		</div>
		<div class="row">
			<div class="form-group col-sm-12">
			    {!! Form::label('observacion', trans('page.label.calificacion.observacion')) !!}
			    {!! Form::textarea('observacion', $entrega->observacion, ['class' => 'form-control', 'id'=>'observacion']) !!}
			</div>
			<div class="clearfix"></div>
			<div class="form-group col-md-12">
				<label>Archivo Adjunto:</label>	
			    <div class="file-list">
			        @if(isset($entrega->file))
			            <div class='file-row'><div><a href='{!! $entrega->file->path !!}' target='blank'>{{ $entrega->file->name }}</a></div><input type='hidden' name='file_id' value='{{ $entrega->file->id }}' class='input-file' /></div>
			        @endif
			        
			    </div>
			</div>
			<div class="clearfix"></div>
			<div class="form-group col-sm-6">
			    <label for="file_uploader" class="btn btn-default custom-file-upload input-font">Subir Archivo</label>
			    <div class="progress progress-xs hidden">
				     <div class="progress-bar progress-bar-danger" style="width: 0%"></div>
				   </div>
			    {{ Form::file('file_uploader',['class' => 'form-control file_uploader hidden', 'id' => 'file_uploader', 'name'=>'file_uploader']) }}
			    <span id='upload_file_message' class="invalid-feedback error"><strong></strong></span>   
			</div>
		</div>
	</div>
	<div class="modal-footer">
	    <input type="submit" class="btn btn-primary" value="Guardar">
	    <a href="#" class="btn btn-default" data-dismiss="modal">{!! trans('page.button.cancel') !!}</a>
	</div>
	{!! Form::close() !!}


@endsection


