@extends('adminlte::page_profesor')
@section('content_header')
        <h1 class="pull-left">{!! trans('page.model.plural.evaluacion'.$cursoMateria->curso->primario) !!}</h1>
        <div class="pull-right">
            <a class="btn btn-primary btn-sm pull-right " href="{{ route('profesor_curso_materia.index',$cursoMateria->id) }}">{!! trans('page.button.back') !!}</a>
            <div class="dropdown boton-dropdown pull-right" style="margin-right: 10px;">
                <button class="btn btn-success  btn-sm dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  {!! trans('page.button.add') !!} {!! trans('page.button.new') !!}
                </button>
                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                  <a class="dropdown-item" href="{{ route('evaluacion.create', ['tp',$cursoMateria->id]) }}">Trabajo Práctico</a>
                  <a class="dropdown-item" href="{{ route('evaluacion.create', ['eval',$cursoMateria->id]) }}">Evaluación</a>
                </div>
            </div>
            

        
           
        </div>
@endsection
@section('content')
    <div class="row">
        <div class="col-xs-12">
        <div class="clearfix"></div>

        @include('flash::message')

        <div class="clearfix"></div>
        <div class="box box-primary">
            <div class="box-body">
                    @include('evaluacion.table')
            </div>
        </div>
        <div class="text-center">
        
        </div>
    </div>
</div>
@endsection

