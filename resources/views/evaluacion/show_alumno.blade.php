@extends('adminlte::page_alumno')
@section('content')
    <div class="row">
        <div class="col-xs-12">
        <div class="box box-primary">
            <div class="box-body">
                <div class="row" style="padding-left: 20px">
                    
                    <div class="col-md-6">
                            
                        <div class="col-md-12">
                            {!! Form::label('type', trans('page.label.evaluacion.type')) !!}:
                            <p>{{ $evaluacion->type }}</p>              
                        </div>

                        <div class='col-md-12 evaluacion-subject'>
                            {!! Form::label('subject', 'Tema:') !!}
                            <p>{{ $evaluacion->subject }}</p>
                        </div>
                        <div class='col-md-12 evaluacion-fecha'>
                            {!! Form::label('date', trans('page.label.evaluacion.'.$evaluacion->type_id.'_date')) !!}:
                            <p>
                                @if( date('Y-m-d', strtotime($evaluacion->fecha_inicio)) == date('Y-m-d', strtotime($evaluacion->fecha_fin)))
                                    {{ date('d/m/Y', strtotime($evaluacion->fecha_inicio)) }} de {{ date('H:i', strtotime($evaluacion->fecha_inicio)) }} a {{ date('H:i', strtotime($evaluacion->fecha_fin)) }} 
                                @else
                                    {{ date('d/m/Y H:i', strtotime($evaluacion->fecha_inicio)) }} al {{ date('d/m/Y H:i', strtotime($evaluacion->fecha_fin)) }} 
                                @endif
                            </p>
                        </div>                     

                        @if($evaluacion->body != '')
                        <div class='col-md-12 evaluacion-body'>
                            {!! Form::label('body', trans('page.label.evaluacion.body')) !!}:
                            {!! $evaluacion->body !!}
                        </div>
                        @endif

                        @if(isset($evaluacion->files) && count($evaluacion->files)>0)
                        <div class='col-md-12'>
                            <strong>Archivos Adjuntos</strong>
                            <ul>
                                @foreach($evaluacion->files as $file)
                                    <li><div><a href='{!! $file->path !!}' target='blank'>{{ $file->name }}</a></div></li>
                                @endforeach
                            </ul>
                        </div>
                        @endif

                        
                    </div>
                    
                    <div class='col-md-6'>
                        @if($evaluacion->type_id == 2 && (strtotime($evaluacion->fecha_inicio) <= time()|| !$evaluacion->fecha_inicio) )
                            @if($entregas = $alumno->entrega($evaluacion->id))
                                @foreach($entregas as $index => $entrega)

                                @php
                                $file_alumno = false;
                                $file_profesor = false;

                                if($entrega->files){
                                    foreach($entrega->files as $file){
                                        if($file->pivot->devolucion == 0){
                                            $file_alumno[] = $file;
                                        }else{
                                            $file_profesor[] = $file;
                                        }
                                    }
                                }
                                @endphp
                                    <div class='col-md-12'>
                                        <div class="panel {{ ($entrega->status == 2)? 'panel-default' : 'panel-info' }}">
                                            <div class="panel-heading">{{ ($entrega->status == 2)? 'Correcciones' : 'Entrega' }}
                                            </div>
                                            <div class="panel-body">
                                                
                                                <div class='col-md-12'>
                                                    <label>Fecha Entrega</label>
                                                    <p>{{ date('d/m/Y H:i', strtotime($entrega->created_at)) }}</p>
                                                </div>
                                                @if($entrega->observacion)
                                                <div class='col-md-12'>
                                                    <label>Comentario </label>
                                                    <div>{!! $entrega->observacion !!}</div>
                                                </div>
                                                @endif
                                                @if($file_alumno)
                                                <div class='col-md-12'>
                                                    <label>Archivo Adjunto</label>
                                                    @foreach($file_alumno as $file)
                                                    <p><a href='{!! $file->path !!}' target='blank'>{{ $file->name }}</a></p>
                                                    @endforeach
                                                </div>
                                                @endif
                                                
                                                @if($entrega->profesor_observacion)
                                                <div class='col-md-12'>
                                                    <label>Observacion del Profesor</label>
                                                    <div>{!! $entrega->profesor_observacion !!}</div>
                                                </div>
                                                @endif
                                                @if($file_profesor)
                                                <div class='col-md-12'>
                                                    <label>Archivo de Observaciones</label>
                                                    @foreach($file_profesor as $file)
                                                    <p><a href='{!! $file->path !!}' target='blank'>{{ $file->name }}</a></p>
                                                    @endforeach
                                                </div>
                                                @endif
                                                
                                                @if(!isset($entregas[$index+1]) && $entrega->status == 2)
                                                    <a href="#" data-url_redir='{{ route("evaluacion_alumno.create_entrega", [$evaluacion->id]) }}' class="btn btn-success open_entrega_form">Subir Trabajo Práctico <i class="fa fa-cloud-upload" aria-hidden="true" style="font-size: 20px;margin-left: 5px;"></i></a> 
                                                @endif
                                                   
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                                
                            @else
                                <div class='col-md-12'> 
                                    <div class="panel panel-info">
                                        <div class="panel-heading">Entrega
                                        </div>
                                        <div class="panel-body">                              
                                            <a href="#" data-url_redir='{{ route("evaluacion_alumno.create_entrega", [$evaluacion->id]) }}' class="btn btn-success open_entrega_form">Subir Trabajo Práctico <i class="fa fa-cloud-upload" aria-hidden="true" style="font-size: 20px;margin-left: 5px;"></i></a> .
                                        </div>
                                    </div>
                                </div>
                                
                            @endif
                        @endif

                        @if($alumno->calificacion($evaluacion->id,true))
                            <div class='clearfix'></div>
                            <div class='col-md-3'> 
                                <div class="panel {{ ($alumno->calificacion($evaluacion->id)->nota >= 6)? 'panel-success' : '' }} {{ ($alumno->calificacion($evaluacion->id)->nota < 6 && $alumno->calificacion($evaluacion->id)->nota > 4)? 'panel-warning' : '' }} {{ ($alumno->calificacion($evaluacion->id)->nota <= 4)? 'panel-danger' : '' }}">
                                    <div class="panel-heading">NOTA:
                                    </div>
                                    <div class="panel-body">                              
                                        <div style="font-size: 24px; text-align: center;">{{ Calif::mostrar_calificacion($alumno->calificacion($evaluacion->id,true)->nota,$evaluacion->cursoMateria->curso->primario) }}</div>
                                    </div>
                                </div>
                            </div>
                        @endif

                    </div>
                    
                    <div class='clearfix'></div>
                    <div class='col-md-6'>
                         <a class="btn btn-success open_message_modal"  href="javascript:;" data-href="{{ route('message.show', [$evaluacion->profesor->user->id, 'mode' => 'modal','reply' => 'evaluacion','reply_id' => $evaluacion->id]) }}"  title="Enviar Mensaje"><span class="hidden-xs">Enviar Mensaje</span><span class="visible-xs"><i class="glyphicon glyphicon-comment"></i></span></a>

                        <a href="{{ route('home_alumno.index_materia',$evaluacion->curso_materia_id) }}" class="btn btn-primary">{!! trans('page.button.back') !!}</a>
                       
                    </div>
                   
                    
                </div>
            </div>
        </div>
        </div>
        <div class="col-xs-12 foro">
            @if($evaluacion->allow_comments == 1)
              @include('evaluacion.comment_form')
            @endif
            @include('evaluacion.comment')
        </div>
    </div>
@endsection

@section('adminlte_js')
<script>
var file_upload_url  = "{{ route('file.upload') }}";

function initUploadConfig() {
    config = {
        type: 'POST',
        url: file_upload_url,
        dataType: 'json',

        add: function (e, data) {
            $('.progress .progress-bar').css('width', '0%');
            $('.progress').removeClass('hidden');
            data.submit();
        },
        done: function (e, data) {

            if(data.result.error){
                $('#upload_file_message').html(data.result.error);
            }else{
                var file_row = "<div class='file-row'><div><a href='"+data.result.path+"' target='blank'>"+data.result.name+"</a></div><input type='hidden' name='files[]' value='"+data.result.file_id+"' class='input-file' /><a href='#' onclick='delete_file(this)'><i class='glyphicon glyphicon-trash'></i></a></div>"
                $('.file-list').append(file_row);
            }
            $('.progress').addClass('hidden');
        },
        progressall: function (e, data) {
            $('#upload_file_message').html('');
            var progress = parseInt(data.loaded / data.total * 100, 10);
            $('.progress .progress-bar').css('width', progress + '%');
        }
    };
    return config;
}

function delete_file(elem){
    var row = $(elem).parents('.file-row');
    row.hide();
    row.find('.input-file').attr('name', 'files_del[]');
}

$(function () {
    $('.open_entrega_form').click(function(){
        $.ajax({
            url: $(this).data('url_redir'),
            type : 'GET',
          success : function(data) {
            $('#ventana-modal').html(data);
            $('#ventana-modal').modal('show');    
            CKEDITOR.replace( 'observacion',{plugins:'emoji,basicstyles,link,wysiwygarea,toolbar,image,colorbutton,list,resize',resize_dir: 'vertical'});
            $('.file_uploader').fileupload(initUploadConfig()).prop('disabled', !$.support.fileInput)
            .parent().addClass($.support.fileInput ? undefined : 'disabled');           
          },

          // código a ejecutar si la petición falla;
           error : function(xhr, status) {
            console.log(status);
                alert('Disculpe, existió un problema');
            },
        });
    });
});
</script>
@endsection