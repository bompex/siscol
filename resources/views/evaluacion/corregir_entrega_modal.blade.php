@extends('layouts.modal')
@section('modal-title')
	<h4>Corrección Entrega de Trabajo Práctico</h4>
@endsection
@section('modal-content')

    {!! Form::open(['route' => ['evaluacion.update_corregir_entrega', $entrega->id], 'onsubmit' => '' ]) !!}
	<div class="modal-body">
		<div class="row">
			<div class="col-md-6">
				<label>Curso:</label>	
				<p>{{ $evaluacion->cursoMateria->curso->name }}</p>
			</div>
			<div class="col-md-6">
				<label>Materia:</label>	
				<p>{{ $evaluacion->cursoMateria->materia->name }}</p>
			</div>
			<div class="col-md-12">
				<label>Tema:</label>	
				<p>{{ $evaluacion->subject }} ({{ $evaluacion->type }})</p>
			</div>
		</div>

        
        <div class="panel {{ ($entrega->status == 2)? 'panel-default' : 'panel-info' }}">
            <div class="panel-body">
    			<div class='col-md-12'>
                    <label>Fecha Entrega</label>
                    <p>{{ date('d/m/Y H:i', strtotime($entrega->created_at)) }}</p>
                </div>
                @if($file_alumno)
                <div class='col-md-12'>
                    <label>Archivo Adjunto</label>
                    @foreach($file_alumno as $file)
                    <p><a href='{!! $file->path !!}' target='blank'>{{ $file->name }}</a></p>
                    @endforeach
                </div>
                @endif
                @if($entrega->observacion)
                <div class='col-md-12'>
                    <label>Comentario</label>
                    <div>{!! $entrega->observacion !!}</div>
                </div>
                @endif
                <br />
                <div class='col-md-12'>
                    {!! Form::label('profesor_observacion', 'Observación:') !!}
                    {!! Form::textarea('profesor_observacion', $entrega->profesor_observacion, ['class' => 'form-control', 'id'=>'profesor_observacion']) !!}
                </div>
                <div class="col-md-12">
                    <label>Archivo de Observaciones:</label> 
                    <div class="file-list">
                        @if($file_profesor)
                            @foreach($file_profesor as $file)
                            <div class='file-row'><div><a href='{!! $file->path !!}' target='blank'>{{ $file->name }}</a></div><input type='hidden' name='files_id[]' value='{{ $file->id }}' class='input-file' /><a href='#' onclick='delete_file(this)'><i class='glyphicon glyphicon-trash'></i></a></div>
                            @endforeach
                        @endif
                        
                    </div>
                </div>
                <div class="clearfix"></div>
                <div class="col-sm-6">
                    <label for="file_uploader" class="btn btn-default custom-file-upload input-font">Subir Archivo</label>
                    <div class="progress progress-xs hidden">
                         <div class="progress-bar progress-bar-danger" style="width: 0%"></div>
                       </div>
                    {{ Form::file('file_uploader',['class' => 'form-control file_uploader hidden', 'id' => 'file_uploader', 'name'=>'file_uploader']) }}
                    <span id='upload_file_message' class="invalid-feedback error"><strong></strong></span>   
                </div>
                <div class="clearfix"></div>
                <div class='col-xs-4'>
                    {!! Form::label('status', 'Permitir Nueva Entrega:') !!}
                    {!! Form::select('status', ['2'=>'Si', '1'=>'No'], '2', ['class' => 'form-control']) !!}
                </div>
    		</div>
        </div>
        
	</div>
	<div class="modal-footer">
        {!! Form::submit(trans('page.button.save'), ['class' => 'btn btn-primary']) !!}
	    <a href="javascript:;" class='btn btn-default' onclick='open_calificacion_form(this)' data-url_redir='{{ route("evaluacion.show_entrega", [$evaluacion->id,$alumno->id]) }}'>
            {!! trans('page.button.cancel') !!}
        </a>
	</div>

    {!! Form::close() !!}
@endsection

