@extends('layouts.modal')
@section('modal-title')
	<h4>Entrega de Trabajo Práctico</h4>
@endsection
@section('modal-content')
	<div class="modal-body">
		<div class="row">
			<div class="col-md-6">
				<label>Curso:</label>	
				<p>{{ $evaluacion->cursoMateria->curso->name }}</p>
			</div>
			<div class="col-md-6">
				<label>Materia:</label>	
				<p>{{ $evaluacion->cursoMateria->materia->name }}</p>
			</div>
			<div class="col-md-12">
				<label>Tema:</label>	
				<p>{{ $evaluacion->subject }} ({{ $evaluacion->type }})</p>
			</div>
		</div>

        @if($entregas)
            @foreach($entregas as $index => $entrega)
            @php
            $file_alumno = false;
            $file_profesor = false;

            if($entrega->files){
                foreach($entrega->files as $file){
                    if($file->pivot->devolucion == 0){
                        $file_alumno[] = $file;
                    }else{
                        $file_profesor[] = $file;
                    }
                }
            }
            @endphp
            <div class="panel {{ ($entrega->status == 2)? 'panel-default' : 'panel-info' }}">
                <div class="panel-body">
        			<div class='col-md-12'>
                        <label>Fecha Entrega</label>
                        <p>{{ date('d/m/Y H:i', strtotime($entrega->created_at)) }}</p>
                    </div>
                    @if($entrega->observacion)
                    <div class='col-md-12'>
                        <label>Comentario</label>
                        <div>{!! $entrega->observacion !!}</div>
                    </div>
                    @endif
                    @if($file_alumno)
                    <div class='col-md-12'>
                        <label>Archivo Adjunto</label>
                        @foreach($file_alumno as $file)
                        <p><a href='{!! $file->path !!}' target='blank'>{{ $file->name }}</a></p>
                        @endforeach
                    </div>
                    @endif
                    @if($entrega->profesor_observacion)
                    <div class='col-md-12'>
                        <label>Observaciones</label>
                        <div>{!! $entrega->profesor_observacion !!}</div>
                    </div>
                    @endif
                    @if($file_profesor)
                    <div class='col-md-12'>
                        <label>Archivo de Observaciones</label>
                        @foreach($file_profesor as $file)
                        <p><a href='{!! $file->path !!}' target='blank'>{{ $file->name }}</a></p>
                        @endforeach
                    </div>
                    @endif
                    @if($entrega->status == 1)
                    <div class='col-md-12'>
                        <a href="javascript:;" class='btn btn-default' onclick='open_correccion_form(this)' data-url_redir='{{ route("evaluacion.corregir_entrega", [$entrega->id]) }}'>
                            Agregar Correción
                        </a>
                    </div>
                    @endif
                    @if($entrega->status == 2 && !isset($entregas[$index+1]))
                    <div class='col-md-12'>
                        <a href="javascript:;" class='btn btn-default' onclick='open_correccion_form(this)' data-url_redir='{{ route("evaluacion.corregir_entrega", [$entrega->id]) }}'>
                            Modificar Correción
                        </a>
                    </div>
                    @endif
        		</div>
            </div>

            @endforeach
        @endif

	</div>
	<div class="modal-footer">
	    <a href="javascript:;" class='btn btn-default' onclick='open_calificacion_form(this)' data-url_redir='{{ route("calificacion.create", [$evaluacion->id, $alumno->id]) }}'>
            {!! trans('page.label.evaluacion.calificar') !!}<i class="glyphicon glyphicon-copy"></i>
        </a>
	    <a href="#" class="btn btn-default" data-dismiss="modal">{!! trans('page.button.cancel') !!}</a>
	</div>

@endsection

<script>

function open_calificacion_form(elem){
    $.ajax({
        url: $(elem).data('url_redir'),
        type : 'GET',
      success : function(data) {
        $('#ventana-modal').html(data);
        $('#ventana-modal').modal('show');                
      },

      // código a ejecutar si la petición falla;
       error : function(xhr, status) {
        console.log(status);
            alert('Disculpe, existió un problema');
        },
    });
}

</script>
