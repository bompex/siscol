<div class="panel panel-default">
    <div class="panel-body">
	    {!! Form::open(['route' => ['evaluacion.store_comment', $evaluacion->id]]) !!}
		<div class='row'>
			<div class="form-group col-md-12">
				<label>Nuevo Comentario</label>
			    {!! Form::textarea('body', '', ['class' => 'form-control']) !!}
			</div>
			<div class="form-group col-md-12">
			    {!! Form::submit(trans('page.button.publish'), ['class' => 'btn btn-primary']) !!}
			</div>
		</div>
		{!! Form::close() !!}
	</div>
</div>

<script>
	$(function () {
            // extraPlugins needs to be set too.
            var editor = CKEDITOR.replace( 'body',{extraPlugins:'div,font',removePlugins: '' ,language: 'es',height:100,resize_dir: 'vertical',removeButtons:'',filebrowserBrowseUrl: '',
            	
	        });

	        CKFinder.setupCKEditor( editor, null, { type: 'Files'} );

	        $('.citar_message_btn').click(function(e){
	        	var body = $(this).parents('.fr-comment').find('.comment-body').html();
	        	var name = $(this).parents('.fr-comment').find('.fr-author').html();
	        	var quote = '<div style="background-color:#e0e0e0; font-size:11px; padding:10px; font-style: italic;" class="quote-message">Cita de: '+name+'<br />'+body+'</div><hr/><p></p>';
	        	CKEDITOR.instances.body.setData(quote);
	        });

        });
</script>