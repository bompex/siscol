<h4>{!! trans('page.model.plural.alumno') !!}</h4>
<div class="col-sm-12 table-responsive">
    <table class="table datatable" id="alumno-evaluacion-table">
        <thead>
            <tr>
                <th>{!! trans('page.label.user.lastname') !!}, {!! trans('page.label.user.name') !!} </th>
                @if($evaluacion->type_id == 2)
                <th class='no-sort'>{!! trans('page.model.entrega') !!}</th>
                @endif
                <th class='no-sort'>{!! trans('page.model.calificacion') !!}</th>
            </tr>
        </thead>
        <tbody>
        @foreach($alumnos as $alumno)
            <tr>
                <td>{{ $alumno->lastname }}, {{ $alumno->firstname }}</td>
                @if($evaluacion->type_id == 2)
                    <td>
                    @if($alumno->entrega($evaluacion->id))
                        <a href="javascript:;" class='btn btn-default btn-xs' onclick='open_entrega_form(this)' data-url_redir='{{ route("evaluacion.show_entrega", [$evaluacion->id,$alumno->id]) }}'>Entregado <i class="glyphicon glyphicon-file"></i></a>
                    @else
                        {!! trans('page.label.evaluacion.no_entrega') !!}
                    @endif
                    </td>
                @endif
                <td>
                
                @if($alumno->calificacion($evaluacion->id))
                    <a href="javascript:;" onclick='open_calificacion_form(this)' data-url_redir='{{ route("calificacion.edit", [$alumno->calificacion($evaluacion->id)->id]) }}' class='btn {{ ($alumno->calificacion($evaluacion->id)->publica)? "btn-success" : "btn-default" }} btn-xs'>{{ $alumno->calificacion($evaluacion->id)->nota }}</a>
                @else
                    <a href="javascript:;" class='btn btn-default btn-xs' onclick='open_calificacion_form(this)' data-url_redir='{{ route("calificacion.create", [$evaluacion->id, $alumno->id]) }}'>
                        {!! trans('page.label.evaluacion.calificar') !!}<i class="glyphicon glyphicon-copy"></i>
                    </a>
                @endif
                    
                </td>
            </tr>
        @endforeach
        </tbody>
        <tfoot>
            <tr>
                <td colspan="3" style="text-align: center;">
                    {!! Form::open(['route' => ['calificacion.publish_all', $evaluacion->id], 'method' => 'patch']) !!}
                        {!! Form::button('Publicar Notas', ['type' => 'submit', 'class' => 'btn btn-success btn-sm', 'onclick' => "return confirm('Desea publicar todas las Notas?')"]) !!}
                    {!! Form::close() !!}
                </td>
            </tr>
        </tfoot>
    </table>
</div>
<div class="clearfix"></div>
<script>

function open_calificacion_form(elem){
    $.ajax({
        url: $(elem).data('url_redir'),
        type : 'GET',
      success : function(data) {
        $('#ventana-modal').html(data);
        $('#ventana-modal').modal('show');                
      },

      // código a ejecutar si la petición falla;
       error : function(xhr, status) {
        console.log(status);
            alert('Disculpe, existió un problema');
        },
    });
}
function open_entrega_form(elem){
    $.ajax({
        url: $(elem).data('url_redir'),
        type : 'GET',
      success : function(data) {
        $('#ventana-modal').html(data);
        $('#ventana-modal').modal('show');                
      },

      // código a ejecutar si la petición falla;
       error : function(xhr, status) {
        console.log(status);
            alert('Disculpe, existió un problema');
        },
    });
}

function open_correccion_form(elem){
    $.ajax({
        url: $(elem).data('url_redir'),
        type : 'GET',
      success : function(data) {
        $('#ventana-modal').html(data);
        $('#ventana-modal').modal('show');   
        
        $('.file_uploader').fileupload(initUploadConfig()).prop('disabled', !$.support.fileInput);

        var editor = CKEDITOR.replace('profesor_observacion',{plugins:'emoji,basicstyles,link,wysiwygarea,toolbar,image,colorbutton,list,resize,uploadimage,filebrowser',resize_dir: 'vertical',filebrowserBrowseUrl: ''});      
        CKFinder.setupCKEditor( editor, null, { type: 'Files'} );            
      },

      // código a ejecutar si la petición falla;
       error : function(xhr, status) {
        console.log(status);
            alert('Disculpe, existió un problema');
        },
    });
}

function save_correccion_obs(){
   $('#profesor_observacion').val(CKEDITOR.instances.profesor_observacion.getData());
}





var file_upload_url  = "{{ route('file.upload') }}";

function initUploadConfig() {
    config = {
        type: 'POST',
        url: file_upload_url,
        dataType: 'json',

        add: function (e, data) {
            $('.progress .progress-bar').css('width', '0%');
            $('.progress').removeClass('hidden');
            data.submit();
        },
        done: function (e, data) {

            if(data.result.error){
                $('#upload_file_message').html(data.result.error);
            }else{
                var file_row = "<div class='file-row'><div><a href='"+data.result.path+"' target='blank'>"+data.result.name+"</a></div><input type='hidden' name='files_id[]' value='"+data.result.file_id+"' class='input-file' /><a href='#' onclick='delete_file(this)'><i class='glyphicon glyphicon-trash'></i></a></div>"
                $('.file-list').append(file_row);
            }
            $('.progress').addClass('hidden');
        },
        progressall: function (e, data) {
            $('#upload_file_message').html('');
            var progress = parseInt(data.loaded / data.total * 100, 10);
            $('.progress .progress-bar').css('width', progress + '%');
        }
    };
    return config;
}
function delete_file(elem){
    var row = $(elem).parents('.file-row');
    row.hide();
    row.find('.input-file').attr('name', 'files_del[]');
}

</script>