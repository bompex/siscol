<div class="panel panel-default">
    <div class="panel-body">
    	@if(isset($comments) && count($comments) > 0)
        	@foreach($comments as $index => $comment)
            	<div class="fr-comment row">
        	    	<div class="col-sm-2 col-xs-12 fr-metadata  row">
        	    		<div class="col-sm-12 col-xs-2 fr-comment-number">#{{  count($comments) - $index }}</div>
        	    		<div class="col-sm-12 col-xs-10 fr-author">{{ $comment->user->lastname }}, {{ $comment->user->firstname }}</div>
        				<div class="col-sm-12 col-xs-6 fr-date">{{ date('d/m/Y', strtotime($comment->created_at)) }}</div>
        				<div class="col-sm-12 col-xs-6 fr-horu">{{ date('H:i:s', strtotime($comment->created_at)) }}</div>
                        
                            <div class="col-sm-12  col-xs-4">
                                <a class="btn btn-success btn-xs citar_message_btn"  href="javascript:;"  title="Citar Mensaje"><span><i class="glyphicon glyphicon-share-alt"></i></span></a>
                                @if($cu_user->isProfesor())
                                    @if(($comment->user->id != $cu_user->id))
                                        <a class="btn btn-success btn-xs open_message_modal"  href="javascript:;" data-href="{{ route('message.show', [$comment->user->id, 'mode' => 'modal','reply' => 'evaluacion_comment','reply_id' => $comment->id]) }}"  title="Enviar Mensaje"><span ><i class="glyphicon glyphicon-comment"></i></span></a>
                                    @endif

                                    {!! Form::open(['route' => ['evaluacion.remove_comment', $evaluacion->id, $comment->id], 'method' => 'delete']) !!}
                                    
                                    {!! Form::button('<span><i class="glyphicon glyphicon-trash"></i></span>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Seguro desea Borrar?')"]) !!}
                                    
                                    {!! Form::close() !!}
                                @endif
                            </div>
                        
        			</div>
        			<div class="col-sm-10 col-xs-12 comment-body">
        		    		{!! $comment->body !!}
        			</div>
            	</div>
        	@endforeach

    	@else

        	<div class='col-md-12 text-center'>
        		No hay Comentarios
        	</div>

    	@endif



    </div>
</div>

