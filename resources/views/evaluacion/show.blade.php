@extends('adminlte::page_profesor')
@section('content')
    <div class="row">
        <div class="col-xs-12">
        <div class="box box-primary">
            <div class="box-body">
                <div class="row" style="padding-left: 20px">
                    
                    @include('evaluacion.show_fields')
                    
                    <div class='clearfix'></div>
                    <div class='col-md-6'>
                        <a href="{{ route('evaluacion.edit', [$evaluacion->curso_materia_id,$evaluacion->id]) }}" class="btn btn-primary">{!! trans('page.button.edit') !!}</a>
                        <a href="{{ route('profesor_curso_materia.index', $evaluacion->curso_materia_id) }}" class="btn btn-default">{!! trans('page.button.back') !!}</a>
                    </div>
                    
                </div>
            </div>
        </div>

        
    </div>
    <div class="col-xs-12 col-md-6">
        <div class="box box-primary">
            <div class="box-body">
                <div class="row" style="padding-left: 20px">
                    @include('evaluacion.show_alumno_table')
                </div>
            </div>
        </div>
    </div>
    <div class="col-xs-12 col-md-6 foro">
        @if($evaluacion->allow_comments == 1)
          @include('evaluacion.comment_form')
        @endif
        @include('evaluacion.comment')
    </div>
    </div>
@endsection
