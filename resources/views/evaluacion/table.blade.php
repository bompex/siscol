<div class="table-responsive">
    <table class="table datatable" id="evaluacion-table">
        <thead>
            <tr>
                <th>{!! trans('page.label.evaluacion.subject') !!}</th>
                <th>{!! trans('page.label.evaluacion.fecha_inicio') !!}</th>
                <th>{!! trans('page.label.evaluacion.fecha_fin') !!}</th>
                <th>{!! trans('page.label.evaluacion.fecha_public') !!}</th>
                <th class='no-sort'>&nbsp;</th>
            </tr>
        </thead>
        <tbody>
        @foreach($next_evaluaciones as $evaluacion)
            <tr>
                <td>{{ $evaluacion->subject }}</td>
                <td>{{ date('d/m/Y H:i', strtotime($evaluacion->fecha_inicio)) }}</td>
                <td>{{ date('d/m/Y H:i', strtotime($evaluacion->fecha_fin)) }}</td>
                <td>{{ date('d/m/Y H:i', strtotime($evaluacion->fecha_public)) }}</td>
                <td>
                    <div class='btn-group'>
                        <a href="{{ route('evaluacion.show', [$evaluacion->id]) }}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                        {!! Form::open(['route' => ['evaluacion.remove', $cursoMateria->id, $evaluacion->id], 'method' => 'delete']) !!}
                                
                        {!! Form::button('<span ><i class="glyphicon glyphicon-trash"></i></span>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Seguro desea Borrar?')"]) !!}
                        
                        {!! Form::close() !!}
                    </div>
                </td>
            </tr>
        @endforeach
        @foreach($prev_evaluaciones as $evaluacion)
            <tr class='old-evaluacion'>
                <td>{{ $evaluacion->subject }}</td>
                <td>{{ date('d/m/Y H:i', strtotime($evaluacion->fecha_inicio)) }}</td>
                <td>{{ date('d/m/Y H:i', strtotime($evaluacion->fecha_fin)) }}</td>
                <td>{{ date('d/m/Y H:i', strtotime($evaluacion->fecha_public)) }}</td>
                <td>
                    <div class='btn-group'>
                        <a href="{{ route('evaluacion.show', [$evaluacion->id]) }}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                        {!! Form::open(['route' => ['evaluacion.remove', $cursoMateria->id, $evaluacion->id], 'method' => 'delete']) !!}
                                
                        {!! Form::button('<span ><i class="glyphicon glyphicon-trash"></i></span>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Seguro desea Borrar?')"]) !!}
                        
                        {!! Form::close() !!}
                    </div>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
