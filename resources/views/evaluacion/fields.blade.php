<h3  class="form-group col-sm-12">{!! $types[$type] !!}</h3>

<div class="clearfix"></div>
<div class="form-group col-sm-12">
    {!! Form::label('subject', trans('page.label.evaluacion.subject')) !!}
    {!! Form::text('subject', $evaluacion->subject, ['class' => 'form-control']) !!}
    {!! Form::hidden('type_id', $type) !!}
</div>

<div class="clearfix"></div>
<div class="form-group col-sm-12">
	<div class="col-sm-6">
			{!! Form::label('fecha_inicio_date', trans('page.label.evaluacion.fecha_inicio'), ['class' => 'label-evaluacion']) !!}
		    {!! Form::date('fecha_inicio_date', ($evaluacion->fecha_inicio!= '')? date('Y-m-d',strtotime($evaluacion->fecha_inicio)) : null, ['class' => 'form-control date-evaluacion','id'=>'fecha_inicio']) !!} 

        	{!! Form::text('fecha_inicio_time', ($evaluacion->fecha_inicio!= '')? date('H:i',strtotime($evaluacion->fecha_inicio)) : '00:00', ['class' => 'form-control timepicker time-evaluacion']) !!}
	</div>
	<div class="form-group col-sm-6">
	    {!! Form::label('fecha_fin_date', trans('page.label.evaluacion.fecha_fin'), ['class' => 'label-evaluacion']) !!}
		    {!! Form::date('fecha_fin_date', ($evaluacion->fecha_fin!= '')? date('Y-m-d',strtotime($evaluacion->fecha_fin)) : null, ['class' => 'form-control date-evaluacion','id'=>'fecha_fin']) !!} 
        	{!! Form::text('fecha_fin_time', ($evaluacion->fecha_fin!= '')? date('H:i',strtotime($evaluacion->fecha_fin)) : '00:00', ['class' => 'form-control timepicker time-evaluacion']) !!}
	</div>
</div>

<div class="clearfix"></div>
<div class="form-group col-sm-12">
    {!! Form::label('body', trans('page.label.evaluacion.body')) !!}
    {!! Form::textarea('body', $evaluacion->body, ['class' => 'form-control']) !!}
</div>
<div class="clearfix"></div>
<div class="form-group col-md-12">
    <div class="file-list">
        @foreach($evaluacion->files as $file)
            <div class='file-row'><div><a href='{!! $file->path !!}' target='blank'>{{ $file->name }}</a></div><input type='hidden' name='files[]' value='{{ $file->id }}' class='input-file' /><a href='#' onclick='delete_file(this)'><i class='glyphicon glyphicon-trash'></i></a></div>
        @endforeach
        
    </div>
</div>
<div class="clearfix"></div>
<div class="form-group col-md-6 col-sm-12">
    <label for="file_uploader" class="btn btn-default custom-file-upload input-font">Subir Archivo</label>
    <div class="progress progress-xs hidden">
     <div class="progress-bar progress-bar-danger" style="width: 0%"></div>
   </div>
    {{ Form::file('file_uploader',['class' => 'form-control file_uploader hidden', 'id' => 'file_uploader', 'name'=>'file_uploader']) }}
    <span id='upload_file_message' class="invalid-feedback error"><strong></strong></span>   
</div>
<div class="clearfix"></div>
<div class="form-group col-md-2">
    {!! Form::label('allow_comments', trans('page.label.topic.allow_comments')) !!}
    {!! Form::select('allow_comments',['0'=>'No', '1' => 'Si'], $evaluacion->allow_comments, ['class' => 'form-control']) !!}
</div>
<div class="clearfix"></div>
<div class="form-group col-sm-6">
            {!! Form::label('fecha_public_date', trans('page.label.evaluacion.fecha_public'), ['class' => 'label-evaluacion']) !!}
            {!! Form::date('fecha_public_date', ($evaluacion->fecha_public!= '')? date('Y-m-d',strtotime($evaluacion->fecha_public)) : date('Y-m-d',time()), ['class' => 'form-control date-evaluacion','id'=>'fecha_public']) !!} 

            {!! Form::text('fecha_public_time', ($evaluacion->fecha_public!= '')? date('H:i',strtotime($evaluacion->fecha_public)) : date('H:i',time()), ['class' => 'form-control timepicker time-evaluacion']) !!}
</div>
<div class="clearfix"></div>
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('profesor_curso_materia.index',$cursoMateria->id) }}" class="btn btn-default">{!! trans('page.button.cancel') !!}</a>
</div>

<script>

</script>


@section('adminlte_js')
<script>
    var file_upload_url  = "{{ route('file.upload') }}";

    function initUploadConfig() {
        config = {
            type: 'POST',
            url: file_upload_url,
            dataType: 'json',

            add: function (e, data) {
                $('.progress .progress-bar').css('width', '0%');
                $('.progress').removeClass('hidden');
                data.submit();
            },
            done: function (e, data) {

                if(data.result.error){
                    $('#upload_file_message').html(data.result.error);
                }else{
                    var file_row = "<div class='file-row'><div><a href='"+data.result.path+"' target='blank'>"+data.result.name+"</a></div><input type='hidden' name='files[]' value='"+data.result.file_id+"' class='input-file' /><a href='#' onclick='delete_file(this)'><i class='glyphicon glyphicon-trash'></i></a></div>"
                    $('.file-list').append(file_row);
                }
                $('.progress').addClass('hidden');
            },
            progressall: function (e, data) {
                $('#upload_file_message').html('');
                var progress = parseInt(data.loaded / data.total * 100, 10);
                $('.progress .progress-bar').css('width', progress + '%');
            }
        };
        return config;
    }

    function delete_file(elem){
        var row = $(elem).parents('.file-row');
        row.hide();
        row.find('.input-file').attr('name', 'files_del[]');
    }

    $(function () {
        $('.file_uploader').fileupload(initUploadConfig()).prop('disabled', !$.support.fileInput)
            .parent().addClass($.support.fileInput ? undefined : 'disabled');

        $('.timepicker').timepicker({
            controlType: 'select',
            oneLine: true,
            stepMinute: 5,
        });

        
        var editor = CKEDITOR.replace( 'body',{extraPlugins:'div,font',removePlugins: '' ,language: 'es',height:100,resize_dir: 'vertical',removeButtons:'',filebrowserBrowseUrl: '',
                
        });

        CKFinder.setupCKEditor( editor, null, { type: 'Files'} );
    });
</script>
@endsection


        