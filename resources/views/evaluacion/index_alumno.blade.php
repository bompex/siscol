@extends('adminlte::page_alumno')
@section('content_header')
        <h1 class="pull-left">{!! trans('page.model.plural.evaluacion'.$cursoMateria->curso->primario) !!}</h1>
        <div class="pull-right">

            <a class="btn btn-primary btn-sm"  href="{{ route('home_alumno.index_materia',$cursoMateria->id) }}">{!! trans('page.button.back') !!}</a>

        
           
        </div>
@endsection
@section('content')
    <div class="row">
        <div class="col-xs-12">
        <div class="clearfix"></div>

        @include('flash::message')

        <div class="clearfix"></div>
        <div class="box box-primary">
            <div class="box-body">
                    <div class="table-responsive">
                        <table class="table datatable" id="evaluacion-table">
                            <thead>
                                <tr>
                                    <th>{!! trans('page.label.evaluacion.type') !!}</th>
                                    <th>{!! trans('page.label.evaluacion.subject') !!}</th>
                                    <th>{!! trans('page.label.evaluacion.fecha_inicio') !!}</th>
                                    <th>{!! trans('page.label.evaluacion.fecha_fin') !!}</th>
                                    <th class='no-sort'>&nbsp;</th>
                                </tr>
                            </thead>
                            <tbody>
                            @foreach($next_evaluaciones as $evaluacion)
                                <tr>
                                    <td>{{ $evaluacion->type }}</td>
                                    <td>{{ $evaluacion->subject }}</td>
                                    <td>{{ date('d/m/Y H:i', strtotime($evaluacion->fecha_inicio)) }}</td>
                                    <td>{{ date('d/m/Y H:i', strtotime($evaluacion->fecha_fin)) }}</td>
                                    <td>
                                        <div class='btn-group'>
                                            <a href="{{ route('evaluacion_alumno.show', [$evaluacion->id]) }}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                            @foreach($prev_evaluaciones as $evaluacion)
                                <tr class='old-evaluacion'>
                                     <td>{{ $evaluacion->type }}</td>
                                    <td>{{ $evaluacion->subject }}</td>
                                    <td>{{ date('d/m/Y H:i', strtotime($evaluacion->fecha_inicio)) }}</td>
                                    <td>{{ date('d/m/Y H:i', strtotime($evaluacion->fecha_fin)) }}</td>
                                    <td>
                                        <div class='btn-group'>
                                            <a href="{{ route('evaluacion_alumno.show', [$evaluacion->id]) }}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
            </div>
        </div>
    </div>
    </div>
@endsection