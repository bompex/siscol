
        <div class="row">
                
			

			<div class='col-md-6 evaluacion-subject'>
				{!! Form::label('subject', 'Tema:') !!}
				<p>{{ $evaluacion->subject }}</p>
			</div>
		    <div class='col-md-6 evaluacion-fecha'>
		    	{!! Form::label('date', trans('page.label.evaluacion.'.$evaluacion->type_id.'_date')) !!}:
		    	<p>
				    @if( date('Y-m-d', strtotime($evaluacion->fecha_inicio)) == date('Y-m-d', strtotime($evaluacion->fecha_fin)))
				      	{{ date('d/m/Y', strtotime($evaluacion->fecha_inicio)) }} de {{ date('H:i', strtotime($evaluacion->fecha_inicio)) }} a {{ date('H:i', strtotime($evaluacion->fecha_fin)) }} 
				    @else
				    	{{ date('d/m/Y H:i', strtotime($evaluacion->fecha_inicio)) }} al {{ date('d/m/Y H:i', strtotime($evaluacion->fecha_fin)) }} 
				    @endif
				</p>
		    </div> 

		    <div class="col-md-6">
		    	{!! Form::label('type', trans('page.label.evaluacion.type')) !!}:
				<p>{{ $evaluacion->type }}</p>			    
			</div>

		    @if($evaluacion->body != '')
			<div class='col-md-12 evaluacion-body'>
				{!! Form::label('body', trans('page.label.evaluacion.body')) !!}:
				{!! $evaluacion->body !!}
			</div>
			@endif

			@if(isset($evaluacion->files) && count($evaluacion->files)>0)
	    	<div class='col-md-12'>
	    		<strong>Archivos Adjuntos</strong>
	    		<ul>
		    		@foreach($evaluacion->files as $file)
			            <li><div><a href='{!! $file->path !!}' target='blank'>{{ $file->name }}</a></div></li>
			        @endforeach
			    </ul>
	    	</div>
	    	@endif

		</div>

