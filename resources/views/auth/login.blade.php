<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">

        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

        <!-- Bootstrap 3.3.6 -->
        <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">

        <!-- Font Awesome -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">

        <!-- Ionicons -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">

        <!-- Theme style -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/admin-lte/2.3.3/css/AdminLTE.min.css">

        <!-- iCheck -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/admin-lte/2.3.3/css/skins/_all-skins.min.css">
        <link rel="stylesheet" href="{{ asset('css/login.css') }}">

        <!-- Google font roboto -->
        <link href="https://fonts.googleapis.com/css?family=Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i" rel="stylesheet">

    </head>
    <body style="background:#EFF5FA">
        <section id="login">
            <div class="container">
                <div class="row" style="margin-top: 100px">
                    <div class="col-md-9 mask-2">

                        <div class="col-md-6 col-sm-6 col-md-offset-4 col-sm-offset-4 col-xs-12 login-wrapper">
                            <div style="text-align: center">
                                {!! config('adminlte.logo') !!}
                            </div>
                            <form method="post" action="{{ url('login') }}" autocomplete="off">
                                {!! csrf_field() !!}

                                <div class="form-group has-feedback {{ $errors->has('email') ? ' has-error' : '' }}">
                                    <label>E-mail</label>
                                    <input type="email" class="form-control" name="email" value="{{ old('email') }}" placeholder="Email" required>
                                    <span class="invalid-feedback error"><strong>{{ $errors->first('email') }}</strong></span>
                                </div>

                                <div class="form-group has-feedback{{ $errors->has('password') ? ' has-error' : '' }}">
                                    <label>Password</label>
                                    <input type="password" class="form-control" placeholder="Password" name="password" required>
                                    <span class="invalid-feedback error"><strong>{{ $errors->first('password') }}</strong></span>
                                </div>
                                <div class="form-group pull-left">
                                    <button type="submit" class="btn btn-primary">{{ __('auth.login') }}</button>
                                </div>
                                <div class="form-group pull-right">
                                    <a style="padding:0;margin-top: 7px" class="btn btn-link forgot" href="{{ url('/password/reset') }}">
                                        {{ __('auth.forgot_password') }}
                                    </a>             
                                </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>


        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
        <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/iCheck/1.0.2/icheck.min.js"></script>

        <script>
            $(function () {
                $('input').iCheck({
                    checkboxClass: 'icheckbox_square-blue',
                    radioClass: 'iradio_square-blue',
                    increaseArea: '20%' // optional
                });
            });
        </script>
    </body>
</html>
