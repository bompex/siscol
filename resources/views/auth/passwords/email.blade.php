@extends('layouts.auth')

@section('css')
    <link rel="stylesheet" href="{{ asset('css/login.css') }}">
@endsection

@section('content')

<div class="container">
    <div class="row" style="margin-top: 100px">
        <div class="col-md-9 mask-2">

            <div class="col-md-6 col-sm-6 col-md-offset-4 col-sm-offset-4 col-xs-12 login-wrapper">
                <div style="text-align: center">
                    <img src="{{ asset('img/logo.png') }}" style="width:200px;"/>
                </div>  
                @if (session('status'))
                <div class="alert alert-success" role="alert">
                    {{ session('status') }}
                </div>
                @endif

                <form method="POST" action="{{ route('password.email') }}">
                    
                    @csrf

                    <div class="form-group row">
                        <span>{{ __('auth.reset_instructions') }}</span>
                    </div>
                    <div class="form-group row">
                        <label for="email">{{ __('E-Mail') }}</label>
                        <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>
                        @error('email')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>

                    <div class="form-group row">
                        <button type="submit" class="btn btn-primary">{{ __('Enviar') }}</button>
                        <a style="margin-left:8px" class="btn btn-default" href="{{ route('login') }}">{{ __('Volver') }}</a>
                    </div>
                </form>                
            </div>
        </div> 
    </div> 
</div>

@endsection
