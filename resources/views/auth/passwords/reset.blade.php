@extends('layouts.auth')

@section('css')
    <link rel="stylesheet" href="{{ asset('css/login.css') }}">
@endsection

@section('content')
<div class="container">
    <div class="row justify-content-center" style="margin-top:150px">
        <div class="col-md-8 col-md-offset-2">
            <div class="card login-wrapper">
                <div class="card-header">
                
                    <div style="text-align: center">
                        <h2>{{ __('auth.reset_title') }}</h2>                        
                        <span>Ingrese su nueva contraseña</span>
                    </div>
                    
                </div>

                <div class="card-body">
                    <form method="POST" action="{{ route('password.update') }}">
                        @csrf

                        <input type="hidden" name="token" value="{{ $token }}">

                        <div class="col-md-8 col-md-offset-2">
                            <div class="form-group row">
                                <label for="email">{{ __('E-Mail') }}</label>
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ $email ?? old('email') }}" required autocomplete="email" autofocus>

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>

                            <div class="form-group row">
                                <label for="password">{{ __('Password') }}</label>
                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>

                            <div class="form-group row">
                                <label for="password-confirm">{{ __('Confirmar') }}</label>
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">
                            </div>

                            <div class="form-group row mb-0">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Restablecer') }}
                                </button>
                            </div>
                        </div>

                        <div class="clearfix"></div>
                        
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
