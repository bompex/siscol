@php
$messag = Messag::getMyMessages();
$new_messag = [];
foreach($messag as $msg){
    if($msg['viewed'] == 0){
        $new_messag[] =  $msg['user']->id;
    }
}
@endphp
<li class="messag-section">
    <a class="dropdown-toggle messag-button" href="#" id="dropdownUserLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        <i class="fa messag-icon fa-envelope"></i>
        <span class="label label-danger">{{ ($new_messag)? count($new_messag) : '' }}</span>
    </a>
    <ul class="dropdown-menu notif-list">
    	@if(count($messag) > 0)
    		@foreach($messag as $msg)
    			<li class="notif-item ">
    				<a class="open_message_modal" href="javascript:;" onclick="mark_as_readed(this)" data-href="{{ route('message.show', [$msg['user']->id, 'mode' => 'modal']) }}"  title="Enviar Mensaje">
	    				<span class="since-time">Hace {!! Notif::humanTiming(strtotime($msg['created_at'])) !!}</span>
                        @if(!$msg['viewed'])
                            <span class='new-message-dot'>•</span>
                        @endif
	    				<span style="white-space:normal">{!! $msg['user']->lastname !!}, {!! $msg['user']->firstname !!}{!! ($msg['user']->isAlumno() && (isset($msg['user']->alumno) && isset($msg['user']->alumno->curso_actual) && isset($msg['user']->alumno->curso_actual->name)) )? " (".$msg['user']->alumno->curso_actual->name.")" : "" !!}</span>

    				</a>
    			</li>
    		@endforeach
    	@else
    		<li class="notif-item"><a href="#">-- no hay mensajes --</a></li>
    	@endif
        
    </ul>
</li>