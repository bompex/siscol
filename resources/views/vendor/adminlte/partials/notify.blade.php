@php
$notify = Notif::getMyNotifications();
$new_notify = [];
foreach($notify as $notif){
    if($notif->viewed == 0){
        $new_notify[] =  $notif->id;
    }
}
@endphp
<li class="notif-section">
    <a class="dropdown-toggle notif-button" href="#" id="dropdownUserLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        <i class="fa notif-icon fa-bell"></i>
        <span class="label label-danger">{{ (count($new_notify) > 0)? count($new_notify) : '' }}</span>
    </a>
    <ul class="dropdown-menu notif-list">
    	@if(count($notify) > 0)
    		@foreach($notify as $notif)
    			<li class="notif-item {{ (!$notif->viewed)? 'notif-new' : '' }}">
    				<a href="{{ ($notif->url)? $notif->url : '#' }}" class="{{ $notif->class }}">
	    				<span class="since-time">Hace {!! Notif::humanTiming(strtotime($notif->created_at)) !!}</span>
	    				<span style="white-space:normal">{!! $notif->message !!}</span>
    				</a>
    			</li>
    		@endforeach
    	@else
    		<li class="notif-item"><a href="#">-- no hay mensajes --</a></li>
    	@endif
        
    </ul>
</li>