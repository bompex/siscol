@extends('adminlte::master')

@section('adminlte_css')
    <link rel="stylesheet"
          href="{{ asset('vendor/adminlte/dist/css/skins/skin-' . config('adminlte.skin', 'blue') . '.min.css')}} ">
    @stack('css')
    @yield('css')
@stop

@section('body_class', 'skin-' . config('adminlte.skin', 'blue') . ' sidebar-mini ' . (config('adminlte.layout') ? [
    'boxed' => 'layout-boxed',
    'fixed' => 'fixed',
    'top-nav' => 'layout-top-nav'
][config('adminlte.layout')] : '') . (config('adminlte.collapse_sidebar') ? ' sidebar-collapse ' : ''))

@section('body')
    <div class="wrapper">

        <div class="modal fade" id="ventana-modal">
            
        </div>

        <!-- Main Header -->
        <header class="main-header">
            
            <!-- Logo -->
            <a href="{{ url(config('adminlte.dashboard_url', 'home')) }}" class="logo  hidden-xs">
                <!-- mini logo for sidebar mini 50x50 pixels -->
                <span class="logo-mini">{!! config('adminlte.logo_mini', '<b>A</b>LT') !!}</span>
                <!-- logo for regular state and mobile devices -->
                <span class="logo-lg">{!! config('adminlte.logo', '<b>Admin</b>LTE') !!}</span>
            </a>

            <!-- Header Navbar -->
            <nav class="navbar navbar-static-top" role="navigation">
                <!-- Sidebar toggle button-->
                <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
                    <span class="sr-only">{{ trans('adminlte::adminlte.toggle_navigation') }}</span>
                </a>
                <img src="/img/logo.png" class="visible-xs" style="float: left;margin: 5px 0;padding: 0px;height: 40px;">

                <!-- Navbar Right Menu -->
                <div class="navbar-custom-menu">
                    @if(\Auth::getUser())
                    <ul class="nav navbar-nav">
                        @include('adminlte::partials.messages') 
                        @include('adminlte::partials.notify') 

                        <li>
                            <a class="dropdown-toggle" href="#" id="dropdownUserLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <i class="fa fa-fw fa-user"></i><span class="hidden-xs">Prof. {{\Auth::User()->lastname}}, {{ \Auth::User()->firstname }}</span>
                            </a>
                            <ul class="dropdown-menu">
                                @if(\Auth::User()->isAdmin() || \Auth::User()->isAdministrativo() || \Auth::User()->isDirector() || \Auth::User()->isPreceptor() || \Auth::User()->isSecretario())
                                <li class="">
                                    <a class="dropdown-item" href="{{ route('home.admin') }}">Ingresar como Administrativo</a>
                                </li>
                                @endif

                                <li class="">
                                    <a class="dropdown-item" href="{{ route('user.change_password') }}">Cambiar Contraseña</a>
                                </li>
                                <li class="">
                                    @if(config('adminlte.logout_method') == 'GET' || !config('adminlte.logout_method') && version_compare(\Illuminate\Foundation\Application::VERSION, '5.3.0', '<'))
                                        <a href="{{ url(config('adminlte.logout_url', 'auth/logout')) }}">
                                            <i class="fa fa-fw fa-power-off"></i> Cerrar sesión
                                        </a>
                                    @else
                                        <a href="#"
                                           onclick="event.preventDefault(); document.getElementById('logout-form').submit();"
                                        >
                                            <i class="fa fa-fw fa-power-off"></i> Cerrar sesión
                                        </a>
                                        <form id="logout-form" action="{{ url(config('adminlte.logout_url', 'auth/logout')) }}" method="POST" style="display: none;">
                                            @if(config('adminlte.logout_method'))
                                                {{ method_field(config('adminlte.logout_method')) }}
                                            @endif
                                            {{ csrf_field() }}
                                        </form>
                                    @endif
                                </li>
                            </ul>

                        </li>
                    </ul>
                    @endif
                </div>
                @if(config('adminlte.layout') == 'top-nav')
                </div>
                @endif
            </nav>
        </header>

        
        <!-- Left side column. contains the logo and sidebar -->
        <aside class="main-sidebar">

            <!-- sidebar: style can be found in sidebar.less -->
            <section class="sidebar">

                <!-- Sidebar Menu -->
                <ul class="sidebar-menu sidebar-cursos" data-widget="tree">
                    <li class="">
                        <a href="{{ route('home.profesor') }}">
                            <span>Inicio</span>
                        </a>
                    </li>
                </ul>
                <!-- Sidebar Menu -->
                <ul class="sidebar-menu sidebar-cursos" data-widget="tree">
                    @foreach(\Auth::User()->profesor->cursoMaterias as $cuMa)
                    <li class="profesor-sidebar">
                        <a href="{{ route('profesor_curso_materia.index',$cuMa->id) }}" style="background-color:{{ $cuMa->materia->color  }}">
                            <span>{{ $cuMa->materia->name }} ({{ $cuMa->curso->name }})</span>
                        </a>
                    </li>
                    @endforeach
                </ul>
                @php
                    $suplencias = \Auth::User()->profesor->cursoMateriasSuplente;
                @endphp
                @if(count($suplencias) > 0)
                <ul class="sidebar-menu sidebar-cursos" data-widget="tree">
                    <li class="header">Suplencias</li>
                    @foreach($suplencias as $cuMa)
                    <li class="profesor-sidebar">
                        <a href="{{ route('profesor_curso_materia.index',$cuMa->id) }}" style="background-color:{{ $cuMa->materia->color  }}">
                            <span>{{ $cuMa->materia->name }} ({{ $cuMa->curso->name }})</span>
                        </a>
                    </li>
                    @endforeach
                </ul>
                @endif
                <ul class="sidebar-menu sidebar-cursos" data-widget="tree">
                    <li class="header">Historico</li>
                    <li class="profesor-sidebar">
                        <a href="{{ route('profesor_curso_materia.history') }}" >
                            <span>Cursos</span>
                        </a>
                    </li>
                </ul>

                <!-- /.sidebar-menu -->
            </section>
            <!-- /.sidebar -->
        </aside> 

        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            

            <!-- Content Header (Page header) -->
            <section class="content-header">
                @yield('content_header')
            </section>

            <!-- Main content -->
            <section class="content">

                @yield('content')

            </section>
            <!-- /.content -->
            
        </div>
        <!-- /.content-wrapper -->

        @include('adminlte::partials.footer')

    </div>
    <!-- ./wrapper -->
@stop

@section('adminlte_js')
    <script src="{{ asset('vendor/adminlte/dist/js/adminlte.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('vendor/jsvalidation/js/jsvalidation.js')}}"></script>
    <script>
        $(document).ready(function() {
            $('.select2').select2();
            $('.datatable').DataTable({
                "paging": false,
                "info": false,
                "columnDefs": [ {
                  "targets"  : 'no-sort',
                  "orderable": false,
                  "order": []
                }],
                "language": {
                    "emptyTable":     "No hay Registros",
                    "info":           "Mostrando _START_ al _END_ de _TOTAL_ registros",
                    "infoEmpty":      "No hay registros",
                    "infoFiltered":   "",
                    "infoPostFix":    "",
                    "thousands":      ",",
                    "lengthMenu":     "Mostrando _MENU_ Registros",
                    "loadingRecords": "Cargando...",
                    "processing":     "Procesando...",
                    "search":         "Buscar:",
                    "zeroRecords":    "No se encuentran registros ",
                    "paginate": {
                        "first":      "Primero",
                        "last":       "Ultimo",
                        "next":       "Siguiente",
                        "previous":   "Previo"
                    },
                    "aria": {
                        "sortAscending":  "",
                        "sortDescending": ""
                    }
                }
            });
        });

    </script>
    @stack('js')
    @yield('js')
@stop
