<!-- Curso Id Field -->
<div class="form-group">
    {!! Form::label('curso_id', 'Curso Id:') !!}
    <p>{{ $cursoAlumno->curso_id }}</p>
</div>

<!-- Alumno Id Field -->
<div class="form-group">
    {!! Form::label('alumno_id', 'Alumno Id:') !!}
    <p>{{ $cursoAlumno->alumno_id }}</p>
</div>

<!-- Registration Date Field -->
<div class="form-group">
    {!! Form::label('registration_date', 'Registration Date:') !!}
    <p>{{ $cursoAlumno->registration_date }}</p>
</div>

<!-- Status Field -->
<div class="form-group">
    {!! Form::label('status', 'Status:') !!}
    <p>{{ $cursoAlumno->status }}</p>
</div>

<!-- Created By Field -->
<div class="form-group">
    {!! Form::label('created_by', 'Created By:') !!}
    <p>{{ $cursoAlumno->created_by }}</p>
</div>

<!-- Updated By Field -->
<div class="form-group">
    {!! Form::label('updated_by', 'Updated By:') !!}
    <p>{{ $cursoAlumno->updated_by }}</p>
</div>

