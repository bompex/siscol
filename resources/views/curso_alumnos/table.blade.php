<div class="table-responsive">
    <table class="table" id="cursoAlumnos-table">
        <thead>
            <tr>
                <th>Curso Id</th>
        <th>Alumno Id</th>
        <th>Registration Date</th>
        <th>Status</th>
        <th>Created By</th>
        <th>Updated By</th>
                <th colspan="3">Action</th>
            </tr>
        </thead>
        <tbody>
        @foreach($cursoAlumnos as $cursoAlumno)
            <tr>
                <td>{{ $cursoAlumno->curso_id }}</td>
            <td>{{ $cursoAlumno->alumno_id }}</td>
            <td>{{ $cursoAlumno->registration_date }}</td>
            <td>{{ $cursoAlumno->status }}</td>
            <td>{{ $cursoAlumno->created_by }}</td>
            <td>{{ $cursoAlumno->updated_by }}</td>
                <td>
                    {!! Form::open(['route' => ['cursoAlumnos.destroy', $cursoAlumno->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{{ route('cursoAlumnos.show', [$cursoAlumno->id]) }}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                        <a href="{{ route('cursoAlumnos.edit', [$cursoAlumno->id]) }}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                        {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
