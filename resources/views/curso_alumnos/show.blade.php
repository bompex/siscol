@extends('layouts.app')
@section('content_header')
    <h1>
            Curso Alumno
        </h1>
@endsection
@section('content')
    <div class="row">
        <div class="col-xs-12">
        <div class="box box-primary">
            <div class="box-body">
                <div class="row" style="padding-left: 20px">
                    @include('curso_alumnos.show_fields')
                    <a href="{{ route('cursoAlumnos.index') }}" class="btn btn-default">Back</a>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
