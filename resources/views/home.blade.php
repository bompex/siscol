
@extends( (\Auth::getUser()->isAlumno()) ? 'adminlte::page_default' : 'adminlte::page' ) 
@section('content')
<div class="clearfix"></div>
    @include('flash::message')
<div class="row"> 
    <div class="col-md-6 col-xs-12">
        @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    Has iniciado sesión!   
    </div>
    <div class="clearfix"></div>
</div>
@endsection
