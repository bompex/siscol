<!-- Firstname Field -->
<div class="form-group">
    {!! Form::label('firstname', trans('page.label.user.name')) !!}
    <p>{{ $alumno->firstname }}</p>
</div>

<!-- Lastname Field -->
<div class="form-group">
    {!! Form::label('lastname', trans('page.label.user.lastname')) !!}
    <p>{{ $alumno->lastname }}</p>
</div>

<!-- Dni Field -->
<div class="form-group">
    {!! Form::label('dni', trans('page.label.user.dni')) !!}
    <p>{{ $alumno->dni }}</p>
</div>

<!-- Gender Field -->
<div class="form-group">
    {!! Form::label('gender', trans('page.label.user.gender')) !!}
    <p>{{ $gender[$alumno->gender] }}</p>
</div>

<!-- Birthdate Field -->
<div class="form-group">
    {!! Form::label('birthdate', trans('page.label.user.birthdate')) !!}
    <p>{{ date('d/m/Y',strtotime($alumno->birthdate)) }}</p>
</div>

<!-- Telefono Field -->
<div class="form-group">
    {!! Form::label('telefono', trans('page.label.user.phone')) !!}
    <p>{{ $alumno->telefono }}</p>
</div>

<!-- Email Field -->
<div class="form-group">
    {!! Form::label('email', trans('page.label.user.email')) !!}
    <p>{{ $alumno->user->email }}</p>
</div>


