<div class="table-responsive">
    <table class="table datatable" id="alumno-table">
        <thead>
            <tr>
                <th>{!! trans('page.label.user.lastname') !!}, {!! trans('page.label.user.name') !!}</th>
                <th>{!! trans('page.label.user.dni') !!}</th>
                <th>{!! trans('page.label.curso.nivel') !!}</th>
                <th>{!! trans('page.model.curso') !!}</th>
                <th class='no-sort'>&nbsp;</th>
            </tr>
        </thead>
        <tbody>
        @foreach($alumno as $alumno)
            <tr>
                <td>{{ $alumno->lastname }}, {{ $alumno->firstname }}</td>
                <td>{{ $alumno->dni }}</td>
                <td>{{ ($alumno->curso_actual)? $alumno->curso_actual->nivel : '--' }}</td>
                <td>
                    {!! ($alumno->curso_actual && strtotime($alumno->curso_actual->date_to) < time())? '<strong>('.date('Y',strtotime($alumno->curso_actual->date_from)).')</strong>' : '' !!}
                    {{ ($alumno->curso_actual)? $alumno->curso_actual->name : '--' }}
                </td>
                <td>
                    {!! Form::open(['route' => ['alumno.destroy', $alumno->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{{ route('alumno.show', [$alumno->id]) }}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                        <a href="{{ route('alumno.edit', [$alumno->id]) }}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                        {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
