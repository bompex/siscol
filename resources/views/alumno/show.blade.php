@extends('layouts.app')
@section('content_header')
        <h1>
            {!! trans('page.model.alumno') !!}
        </h1>
@endsection
@section('content')
    <div class="row">
        <div class="col-xs-12">
        <div class="box box-primary">
            <div class="box-body">
                <div class="row" style="padding-left: 20px">
                    @include('alumno.show_fields')
                    <a href="{{ route('alumno.index') }}" class="btn btn-default">{!! trans('page.button.back') !!}</a>
                </div>
            </div>
        </div>
    </div>
    </div>
@endsection
