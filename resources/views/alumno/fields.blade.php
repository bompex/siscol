
<!-- Firstname Field -->
<div class="form-group col-sm-6">
    {!! Form::label('firstname', trans('page.label.user.name')) !!}
    {!! Form::text('firstname', null, ['class' => 'form-control']) !!}
</div>

<!-- Lastname Field -->
<div class="form-group col-sm-6">
    {!! Form::label('lastname', trans('page.label.user.lastname')) !!}
    {!! Form::text('lastname', null, ['class' => 'form-control']) !!}
</div>

<!-- Dni Field -->
<div class="form-group col-sm-6">
    {!! Form::label('dni', trans('page.label.user.dni')) !!}
    {!! Form::text('dni', null, ['class' => 'form-control']) !!}
</div>

<!-- Birthdate Field -->
<div class="form-group col-sm-6">
    {!! Form::label('birthdate', trans('page.label.user.birthdate')) !!}
    {!! Form::date('birthdate', ($alumno->birthdate!= '')? date('Y-m-d',strtotime($alumno->birthdate)) : null
        , ['class' => 'form-control','id'=>'birthdate']) !!}
</div>


<div class="form-group col-sm-6">
    {!! Form::label('gender', trans('page.label.user.gender')) !!}
    {{ Form::select('gender', $genders, $alumno->gender ? $alumno->gender : 'm', ['class' => 'form-control', 'id'=>'gender']) }}
</div>


<!-- Telefono Field -->
<div class="form-group col-sm-6">
    {!! Form::label('telefono', trans('page.label.user.phone')) !!}
    {!! Form::text('telefono', null, ['class' => 'form-control']) !!}
</div>

<!-- Email Field -->
<div class="form-group col-sm-6">
    {!! Form::label('email', trans('page.label.user.email')) !!}
    @if(!$alumno->id || $alumno->id == '')
    {!! Form::email('email', $alumno->user->email, ['class' => 'form-control']) !!}
    @else
    {!! Form::email('email', $alumno->user->email, ['class' => 'form-control', 'readonly'=>true]) !!}
    @endif
</div>

<!-- Password Field -->
<div class="form-group col-sm-6">
    {!! Form::label('password', trans('page.label.user.password')) !!}
    {{ Form::password('password', ['class' => 'form-control']) }}
</div>

<!-- Password Field -->
<div class="form-group col-sm-6">
    {!! Form::label('password_confirmation', trans('page.label.user.password_confirm')) !!}
    {{ Form::password('password_confirmation', ['class' => 'form-control']) }}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit(trans('page.button.save'), ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('alumno.index') }}" class="btn btn-default">{!! trans('page.button.cancel') !!}</a>
</div>


@section('scripts')
    <script type="text/javascript">
        $('#birthdate').datetimepicker({
            format: 'YYYY-MM-DD HH:mm:ss',
            useCurrent: false
        })
        $('#gender').select2();
    </script>
@endsection