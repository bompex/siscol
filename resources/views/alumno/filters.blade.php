<div>
    
    
    <div class="col-r col-md-3 col-xs-12 input-filtro">
        <label>{{ trans('page.model.curso') }}:</label>
        {{ Form::select('curso_id', $cursos, isset($curso_id) ? $curso_id : null, [
            'placeholder' => 'Todos' ,
            'id' => 'curso_id',
            'class' => 'form-control'
        ]) }}
    </div>

    <div class="col col-md-3 col-xs-12 input-filtro filt-btn">
        <div class="col col-md-6 col-xs-6">
            {!! Form::submit(trans('page.button.search'), ['class' => 'btn btn-success']) !!}
        </div>
        <div class="col col-md-6 col-xs-6">
            {!! Form::submit(trans('page.button.download_xls'), ['class' => 'btn btn-primary', 'name' => 'download-xls']) !!}
        </div>
    </div>
</div>

<div class="clearfix"></div>


<script type="text/javascript">
    $(function () {
        $("#search-btn").click(function () {
            $('#filter-form').submit();
        });

    });

</script>