@extends('layouts.app')
@section('content_header')
    <h1 class="pull-left">{!! trans('page.model.plural.alumno') !!}</h1>
    <h1 class="pull-right">
       <a class="btn btn-primary pull-right" style="margin-top: -4px;" href="{{ route('alumno.create') }}">{!! trans('page.button.add') !!} {!! trans('page.button.new') !!}</a>
    </h1>
@endsection
@section('content')
    <div class="row">
        <div class="col-xs-12">
            @include('flash::message')
            {{ Form::open(['route' => 'alumno.index', 'method' => 'get', 'id'=>'filter-form']) }}
            <div class="box box-primary">
                <div class="box-body">
                    <div class="filters">
                        @include('alumno.filters')               
                    </div>
                </div>
            </div>
            {{ Form::close() }}

            <div class="clearfix"></div>
            <div class="box box-primary">
                <div class="box-body">
                        @include('alumno.table')
                </div>
            </div>
            <div class="text-center">
            
            </div>
        </div>
    </div>
@endsection

