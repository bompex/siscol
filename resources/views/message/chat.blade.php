<div class="chat-wrapper col-xs-12 ">
	@if(isset($messages) && count($messages)>0)
	@foreach($messages as $message)
		<div class="{{ ($message->is_own($user->id) )? 'own-message' : 'other-message' }}">
			
			<div class="message-body">
				{!! $message->body !!}

				@if(isset($message->files) && count($message->files)>0)
		    		@foreach($message->files as $file)
			            <div><a href='{!! $file->path !!}' target='blank'>{{ $file->name }}</a></div>
			        @endforeach
		    	@endif

			</div>
			
			<div class="message-foot">
				<span>{{ date('d/m H:i', strtotime($message->created_at)) }}</span>
				<span></span>
			</div>
		</div>
		<div class="clearfix"></div>
		@php
			$message->mark_readed();
		@endphp
	@endforeach
	@else
		<div class="message-empty">
			<p>No hay ningun mensaje</p>
		</div>
	@endif

</div>
