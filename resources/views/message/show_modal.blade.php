@extends('layouts.modal')
@section('modal-title')
	<h4>Mensajes: {{ $user_receiver->lastname }}, {{ $user_receiver->firstname }}{!! ($user_receiver->isAlumno())? " (".$user_receiver->alumno->curso_actual->name.")" : "" !!}</h4>
@endsection
@section('modal-content')
	<div class="modal-body">
        @include('message.chat')
	</div>
	<div class="modal-footer" style="overflow: hidden;">
	    {!! Form::open(['route' => ['message.store', $user_receiver->id], 'id' => 'message-form-modal' ]) !!}
            @include('message.create')
        {!! Form::close() !!}
	</div>

@endsection


