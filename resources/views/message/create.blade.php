<div class="form-group col-md-12">
    {!! Form::textarea('body', $message, ['class' => 'form-control', 'id'=>'body']) !!}
</div>
<div class="clearfix"></div>
<div class="form-group col-md-12">
    <div class="file-list">              
    </div>
</div>
<div class="clearfix"></div>

<div class="form-group col-md-12">
    {!! Form::submit(trans('page.button.send'), ['class' => 'btn btn-primary btn-ssm']) !!}
    <label for="file_uploader" class="btn btn-default btn-ssm custom-file-upload input-font">Subir Archivo</label>
    <div class="progress progress-xs hidden">
     <div class="progress-bar progress-bar-danger" style="width: 0%"></div>
   </div>
    {{ Form::file('file_uploader',['class' => 'form-control file_uploader hidden', 'id' => 'file_uploader', 'name'=>'file_uploader']) }}
    <span id='upload_file_message' class="invalid-feedback error"><strong></strong></span>  
</div>


