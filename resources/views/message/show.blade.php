@extends(($cu_user->isProfesor()) ? 'adminlte::page_profesor' : 'adminlte::page_alumno')
@section('content_header')
<h1 class="pull-left">{{ $user_receiver->lastname }}, {{ $user_receiver->firstname }} </h1>
<h1 class="pull-right">
   <a class="btn btn-primary btn-sm"  href="{!! $redir_back !!}">{!! trans('page.button.back') !!}</a>
</h1>
@endsection
@section('content')
    <div class="row">
        <div class="col-xs-12 col-md-12">
        <div class="box box-primary">
            <div class="box-body">
                <div class="" style="">
                    @include('message.chat')
                </div>
                <div class="clearfix"></div>
                <div class="" style="margin-top: 10px">
                    {!! Form::open(['route' => ['message.store', $user_receiver->id] ]) !!}
                        @include('message.create')
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection


@section('adminlte_js')
<script>
    var file_upload_url  = "{{ route('file.upload') }}";

    function initUploadConfig() {
        config = {
            type: 'POST',
            url: file_upload_url,
            dataType: 'json',

            add: function (e, data) {
                $('.progress .progress-bar').css('width', '0%');
                $('.progress').removeClass('hidden');
                data.submit();
            },
            done: function (e, data) {

                if(data.result.error){
                    $('#upload_file_message').html(data.result.error);
                }else{
                    var file_row = "<div class='file-row'><div><a href='"+data.result.path+"' target='blank'>"+data.result.name+"</a></div><input type='hidden' name='files[]' value='"+data.result.file_id+"' class='input-file' /><a href='#' onclick='delete_file(this)'><i class='glyphicon glyphicon-trash'></i></a></div>"
                    $('.file-list').append(file_row);
                }
                $('.progress').addClass('hidden');
            },
            progressall: function (e, data) {
                $('#upload_file_message').html('');
                var progress = parseInt(data.loaded / data.total * 100, 10);
                $('.progress .progress-bar').css('width', progress + '%');
            }
        };
        return config;
    }

    function delete_file(elem){
        var row = $(elem).parents('.file-row');
        row.hide();
        row.find('.input-file').attr('name', 'files_del[]');
    }

    $(function () {
        $('.file_uploader').fileupload(initUploadConfig()).prop('disabled', !$.support.fileInput)
            .parent().addClass($.support.fileInput ? undefined : 'disabled');

            // extraPlugins needs to be set too.
            CKEDITOR.replace( 'body',{plugins:'emoji,wysiwygarea,toolbar',height: 60});

    });
    $(function () {
        
        var chat = $('.chat-wrapper');
        chat.scrollTop(chat[0].scrollHeight);

    });
</script>
@endsection