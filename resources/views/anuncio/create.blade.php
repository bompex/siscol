@extends('adminlte::page_profesor')
@section('content_header')
    <h1>
        {!! trans('page.model.anuncio') !!}
    </h1>
@endsection
@section('content')
    <div class="row">
        <div class="col-xs-12">
        @include('adminlte-templates::common.errors')
        <div class="box box-primary">
            <div class="box-body">
                <div class="row">
                    {!! Form::open(['route' => ['anuncio.store', $cursoMateria->id]]) !!}

                        @include('anuncio.fields')

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
