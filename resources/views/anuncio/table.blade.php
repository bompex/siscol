<div class="table-responsive">
    <table class="table datatable" id="evaluacion-table">
        <thead>
            <tr>
                <th>{!! trans('page.label.anuncio.subject') !!}</th>
                <th>{!! trans('page.label.anuncio.fecha') !!}</th>
                <th>{!! trans('page.label.anuncio.fecha_inicio') !!}</th>
                <th>{!! trans('page.label.anuncio.fecha_fin') !!}</th>
                <th class='no-sort'>&nbsp;</th>
            </tr>
        </thead>
        <tbody>
        @foreach($anuncios as $anuncio)
            <tr>
                <td>{{ $anuncio->subject }}</td>
                <td>{{ date('d/m/Y H:i', strtotime($anuncio->fecha)) }}</td>
                <td>{{ ($anuncio->fecha_inicio)? date('d/m/Y H:i', strtotime($anuncio->fecha_inicio)) : "--" }}</td>
                <td>{{ ($anuncio->fecha_fin)? date('d/m/Y H:i', strtotime($anuncio->fecha_fin)) : "--" }}</td>
                <td>
                    <div class='btn-group'>
                        <a href="{{ route('anuncio.edit', [$cursoMateria->id, $anuncio->id]) }}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                        {!! Form::open(['route' => ['anuncio.remove', $cursoMateria->id, $anuncio->id], 'method' => 'delete']) !!}
                                
                        {!! Form::button('<span ><i class="glyphicon glyphicon-trash"></i></span>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Seguro desea Borrar?')"]) !!}
                        
                        {!! Form::close() !!}
                    </div>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
