@extends('adminlte::page_profesor')
@section('content_header')
        <h1 class="pull-left">{!! trans('page.model.plural.anuncio') !!}</h1>
        <h1 class="pull-right">
           <a class="btn  btn-success"  style="margin-right: 10px" href="{{ route('anuncio.create',[$cursoMateria->id]) }}">{!! trans('page.button.add') !!} {!! trans('page.button.new') !!}</a>
           <a class="btn btn-primary btn-sm" href="{{ route('profesor_curso_materia.index',$cursoMateria->id) }}">{!! trans('page.button.back') !!}</a>
        </h1>
@endsection
@section('content')
    <div class="row">
        <div class="col-xs-12">
        <div class="clearfix"></div>

        @include('flash::message')

        <div class="clearfix"></div>
        <div class="box box-primary">
            <div class="box-body">
                    @include('anuncio.table')
            </div>
        </div>
        <div class="text-center">
        
        </div>
    </div>
</div>
@endsection
