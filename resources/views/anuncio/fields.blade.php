
<div class="form-group col-md-12">
    {!! Form::label('subject', trans('page.label.anuncio.subject')) !!}
    {!! Form::text('subject', $anuncio->subject, ['class' => 'form-control']) !!}
</div>
<div class="clearfix"></div>

<div class="form-group col-md-12">
    {!! Form::label('body', trans('page.label.anuncio.body')) !!}
    {!! Form::textarea('body', $anuncio->body, ['class' => 'form-control']) !!}
</div>
<div class="clearfix"></div>
<div class="form-group col-sm-12">
    <div class="form-check">
        <input type="checkbox" class="form-check-input active_fechas" id="active_fechas" {!! ($anuncio->fecha_inicio!= '')? 'checked="checked"' : '' !!} >
        <label class="form-check-label" for="active_fechas">Agregar Fecha</label>
    </div>
    <div class="clearfix"></div>
    <div class="col-sm-6 fechas_fields">
            {!! Form::label('fecha_inicio_date', trans('page.label.anuncio.fecha_inicio'), ['class' => 'label-evaluacion']) !!}
            {!! Form::date('fecha_inicio_date', ($anuncio->fecha_inicio!= '')? date('Y-m-d',strtotime($anuncio->fecha_inicio)) : null, ['class' => 'form-control date-evaluacion','id'=>'fecha_inicio' ]) !!} 

            {!! Form::text('fecha_inicio_time', ($anuncio->fecha_inicio!= '')? date('H:i',strtotime($anuncio->fecha_inicio)) : '00:00', ['class' => 'form-control timepicker time-evaluacion']) !!}
    </div>
    <div class="form-group col-sm-6  fechas_fields">
        {!! Form::label('fecha_fin_date', trans('page.label.anuncio.fecha_fin'), ['class' => 'label-evaluacion']) !!}
            {!! Form::date('fecha_fin_date', ($anuncio->fecha_fin!= '')? date('Y-m-d',strtotime($anuncio->fecha_fin)) : null, ['class' => 'form-control date-evaluacion','id'=>'fecha_fin']) !!} 
            {!! Form::text('fecha_fin_time', ($anuncio->fecha_fin!= '')? date('H:i',strtotime($anuncio->fecha_fin)) : '00:00', ['class' => 'form-control timepicker time-evaluacion']) !!}
    </div>
</div>
<div class="clearfix"></div>
<div class="form-group col-md-12">
    <div class="file-list">
        @foreach($anuncio->files as $file)
            <div class='file-row'><div><a href='{!! $file->path !!}' target='blank'>{{ $file->name }}</a></div><input type='hidden' name='files[]' value='{{ $file->id }}' class='input-file' /><a href='#' onclick='delete_file(this)'><i class='glyphicon glyphicon-trash'></i></a></div>
        @endforeach
        
    </div>
</div>
<div class="clearfix"></div>

<div class="form-group col-md-6">
    <label for="file_uploader" class="btn btn-default custom-file-upload input-font">Subir Archivo</label>
    <div class="progress progress-xs hidden">
     <div class="progress-bar progress-bar-danger" style="width: 0%"></div>
   </div>
    {{ Form::file('file_uploader',['class' => 'form-control file_uploader hidden', 'id' => 'file_uploader', 'name'=>'file_uploader']) }}
    <span id='upload_file_message' class="invalid-feedback error"><strong></strong></span>   
</div>
<div class="clearfix"></div>

<div class="clearfix"></div>
<div class="form-group col-sm-12">
    <div class="col-sm-6">
            {!! Form::label('fecha_inicio_date', trans('page.label.anuncio.fecha'), ['class' => 'label-evaluacion']) !!}
            {!! Form::date('fecha_date', ($anuncio->fecha!= '')? date('Y-m-d',strtotime($anuncio->fecha)) : date('Y-m-d',time()), ['class' => 'form-control date-evaluacion','id'=>'fecha']) !!} 

            {!! Form::text('fecha_time',($anuncio->fecha!= '')? date('H:i',strtotime($anuncio->fecha)) : date('H:i',time()), ['class' => 'form-control timepicker time-evaluacion']) !!}
    </div>
</div>
<!-- Submit Field -->
<div class="form-group col-md-12">
    {!! Form::submit(trans('page.button.publish'), ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('anuncio.index',$cursoMateria->id) }}" class="btn btn-default">{!! trans('page.button.cancel') !!}</a>
</div>


@section('adminlte_js')
<script>
	var file_upload_url  = "{{ route('file.upload') }}";

    function initUploadConfig() {
        config = {
            type: 'POST',
            url: file_upload_url,
            dataType: 'json',

            add: function (e, data) {
                $('.progress .progress-bar').css('width', '0%');
                $('.progress').removeClass('hidden');
                data.submit();
            },
            done: function (e, data) {

                if(data.result.error){
                    $('#upload_file_message').html(data.result.error);
                }else{
                    var file_row = "<div class='file-row'><div><a href='"+data.result.path+"' target='blank'>"+data.result.name+"</a></div><input type='hidden' name='files[]' value='"+data.result.file_id+"' class='input-file' /><a href='#' onclick='delete_file(this)'><i class='glyphicon glyphicon-trash'></i></a></div>"
                    $('.file-list').append(file_row);
                }
                $('.progress').addClass('hidden');
            },
            progressall: function (e, data) {
                $('#upload_file_message').html('');
                var progress = parseInt(data.loaded / data.total * 100, 10);
                $('.progress .progress-bar').css('width', progress + '%');
            }
        };
        return config;
    }

    function delete_file(elem){
        var row = $(elem).parents('.file-row');
        row.hide();
        row.find('.input-file').attr('name', 'files_del[]');
    }

    $(function () {
        $('.file_uploader').fileupload(initUploadConfig()).prop('disabled', !$.support.fileInput)
            .parent().addClass($.support.fileInput ? undefined : 'disabled');

            // extraPlugins needs to be set too.
            var editor = CKEDITOR.replace( 'body',{language: 'es',resize_dir: 'vertical',removeButtons:'',filebrowserBrowseUrl: '',});

            CKFinder.setupCKEditor( editor, null, { type: 'Files'} );

        $('#active_fechas').change(function(){
            if( $(this).prop('checked') ) {
                $('.fechas_fields').show();
                $('.fechas_fields input').removeAttr('disabled');
            }else{
                $('.fechas_fields').hide();
                $('.fechas_fields input').attr('disabled',true);
            }
        });
        if( $('#active_fechas').prop('checked') ) {
            $('.fechas_fields').show();
            $('.fechas_fields input').removeAttr('disabled');
        }else{
            $('.fechas_fields').hide();
            $('.fechas_fields input').attr('disabled',true);
        }

    });
</script>
@endsection