@extends('layouts.modal')
@section('modal-title')
	<h4>{!! trans('page.model.anuncio') !!}</h4>
@endsection
@section('modal-content')
	<div class="modal-body">
		<div class="row">
			<div class="col-md-6">
				<label>Asunto:</label>	
				<p>{{ $anuncio->subject }}</p>
			</div>
            <div class="clearfix"></div>
			<div class="col-md-6">
				<label>Mensaje:</label>	
				<p>{!! $anuncio->body !!}</p>
			</div>
            <div class="clearfix"></div>
            @if($anuncio->files && count($anuncio->files) > 0 )
            <div class='col-md-12'>
                <label>Archivo Adjunto</label>
                @foreach($anuncio->files as $file)
                <p><a href='{!! $file->path !!}' target='blank'>{{ $file->name }}</a></p>
                @endforeach
            </div>
            @endif
		</div>

        

	</div>
	<div class="modal-footer">
	    <a href="#" class="btn btn-default" data-dismiss="modal">Cerrar</a>
	</div>

@endsection

