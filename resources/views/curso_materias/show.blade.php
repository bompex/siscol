@extends('layouts.app')
@section('content_header')
    <h1>
            Curso Materia
        </h1>
@endsection
@section('content')
    <div class="row">
        <div class="col-xs-12">
        <div class="box box-primary">
            <div class="box-body">
                <div class="row" style="padding-left: 20px">
                    @include('curso_materias.show_fields')
                    <a href="{{ route('cursoMaterias.index') }}" class="btn btn-default">Back</a>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
