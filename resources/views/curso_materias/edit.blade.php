@extends('layouts.app')
@section('content_header')
    <h1>
            Curso Materia
        </h1>
@endsection
@section('content')
   <div class="row">
        <div class="col-xs-12">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($cursoMateria, ['route' => ['cursoMaterias.update', $cursoMateria->id], 'method' => 'patch']) !!}

                        @include('curso_materias.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
 </div>
@endsection