<!-- Curso Id Field -->
<div class="form-group">
    {!! Form::label('curso_id', 'Curso Id:') !!}
    <p>{{ $cursoMateria->curso_id }}</p>
</div>

<!-- Profesor Id Field -->
<div class="form-group">
    {!! Form::label('profesor_id', 'Profesor Id:') !!}
    <p>{{ $cursoMateria->profesor_id }}</p>
</div>

<!-- Materia Id Field -->
<div class="form-group">
    {!! Form::label('materia_id', 'Materia Id:') !!}
    <p>{{ $cursoMateria->materia_id }}</p>
</div>

<!-- Created By Field -->
<div class="form-group">
    {!! Form::label('created_by', 'Created By:') !!}
    <p>{{ $cursoMateria->created_by }}</p>
</div>

<!-- Updated By Field -->
<div class="form-group">
    {!! Form::label('updated_by', 'Updated By:') !!}
    <p>{{ $cursoMateria->updated_by }}</p>
</div>

