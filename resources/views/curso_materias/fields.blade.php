<!-- Curso Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('curso_id', 'Curso Id:') !!}
    {!! Form::number('curso_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Profesor Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('profesor_id', 'Profesor Id:') !!}
    {!! Form::number('profesor_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Materia Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('materia_id', 'Materia Id:') !!}
    {!! Form::number('materia_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Created By Field -->
<div class="form-group col-sm-6">
    {!! Form::label('created_by', 'Created By:') !!}
    {!! Form::number('created_by', null, ['class' => 'form-control']) !!}
</div>

<!-- Updated By Field -->
<div class="form-group col-sm-6">
    {!! Form::label('updated_by', 'Updated By:') !!}
    {!! Form::number('updated_by', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('cursoMaterias.index') }}" class="btn btn-default">Cancel</a>
</div>
