<div class="table-responsive">
    <table class="table" id="cursoMaterias-table">
        <thead>
            <tr>
                <th>Curso Id</th>
        <th>Profesor Id</th>
        <th>Materia Id</th>
        <th>Created By</th>
        <th>Updated By</th>
                <th colspan="3">Action</th>
            </tr>
        </thead>
        <tbody>
        @foreach($cursoMaterias as $cursoMateria)
            <tr>
                <td>{{ $cursoMateria->curso_id }}</td>
            <td>{{ $cursoMateria->profesor_id }}</td>
            <td>{{ $cursoMateria->materia_id }}</td>
            <td>{{ $cursoMateria->created_by }}</td>
            <td>{{ $cursoMateria->updated_by }}</td>
                <td>
                    {!! Form::open(['route' => ['cursoMaterias.destroy', $cursoMateria->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{{ route('cursoMaterias.show', [$cursoMateria->id]) }}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                        <a href="{{ route('cursoMaterias.edit', [$cursoMateria->id]) }}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                        {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
