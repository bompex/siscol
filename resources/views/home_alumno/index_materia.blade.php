@extends('adminlte::page_alumno')

@section('content_header')

<h1 class="pull-left">{!! trans('page.model.curso') !!}: {{ $cursoMateria->curso->name }} - {{ $cursoMateria->materia->name }} </h1>
<h1 class="pull-right">
    <a class="btn btn-success btn-sm open_message_modal" href="javascript:;" data-href="{{ route('message.show', [$cursoMateria->profesor->user->id, 'mode' => 'modal']) }}"  title="Enviar Mensaje"><span class="hidden-xs">Enviar Mensaje</span><span class="visible-xs"><i class="glyphicon glyphicon-comment"></i></span></a>
   <a class="btn btn-primary btn-sm"  href="{{ route('home.alumno') }}">{!! trans('page.button.back') !!}</a>
</h1>

@endsection

@section('content')
<div class="clearfix"></div>
    @include('flash::message')
<div class="row">
    <div class="col-md-6  col-xs-12">
        @include('home_alumno.foro')

        @include('home_alumno.evaluacion')

        @include('home_alumno.evaluacion_prev')
    </div>
    <div class="col-md-6  col-xs-12">
    	@include('home_alumno.horario_materia')    
        @include('home_alumno.anuncio') 
        @include('home_alumno.calendar')    
    </div>
    <div class="clearfix"></div>
</div>
@endsection

