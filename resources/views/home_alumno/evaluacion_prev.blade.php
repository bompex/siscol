<div class="panel panel-info">
    <div class="panel-heading">{!! trans('page.model.plural.evaluacion'.$cursoMateria->curso->primario) !!} Realizadas
    </div>
    <div class="panel-body">
      <ul class='foro-teaser'>
        @if(count($evaluaciones_prev) >0)
          @foreach($evaluaciones_prev as $evaluacion)
            <li class="{{ ($evaluacion->type_id == 2 && count($alumno->entrega($evaluacion->id)) > 0)? 'class-entregado' : '' }} {{ ($evaluacion->type_id == 2 && count($alumno->entrega($evaluacion->id)) == 0)? 'class-pendiente-entrega' : '' }}">
              <div class="col-md-12 fr-subject"><a href="{{ route('evaluacion_alumno.show', $evaluacion->id) }}">{{ $evaluacion_types[$evaluacion->type_id] }}: {{ $evaluacion->subject }}</a> 
              @if($evaluacion->type_id == 2 && count($alumno->entrega($evaluacion->id)) > 0)
                (ENTREGADO)
              @elseif($evaluacion->type_id == 2 && count($alumno->entrega($evaluacion->id)) == 0)
                (PENDIENTE)
              @endif
              </div>
              <div class=' fr-info'>
                @if( date('Y-m-d', strtotime($evaluacion->fecha_inicio)) == date('Y-m-d', strtotime($evaluacion->fecha_fin)))
                  <div class="col-sm-5 fr-date">{{ date('d/m/Y', strtotime($evaluacion->fecha_inicio)) }} de {{ date('H:i', strtotime($evaluacion->fecha_inicio)) }} a {{ date('H:i', strtotime($evaluacion->fecha_fin)) }} </div>
                @else
                  <div class="col-sm-5 fr-date">{{ date('d/m/Y H:i', strtotime($evaluacion->fecha_inicio)) }} al {{ date('d/m/Y H:i', strtotime($evaluacion->fecha_fin)) }} </div>
                @endif
                
                
                <div class="col-sm-5 fr-author">Por: {{ $evaluacion->profesor->lastname }}, {{ $evaluacion->profesor->firstname }}</div>
                @if($alumno->calificacion($evaluacion->id))
                <div class="col-sm-2 fr-nota">Nota:{{ $alumno->calificacion($evaluacion->id)->nota }}</div>
                @endif
                <div class="clearfix"></div>
              </div>
              <div class="clearfix"></div>
            </li>   
          @endforeach
        @else
        <li>
          <div class="col-md-12 fr-no-message">{!! trans('page.message.no_message') !!}</div>
          <div class="clearfix"></div>
        </li>  
        @endif    
      </ul>
      <div class='col-md-12  text-center'><a class="btn btn-primary btn-ssm text-center"  href="{{ route('evaluacion_alumno.index', $cursoMateria->id) }}">Ver todas las {!! trans('page.model.plural.evaluacion'.$cursoMateria->curso->primario) !!}</a></div>
    </div>
</div>

