<div class="panel panel-info">
    <div class="panel-heading">{!! trans('page.model.plural.anuncio') !!}
    </div>
    <div class="panel-body">
      <ul class='foro-teaser'>
        @if(count($anuncios) >0)
          @foreach($anuncios as $anuncio)
            <li class='row'>

              <div class="col-md-12 fr-subject"><a class="open_message_modal"  href="javascript:;" data-href="{{ route('anuncio.show', [$anuncio->id]) }}">{{ $anuncio->subject }}</a></div>
              <div class='col-md-12 fr-info row'>
                <div class="col-sm-4 col-xs-6 fr-author">{{ $anuncio->cursoMateria->materia->name }}</div>
                <div class="col-sm-4 col-xs-6 fr-date">{{ date('d/m/Y H:i', strtotime($anuncio->fecha)) }}</div>
                <div class="clearfix"></div>
              </div>
              <div class="clearfix"></div>
            </li>   
          @endforeach
        @else
        <li  class='row'>
          <div class="col-md-12 fr-no-message">{!! trans('page.message.no_message') !!}</div>
          <div class="clearfix"></div>
        </li>  
        @endif    
      </ul>
      
    </div>
</div>
