<div class="panel panel-info">
    <div class="panel-heading">{!! trans('page.model.plural.topic') !!}
    </div>
    <div class="panel-body">
    	<ul class='foro-teaser'>
    		@if(count($topics) >0)
    			@foreach($topics as $topic)
		  			<li>
		  				<div class="col-md-12 fr-subject"><a href="{{ route('curso_materia_foro.show_topic', $topic->id) }}">{{ $topic->subject }}</a></div>
		  				<div class='col-md-12 fr-info'>
		  					<div class="col-sm-4 fr-comments"><span><i class="glyphicon glyphicon-comment"></i>&nbsp; 0</span></div>
			  				<div class="col-sm-4 fr-date">{{ date('d/m/Y H:i:s', strtotime($topic->created_at)) }}</div>
			  				<div class="col-sm-4 fr-author">Por: {{ $topic->user->lastname }}, {{ $topic->user->firstname }}</div>
			  				<div class="clearfix"></div>
			  			</div>
		  				<div class="clearfix"></div>
		  			</li>  	
  				@endforeach
  			@else
  			<li>
  				<div class="col-md-12 fr-no-message">{!! trans('page.message.no_message') !!}</div>
  				<div class="clearfix"></div>
  			</li>  
  			@endif		
    	</ul>
      <div class='col-md-12  text-center'><a class="btn btn-primary btn-ssm text-center"  href="{{ route('curso_materia_foro.index', $cursoMateria->id) }}">Ver todas los {!! trans('page.model.plural.topic') !!}</a></div>
    </div>
</div>



