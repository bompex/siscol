@extends('adminlte::page_alumno')

@section('content_header')


<h1 class="pull-left">{!! trans('page.model.curso') !!} Año Anterior: {{ $curso->name }} </h1>

@endsection

@section('content')
<div class="clearfix"></div>
    @include('flash::message')
<div class="row"> 
	<div class="col-md-3 col-xs-12">
    	<div class="panel panel-info">
		    <div class="panel-heading">Materias</div>
		    <div class="panel-body">
		    	<ul class="sidebar-menu sidebar-cursos" data-widget="tree">
                    @foreach($curso->cursoMaterias as $cuMa)
                    <li class="profesor-sidebar">
                        <a href="{{ route('home_alumno.index_materia',$cuMa->id) }}" style="background-color:{{ $cuMa->materia->color  }}">
                            <span>{{ $cuMa->materia->name }}</span>
                        </a>
                    </li>
                    @endforeach
                </ul>
		    </div>
		</div>   
    </div>
    <div class="col-md-6 col-xs-12">
	    @include('home_alumno.previo_horario')  
    </div>
    <div class="col-md-6  col-xs-12"> 
        @include('home_alumno.anuncio')         
    </div>
    <div class="clearfix"></div>
</div>




@endsection