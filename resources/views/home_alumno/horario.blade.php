<div class="panel panel-info">
    <div class="panel-heading">Horario</div>
    <div class="panel-body">
    	<div id='alumno_schedule'></div>

    </div>
</div>
<script>
(function ($) {
      $("#alumno_schedule").dayScheduleSelector({

        days: [1,2,3,4,5],
        stringDays  : ['Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes'],
        @if($alumno->curso->turno == 'mañana')
            structure : ['07:35','08:35','09:35','09:50','10:50','11:50','12:00','13:00'],
            turno : 'mañana'
        @else
            structure : ['13:00','13:50','14:00','14:50','15:40','15:55','16:45','17:30'],
            turno : 'tarde'
        @endif
      });
      $("#alumno_schedule").data('artsy.dayScheduleSelector').deserialize(
      	{!! json_encode($alumno->curso->getFullSchedule($alumno->gender)) !!}
      	);

      $('#alumno_schedule td.time-slot.data-selected:has(i)').on( "click", function() {
          var id = $(this).data('cmh_id');
            $.ajax({
              url: "{{ route('home_alumno.show_horario_note', '') }}/"+id,
              type : 'GET',
            success : function(data) {
              $('#ventana-modal').html(data);
              $('#ventana-modal').modal('show');  
              CKEDITOR.replace( 'cmh_note',{plugins:'emoji,basicstyles,link,wysiwygarea,toolbar,image,colorbutton,list,resize',resize_dir: 'vertical'});

            },

            // código a ejecutar si la petición falla;
             error : function(xhr, status) {
              console.log(status);
                  alert('Disculpe, existió un problema');
              },
          });
      })
      
    })($);

</script>