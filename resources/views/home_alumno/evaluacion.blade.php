<div class="panel panel-info">
    <div class="panel-heading">Próximas {!! trans('page.model.plural.evaluacion'.$cursoMateria->curso->primario) !!} 
    </div>
    <div class="panel-body">
      <ul class='foro-teaser'>
        @if(count($evaluaciones) >0)
          @foreach($evaluaciones as $evaluacion)
            <li class="{{ ($evaluacion->type_id == 2 && count($alumno->entrega($evaluacion->id)) > 0)? 'class-entregado' : '' }} {{ ($evaluacion->type_id == 2 && count($alumno->entrega($evaluacion->id)) == 0  && strtotime($evaluacion->fecha_inicio) <= time())? 'class-a-entregar' : '' }}">
              <div class="col-md-12 fr-subject"><a href="{{ route('evaluacion_alumno.show', $evaluacion->id) }}">{{ $evaluacion_types[$evaluacion->type_id] }}: {{ $evaluacion->subject }}</a> 
              @if($evaluacion->type_id == 2 && count($alumno->entrega($evaluacion->id)) > 0)
                (ENTREGADO)
              @elseif($evaluacion->type_id == 2 && count($alumno->entrega($evaluacion->id)) == 0  && strtotime($evaluacion->fecha_inicio) <= time())
                (PENDIENTE)
              @endif
              </div>
              <div class='col-md-12 fr-info'>
                @if( date('Y-m-d', strtotime($evaluacion->fecha_inicio)) == date('Y-m-d', strtotime($evaluacion->fecha_fin)))
                  <div class="col-sm-6 fr-date">{{ date('d/m/Y', strtotime($evaluacion->fecha_inicio)) }} de {{ date('H:i', strtotime($evaluacion->fecha_inicio)) }} a {{ date('H:i', strtotime($evaluacion->fecha_fin)) }} </div>
                @else
                  <div class="col-sm-6 fr-date">{{ date('d/m/Y H:i', strtotime($evaluacion->fecha_inicio)) }} al {{ date('d/m/Y H:i', strtotime($evaluacion->fecha_fin)) }} </div>
                @endif
                
                
                <div class="col-sm-6 fr-author">Por: {{ $evaluacion->profesor->lastname }}, {{ $evaluacion->profesor->firstname }}</div>
                <div class="clearfix"></div>
              </div>
              <div class="clearfix"></div>
            </li>   
          @endforeach
        @else
        <li>
          <div class="col-md-12 fr-no-message">{!! trans('page.message.no_message') !!}</div>
          <div class="clearfix"></div>
        </li>  
        @endif    
      </ul>
    </div>
</div>

