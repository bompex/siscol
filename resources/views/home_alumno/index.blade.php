@extends('adminlte::page_alumno')

@section('content_header')



@endsection

@section('content')
<div class="clearfix"></div>
    @include('flash::message')
<div class="row"> 
    <div class="col-md-6 col-xs-12">
    	@include('home_alumno.horario')    
    </div>
    <div class="col-md-6  col-xs-12"> 
        @include('home_alumno.anuncio')  
        @include('home_alumno.calendar') 
    </div>
    <div class="clearfix"></div>
</div>
@endsection

