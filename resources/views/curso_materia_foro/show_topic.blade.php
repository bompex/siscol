@extends(($cu_user->isProfesor()) ? 'adminlte::page_profesor' : 'adminlte::page_alumno')


@section('content_header')

<h1 class="pull-left">{!! trans('page.model.curso') !!}: {{ $cursoMateria->curso->name }} - {{ $cursoMateria->materia->name }} </h1>
<h1 class="pull-right">
	@if($cu_user->isProfesor())
   		<a class="btn btn-primary btn-sm" href="{{ route('profesor_curso_materia.index', $cursoMateria->id) }}">{!! trans('page.button.back') !!}</a>
   	@elseif($cu_user->isAlumno())
   		<a class="btn btn-primary btn-sm" href="{{ route('home_alumno.index_materia',$cursoMateria->id) }}">{!! trans('page.button.back') !!}</a>
   	@endif
</h1>

@endsection

@section('content')

@include('flash::message')
<div class='row'>
  <div class="col-xs-12 foro">
    @include('curso_materia_foro.topic')
    @if($topic->allow_comments == 1)
      @include('curso_materia_foro.comment_form')
    @endif
  	@include('curso_materia_foro.comment')

      
  </div>
</div>
@endsection


@section('adminlte_js')
<script>

$(function () {
    $('.open_comment_form').click(function(){
        $.ajax({
            url: $(this).data('href'),
            type : 'GET',
          success : function(data) {
            $('#ventana-modal').html(data);
            $('#ventana-modal').modal('show');    
            var editor = CKEDITOR.replace( 'body',{extraPlugins:'div,font',removePlugins: '' ,language: 'es',height:300,resize_dir: 'vertical',removeButtons:'',filebrowserBrowseUrl: '',
              
            });

            CKFinder.setupCKEditor( editor, null, { type: 'Files'} );          
          },

          // código a ejecutar si la petición falla;
           error : function(xhr, status) {
            console.log(status);
                alert('Disculpe, existió un problema');
            },
        });
    });

    $('.citar_message_btn').click(function(e){
      var body = $(this).parents('.fr-comment').find('.comment-body').html();
      var name = $(this).parents('.fr-comment').find('.fr-author').html();
      var quote = '<div style="background-color:#e0e0e0; font-size:11px; padding:10px; font-style: italic;" class="quote-message">Cita de: '+name+'<br />'+body+'</div><hr/><p></p>';
      $.ajax({
            url: $(this).data('href'),
            type : 'GET',
          success : function(data) {
            $('#ventana-modal').html(data);
            $('#ventana-modal').modal('show');    
            var editor = CKEDITOR.replace( 'body',{extraPlugins:'div,font',removePlugins: '' ,language: 'es',height:300,resize_dir: 'vertical',removeButtons:'',filebrowserBrowseUrl: '',
              
            });

            CKFinder.setupCKEditor( editor, null, { type: 'Files'} );      

            CKEDITOR.instances.body.setData(quote);    
          },

          // código a ejecutar si la petición falla;
           error : function(xhr, status) {
            console.log(status);
                alert('Disculpe, existió un problema');
            },
        });
      
    });
});
</script>
@endsection