@extends(($cu_user->isProfesor()) ? 'adminlte::page_profesor' : 'adminlte::page_alumno')

@section('content_header')
        <h1 class="pull-left">{!! trans('page.model.plural.topic') !!}</h1>
        <div class="pull-right">
            @if($cu_user->isProfesor())
               <a class="btn btn-success btn-sm" style="margin-right: 10px" href="{{ route('profesor_curso_materia.create_topic', $cursoMateria->id) }}">{!! trans('page.button.add') !!} {!! trans('page.button.new') !!}</a>
               <a class="btn btn-primary btn-sm" href="{{ route('profesor_curso_materia.index',$cursoMateria->id) }}">{!! trans('page.button.back') !!}</a>
            @elseif($cu_user->isAlumno())
                <a class="btn btn-primary btn-sm" href="{{ route('home_alumno.index_materia',$cursoMateria->id) }}">{!! trans('page.button.back') !!}</a>
            @endif
        </div>
@endsection
@section('content')
    <div class="row">
        <div class="col-xs-12">
        <div class="clearfix"></div>

        @include('flash::message')

        <div class="clearfix"></div>
        <div class="box box-primary">
            <div class="box-body">
                @if($cu_user->isProfesor())
                <div class="pull-left">
                    <a class="btn btn-success btn-sm" href="javascript:sort_topics();">Guardar Orden de Tablones</a>
                </div>
                <div class="clearfix"></div>
                @endif
                    @include('curso_materia_foro.table')
            </div>
        </div>
    </div>
</div>
@endsection

