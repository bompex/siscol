<div class="table-responsive">
    <table class="table datatable topics-table" id="topic-table">
        <thead>
            <tr>
                @if($cu_user->isProfesor())
                <th class='no-sort'>&nbsp;</th>
                @endif
                <th>{!! trans('page.label.topic.subject') !!}</th>
                <th>{!! trans('page.label.topic.status') !!}</th>
                <th>Permite Comentario</th>
                <th>Fecha Creacion</th>
                <th class='no-sort'>&nbsp;</th>
            </tr>
        </thead>
        <tbody>
        @foreach($topics as $topic)
            <tr>
                @if($cu_user->isProfesor())
                <td><span class="ui-icon ui-icon-arrowthick-2-n-s"></span>{!! Form::hidden('order[]', $topic->id) !!}</td>
                @endif
                <td>{{ $topic->subject }}</td>
                <td>{{ ($topic->active == 1)? 'Publicado' : 'Archivado' }}</td>
                <td>{{ ($topic->allow_comments == 1)? 'Si' : 'No' }}</td>
                <td>{{ date('d/m/Y H:i', strtotime($topic->created_at)) }}</td>
                <td>
                    <div class='btn-group'>
                        <a href="{{ route('curso_materia_foro.show_topic', $topic->id) }}" class='btn btn-default btn-xs' title='Ver'><i class="glyphicon glyphicon-eye-open"></i></a>
                        @if($cu_user->isProfesor())
                            <a href="{{ route('profesor_curso_materia.edit_topic', [$cursoMateria->id, $topic->id]) }}" class='btn btn-default btn-xs'  title='Editar'><i class="glyphicon glyphicon-edit"></i></a>
                            @if($topic->active == 1)
                            <a href="{{ route('profesor_curso_materia.archive_topic', [$cursoMateria->id, $topic->id]) }}" class='btn btn-default btn-xs'  title='Archivar'><i class="glyphicon glyphicon-lock"></i></a>
                            @else
                            <a href="{{ route('profesor_curso_materia.publish_topic', [$cursoMateria->id, $topic->id]) }}" class='btn btn-default btn-xs'  title='Publicar'><i class="glyphicon glyphicon-bullhorn"></i></a>
                            @endif

                            {!! Form::open(['route' => ['profesor_curso_materia.remove_topic', $cursoMateria->id, $topic->id], 'method' => 'delete']) !!}
                                
                            {!! Form::button('<span ><i class="glyphicon glyphicon-trash"></i></span>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Seguro desea Borrar?')"]) !!}
                            
                            {!! Form::close() !!}
                        @endif
                    </div>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
@if($cu_user->isProfesor())
<script>
$( function() {
    $( ".topics-table tbody" ).sortable({
      placeholder: "ui-state-highlight"
    });
} );

function sort_topics(){
    var data = $('input[name="order[]"]').serializeArray();
    data.push({"name":"_token", "value": "{{ csrf_token() }}"})
    $.ajax({
        url: "{{ route('profesor_curso_materia.sort_topic', [$cursoMateria->id]) }}",
        type: 'patch',
        data: data,
        success: function(response){
            location.reload();
        }
    });
}
</script>
@endif
