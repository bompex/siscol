<div class="panel panel-default">
    <div class="panel-body">
    	<div class="col-xs-12">
    		<h3>{{ $topic->subject }} {{ ($topic->active == 0)? '(Archivado)' : '' }}</h3>
    	</div>
    	<div class="col-sm-2 fr-metadata row">
    		<div class="col-sm-12 col-xs-12 fr-author">{{ $topic->user->lastname }}, {{ $topic->user->firstname }}</div>
			<div class="col-sm-12 col-xs-4 fr-date">{{ date('d/m/Y', strtotime($topic->created_at)) }}</div>
			<div class="col-sm-12 col-xs-4 fr-horu">{{ date('H:i:s', strtotime($topic->created_at)) }}</div>
			@if($cu_user->isProfesor())
				<div class="col-sm-12  col-xs-4">
					<a class="btn btn-primary btn-xs" href="{{ route('profesor_curso_materia.edit_topic', [$cursoMateria->id, $topic->id]) }}" title="Editar"><span class="hidden-xs">{!! trans('page.button.edit') !!}</span><span class="visible-xs"><i class="glyphicon glyphicon-edit"></i></span></a>&nbsp;
				@if($topic->active == 1)
					<a class="btn btn-danger btn-xs" href="{{ route('profesor_curso_materia.archive_topic', [$cursoMateria->id, $topic->id]) }}"  title="Archivar"><span class="hidden-xs">{!! trans('page.button.archive') !!}</span><span class="visible-xs"><i class="glyphicon glyphicon-lock"></i></span></a>
				@else
					<a class="btn btn-success btn-xs" href="{{ route('profesor_curso_materia.publish_topic', [$cursoMateria->id, $topic->id]) }}"  title="Publicar"><span class="hidden-xs">{!! trans('page.button.publish') !!}</span><span class="visible-xs"><i class="glyphicon glyphicon-bullhorn"></i></span></a>
				@endif
					{!! Form::open(['route' => ['profesor_curso_materia.remove_topic', $cursoMateria->id, $topic->id], 'method' => 'delete']) !!}
                        
                    {!! Form::button('<span class="hidden-xs">Eliminar</span><span class="visible-xs"><i class="glyphicon glyphicon-trash"></i></span>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Seguro desea Borrar?')"]) !!}
                    
                    {!! Form::close() !!}
				</div>
			@else
				<div class="col-sm-12  col-xs-4">
					<a class="btn btn-success btn-xs open_message_modal"  href="javascript:;" data-href="{{ route('message.show', [$topic->user->id, 'mode' => 'modal','reply' => 'topic','reply_id' => $topic->id]) }}"  title="Enviar Mensaje"><span class="hidden-xs">Enviar Mensaje</span><span class="visible-xs"><i class="glyphicon glyphicon-comment"></i></span></a>
				</div>
			@endif
			
		</div>
		<div class="col-sm-10 topic-body">
			
	    	<div>
	    		{!! $topic->body !!}
	    	</div>
	    	@if(isset($topic->files) && count($topic->files)>0)
	    	<div>
	    		<strong>Archivos Adjuntos</strong>
	    		<ul>
		    		@foreach($topic->files as $file)
			            <li><div><a href='{!! $file->path !!}' target='blank'>{{ $file->name }}</a></div></li>
			        @endforeach
			    </ul>
	    	</div>
	    	@endif
		</div>
    	
    </div>
</div>