@extends('layouts.modal')
@section('modal-title')
	<h4>Agregar comentario</h4>
@endsection
@section('modal-content')
	{!! Form::open(['route' => ['foro.store_comment', $topic->id]]) !!}
	{!! Form::hidden('comment_parent_id', $parent_id) !!}
	<div class="modal-body">
	<div class='row'>
		<div class="form-group col-md-12">
			<label>Nuevo Comentario</label>
		    {!! Form::textarea('body', '', ['class' => 'form-control']) !!}
		</div>
		<div class="form-group col-md-12">
		    
		</div>
	</div>
	<div class="modal-footer">
	    {!! Form::submit(trans('page.button.publish'), ['class' => 'btn btn-primary']) !!}
	    <a href="#" class="btn btn-default" data-dismiss="modal">{!! trans('page.button.cancel') !!}</a>
	</div>
	{!! Form::close() !!}

@endsection
