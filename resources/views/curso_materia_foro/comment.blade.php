<div class="panel panel-default">
    <div class="panel-body">
    	@if(isset($comments) && count($comments) > 0)
        	@foreach($comments as $index => $comment)
            	<div class="fr-comment row">
        	    	<div class="col-sm-2 col-xs-12 fr-metadata  row">
        	    		<div class="col-sm-12 col-xs-2 fr-comment-number">#{{  count($comments) - $index }}</div>
        	    		<div class="col-sm-12 col-xs-10 fr-author">{{ $comment->user->lastname }}, {{ $comment->user->firstname }}</div>
        				<div class="col-sm-12 col-xs-6 fr-date">{{ date('d/m/Y', strtotime($comment->created_at)) }}</div>
        				<div class="col-sm-12 col-xs-6 fr-horu">{{ date('H:i:s', strtotime($comment->created_at)) }}</div>
                        <div class="col-sm-12  col-xs-4">
                            <a class="btn btn-success btn-xs citar_message_btn"  href="javascript:;"  title="Citar" data-href="{{ route('foro.create_comment', [$topic->id]) }}"><span class="hidden-xs">Citar</span><span class="visible-xs"><i class="glyphicon glyphicon-share-alt"></i></span></a>
                            @if(!isset($comment->childrens) || count($comment->childrens) == 0)
                            <a class="btn btn-success btn-xs open_comment_form"  href="javascript:;" data-href="{{ route('foro.create_comment', [$topic->id, 'parent_id' => $comment->id]) }}"  title="Agregar Comentario">
                                <span>Agregar Comentario</span></span>
                            </a>
                            
                            @endif
                            @if($cu_user->isProfesor())
                                @if(($comment->user->id != $cu_user->id))
                                    <a class="btn btn-success btn-xs open_message_modal"  href="javascript:;" data-href="{{ route('message.show', [$comment->user->id, 'mode' => 'modal','reply' => 'topic_comment','reply_id' => $comment->id]) }}"  title="Enviar Mensaje"><span class="hidden-xs">Enviar Mensaje</span><span class="visible-xs"><i class="glyphicon glyphicon-comment"></i></span></a>
                                @endif

                                {!! Form::open(['route' => ['foro.remove_comment', $topic->id, $comment->id], 'method' => 'delete']) !!}
                                
                                {!! Form::button('<span class="hidden-xs">Borrar</span><span class="visible-xs"><i class="glyphicon glyphicon-trash"></i></span>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Seguro desea Borrar?')"]) !!}
                                
                                {!! Form::close() !!}
                            @endif

                        </div>
                        
        			</div>
        			<div class="col-sm-10 col-xs-12 comment-body">
        		    		{!! $comment->body !!}
        			</div>
            	
                @if(isset($comment->childrens) && count($comment->childrens) > 0)
                <div class="clearfix"></div>
                <div class="toggle-subcomments">-Ver comentarios-</div>
                <div class="fr-subcomment" style="display:none">
                    @foreach($comment->childrens as $subindex => $subcomment)
                        <div class="fr-comment row">
                            <div class="col-sm-2 col-xs-12 fr-metadata  row">
                                <div class="col-sm-12 col-xs-2 fr-comment-number">#{{  $subindex+1 }}</div>
                                <div class="col-sm-12 col-xs-10 fr-author">{{ $subcomment->user->lastname }}, {{ $subcomment->user->firstname }}</div>
                                <div class="col-sm-12 col-xs-6 fr-date">{{ date('d/m/Y', strtotime($subcomment->created_at)) }}</div>
                                <div class="col-sm-12 col-xs-6 fr-horu">{{ date('H:i:s', strtotime($subcomment->created_at)) }}</div>
                                <div class="col-sm-12  col-xs-4">
                                    <a class="btn btn-success btn-xs citar_message_btn"  href="javascript:;"  title="Citar" data-href="{{ route('foro.create_comment', [$topic->id, 'parent_id' => $comment->id]) }}"><span class="hidden-xs">Citar</span><span class="visible-xs"><i class="glyphicon glyphicon-share-alt"></i></span></a>
                                    @if($cu_user->isProfesor())
                                        @if(($subcomment->user->id != $cu_user->id))
                                            <a class="btn btn-success btn-xs open_message_modal"  href="javascript:;" data-href="{{ route('message.show', [$subcomment->user->id, 'mode' => 'modal','reply' => 'topic_comment','reply_id' => $subcomment->id]) }}"  title="Enviar Mensaje"><span class="hidden-xs">Enviar Mensaje</span><span class="visible-xs"><i class="glyphicon glyphicon-comment"></i></span></a>
                                        @endif

                                        {!! Form::open(['route' => ['foro.remove_comment', $topic->id, $subcomment->id], 'method' => 'delete']) !!}
                                        
                                        {!! Form::button('<span class="hidden-xs">Borrar</span><span class="visible-xs"><i class="glyphicon glyphicon-trash"></i></span>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Seguro desea Borrar?')"]) !!}
                                        
                                        {!! Form::close() !!}
                                    @endif

                                </div>
                                
                            </div>
                            <div class="col-sm-10 col-xs-12 comment-body">
                                    {!! $subcomment->body !!}
                            </div>
                        </div>
                    @endforeach
                    <div style="text-align: center">
                        <a class="btn btn-success btn-xs open_comment_form"  href="javascript:;" data-href="{{ route('foro.create_comment', [$topic->id, 'parent_id' => $comment->id]) }}"  title="Agregar Comentario">
                        <span>Agregar Comentario</span></span>
                        </a>
                    </div>
                </div>
                @endif
                </div>
        	@endforeach

    	@else

        	<div class='col-md-12 text-center'>
        		No hay Comentarios
        	</div>

    	@endif



    </div>
</div>

<script>
$( function() {
    $( ".toggle-subcomments" ).click(function() {
        if($(this).html() == '-Ver comentarios-'){
            $(this).html('-Ocultar comentarios-');
        }else{
            $(this).html('-Ver comentarios-');
        }
        $(this).parent().find('.fr-subcomment').slideToggle( "slow" )
    });
} );


    
</script>

