<!DOCTYPE html>
<html>
<body style="padding: 0px; margin: 0px;background-color:rgb(245,249,252);">
    <div style="width:100%; text-align: center;">
        <img src="{{ config('app.url') }}/img/logo.png" alt="" style="width:200px; height: 44px; margin:20px auto 0;" />
    </div>
    <div class="div-body"> 
        <h2 style="font-family: 'Fira Sans', sans-serif; font-size: 22px; font-weight: bold; margin:20px 0 0; line-height: 1.4; padding:0; text-align: center; color: #000;">
            {{ trans('email.reset_title') }}
        </h2>
        <p style="width: 400px; color:#757575; font-family: 'Fira Sans', sans-serif; font-size:12px; line-height:1.4; font-weight:100; padding:0px 25px 25px; margin: 0 auto; border-bottom:0px dashed #ddd; text-align: center">  
            {{ trans('email.reset_instructions') }}
        </p>
        
        {{-- Action Button --}}
        @isset($actionText)
            @component('mail::button', ['url' => $actionUrl, 'color' => 'primary'])
            {{ $actionText }}
            @endcomponent
        @endisset

        <br/>
         
        <p style="width: 400px; color:#757575; font-family: 'Fira Sans', sans-serif; font-size:12px; line-height:1.4; font-weight:100; padding:0px 25px 25px; margin: 0 auto 20px; border-bottom:0px dashed #ddd; text-align: center">  
            {{ trans('email.reset_ignore_mail') }}                            
        </p> 

        <br/>
        <br/>
        <br/>
        <br/>
        <br/>


        <div class="div-footer" style="">
            <div style="text-align: center">
                <a target="_blank" href="{{ config('app.url') }}">
                </a>
            </div>
        </div>
    </div>
    <div style="clear:both"></div>
</body>
</html>