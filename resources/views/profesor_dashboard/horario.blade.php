@if($profesor->getFullSchedule('mañana'))
<div class="col-md-8 col-xs-12">
<div class="panel panel-info">
    <div class="panel-heading">Horario</div>
    <div class="panel-body row" style="overflow:auto">
    	<div id='prof_schedule'></div>
    </div>
</div>

<script>
(function ($) {
      $("#prof_schedule").dayScheduleSelector({

        days: [1,2,3,4,5],
        stringDays  : ['Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes'],
        structure : ['07:35','08:35','09:35','09:50','10:50','11:50','12:00','13:00'],
        turno : 'mañana'

      });
      $("#prof_schedule").data('artsy.dayScheduleSelector').deserialize(
      	{!! json_encode($profesor->getFullSchedule('mañana')) !!}
      	);
    })($);
</script>
</div>
@endif
@if($profesor->getFullSchedule('tarde'))
<div class="col-md-8 col-xs-12">
<div class="panel panel-info">
    <div class="panel-heading">Horario</div>
    <div class="panel-body row" style="overflow:auto">
      <div id='prof_schedule_tarde'></div>
    </div>
</div>

<script>
(function ($) {
      $("#prof_schedule_tarde").dayScheduleSelector({

        days: [1,2,3,4,5],
        stringDays  : ['Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes'],
        structure : ['13:00','13:50','14:00','14:50','15:40','15:55','16:45','17:30'],
        turno : 'tarde'

      });
      $("#prof_schedule_tarde").data('artsy.dayScheduleSelector').deserialize(
        {!! json_encode($profesor->getFullSchedule('tarde')) !!}
        );
    })($);
</script>
</div>
@endif


