<div class="panel panel-info">
  <div class="panel-heading">Cursos - Materias</div>
  <div class="panel-body">
  	<ul class='dash-prof-materia-table'>
  			@foreach($profesor->cursoMaterias as $cursoMateria)
  			<a href='{{ route('profesor_curso_materia.index',$cursoMateria->id) }}'>
	  			<li class = 'materia-titular'>
	  				<div class="col-sm-4">{{ trans('page.model.curso') }}: {{ $cursoMateria->curso->name }}</div>
	  				<div class="col-sm-8">{{ $cursoMateria->materia->name }}</div>
	  				<div class="clearfix"></div>
	  			</li>
  			
  			@endforeach
  			@foreach($profesor->cursoMateriasSuplente as $cursoMateria)
  			<a href='{{ route('profesor_curso_materia.index',$cursoMateria->id) }}'>
	  			<li class = 'materia-suplente'>
	  				<div class="col-sm-4">{{ trans('page.model.curso') }}: {{ $cursoMateria->curso->name }}</div>
	  				<div class="col-sm-8">{{ $cursoMateria->materia->name }} ({{ trans('page.label.suplente') }})</div>
	  				<div class="clearfix"></div>
	  			</li>
	  		</a>
  			@endforeach
  	</ul>

  </div>
</div>

