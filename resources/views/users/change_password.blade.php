@extends(($cu_user->isProfesor()) ? 'adminlte::page_profesor' : ( ($cu_user->isAlumno()) ? 'adminlte::page_alumno' : 'adminlte::page' ) )

@section('content_header')
        <h1>
            Usuario
        </h1>
@endsection

@section('content')
    <div class="row">
        <div class="col-xs-12">
        <div class="clearfix"></div>

        @include('flash::message')
        <div class="box box-primary">

            <div class="box-body">
                <div class="row">
                    {!! Form::open(['route' => 'user.store_password']) !!}

                        <!-- Name Field -->
                        <div class="form-group col-sm-6">
                            {!! Form::label('current_password', 'Contraseña actual:') !!}
                            {!! Form::password('current_password', ['class' => 'form-control']) !!}
                        </div>
                        <div class="clearfix"></div>

                        <!-- Email Field -->
                        <div class="form-group col-sm-6">
                            {!! Form::label('password', 'Nueva Contraseña:') !!}
                            {!! Form::password('password', ['class' => 'form-control']) !!}
                        </div>
                        <div class="clearfix"></div>

                        <!-- Password Field -->
                        <div class="form-group col-sm-6">
                            {!! Form::label('password_confirmation', 'Repetir Contraseña:') !!}
                            {!! Form::password('password_confirmation', ['class' => 'form-control']) !!}
                        </div>
                        <div class="clearfix"></div>

                        <!-- Submit Field -->
                        <div class="form-group col-sm-12">
                            {!! Form::submit('Guardar', ['class' => 'btn btn-primary']) !!}
                            <a href="{!! route('users.index') !!}" class="btn btn-default">Cancelar</a>
                        </div>

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
