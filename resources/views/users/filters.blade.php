{!! Form::open(['method'=>'GET','action' => 'UserController@index', 'role'=>'form', 'id' =>'users_filters']) !!}

<div class="box-body filters" id="event_filters">
    
    <div class="form-group col-md-2" >
        {!! Form::label('filters_name', 'Nombre') !!}
        {!! Form::text('filters[name]', isset($filters['name']) ? $filters['name'] : null, 
        ['class' => 'form-control', 'id' => 'filters_name']) !!}
    </div>
    
    <div class="form-group col-md-2" >
        {!! Form::label('filters_email', 'E-mail') !!}
        {!! Form::text('filters[email]', isset($filters['email']) ? $filters['email'] : null, 
        ['class' => 'form-control', 'id' => 'filters_email']) !!}
    </div>


    <div class="form-group col-md-2" >
        {!! Form::label('filters_role_id', 'Rol') !!}
        {!! Form::select('filters[role_id]', [null=>'Todos']+$roles, isset($filters['role_id']) ? $filters['role_id'] : null, 
        ['class' => 'form-control', 'id' => 'filters_role_id']) !!}
    </div>

    <div class="form-group col-md-12">
        {!! Form::submit('Filtrar', ['class' => 'btn btn-primary filter-btn']) !!}
        <a class="btn btn-danger clean-filter-btn" href="{!! route('users.index') !!}">Limpiar filtros</a>        
    </div>
    
    <div class="clearfix"></div>
    
</div>

<div class="clearfix"></div>

{!! Form::close() !!}

