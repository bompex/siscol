@extends('adminlte::page')
@section('content_header')
        <h1>
            Usuario
        </h1>
@endsection
@section('content')
   <div class="row">
        <div class="col-xs-12">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($user, ['route' => ['users.update', $user->id], 'method' => 'patch']) !!}

                        @include('users.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div></div>
@endsection

@section('js')
    <script type="text/javascript">


        $('#roles').select2({});
        

    </script>
@endsection
