@extends('adminlte::page')

@section('content_header')
    <h1 class="pull-left">Usuarios del Sistema</h1>
    <h1 class="pull-right">
       <a class="btn btn-primary pull-right" style="margin-top: -10px;margin-bottom: 5px" href="{!! route('users.create') !!}">Nuevo</a>
    </h1>
@endsection

@section('content')
    
    <div class="clearfix"></div>
    @include('flash::message')
    <div class="clearfix"></div>
    @include('users.filters')
    <div class="clearfix"></div>

    <div class="box box-primary">
        <div class="box-body">
        @if(count($users) > 0)
            @include('users.table')
        @else
            <div clas="col-md-12">
                <span>No hay resultados para mostrar.</span>
            </div>
        @endif
        </div>
    </div>
    <div class="text-center">
        {!! $users->appends(Request::all())->render() !!}
    </div>
@endsection

@section('js')

    <script type="text/javascript">
        
        $(function(){
            $('#filters_role_id').select2({
                placeholder: 'Todos'
            });
            $('#filters_country_id').select2({
                placeholder: 'Todos'
            });
            $('#filters_company').select2({
                placeholder: 'Todos'
            });
        })

    </script>

@endsection

