<?php

return array(
	'nuevo_tablon' => "Nuevo tablon en :materia",
	'nueva_evaluacion' => "Nueva Actividad en :materia",
	'edit_evaluacion' => 'Ha habido cambios en la Actividad ":evaluacion"',
	'nueva_correccion' => 'Ha habido Observacion sobre el trabajo ":evaluacion"',
	'nuevo_mensaje' => 'Tiene un nuevo mensaje de :usuario',
	'nueva_entrega' => 'Actividad <strong>:evaluacion</strong>:<br /><strong>:usuario</strong> entregó una actividad.',
	'nuevo_comentario_tablon' => '<strong>:usuario</strong> hizo un comentario en el Tablon <strong>":tablon"</strong>.',
	'nuevo_comentario_evaluacion' => '<strong>:usuario</strong> hizo un comentario en la Actividad <strong>":evaluacion"</strong>.',
	'nuevo_anuncio' => "Nuevo Anuncio en :materia",
	'edit_anuncio' => "Ha habido cambios en un Anuncio de :materia",
);