<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'disabled' => 'Esta cuenta se encuentra desactivada.',
    'failed' => 'Estas credenciales no coinciden con nuestros registros.',
    'throttle' => 'Demasiados intentos fallidos en muy poco tiempo. Por favor intente de nuevo en :seconds segundos.',
    'unauthorized_title' => 'No autorizado',
    'unauthorized' => 'Usted no esta autorizado a acceder a esta pagina.',
    'go_home' => 'Ir al inicio',
    'forbidden_title' => 'No permitido',
    'forbidden' => 'Usted no tiene permisos para acceder a esta pagina.',
    'session_expired_title' => 'La sesión expiró',
    'session_expired' => 'La sesión ha expirado. Por favor refresque la pagina e intente nuevamente.',
    'reset_instructions' => 'Ingrese el email utilizado para acceder al sistema y recibirá un link para restablecer su contraseña.',
    'forgot_password' => 'Olvide mi contraseña',
    'login' => 'Acceder',
    'reset_title' => 'Restablecer contraseña',
];
