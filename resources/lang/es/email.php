<?php 

return array(
	'new_account' => 'Nueva cuenta',
    'welcome' => 'Bienvenido!',
    'register_instructions' => 'Para completar su registro haga click en el botón de abajo y complete la información solicitada.',
    'complete_signin' => 'Completar registro',
    'ignore_mail' => 'Si no solicitó esta cuenta, por favor ignore este correo.',
    'reset_password' => 'Renicio de contraseña',
    'reset_title' => 'Renicio de contraseña',
    'reset_instructions' => 'Para restablecer su contraseña haga click en el botón de abajo y siga los pasos establecidos.',    
    'reset_ignore_mail' => 'Si no solicitó restablecer su contraseña, por favor ignore este correo.',
    'reset' => 'Restablecer',
);