<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */
    'accepted' => 'El campo :attribute debe ser aceptado.',
    'active_url' => 'El campo :attribute no es una URL válida.',
    'after' => 'El campo :attribute debe ser una fecha posterior a :date.',
    'after_or_equal' => 'El campo :attribute debe ser una fecha igual o posterior a :date UTC.',
    'after_or_equal_date' => 'El campo :attribute debe ser una fecha igual o posterior a :date UTC.',
    'alpha' => 'El campo :attribute sólo puede contener letras.',
    'alpha_dash' => 'El campo :attribute sólo puede contener letras, números y guiones.',
    'alpha_num' => 'El campo :attribute sólo puede contener letras y números.',
    'array' => 'El campo :attribute debe ser un arreglo.',
    'before' => 'El campo :attribute debe ser una fecha antes :date.',
    'before_or_equal' => 'El campo :attribute debe ser una fecha antes o igual a hoy.',
    'between' => [
        'numeric' => 'El campo :attribute debe estar entre :min - :max.',
        'file' => 'El campo :attribute debe estar entre :min - :max kilobytes.',
        'string' => 'El campo :attribute debe estar entre :min - :max caracteres.',
        'array' => 'El campo :attribute debe tener entre :min y :max elementos.',
    ],
    'boolean' => 'El campo :attribute debe ser verdadero o falso.',
    'confirmed' => 'El campo de confirmación de :attribute no coincide.',
    'date' => 'El campo :attribute no es una fecha válida.',
    'date_format' => 'El campo :attribute no corresponde con el formato :format.',
    'different' => 'Los campos :attribute y :other deben ser diferentes.',
    'digits' => 'El campo :attribute debe ser de :digits dígitos.',
    'digits_between' => 'El campo :attribute debe tener entre :min y :max dígitos.',
    'dimensions' => 'El campo :attribute no tiene una dimensión válida.',
    'distinct' => 'El campo :attribute tiene un valor duplicado.',
    'email' => 'El formato del :attribute es inválido.',
    'exists' => 'El campo :attribute seleccionado es inválido.',
    'file' => 'El campo :attribute debe ser un archivo.',
    'filled' => 'El campo :attribute es requerido.',
    'greater_than' => 'El campo :attribute debe ser mayor a :min',
    'image' => 'El campo :attribute debe ser una imagen.',
    'in' => 'El campo :attribute seleccionado es inválido.',
    'in_array' => 'El campo :attribute no existe en :other.',
    'integer' => 'El campo :attribute debe ser un entero.',
    'ip' => 'El campo :attribute debe ser una dirección IP válida.',
    'json' => 'El campo :attribute debe ser una cadena JSON válida.',
    'max' => [
        'numeric' => 'El campo :attribute debe ser menor que :max.',
        'file' => 'El tamaño de :attribute debe ser menor que :max kilobytes.',
        'string' => 'El campo :attribute debe ser menor que :max caracteres.',
        'array' => 'El campo :attribute debe tener al menos :min elementos.',
    ],
    'mimes' => 'El campo :attribute debe ser un archivo de tipo: :values.',
    'mimetypes' => 'El campo :attribute debe ser un archivo de tipo: :values.',
    'min' => [
        'numeric' => 'El campo :attribute debe tener al menos :min.',
        'file' => 'El campo :attribute debe tener al menos :min kilobytes.',
        'string' => 'El campo :attribute debe tener al menos :min caracteres.',
        'array' => 'El campo :attribute debe tener al menos :min items.',
    ],
    'not_in' => 'El campo :attribute seleccionado es invalido.',
    'numeric' => 'El campo :attribute debe ser un número.',
    'present' => 'El campo :attribute debe estar presente.',
    'regex' => 'El formato del campo :attribute es inválido.',
    'required' => 'El campo :attribute es requerido.',
    'required_if' => 'El campo :attribute es requerido.',
    'required_unless' => 'El campo :attribute es requerido a menos que :other esté presente en :values.',
    'required_with' => 'El campo :attribute es requerido cuando :values está presente.',
    'required_with_all' => 'El campo :attribute es requerido cuando :values está presente.',
    'required_without' => 'El campo :attribute es requerido cuando :values no está presente.',
    'required_without_all' => 'El campo :attribute es requerido cuando ningún :values está presente.',
    'same' => 'El campo :attribute y :other debe coincidir.',
    'size' => [
        'numeric' => 'El campo :attribute debe ser :size.',
        'file' => 'El archivo :attribute debe tener :size kilobytes.',
        'string' => 'El campo :attribute debe tener :size caracteres.',
        'array' => 'El campo :attribute debe contener :size elementos.',
    ],
    'string' => 'El campo :attribute debe ser una cadena.',
    'timezone' => 'El campo :attribute debe ser una zona válida.',
    'unique' => 'El campo :attribute ya ha sido tomado.',
    'url' => 'El formato de :attribute es inválido.',
    'uploaded' => 'El campo :attribute no ha podido ser cargado.',
    'mimes' => 'El tipo de archivo no es válido.',
    'geo_targetings.*' => 'El campo :attribute es requerido.',
    /*
    |--------------------------------------------------------------------------
    | Custom Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | Here you may specify custom validation messages for attributes using the
    | convention "attribute.rule" to name the lines. This makes it quick to
    | specify a specific custom language line for a given attribute rule.
    |
    */

    'custom' => [
        'attachment_file' => [
            'extension' => 'La extensión :ext no es válida',
        ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Attributes
    |--------------------------------------------------------------------------
    |
    | The following language lines are used to swap attribute place-holders
    | with something more reader friendly such as E-Mail Address instead
    | of "email". This simply helps us make messages a little cleaner.
    |
    */

    'attributes' => [
        //Users
        'name' => 'Nombre',
        'last_name' => 'Apellido',
        'email' => 'Email',
        'password' => 'Password',
        'password_confirmation' => 'Confirmación password',
        
        //Advertisers
        'country_id' => 'País',
        'markup' => 'Margen',
        'news' => 'Email',
        'news.email.*' => 'Email',
        'news.email'=>'Email',

        'category_id' => 'Categoría',
        'daily_budget' => 'Presupuesto diario',
        'start_date' => 'Fecha inicio',
        'start_date.date' => 'Fecha inicio',
        'end_date' => 'Fecha fin',        
        'end_date.date' => 'Fecha fin',
        'campaign_name_exist' => 'El nombre de campaña ya existe.',
        'push_creative_text.name' => 'Nombre anuncio',
        'push_creative_text.title' => 'Titulo anuncio',
        'push_creative_text.desc' => 'Descripción anuncio',
        'push_creative_text.creative_urls' => 'Link URL',
        'push_creative_text.creative_urls.*' => 'Link URL', 
        'push_creative_text.impression_tracking_url' => 'URL de tracking de impresión',
        
        'push_creative_banner.name' =>'Nombre anuncio',
        'push_creative_banner.creative_urls' => 'Link URL',
        'push_creative_banner.upload_banner_url' => 'Imagen Banner',
        'push_creative_banner.creative_urls.*' => 'Link URL',
        'push_creative_banner.impression_tracking_url' => 'URL de tracking de impresión',
        
        'push_big_picture.name' => 'Nombre anuncio',
        'push_big_picture.title' => 'Titulo anuncio',
        'push_big_picture.desc' => 'Descripción anuncio',
        'push_big_picture.creative_urls' => 'Link URL',
        'push_big_picture.creative_urls.*' => 'Link URL',
        'push_big_picture.upload_icon_url' => 'Imagen Icono',
        'push_big_picture.upload_big_picture_url' => 'Imagen',
        'push_big_picture.impression_tracking_url' => 'URL de tracking de impresión',

        'sync_campaigns.*.config_id' => 'Config ID',
        'sync_campaigns.*.imp_campaign_id' => 'ID campaña IMP',
        'sync_campaigns.*.clk_campaign_id' => 'ID campaña CLK',
        
    ],
];