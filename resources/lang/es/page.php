<?php

return array(

	'label' => [
		'user' =>[
			'name' => 'Nombre',
			'lastname' => 'Apellido',
			'dni' => 'DNI',
			'birthdate' => 'Fecha de Nacimiento',
			'phone' => 'Telefono',
			'email' => 'Email',
			'gender' => 'Sexo',
			'password' => 'Contraseña',
			'password_confirm' => 'Confirmar Contraseña',
			'rol' => 'Rol',
			'nivel' => 'Nivel'
		],
		'curso' =>[
			'name' => 'Curso',
			'turno' => 'Turno',
			'nivel' => 'Nivel',
			'date_from' => 'Fecha Inicio',
			'date_to' => 'Fecha Fin',
			'active' => 'En Curso',
			'materias' => 'Materias',
			'alumnos' => 'Alumnos',
			'sin_suplente' => 'Sin Suplente',
			'alumno_comment' => 'Observación'
			
		],
		'curso_horario' =>[
			'horario' => 'Horario',
			'dia' => 'día',
			'hora_inicio' => 'Hora Inicio',
			'hora_fin' => 'Hora Fin',
			'note' => 'Nota',
		],
		'materia' =>[
			'name' => 'Nombre',
			'color' => 'Color',
		],
		'anuncio' =>[
			'subject' => 'Titulo',
			'body' => 'Mensaje',
			'fecha' => 'Fecha de Publicación',
			'fecha_inicio' => 'Fecha Inicio',
			'fecha_fin' => 'Fecha Fin',
			'allow_comments' => 'Permitir Comentarios'
		],
		'topic' =>[
			'subject' => 'Asunto',
			'body' => 'Mensaje',
			'status' => 'Estado',
			'allow_comments' => 'Permitir Comentarios'
		],
		'evaluacion' =>[
			'subject' => 'Titulo',
			'type' => 'Tipo',
			'fecha_inicio' => 'Fecha Inicio',
			'fecha_fin' => 'Fecha Fin',
			'body' => 'Nota',
			'1_date' => 'Fecha de Examen',
			'2_date' => 'Fecha de Entrega',
			'no_entrega' => 'Sin entrega',
			'calificar' => 'Calificar',
			'fecha_public' => 'Fecha de Publicación',
		],
		'evaluacion0' =>[
			'subject' => 'Titulo',
			'type' => 'Tipo',
			'fecha_inicio' => 'Fecha Inicio',
			'fecha_fin' => 'Fecha Fin',
			'body' => 'Nota',
			'1_date' => 'Fecha de Examen',
			'2_date' => 'Fecha de Entrega',
			'no_entrega' => 'Sin entrega',
			'calificar' => 'Calificar',
		],
		'evaluacion1' =>[
			'subject' => 'Titulo',
			'type' => 'Tipo',
			'fecha_inicio' => 'Fecha Inicio',
			'fecha_fin' => 'Fecha Fin',
			'body' => 'Nota',
			'1_date' => 'Fecha de Examen',
			'2_date' => 'Fecha de Entrega',
			'no_entrega' => 'Sin entrega',
			'calificar' => 'Calificar',
		],
		'calificacion' =>[
			'nota' => 'Nota',
			'observacion' => 'Observacion',
			'publica' => 'Publicar Nota',
		],

		'suplente' => 'Suplente',

	],

	'button' => [
		'save' => 'Guardar',
		'cancel' => 'Cancelar',
		'add' => 'Agregar',
		'new' => 'Nuevo',
		'newa' => 'Nueva',
		'search' => 'Buscar',
		'clear_filter' => 'Limpiar Filtro',
		'download_xls' => 'Descargar XLS',
		'back' => 'Volver',
		'publish' => 'Publicar',
		'archive' => 'Archivar',
		'view_all' => 'Ver Todo',
		'edit' => 'Editar',
		'send' => 'Enviar',
	],

	'form' => [
		'action' => [
			'message'=>[
				'success' => ":model guardado satisfactoriamente",
			    'update' => ":model modificado satisfactoriamente",
			    'not_found' => "No se encuentra el registro.",
			    'not_save' => "Se ha producido un error al intentar guardar el :model",
			],
			
		],
	],

	'model' => [
		'administrativo' => 'Administrativo',
		'alumno' => 'Alumno',
		'anuncio' => 'Anuncio',
		'asistencia' => 'Asistencia',
		'curso' => 'Curso',
		'curso0' => 'Curso Secundario',
		'curso1' => 'Curso Primario',
		'cursoSecundario' => 'Curso Secundario',
		'cursoPrimario' => 'Curso Primario',
		'materia' => 'Materia',
		'materia0' => 'Materia',
		'materia1' => 'Area',
		'materiaSecundario' => 'Materia',
		'materiaPrimario' => 'Area',
		'profesor' => 'Profesor',
		'topic' => 'Tablón',
		'evaluacion' => 'Actividad',
		'evaluacion0' => 'Actividad',
		'evaluacion1' => 'Actividad',
		'entrega' => 'Entrega',
		'calificacion' => 'Calificación',
		'plural' => [
				'administrativo' => 'Administrativos',
				'alumno' => 'Alumnos',
				'anuncio' => 'Anuncios',
				'asistencia' => 'Asistencias',
				'curso' => 'Cursos',
				'curso0' => 'Cursos Secundario',
				'curso1' => 'Cursos Primario',
				'cursoSecundario' => 'Cursos Secundario',
				'cursoPrimario' => 'Cursos Primario',
				'materia' => 'Materias',
				'materia0' => 'Materias',
				'materia1' => 'Areas',
				'materiaSecundario' => 'Materias',
				'materiaPrimario' => 'Areas',
				'profesor' => 'Profesores',
				'topic' => 'Tablones',
				'evaluacion' => 'Actividades',
				'evaluacion0' => 'Actividades',
				'evaluacion1' => 'Actividades',
				'entrega' => 'Entregas',
				'calificacion' => 'Calificaciones',
		],
	],

	'calendar' => [
		'monday' => 'Lunes',
		'tuesday' => 'Martes',
		'wednesday' => 'Miércoles',
		'thursday' => 'Jueves',
		'friday' => 'Viernes',
		'saturday' => 'Sabado',
		'sunday' => 'Domingo',
	],


	'message' => [
		'access_denided' => 'Acceso Denegado',
		'no_message' => 'No Hay Mensajes',
	],

);













