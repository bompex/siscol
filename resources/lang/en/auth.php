<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */
    'disabled' => 'This account is disabled.',
    'failed' => 'These credentials do not match our records.',
    'throttle' => 'Too many login attempts. Please try again in :seconds seconds.',
    'unauthorized_title' => 'Unauthorized',
    'unauthorized' => 'You can\'t access to this page',
    'go_home' => 'Go home',
    'forbidden_title' => 'Not allowed',
    'forbidden' => 'You don\'t have permissions to access this page.',
    'session_expired_title' => 'Session expired',
    'session_expired' => 'Session has expired. Please refresh and try again.',
    'reset_instructions' => "Enter the email you use to access the system and you'll receive a link to reset your password",
    'forgot_password' => 'Forgot password',
    'login' => 'Login',
    'reset_title' => 'Reset password',
];
