<?php 

return array(
	'new_account' => 'New account',
    'welcome' => 'Welcome!',
    'register_instructions' => 'In order to complete the registration process, please click below and fill the requested information.',
    'complete_signin' => 'Complete registration process',
    'ignore_mail' => 'If you did not request to create this account, please ignore this email.',
    'reset_password' => 'Reset your password',
    'reset_title' => 'Reset password',
    'reset_instructions' => 'In order to reset you password, please click below and follow the instructions.',    
    'reset_ignore_mail' => 'If you did not request to reset you password, please ignore this email.',
    'reset' => 'Reset',
);