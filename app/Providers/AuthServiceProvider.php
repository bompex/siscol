<?php

namespace App\Providers;

use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        // 'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        Gate::define('edit-users', function ($user) {
            return $user->hasRole('admin');
        });

        Gate::define('use-backend', function ($user) {
            if($user->hasRole('adminisrtativo')||$user->hasRole('admin')||$user->hasRole('director')||$user->hasRole('secretario'))
                return true;

            return false;
        });

        Gate::define('use-preceptor', function ($user) {
            if($user->hasRole('adminisrtativo')||$user->hasRole('admin')||$user->hasRole('director')||$user->hasRole('secretario')||$user->hasRole('preceptor'))
                return true;
            return false;

        });
        Gate::define('use-superadmin', function ($user) {
           return $user->hasRole('admin');

        });

    }
}
