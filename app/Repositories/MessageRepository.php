<?php

namespace App\Repositories;

use App\Models\Message;
use App\Repositories\BaseRepository;


class MessageRepository extends BaseRepository
{
    
    /**
     * Configure the Model
     **/
    public function model()
    {
        return Message::class;
    }

    // public function getMessagesByUser($user_id){
    //     return $this->model()::where('sender_user_id', array(1, 2, 3))->whereIn('receiver_user_id', array(1, 2, 3))->orderBy('created_at')->get();
    // }

    public function getMessagesByConversation($sender_id, $receiver_id){
        return $this->model()::whereIn('sender_user_id', array($sender_id, $receiver_id))->whereIn('receiver_user_id', array($sender_id, $receiver_id))->orderBy('created_at', 'ASC')->get();
    }
}
