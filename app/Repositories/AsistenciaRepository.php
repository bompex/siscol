<?php

namespace App\Repositories;

use App\Models\Asistencia;
use App\Repositories\BaseRepository;

class AsistenciaRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Asistencia::class;
    }

    public function getAll($curso_id = '_empty_') {
        

        $query = $this->model;

        if ($curso_id != null && $curso_id != '_empty_') {
            $query = $query->where('curso_id', $curso_id);
        }

        $query = $query->orderBy('fecha', 'DESC');


        return $query->get();
    }
}
