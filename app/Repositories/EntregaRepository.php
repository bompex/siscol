<?php

namespace App\Repositories;

use App\Models\Entrega;
use App\Repositories\BaseRepository;

class EntregaRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Entrega::class;
    }
}
