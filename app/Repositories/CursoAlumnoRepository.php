<?php

namespace App\Repositories;

use App\Models\CursoAlumno;
use App\Repositories\BaseRepository;

/**
 * Class CursoAlumnoRepository
 * @package App\Repositories
 * @version April 26, 2020, 8:49 pm UTC
*/

class CursoAlumnoRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'curso_id',
        'alumno_id',
        'registration_date',
        'status',
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return CursoAlumno::class;
    }

    public function promote($curso, $alumnos){
        if(!is_array($alumnos)){
            $alumnos[] = $alumnos;
        }

        foreach($alumnos as $alumno_id){
            $cursoAlumno_old = $this->model()::where('alumno_id', $alumno_id)->get();
            foreach ($cursoAlumno_old as $item) {
                $item->status = 0;
                $item->save();
            }
            $data = ['curso_id' => $curso->id,
                    'alumno_id' => $alumno_id,
                    'registration_date' => date('Y-m-d H:i:s',time()),
                    'status' => 1];
            $cursoAlumno = new CursoAlumno();
            $cursoAlumno->create($data);
        }
    }
}
