<?php

namespace App\Repositories;

use DB;
use App\Models\Profesor;
use App\Models\User;
use App\Models\Role;
use App\Repositories\BaseRepository;

/**
 * Class ProfesorRepository
 * @package App\Repositories
 * @version April 16, 2020, 8:45 pm UTC
*/

class ProfesorRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'user_id',
        'firstname',
        'lastname',
        'dni',
        'birthdate',
        'telefono'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Profesor::class;
    }

    public function create(array $attributes){

        $user_input = [
            'name' => $attributes['firstname']."_".$attributes['lastname'],
            'email' => $attributes['email'],
            'password' => $attributes['password']
        ];

        $u = User::firstOrCreate($user_input);
        $u->roles()->sync([3]);
        $u->save();

        $profesor_input = [
            'user_id' => $u->id,
            'firstname'  => $attributes['firstname'],
            'lastname'  => $attributes['lastname'],
            'dni'  => $attributes['dni'],
            'birthdate'  => $attributes['birthdate'],
            'telefono'  => $attributes['telefono'],
            'primario'  => (isset($attributes['primario']))? 1 : 0,
            'secundario'  => (isset($attributes['secundario']))? 1 : 0,
        ];

        $profesor = parent::create($profesor_input);

        return $profesor;
    }


    public function update(array $attributes, $id){

        $profesor = parent::find($id);

        $profesor->user->name = $attributes['firstname']."_".$attributes['lastname'];
        if($attributes['password'] && $attributes['password'] != ''){
            $profesor->user->password = $attributes['password'];    
        }
        $profesor->user->save();

        $profesor->firstname = $attributes['firstname'];
        $profesor->lastname = $attributes['lastname'];
        $profesor->dni = $attributes['dni'];
        $profesor->birthdate = $attributes['birthdate'];
        $profesor->telefono = $attributes['telefono'];
        $profesor->primario = (isset($attributes['primario']))? 1 : 0;
        $profesor->secundario = (isset($attributes['secundario']))? 1 : 0;

        $profesor->save();

        return $profesor;
    }

    public function selectArray($nivel = false){
        $qry = $this->model()::select(DB::raw("id, CONCAT(lastname,', ',firstname) AS name"));
        if($nivel === 0){
            $qry = $qry->where('secundario',1);
        }
        if($nivel === 1){
            $qry = $qry->where('primario',1);
        }
        return $qry->orderBy('name')->pluck('name','id');
    }

}
