<?php

namespace App\Repositories;

use App\Models\Anuncio;
use App\Repositories\BaseRepository;

class AnuncioRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Anuncio::class;
    }
}
