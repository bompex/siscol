<?php

namespace App\Repositories;

use App\Models\Curso;
use App\Models\CursoAlumno;
use App\Repositories\BaseRepository;

/**
 * Class CursoRepository
 * @package App\Repositories
 * @version April 16, 2020, 8:46 pm UTC
*/

class CursoRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'turno',
        'date_from',
        'date_to',
        'active',
        'created_by',
        'updated_by'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Curso::class;
    }

    public function storeAlumnos($id, $alumnos_id){
        $curso = self::find($id);

        $exist_alumno = [];

        foreach($curso->cursoAlumnos as $cursoAlumno){
            if(!in_array($cursoAlumno->alumno_id, $alumnos_id)){
                $cursoAlumno->delete();
            }else{
                $exist_alumno[] = $cursoAlumno->alumno_id;
            }
            
        }

        

        foreach($alumnos_id as $alumno_id){
            if(!in_array($alumno_id, $exist_alumno)){
                $data = ['curso_id' => $id,
                        'alumno_id' => $alumno_id,
                        'registration_date' => date('Y-m-d H:i:s',time()),
                        'status' => 1];
                $cursoAlumno = new CursoAlumno();
                $cursoAlumno->create($data);
                echo "new=".$cursoAlumno->alumno_id.PHP_EOL;
            }
        }


        return self::find($id);

    }


    public function createFromExist($curso_old_id){
        $curso_old = $this->find($curso_old_id);

        //crea nuevo curso
        $curso = $curso_old->replicate();
        $curso->date_from = date('Y-01-01',time());
        $curso->date_to = date('Y-12-31',time());
        $curso->active = 1;
        $curso->push();

        //copia estructura de materias
        foreach ($curso_old->cursoMaterias as $cursoMateria_old) {
                $cursoMateria = $cursoMateria_old->replicate();
                $cursoMateria->curso_id = $curso->id;
                $cursoMateria->push();
                foreach($cursoMateria_old->cursoMateriaHorarios as $cursoMateriaHorario_old){
                    $cursoMateriaHorario  = $cursoMateriaHorario_old->replicate();
                    $cursoMateriaHorario->curso_materia_id = $cursoMateria->id;
                    $cursoMateriaHorario->note = '';
                    $cursoMateriaHorario->push();
                }
        }

        return $curso;   
    }

    public function copyConfigFromExist($curso, $curso_old_id){

        $curso_old = $this->find($curso_old_id);

        //copia estructura de materias
        foreach ($curso_old->cursoMaterias as $cursoMateria_old) {
                $cursoMateria = $cursoMateria_old->replicate();
                $cursoMateria->curso_id = $curso->id;
                $cursoMateria->push();
                foreach($cursoMateria_old->cursoMateriaHorarios as $cursoMateriaHorario_old){
                    $cursoMateriaHorario  = $cursoMateriaHorario_old->replicate();
                    $cursoMateriaHorario->curso_materia_id = $cursoMateria->id;
                    $cursoMateriaHorario->note = '';
                    $cursoMateriaHorario->push();
                }
        }

        return $curso;   

    }

    public function selectArray(){
        return $this->model()::where('date_to','>',date('Y-m-d',time()))->orderBy('name')->pluck('name','id');
    }

    public function getByNivel($nivel, $periodo = false){

        $model = $this->model()::orderBy('name');

        if($nivel !== false){
            $model =  $model->where('primario', $nivel);
        }

        if($periodo !== false){
            $model = $model->whereBetween('date_from', [$periodo.'-01-01', $periodo.'-12-31']);
        }

        return $model->get();
        
    }

    public function getPeriodos($nivel){
        $model = $this->model()::selectRaw('YEAR(date_from) AS periodo')->groupBy('periodo')->orderBy('periodo');
        if($nivel){
            $model = $model->where('primario', $nivel);
        }
        $result = $model->get();
        $periodos =[];
        foreach($result as $row){
            $periodo[$row->periodo] = $row->periodo;
        }
        return $periodo;
        
    }
}
