<?php

namespace App\Repositories;

use App\Models\CursoMateria;
use App\Repositories\BaseRepository;

/**
 * Class CursoMateriaRepository
 * @package App\Repositories
 * @version April 25, 2020, 9:14 pm UTC
*/

class CursoMateriaRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'curso_id',
        'profesor_id',
        'profesor_suplente_id',
        'materia_id',
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return CursoMateria::class;
    }
}
