<?php

namespace App\Repositories;

use App\Models\User;
use App\Repositories\BaseRepository;

/**
 * Class UserRepository
 * @package App\Repositories
 * @version July 22, 2019, 3:25 pm UTC
*/

class UserRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'email',
        'email_verified_at',
        'password',
        'remember_token'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable() {
        return $this->fieldSearchable;
    }

    public function model() {
        return User::class;
    }

    protected function applyConditions(array $where) {
        
        $this->model = $this->model->with('roles');
                                   
        foreach ($where as $field => $value) {
            if(in_array($field, ['name','email'])) {
                $this->model = $this->model->where($field, 'like', '%'.$value.'%');
            }
            else if($field == 'role_id') {
                $this->model = $this->model->whereHas('roles', function($q) use ($value) {
                   $q->where('id','=',$value); 
                });
            }
            else {
                $this->model = $this->model->where($field, '=', $value);
            }
        }
    }

    public function changePassword($user, $new_password){

        $user->password = $new_password;
        $user->save();

        return $user;
    }    
}
