<?php

namespace App\Repositories;

use App\Models\Administrativo;
use App\Models\User;
use App\Models\Role;
use App\Repositories\BaseRepository;

/**
 * Class AdministrativoRepository
 * @package App\Repositories
 * @version April 16, 2020, 8:45 pm UTC
*/

class AdministrativoRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'user_id',
        'firstname',
        'lastname',
        'dni',
        'birthdate',
        'telefono',
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Administrativo::class;
    }

    public function create(array $attributes){

        $user_input = [
            'name' => $attributes['firstname']."_".$attributes['lastname'],
            'email' => $attributes['email'],
            'password' => $attributes['password']
        ];

        $u = User::firstOrCreate($user_input);
        $u->roles()->sync(Role::where('name', $attributes['rol'])->first());
        $u->save();

        $administrativo_input = [
            'user_id' => $u->id,
            'firstname'  => $attributes['firstname'],
            'lastname'  => $attributes['lastname'],
            'dni'  => $attributes['dni'],
            'birthdate'  => $attributes['birthdate'],
            'telefono'  => $attributes['telefono'],
        ];

        $administrativo = parent::create($administrativo_input);

        return $administrativo;
    }


    public function update(array $attributes, $id){

        $administrativo = parent::find($id);

        $administrativo->user->name = $attributes['firstname']."_".$attributes['lastname'];
        if($attributes['password'] && $attributes['password'] != ''){
            $administrativo->user->password = $attributes['password'];    
        }
        $administrativo->user->save();
        $administrativo->user->roles()->sync(Role::where('name', $attributes['rol'])->first());

        $administrativo->firstname = $attributes['firstname'];
        $administrativo->lastname = $attributes['lastname'];
        $administrativo->dni = $attributes['dni'];
        $administrativo->birthdate = $attributes['birthdate'];
        $administrativo->telefono = $attributes['telefono'];

        $administrativo->save();

        return $administrativo;
    }

}
