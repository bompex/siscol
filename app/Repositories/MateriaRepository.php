<?php

namespace App\Repositories;

use DB;
use App\Models\Materia;
use App\Repositories\BaseRepository;

/**
 * Class MateriaRepository
 * @package App\Repositories
 * @version April 16, 2020, 8:46 pm UTC
*/

class MateriaRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Materia::class;
    }

    public function selectArray($nivel = false){
        $qry = $this->model()::select(DB::raw("id, name"));
        if($nivel === 0){
            $qry = $qry->where('primario',0);
        }
        if($nivel === 1){
            $qry = $qry->where('primario',1);
        }
        return $qry->orderBy('name')->pluck('name','id');
    }

    public function getByNivel($nivel){
        return $this->model()::where('primario', $nivel)->get();
    }
}
