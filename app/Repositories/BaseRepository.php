<?php

namespace App\Repositories;

use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Schema;
use InfyOm\Generator\Common\BaseRepository as Repository;

abstract class BaseRepository extends Repository {

	protected function processConditions($conditions) {
		if(!$conditions) return [];
		foreach($conditions as $column => $value) {
			if(!$value) {
				unset($conditions[$column]);
			}
		}
		return $conditions ?? []; 
	}

    public function filter($conditions) {    
        $conditions = $this->processConditions($conditions);
        $this->applyConditions($conditions);
        return $this;                    
    }
}