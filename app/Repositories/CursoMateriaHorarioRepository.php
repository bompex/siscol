<?php

namespace App\Repositories;

use App\Models\CursoMateriaHorario;
use App\Repositories\BaseRepository;

/**
 * Class CursoMateriaRepository
 * @package App\Repositories
 * @version April 25, 2020, 9:14 pm UTC
*/

class CursoMateriaHorarioRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return CursoMateriaHorario::class;
    }

    
}
