<?php

namespace App\Repositories;

use DB;
use App\Models\Alumno;
use App\Models\User;
use App\Models\Role;
use App\Repositories\BaseRepository;

/**
 * Class AlumnoRepository
 * @package App\Repositories
 * @version April 16, 2020, 8:24 pm UTC
*/

class AlumnoRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'user_id',
        'firstname',
        'lastname',
        'dni',
        'birthdate',
        'telefono',
        'gender',
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Alumno::class;
    }

    public function create(array $attributes){

        $user_input = [
            'name' => $attributes['firstname']."_".$attributes['lastname'],
            'email' => $attributes['email'],
            'password' => $attributes['password']
        ];

        $u = User::firstOrCreate($user_input);
        $u->roles()->sync([2]);
        $u->save();

        $alumno_input = [
            'user_id' => $u->id,
            'firstname'  => $attributes['firstname'],
            'lastname'  => $attributes['lastname'],
            'dni'  => $attributes['dni'],
            'birthdate'  => $attributes['birthdate'],
            'telefono'  => $attributes['telefono'],
            'gender'  => $attributes['gender'],
        ];

        $alumno = parent::create($alumno_input);

        return $alumno;
    }


    public function update(array $attributes, $id){

        $alumno = parent::find($id);

        $alumno->user->name = $attributes['firstname']."_".$attributes['lastname'];
        if($attributes['password'] && $attributes['password'] != ''){
            $alumno->user->password = $attributes['password'];    
        }
        $alumno->user->save();

        $alumno->firstname = $attributes['firstname'];
        $alumno->lastname = $attributes['lastname'];
        $alumno->dni = $attributes['dni'];
        $alumno->birthdate = $attributes['birthdate'];
        $alumno->telefono = $attributes['telefono'];
        $alumno->gender = $attributes['gender'];

        $alumno->save();

        return $alumno;
    }

    public function selectArray(){
        return $this->model()::select(DB::raw("id, CONCAT(lastname,', ',firstname) AS name"))->orderBy('name')->pluck('name','id');
    }


    public function getAll($curso_id = '_empty_', $orderBy = '', $sortType = '', $paginate = true) {
        

        $query = $this->model;

        $query = $query->select($this->model->getTable() .'.*');

        if ($curso_id != null && $curso_id != '_empty_') {
            $query = $query->join('curso_alumno', $this->model->getTable().'.id', '=', 'curso_alumno.alumno_id');
            $query = $query->where('curso_alumno.curso_id', $curso_id);
            $query = $query->where('curso_alumno.status', '1');
        }

        if (empty($orderBy)) {
            $query = $query->orderBy($this->model->getTable() . '.lastname', 'ASC'); 
            $query = $query->orderBy($this->model->getTable() . '.firstname', 'ASC'); 
        }else{
            $query = $query->orderBy($orderBy, $sortType); 
        }

        
        
        
        if ($paginate) {
           //return $query->paginate(self::RESULT_PER_PAGE);
        }

        return $query->get();
    }

    public function getAlumnoWithoutCurso(){
        
    }
}
