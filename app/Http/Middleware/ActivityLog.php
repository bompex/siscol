<?php

namespace App\Http\Middleware;

use Closure;
use \Auth;
use Illuminate\Support\Facades\Log;
use Jenssegers\Agent\Agent;
use App\Models\ActivityLog as ActivityLogger;

class ActivityLog {
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null) {

        $agent = new Agent();
        $user = Auth::user();

        $activity = new ActivityLogger($request, $user, $agent);
        $activity->process();

        return $next($request);
    }


}
