<?php

namespace App\Http\Middleware;

// use Session;
use Closure;
use Config;

class VerifySubdomain
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        //Get the team slug

        // $pieces = explode('.', $request->getHost());


        // $slug = $pieces[0];//trim(str_replace($base, '', $domain), '.');

        // if (!$slug) {
        //     return abort(404);
        // }
        // switch ($slug) {
        //     default:
        //         $name =     'Sistema';
        //         $logo =     '<img src="/img/logo_color.png" class="logo"/>';
        //         $minilogo = '<span class="collapse_logo">J</span><span class="collapse_logo2">N</span>';
        //         break;
        // }

        // Config::set('app.name',$name);
        // Config::set('adminlte.logo',$logo);
        // Config::set('adminlte.logo_mini',$minilogo);

        return $next($request);
    }

}
