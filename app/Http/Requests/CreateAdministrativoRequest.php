<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Models\Administrativo;
use App\Models\User;

class CreateAdministrativoRequest extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = Administrativo::$rules;
        $rules['email'] = 'required|email|unique:users';
        $rules['password'] = 'sometimes|min:6|confirmed|required_with:password_confirmed';

        return $rules;
    }
}
