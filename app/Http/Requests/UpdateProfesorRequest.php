<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Models\Profesor;

class UpdateProfesorRequest extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = Profesor::$rules;
        $rules['password'] = 'sometimes|nullable|min:6|confirmed|required_with:password_confirmed';

        return $rules;
    }
}
