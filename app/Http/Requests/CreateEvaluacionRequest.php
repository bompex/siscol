<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Models\Evaluacion;

class CreateEvaluacionRequest extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = Evaluacion::$rules;
        $rules['fecha_inicio_date'] = 'after_or_equal:'.date('Y-m-d');
        $rules['fecha_fin_date'] = 'after_or_equal:'.date('Y-m-d');
        return $rules;
    }
}
