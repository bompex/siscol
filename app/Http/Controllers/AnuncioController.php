<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\AnuncioRepository;
use App\Repositories\CursoMateriaRepository;
use App\Repositories\FileRepository;
use App\Models\Anuncio;
use App\Models\AnuncioFile;
use Flash;
use Response;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;

use App\Http\Requests\CreateAnuncioRequest;
use App\Http\Requests\UpdateAnuncioRequest;

use App\Utils\NotificationHelper;

class AnuncioController extends AppBaseController
{

    private $anuncioRepository;
    private $cursoMateriaRepository;
    private $fileRepository;

    public function __construct(AnuncioRepository $anuncioRepo, CursoMateriaRepository $cursoMateriaRepo, FileRepository $fileRepo)
    {
        $this->anuncioRepository = $anuncioRepo;
        $this->cursoMateriaRepository = $cursoMateriaRepo;
        $this->fileRepository = $fileRepo;
    }

    public function index($curso_materia_id){
        $cu_user = Auth::user();
        $curso_materia = $this->cursoMateriaRepository->find($curso_materia_id);

        $anuncios = $curso_materia->anuncios;
        

        return view('anuncio.index')
            ->with(['cursoMateria' => $curso_materia, 'anuncios' => $anuncios]);
    }

    public function show($anuncio_id){

        $anuncio = $this->anuncioRepository->find($anuncio_id);

        

        return view('anuncio.show_modal')
            ->with(['anuncio' => $anuncio]);
    }

    public function create($curso_materia_id)
    {
        $user_sender = Auth::user();
        $curso_materia = $this->cursoMateriaRepository->find($curso_materia_id);

        $anuncio = new Anuncio();

        return view('anuncio.create')
            ->with(['cursoMateria'=> $curso_materia, 'anuncio' => $anuncio]);
    }


    public function store($curso_materia_id, CreateAnuncioRequest $request)
    {
        $input = $request->all();

        $cursoMateria = $this->cursoMateriaRepository->find($curso_materia_id);
        $input['curso_materia_id'] = $curso_materia_id;

        $input['fecha'] = $input['fecha_date']." ".$input['fecha_time'].":00";
        if(isset($input['fecha_inicio'])){
           $input['fecha_inicio'] = $input['fecha_inicio_date']." ".$input['fecha_inicio_time'].":00"; 
        }
        if(isset($input['fecha_fin'])){
           $input['fecha_fin'] = $input['fecha_fin_date']." ".$input['fecha_fin_time'].":00";
        }
        
        

        try {

            $anuncio = $this->anuncioRepository->create($input);

            if(isset($input['files_del']) && count($input['files_del']) > 0){
                foreach($input['files_del'] as $file_del_id){
                    $this->fileRepository->delete($file_del_id);
                }
            }

            if(time() > strtotime($input['fecha'])){
               NotificationHelper::nofity_curso($cursoMateria->curso, trans('notify.nuevo_anuncio',['materia'=>$cursoMateria->materia->name]), route('anuncio.show', [$anuncio->id]), 'open-in-modal'); 
            }
            Flash::success(trans('page.form.action.message.success', ['model' => trans('page.model.anuncio')]));
        } catch (\Exception $e) {
            Log::error($e->getMessage());
        }

        return redirect(route('anuncio.index', $cursoMateria->id));
    }

    public function edit($curso_materia_id, $id){
        $cu_user = Auth::user();

        $cursoMateria = $this->cursoMateriaRepository->find($curso_materia_id);

        $anuncio = $this->anuncioRepository->find($id);

        return view('anuncio.edit')
            ->with(['cu_user' => $cu_user ,'cursoMateria'=> $cursoMateria, 'anuncio' => $anuncio]);
    }

    public function update($curso_materia_id, $id, UpdateAnuncioRequest $request){
        $input = $request->all();

        $cu_user = Auth::user();

        $input['fecha'] = $input['fecha_date']." ".$input['fecha_time'].":00";
        $input['fecha_inicio'] = $input['fecha_inicio_date']." ".$input['fecha_inicio_time'].":00";
        $input['fecha_fin'] = $input['fecha_fin_date']." ".$input['fecha_fin_time'].":00";

        try {

            $anuncio = $this->anuncioRepository->update($input, $id);

            if(isset($input['files_del']) && count($input['files_del']) > 0){
                foreach($input['files_del'] as $file_del_id){
                    $anuncioFile = anuncioFile::where('file_id', $file_del_id)->where('anuncio_id', $anuncio->id)->first();
                    if($anuncioFile){
                        $anuncioFile->delete();
                    }
                    $this->fileRepository->delete($file_del_id);
                }
            }

            if(time() > strtotime($input['fecha'])){
               NotificationHelper::nofity_curso($anuncio->cursoMateria->curso, trans('notify.edit_anuncio',['materia'=>$anuncio->cursoMateria->materia->name]), route('anuncio.show', [$anuncio->id]), 'open-in-modal'); 
            }
            Flash::success(trans('page.form.action.message.success', ['model' => trans('page.model.anuncio')]));

        } catch (\Exception $e) {
            Log::error($e->getMessage());
            Flash::error(trans('page.form.action.message.not_save', ['model' => trans('page.model.anuncio')]));
        }


        return redirect(route('anuncio.index',$curso_materia_id));
    }

    public function destroy($curso_materia_id, $id)
    {
        $anuncio = $this->anuncioRepository->find($id);

        if (empty($anuncio)) {
            Flash::error(trans('page.form.action.message.not_found'));

            return redirect(route('anuncio.index',$curso_materia_id));
        }

        $anuncio->disable();

        Flash::success('El Anuncio se ha eliminado.');

        return redirect(route('anuncio.index', $curso_materia_id));
    }

}
