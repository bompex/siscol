<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateProfesorRequest;
use App\Http\Requests\UpdateProfesorRequest;
use App\Repositories\ProfesorRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use App\Models\Administrativo;
use App\Models\Profesor;
use App\Models\User;
use Illuminate\Support\Facades\Log;
use Flash;
use Response;
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\ProfesorExport;

class ProfesorController extends AppBaseController
{
    /** @var  ProfesorRepository */
    private $profesorRepository;

    public function __construct(ProfesorRepository $profesorRepo)
    {
        $this->profesorRepository = $profesorRepo;
    }

    /**
     * Display a listing of the Profesor.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $parameters = $request->all();
        if(isset($parameters['download-xls'])){
            return Excel::download(new ProfesorExport($this->profesorRepository), 'Profesores.xlsx');
        }

        $profesor = $this->profesorRepository->all();

        return view('profesor.index')
            ->with('profesor', $profesor);
    }

    /**
     * Show the form for creating a new Profesor.
     *
     * @return Response
     */
    public function create()
    {
        $profesor = new Profesor();
        $profesor->user = new User();
        $profesor->secundario = 1;
        return view('profesor.create')->with(['profesor' => $profesor]);
    }

    /**
     * Store a newly created Profesor in storage.
     *
     * @param CreateProfesorRequest $request
     *
     * @return Response
     */
    public function store(CreateProfesorRequest $request)
    {
        $input = $request->all();
        

        try {

            $profesor = $this->profesorRepository->create($input);
            Flash::success(trans('page.form.action.message.success', ['model' => trans('page.model.profesor')]));

        } catch (\Exception $e) {
            Log::error($e->getMessage());
            Flash::error(trans('page.form.action.message.not_save', ['model' => trans('page.model.profesor')]));
        }

        return redirect(route('profesor.index'));
    }

    /**
     * Display the specified Profesor.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $profesor = $this->profesorRepository->find($id);

        if (empty($profesor)) {
            Flash::error(trans('page.form.action.message.not_found'));

            return redirect(route('profesor.index'));
        }

        return view('profesor.show')->with('profesor', $profesor);
    }

    /**
     * Show the form for editing the specified Profesor.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $profesor = $this->profesorRepository->find($id);


        if (empty($profesor)) {
            Flash::error(trans('page.form.action.message.not_found'));

            return redirect(route('profesor.index'));
        }

        return view('profesor.edit')->with('profesor', $profesor);
    }

    /**
     * Update the specified Profesor in storage.
     *
     * @param int $id
     * @param UpdateProfesorRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateProfesorRequest $request)
    {
        $profesor = $this->profesorRepository->find($id);

        if (empty($profesor)) {
            Flash::error('Profesor not found');

            return redirect(route('profesor.index'));
        }

        try {

            $profesor = $this->profesorRepository->update($request->all(), $id);
            Flash::success(trans('page.form.action.message.update', ['model' => trans('page.model.profesor')]));

        } catch (\Exception $e) {
            Log::error($e->getMessage());
            Flash::error(trans('page.form.action.message.not_save', ['model' => trans('page.model.profesor')]));
        }

        return redirect(route('profesor.index'));

    }

    public function store_admin($id){
        $profesor = $this->profesorRepository->find($id);

        $admin = Administrativo::where('user_id',$profesor->user_id)->withTrashed()->first();

        if(isset($admin->id)){
            $admin->deleted_at = null;
            $admin->save();
        }else{
            $admin = Administrativo::firstOrCreate([
                'user_id' => $profesor->user_id,
                'firstname' =>$profesor->firstname,
                'lastname' => $profesor->lastname,
                'dni' => $profesor->dni,
                'birthdate' => $profesor->birthdate,
                'telefono' => $profesor->telefono
            ]);
        }

        $profesor->user->roles()->attach(4);
       

        return redirect(route('administrativo.edit',$admin->id));
    }


    public function destroy($id)
    {
        $profesor = $this->profesorRepository->find($id);

        if (empty($profesor)) {
            Flash::error(trans('page.form.action.message.not_found'));

            return redirect(route('profesor.index'));
        }

        $profesor->user->roles()->detach(3);
        $this->profesorRepository->delete($id);

        Flash::success('Profesor deleted successfully.');

        return redirect(route('profesor.index'));
    }
}
