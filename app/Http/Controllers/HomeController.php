<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $cu_user = Auth::user();
        if($cu_user->isAdmin() || $cu_user->isAdministrativo() || $cu_user->isDirector() || $cu_user->isSecretario() || $cu_user->isPreceptor()){
            if($cu_user->isProfesor()){
                //return redirect(route('home.select_role'));
            }
            return redirect(route('home.admin'));
        }
        if($cu_user->isProfesor()){
            return redirect(route('home.profesor'));
        }
        if($cu_user->isAlumno()){
            return redirect(route('home.alumno'));
        }
        return view('home');
    }

    public function indexProfesor(){

        $cu_user = Auth::user();

        $profesor = $cu_user->profesor;


        return view('profesor_dashboard.index')->with(['cu_user'=>$cu_user, 'profesor' => $profesor]);
    }

    public function indexAdmin(){
        return view('home');
    }

    public function selectRole(){
        return view('select_role');
    }


}
