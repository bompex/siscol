<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateTopicRequest;
use App\Http\Requests\UpdateTopicRequest;
use App\Http\Requests\CreateCommentRequest;

use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

use App\Repositories\CursoRepository;
use App\Repositories\AlumnoRepository;
use App\Repositories\ProfesorRepository;
use App\Repositories\MateriaRepository;
use App\Repositories\CursoMateriaRepository;
use App\Repositories\TopicRepository;
use App\Repositories\TopicCursoMateriaRepository;
use App\Repositories\TopicCommentRepository;

use App\Models\Curso;
use App\Models\CursoMateria;
use App\Models\Topic;
use App\Models\TopicCursoMateria;

use Illuminate\Support\Facades\Auth;

use Flash;
use Response;

use App\Utils\NotificationHelper;

class CursoMateriaForoController extends AppBaseController
{
    private $topicRepository;
    private $topicCursoMateriaRepository;
    private $cursoMateriaRepository;
    private $topicCommentRepository;

    public function __construct(TopicRepository $topicRepo, TopicCursoMateriaRepository $topicCursoMateriaRepo, CursoMateriaRepository $cursoMateriaRepo, TopicCommentRepository $topicCommentRepo)
    {
        $this->topicRepository = $topicRepo;
        $this->topicCursoMateriaRepository = $topicCursoMateriaRepo;
        $this->cursoMateriaRepository = $cursoMateriaRepo;
        $this->topicCommentRepository = $topicCommentRepo;
    }

    /**
     * Display a listing of the Curso.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index($id)
    {
        $cu_user = Auth::user();
        $cursoMateria = $this->cursoMateriaRepository->find($id);

        if($cu_user->isAlumno()){
            $topics = $cursoMateria->topics()->where('active',1)->orderBy('sort', 'asc')->orderBy('created_at', 'desc')->get();
        }else{
            $topics = $cursoMateria->topics()->orderBy('sort', 'asc')->orderBy('created_at', 'desc')->get();   
        }

        return view('curso_materia_foro.index')
            ->with(['cu_user'=>$cu_user, 'cursoMateria'=> $cursoMateria, 'topics' => $topics]);
    }

    public function showTopic($id){
        $cu_user = Auth::user();


        if(!$cu_user->isProfesor() && !$cu_user->isAlumno()){
            return redirect(route('supervision.show_topic',$id));
        }
        

        $topic = $this->topicRepository->find($id);

        $comments = $topic->comments()->whereNull('comment_parent_id')->orderBy('created_at', 'DESC')->get();

        $cursoMateria = $topic->cursoMateria[0];

        return view('curso_materia_foro.show_topic')
            ->with(['cu_user' => $cu_user ,'cursoMateria'=> $cursoMateria, 'topic' => $topic, 'comments' => $comments]);
    }

    // public function showTopicAlumno($topic_id){
    //     $cu_user = Auth::user();

    //     $topic = $this->topicRepository->find($topic_id);

    //     $comments = $topic->comments()->orderBy('created_at', 'ASC')->get();

    //     $cursoMateria = $topic->cursoMateria[0];

    //     return view('curso_materia_foro.show_topic_alumno')
    //         ->with(['cu_user' => $cu_user ,'cursoMateria'=> $cursoMateria, 'topic' => $topic, 'comments' => $comments]);
    // }

    public function create_comment($topic_id){
        $cu_user = Auth::user();

        $topic = $this->topicRepository->find($topic_id);

        $cursoMateria = $topic->cursoMateria[0];

        $parent_id = null;

        if(isset($_GET['parent_id']) && is_numeric($_GET['parent_id'])){
            $parent_id = $_GET['parent_id'];
        }

        return view('curso_materia_foro.comment_form_modal')
            ->with(['cu_user' => $cu_user ,'cursoMateria'=> $cursoMateria, 'topic' => $topic, 'parent_id' => $parent_id]);
    }

    public function store_comment($topic_id, CreateCommentRequest $request){
        $cu_user = Auth::user();

        $input = $request->all();

        $topic = $this->topicRepository->find($topic_id);

        $input['user_id'] = $cu_user->id;
        $input['topic_id'] = $topic_id;
        $input['active'] = 1;


        $this->topicCommentRepository->create($input);
        
        if($cu_user->isProfesor()){
            NotificationHelper::nofity_curso($topic->cursoMateria[0]->curso, trans('notify.nuevo_comentario_tablon',['usuario'=> $cu_user->profesor->nombre, 'tablon'=>$topic->subject]), route('curso_materia_foro.show_topic', $topic->id));
        }else{
            NotificationHelper::nofity_profesor($topic->cursoMateria[0]->profesor, trans('notify.nuevo_comentario_tablon',['usuario'=> $cu_user->alumno->nombre_curso, 'tablon'=>$topic->subject]), route('curso_materia_foro.show_topic', $topic->id));
        }
        

        return redirect(route('curso_materia_foro.show_topic', $topic_id));
        
    }

    public function destroy_comment($topic_id, $id)
    {
        $topicComment = $this->topicCommentRepository->find($id);

        if (empty($topicComment)) {
            Flash::error(trans('page.form.action.message.not_found'));

            return redirect(route('curso_materia_foro.show_topic', $topic_id));
        }

        $topicComment->disable();

        Flash::success('El Comentario se ha ocultado.');

        return redirect(route('curso_materia_foro.show_topic', $topic_id));
    }

}
