<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateMateriaRequest;
use App\Http\Requests\UpdateMateriaRequest;
use App\Repositories\MateriaRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Flash;
use Response;

use App\Models\Materia;

class MateriaController extends AppBaseController
{
    /** @var  MateriaRepository */
    private $materiaRepository;

    public function __construct(MateriaRepository $materiaRepo)
    {
        $this->materiaRepository = $materiaRepo;
    }

    /**
     * Display a listing of the Materia.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $nivel = $request->input('nivel');

        if(!$nivel){
           $nivel = 'Secundario';
        }

        $materia_nivel = array_flip(Materia::$nivel);
        $materias = $this->materiaRepository->getByNivel($materia_nivel[$nivel]);

        return view('materia.index')
            ->with(['materias' => $materias, 'nivel' => $nivel, 'gender' => Materia::$gender ]);
    }

    /**
     * Show the form for creating a new Materia.
     *
     * @return Response
     */
    public function create(Request $request)
    {
        $materia = new Materia();
        $nivel = $request->input('nivel');
        if($nivel){
            $materia_nivel = array_flip(Materia::$nivel);
            $preset_nivel = $materia_nivel[$nivel];
        }else{
            $preset_nivel = 0;
        }

        $nivel = Materia::$nivel;

        return view('materia.create')->with(['nivel' => $nivel, 'preset_nivel' => $preset_nivel, 'gender' => Materia::$gender, 'materia' => $materia ]);
    }

    /**
     * Store a newly created Materia in storage.
     *
     * @param CreateMateriaRequest $request
     *
     * @return Response
     */
    public function store(CreateMateriaRequest $request)
    {
        $input = $request->all();

        try {

            $materia = $this->materiaRepository->create($input);

            Flash::success(trans('page.form.action.message.success', ['model' => trans('page.model.materia')]));

        } catch (\Exception $e) {
            Log::error($e->getMessage());
            Flash::error(trans('page.form.action.message.not_save', ['model' => trans('page.model.materia')]));
        }

        

        return redirect(route('materia.index',['nivel'=> Materia::$nivel[$materia->primario]]));
    }

    /**
     * Display the specified Materia.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $materia = $this->materiaRepository->find($id);

        if (empty($materia)) {
            Flash::error('Materia not found');

            return redirect(route('materia.index'));
        }

        return view('materia.show')->with(['materia' => $materia, 'gender' => Materia::$gender]);
    }

    /**
     * Show the form for editing the specified Materia.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $materia = $this->materiaRepository->find($id);

        if (empty($materia)) {
            Flash::error('Materia not found');

            return redirect(route('materia.index'));
        }

        $nivel = Materia::$nivel;

        return view('materia.edit')->with(['materia' => $materia,'nivel' => $nivel,  'preset_nivel' => $materia->primario, 'gender' => Materia::$gender ]);
    }

    /**
     * Update the specified Materia in storage.
     *
     * @param int $id
     * @param UpdateMateriaRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateMateriaRequest $request)
    {
        $materia = $this->materiaRepository->find($id);

        if (empty($materia)) {
            Flash::error('Materia not found');

            return redirect(route('materia.index'));
        }

        try {

            $materia = $this->materiaRepository->update($request->all(), $id);
            Flash::success(trans('page.form.action.message.update', ['model' => trans('page.model.materia')]));

        } catch (\Exception $e) {
            Log::error($e->getMessage());
            Flash::error(trans('page.form.action.message.not_save', ['model' => trans('page.model.materia')]));
        }

        return redirect(route('materia.index',['nivel'=> materia::$nivel[$materia->primario]]));
    }

    /**
     * Remove the specified Materia from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $materia = $this->materiaRepository->find($id);

        if (empty($materia)) {
            Flash::error('Materia not found');

            return redirect(route('materia.index'));
        }

        $this->materiaRepository->delete($id);

        Flash::success('Materia deleted successfully.');

        return redirect(route('materia.index',['nivel'=> materia::$nivel[$materia->primario]]));
    }
}
