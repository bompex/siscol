<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateTopicRequest;
use App\Http\Requests\UpdateTopicRequest;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

use App\Repositories\CursoRepository;
use App\Repositories\AlumnoRepository;
use App\Repositories\ProfesorRepository;
use App\Repositories\MateriaRepository;
use App\Repositories\CursoMateriaRepository;
use App\Repositories\CursoMateriaHorarioRepository;
use App\Repositories\TopicRepository;
use App\Repositories\TopicCursoMateriaRepository;
use App\Repositories\FileRepository;

use App\Models\Curso;
use App\Models\CursoMateria;
use App\Models\CursoMateriaHorario;
use App\Models\CursoMateriaAlumnoComment;
use App\Models\Topic;
use App\Models\Evaluacion;
use App\Models\TopicFile;

use Illuminate\Support\Facades\Auth;

use Flash;
use Response;

class HomeAlumnoController extends AppBaseController
{
    /** @var  CursoRepository */
    private $cursoRepository;
    private $materiaRepository;
    private $profesorRepository;
    private $cursoMateriaRepository;
    private $cursoMateriaHorarioRepository;
    private $alumnoRepository;
    private $topicRepository;
    private $topicCursoMateriaRepository;
    private $fileRepository;

    public function __construct(CursoRepository $cursoRepo, MateriaRepository $materiaRepo, ProfesorRepository $profesorRepo, CursoMateriaRepository $cursoMateriaRepo, AlumnoRepository $alumnoRepo, CursoMateriaHorarioRepository $cursoMateriaHorarioRepo, TopicRepository $topicRepo, TopicCursoMateriaRepository $topicCursoMateriaRepo, FileRepository $fileRepo)
    {
        $this->cursoRepository = $cursoRepo;
        $this->materiaRepository = $materiaRepo;
        $this->profesorRepository = $profesorRepo;
        $this->cursoMateriaRepository = $cursoMateriaRepo;
        $this->cursoMateriaHorarioRepository = $cursoMateriaHorarioRepo;
        $this->alumnoRepository = $alumnoRepo;
        $this->topicRepository = $topicRepo;
        $this->topicCursoMateriaRepository = $topicCursoMateriaRepo;
        $this->fileRepository = $fileRepo;
    }


    public function index()
    {
        $today = date('Y-m-d H:i:s',time());
        $cu_user = Auth::user();
        $alumno = $cu_user->alumno;

        if(!$alumno->curso){
            return view('home')
            ->with(['cu_user' => $cu_user ,'alumno'=> $alumno]);
        }

        $anuncios = $alumno->curso->getAnuncios($alumno->gender);
        $calendar = $alumno->curso->getCalendar($alumno->gender);


        return view('home_alumno.index')
            ->with(['cu_user' => $cu_user ,'alumno'=> $alumno,'anuncios'=>$anuncios, 'calendar' => $calendar]);
    }



    public function showNota($cusro_materia_horario_id){
    	$cu_user = Auth::user();
        $alumno = $cu_user->alumno;

        $cursoMateriaHorario = $this->cursoMateriaHorarioRepository->find($cusro_materia_horario_id);
        $cursoMateria = $cursoMateriaHorario->cursoMateria;

        return view('home_alumno.curso_materia_horario_nota')
            ->with(['cursoMateriaHorario' => $cursoMateriaHorario ,'cursoMateria'=> $cursoMateria]);
    }


    public function indexMateria($curso_materia_id){
        $today = date('Y-m-d H:i:s',time());
        $cu_user = Auth::user();
        $alumno = $cu_user->alumno;

        $cursoMateria = $this->cursoMateriaRepository->find($curso_materia_id);

        $topics = $cursoMateria->topics()->where('active',1)->where('private',0)->orderBy('sort', 'asc')->orderBy('created_at', 'desc')->get();

        $evaluaciones = $cursoMateria->evaluaciones()->where('fecha_public','<=', $today)->where('fecha_fin','>=', $today)->orderBy('created_at', 'desc')->get();
        $evaluaciones_prev = $cursoMateria->evaluaciones()->where('fecha_fin','<', $today)->orderBy('created_at', 'asc')->get();

        $evaluacion_types = Evaluacion::$types;

        $anuncios = $cursoMateria->anuncios()->where('fecha','<=', $today)->orderBy('fecha', 'desc')->get();
        $calendar = $cursoMateria->getCalendar($alumno->gender);

        return view('home_alumno.index_materia')
            ->with(['cu_user' => $cu_user ,'cursoMateria'=> $cursoMateria, 'alumno' => $alumno, 'topics'=>$topics, 'evaluaciones' => $evaluaciones, 'evaluaciones_prev' => $evaluaciones_prev, 'evaluacion_types' => $evaluacion_types, 'anuncios' => $anuncios, 'calendar' => $calendar]);
    }

    public function curso_previo(){
        $today = date('Y-m-d H:i:s',time());
        $cu_user = Auth::user();
        $alumno = $cu_user->alumno;

        $curso  = $cu_user->alumno->curso_previo;
        $anuncios = $curso->getAnuncios($alumno->gender);
        $calendar = $curso->getCalendar($alumno->gender);


        return view('home_alumno.previo')
            ->with(['cu_user' => $cu_user, 'curso' => $curso, 'anuncios'=>$anuncios, 'calendar' => $calendar]);
    }
}