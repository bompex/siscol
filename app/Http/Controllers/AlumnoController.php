<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateAlumnoRequest;
use App\Http\Requests\UpdateAlumnoRequest;
use App\Repositories\AlumnoRepository;
use App\Repositories\CursoRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use App\Models\Alumno;
use App\Models\User;
use Flash;
use Response;
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\AlumnoExport;

class AlumnoController extends AppBaseController
{
    /** @var  AlumnoRepository */
    private $alumnoRepository;
    private $cursoRepository;

    public function __construct(AlumnoRepository $alumnoRepo, CursoRepository $cursoRepo)
    {
        $this->alumnoRepository = $alumnoRepo;
        $this->cursoRepository = $cursoRepo;
    }

    /**
     * Display a listing of the Alumno.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $parameters = $request->all();
        $curso_id = $request->input('curso_id');
        $order_by = $request->input('sort');
        $order_type = $request->input('direction');

        if(isset($parameters['download-xls'])){
            return Excel::download(new AlumnoExport($this->alumnoRepository, $curso_id, $order_by, $order_type), 'Alumnos.xlsx');
        }

        $alumno = $this->alumnoRepository->getAll($curso_id, $order_by, $order_type);

        $cursos = $this->cursoRepository->selectArray();

        return view('alumno.index')
            ->with(['alumno' => $alumno,
                    'cursos' => $cursos,
                    'curso_id' => $curso_id]);
    }

    /**
     * Show the form for creating a new Alumno.
     *
     * @return Response
     */
    public function create()
    {
        $alumno = new Alumno();
        $alumno->user = new User();

        $gender = Alumno::$gender;

        return view('alumno.create')->with(['alumno' => $alumno, 'genders' => $gender]);
    }

    /**
     * Store a newly created Alumno in storage.
     *
     * @param CreateAlumnoRequest $request
     *
     * @return Response
     */
    public function store(CreateAlumnoRequest $request)
    {
        $input = $request->all();

        try {

            $alumno = $this->alumnoRepository->create($input);
            Flash::success(trans('page.form.action.message.success', ['model' => trans('page.model.alumno')]));

        } catch (\Exception $e) {
            Log::error($e->getMessage());
            Flash::error(trans('page.form.action.message.not_save', ['model' => trans('page.model.alumno')]));
        }

        return redirect(route('alumno.index'));
    }

    /**
     * Display the specified Alumno.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $alumno = $this->alumnoRepository->find($id);

        if (empty($alumno)) {
            Flash::error(trans('page.form.action.message.not_found'));

            return redirect(route('alumno.index'));
        }

        $gender = Alumno::$gender;

        return view('alumno.show')->with(['alumno'=> $alumno, 'gender' => $gender]);
    }

    /**
     * Show the form for editing the specified Alumno.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $alumno = $this->alumnoRepository->find($id);

        if (empty($alumno)) {
            Flash::error(trans('page.form.action.message.not_found'));

            return redirect(route('alumno.index'));
        }

        $gender = Alumno::$gender;


        return view('alumno.edit')->with(['alumno' => $alumno, 'genders' => $gender]);
    }

    /**
     * Update the specified Alumno in storage.
     *
     * @param int $id
     * @param UpdateAlumnoRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateAlumnoRequest $request)
    {
        $alumno = $this->alumnoRepository->find($id);

        if (empty($alumno)) {
            Flash::error(trans('page.form.action.message.not_found'));

            return redirect(route('alumno.index'));
        }

        try {

            $alumno = $this->alumnoRepository->update($request->all(), $id);
            Flash::success(trans('page.form.action.message.update', ['model' => trans('page.model.alumno')]));

        } catch (\Exception $e) {
            Log::error($e->getMessage());
            Flash::error(trans('page.form.action.message.not_save', ['model' => trans('page.model.alumno')]));
        }

        return redirect(route('alumno.index'));

    }

    /**
     * Remove the specified Alumno from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $alumno = $this->alumnoRepository->find($id);

        if (empty($alumno)) {
            Flash::error(trans('page.form.action.message.not_found'));

            return redirect(route('alumno.index'));
        }

        $this->alumnoRepository->delete($id);

        Flash::success('Alumno deleted successfully.');

        return redirect(route('alumno.index'));
    }
}
