<?php

namespace App\Http\Controllers;

use App\Models\Message;
use Illuminate\Http\Request;
use App\Repositories\UserRepository;
use App\Repositories\MessageRepository;
use App\Repositories\FileRepository;
use Flash;
use Response;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;

use App\Utils\NotificationHelper;

class MessageController extends AppBaseController
{

    private $userRepository;
    private $messageRepository;
    private $fileRepository;

    public function __construct(UserRepository $userRepo, MessageRepository $messageRepo, FileRepository $fileRepo)
    {
        $this->userRepository = $userRepo;
        $this->messageRepository = $messageRepo;
        $this->fileRepository = $fileRepo;
    }

    public function show($user_id){
        $cu_user = Auth::user();
        $user = $this->userRepository->find($user_id);

        $messages = $this->messageRepository->getMessagesByConversation($cu_user->id, $user->id);

        if($cu_user->isAlumno() && $user->isAlumno()){
            return redirect(route('home.alumno'));
        }
        $redir_back = "#";

        if($cu_user->isAlumno()){
            foreach($cu_user->alumno->curso->cursoMaterias as $cuMa){
                if($cuMa->profesor->user->id == $user->id){
                    $redir_back = route('home_alumno.index_materia',$cuMa->id);
                }
            }
        }
        if($cu_user->isProfesor()){
            if($user->isAlumno()){
                foreach($user->alumno->curso->cursoMaterias as $cuMa){
                    if($cuMa->profesor->user->id == $cu_user->id){
                    $redir_back = route('profesor_curso_materia.index',$cuMa->id);
                    }
                } 
            }
            
        }
        if(isset($_GET['reply']) && $_GET['reply'] != '' && isset($_GET['reply_id']) && is_numeric($_GET['reply_id']) ){
            $message = Message::getReplyMessage($_GET['reply'], $_GET['reply_id'], $user);
        }else{
            $message = '';
        }



        if(isset($_GET['mode']) && $_GET['mode'] == 'modal'){
            return view('message.show_modal')
            ->with(['messages' => $messages, 'cu_user' => $cu_user,'user' => $cu_user, 'user_receiver' => $user, 'redir_back' => $redir_back, 'message'=>$message]);
        }
        return view('message.show')
            ->with(['messages' => $messages, 'cu_user' => $cu_user,'user' => $cu_user, 'user_receiver' => $user, 'redir_back' => $redir_back, 'message'=>$message]);
    }

    public function create_modal($receiver_id)
    {
        $user_sender = Auth::user();
        $user_receiver = $this->userRepository->find($receiver_id);

        $message = '';

        return view('message.create_modal')
            ->with(['user_sender'=> $user_sender, 'user_receiver' => $user_receiver, 'message'=>$message]);
    }


    public function store($receiver_id, Request $request)
    {
        $input = $request->all();

        $user_sender = Auth::user();

        $input['sender_user_id'] = $user_sender->id;
        $input['receiver_user_id'] = $receiver_id;
        $user_receiver = $this->userRepository->find($receiver_id);
        if(!($user_sender->isAlumno() && $user_receiver->isAlumno())){
        
            try {

                if(isset($input['files_del']) && count($input['files_del']) > 0){
                    foreach($input['files_del'] as $file_del_id){
                        $this->fileRepository->delete($file_del_id);
                    }
                }

                $message = $this->messageRepository->create($input);
   
                //NotificationHelper::nofity($user_receiver, trans('notify.nuevo_mensaje',['usuario'=> $user_sender->lastname.", ".$user_sender->firstname]), route('message.show', [$user_sender->id]));
            } catch (\Exception $e) {
                Log::error($e->getMessage());
            }
        }

        if(isset($_GET['mode']) && $_GET['mode'] == 'modal'){
            return redirect(route('message.show', [$user_receiver->id, 'mode' => 'modal']));
        }

        return redirect(route('message.show', $user_receiver->id));
    }

}
