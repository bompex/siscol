<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateTopicRequest;
use App\Http\Requests\UpdateTopicRequest;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

use App\Repositories\CursoRepository;
use App\Repositories\AlumnoRepository;
use App\Repositories\ProfesorRepository;
use App\Repositories\MateriaRepository;
use App\Repositories\CursoMateriaRepository;
use App\Repositories\CursoMateriaHorarioRepository;
use App\Repositories\TopicRepository;
use App\Repositories\TopicCursoMateriaRepository;
use App\Repositories\FileRepository;

use App\Models\Curso;
use App\Models\CursoMateria;
use App\Models\CursoMateriaHorario;
use App\Models\CursoMateriaAlumnoComment;
use App\Models\Topic;
use App\Models\Evaluacion;
use App\Models\TopicFile;

use Illuminate\Support\Facades\Auth;

use App\Utils\NotificationHelper;

use Flash;
use Response;

class ProfesorCursoMateriaController extends AppBaseController
{
    /** @var  CursoRepository */
    private $cursoRepository;
    private $materiaRepository;
    private $profesorRepository;
    private $cursoMateriaRepository;
    private $cursoMateriaHorarioRepository;
    private $alumnoRepository;
    private $topicRepository;
    private $topicCursoMateriaRepository;
    private $fileRepository;

    public function __construct(CursoRepository $cursoRepo, MateriaRepository $materiaRepo, ProfesorRepository $profesorRepo, CursoMateriaRepository $cursoMateriaRepo, AlumnoRepository $alumnoRepo, CursoMateriaHorarioRepository $cursoMateriaHorarioRepo, TopicRepository $topicRepo, TopicCursoMateriaRepository $topicCursoMateriaRepo, FileRepository $fileRepo)
    {
        $this->cursoRepository = $cursoRepo;
        $this->materiaRepository = $materiaRepo;
        $this->profesorRepository = $profesorRepo;
        $this->cursoMateriaRepository = $cursoMateriaRepo;
        $this->cursoMateriaHorarioRepository = $cursoMateriaHorarioRepo;
        $this->alumnoRepository = $alumnoRepo;
        $this->topicRepository = $topicRepo;
        $this->topicCursoMateriaRepository = $topicCursoMateriaRepo;
        $this->fileRepository = $fileRepo;
    }

    /**
     * Display a listing of the Curso.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index($id)
    {
        $today = date('Y-m-d H:i:s',time());
        $week_past = date('Y-m-d H:i:s',time()-604800);
        $cu_user = Auth::user();
        $profesor = $cu_user->profesor;

        $cursoMateria = $this->cursoMateriaRepository->find($id);

        if($cursoMateria->profesor->id != $profesor->id){
            if(!isset($cursoMateria->profesorSuplente) || $cursoMateria->profesorSuplente->id != $profesor->id){
                Flash::error(trans('page.message.access_denided'));
                return redirect(route('home.profesor'));
            }
            
        }
        if($cursoMateria->materia->gender == "A"){
            $alumnos = $cursoMateria->curso->alumnos()->whereNull('curso_alumno.deleted_at')->where('curso_alumno.status','1')->orderBy('lastname', 'asc')->orderBy('firstname', 'asc')->get();
        }else{
            $alumnos = $cursoMateria->curso->alumnos()->whereNull('curso_alumno.deleted_at')->where('curso_alumno.status','1')->where('alumnos.gender',$cursoMateria->materia->gender)->orderBy('lastname', 'asc')->orderBy('firstname', 'asc')->get();

        }
        
        $topics = $cursoMateria->topics()->where('active',1)->orderBy('sort', 'asc')->orderBy('created_at', 'desc')->get();

        $evaluaciones = $cursoMateria->evaluaciones()->where('fecha_fin','>=', $today)->orderBy('created_at', 'desc')->get();

        $evaluaciones_prev = $cursoMateria->evaluaciones()->where('fecha_fin','<', $today)->orderBy('created_at', 'desc')->get();

        $evaluacion_types = Evaluacion::$types;

        $anuncios = $cursoMateria->anuncios()->where('fecha','<', $today)->orderBy('fecha', 'desc')->get();

        return view('profesor_curso_materia.index')
            ->with(['cu_user' => $cu_user ,'cursoMateria'=> $cursoMateria, 'alumnos' => $alumnos, 'topics'=>$topics, 'evaluaciones' => $evaluaciones, 'evaluaciones_prev' => $evaluaciones_prev, 'evaluacion_types' => $evaluacion_types, 'anuncios' => $anuncios]);
    }

    public function createTopic($curso_materia_id){
        $cu_user = Auth::user();

        $cursoMateria = $this->cursoMateriaRepository->find($curso_materia_id);

        $topic = new Topic();

        return view('profesor_curso_materia.topic_create')
            ->with(['cu_user' => $cu_user ,'cursoMateria'=> $cursoMateria, 'topic' => $topic]);
    }

    public function storeTopic($curso_materia_id, CreateTopicRequest $request){
        $input = $request->all();

        $cu_user = Auth::user();

        $input['user_id'] = $cu_user->id;

        $input['topic_parent_id'] = null;

        try {

            $topic = $this->topicRepository->create($input);
            if($topic->id){
                $tcm['topic_id'] = $topic->id;
                $tcm['curso_materia_id'] = $curso_materia_id;
                $topic_curso_materia = $this->topicCursoMateriaRepository->create($tcm);
            }

            if(isset($input['files_del']) && count($input['files_del']) > 0){
                foreach($input['files_del'] as $file_del_id){
                    $this->fileRepository->delete($file_del_id);
                }
            }

            $cursoMateria = $this->cursoMateriaRepository->find($curso_materia_id);
            NotificationHelper::nofity_curso($cursoMateria->curso, trans('notify.nuevo_tablon',['materia'=>$cursoMateria->materia->name]), route('curso_materia_foro.show_topic', $topic->id));
            Flash::success(trans('page.form.action.message.success', ['model' => trans('page.model.topic')]));

        } catch (\Exception $e) {
            Log::error($e->getMessage());
            Flash::error(trans('page.form.action.message.not_save', ['model' => trans('page.model.topic')]));
        }


        return redirect(route('profesor_curso_materia.index',$curso_materia_id));
    }

    public function editTopic($curso_materia_id, $id){
        $cu_user = Auth::user();

        $cursoMateria = $this->cursoMateriaRepository->find($curso_materia_id);

        $topic = $this->topicRepository->find($id);

        return view('profesor_curso_materia.topic_edit')
            ->with(['cu_user' => $cu_user ,'cursoMateria'=> $cursoMateria, 'topic' => $topic]);
    }

    public function updateTopic($curso_materia_id, $id, CreateTopicRequest $request){
        $input = $request->all();

        $cu_user = Auth::user();

        try {

            $topic = $this->topicRepository->update($input, $id);

            if(isset($input['files_del']) && count($input['files_del']) > 0){
                foreach($input['files_del'] as $file_del_id){
                    $topicFile = TopicFile::where('file_id', $file_del_id)->where('topic_id', $topic->id)->first();
                    if($topicFile){
                        $topicFile->delete();
                    }
                    $this->fileRepository->delete($file_del_id);
                }
            }

            Flash::success(trans('page.form.action.message.success', ['model' => trans('page.model.topic')]));

        } catch (\Exception $e) {
            Log::error($e->getMessage());
            Flash::error(trans('page.form.action.message.not_save', ['model' => trans('page.model.topic')]));
        }


        return redirect(route('profesor_curso_materia.index',$curso_materia_id));
    }


    public function archiveTopic($curso_materia_id, $id)
    {
        $topic = $this->topicRepository->find($id);

        if (empty($topic)) {
            Flash::error(trans('page.form.action.message.not_found'));

            return redirect(route('profesor_curso_materia.index',$curso_materia_id));
        }

        $topic->active = 0;

        $topic->save();

        Flash::success(trans('page.form.action.message.success', ['model' => trans('page.model.topic')]));

        return redirect(route('profesor_curso_materia.index',$curso_materia_id));
    }

    public function publishTopic($curso_materia_id, $id)
    {
        $topic = $this->topicRepository->find($id);

        if (empty($topic)) {
            Flash::error(trans('page.form.action.message.not_found'));

            return redirect(route('profesor_curso_materia.index',$curso_materia_id));
        }

        $topic->active = 1;

        $topic->save();

        Flash::success(trans('page.form.action.message.success', ['model' => trans('page.model.topic')]));

        return redirect(route('profesor_curso_materia.index',$curso_materia_id));
    }
    public function destroyTopic($curso_materia_id, $topic_id)
    {
        $topic = $this->topicRepository->find($topic_id);

        if (empty($topic)) {
            Flash::error(trans('page.form.action.message.not_found'));

            return redirect(route('profesor_curso_materia.index',$curso_materia_id));
        }

        $topic->disable();

        Flash::success('El Tablón se ha eliminado.');

        return redirect(route('curso_materia_foro.index', $curso_materia_id));
    }

    public function sortTopic($curso_materia_id, Request $request)
    {
        $input = $request->all();

        $cantidad = count($input['order']);

        foreach($input['order'] as $index => $topic_id){
            $sort_val = 0 - $cantidad + $index;
            $topic = $this->topicRepository->find($topic_id);
            $topic->sort = $sort_val;
            $topic->save();
        }
        Flash::success('El Tablón se ha actualizado.');

        
    }

    public function addCursoMateriaNota($cusro_materia_horario_id){
        $cursoMateriaHorario = $this->cursoMateriaHorarioRepository->find($cusro_materia_horario_id);
        $cursoMateria = $cursoMateriaHorario->cursoMateria;

        return view('profesor_curso_materia.curso_materia_horario_add')
            ->with(['cursoMateriaHorario' => $cursoMateriaHorario ,'cursoMateria'=> $cursoMateria]);
    }


    public function storeCursoMateriaNota($cusro_materia_horario_id, Request $request){
        $input = $request->all();

        $cursoMateriaHorario = $this->cursoMateriaHorarioRepository->find($cusro_materia_horario_id);

        $cursoMateriaHorario->note = $input['note'];

        $cursoMateriaHorario->save();

        Flash::success(trans('page.form.action.message.success', ['model' => trans('page.label.curso_horario.note')]));

        return redirect(route('profesor_curso_materia.index',$cursoMateriaHorario->cursoMateria->id));
    }

    public function addCursoMateriaAlumnoNota($curso_materia_id, $alumno_id){


        $CursoMateriaAlumnoComment = CursoMateriaAlumnoComment::where('curso_materia_id',$curso_materia_id)->where('alumno_id',$alumno_id)->first();

        $cursoMateria = $this->cursoMateriaRepository->find($curso_materia_id);

        $alumno = $this->alumnoRepository->find($alumno_id);
        
        if(!$CursoMateriaAlumnoComment){
            $CursoMateriaAlumnoComment = new CursoMateriaAlumnoComment();
        }

        return view('profesor_curso_materia.curso_materia_alumno_comment_add')
            ->with(['CursoMateriaAlumnoComment' => $CursoMateriaAlumnoComment ,'cursoMateria'=> $cursoMateria, 'alumno'=> $alumno]);
    }


    public function storeCursoMateriaAlumnoNota($curso_materia_id, $alumno_id, Request $request){
        $input = $request->all();

        $cu_user = Auth::user();
        $profesor = $cu_user->profesor;

        $CursoMateriaAlumnoComment = CursoMateriaAlumnoComment::where('curso_materia_id',$curso_materia_id)->where('alumno_id',$alumno_id)->first();

        $cursoMateria = $this->cursoMateriaRepository->find($curso_materia_id);

        $alumno = $this->alumnoRepository->find($alumno_id);
        
        if(!$CursoMateriaAlumnoComment){
            $CursoMateriaAlumnoComment = new CursoMateriaAlumnoComment();
        }

        $CursoMateriaAlumnoComment->curso_materia_id = $curso_materia_id;
        $CursoMateriaAlumnoComment->alumno_id = $alumno_id;
        $CursoMateriaAlumnoComment->profesor_id = $profesor->id;
        $CursoMateriaAlumnoComment->body = $input['body'];

        $CursoMateriaAlumnoComment->save();

        Flash::success(trans('page.form.action.message.success', ['model' => trans('page.label.curso.alumno_comment')]));

        return redirect(route('profesor_curso_materia.index',$curso_materia_id));
    }


    public function history(request $request)
    {
        $today = date('Y-m-d H:i:s',time());
        $cu_user = Auth::user();
        $periodo = $request->input('periodo');

        if(!$periodo){
            $periodo = date('Y',strtotime('-1 year'));
        }

        $curso_materias = $cu_user->profesor->cursoMateriasHistory($periodo);
        $periodos = $this->cursoRepository->getPeriodos(false);
   
        
        return view('profesor_curso_materia.history')
            ->with(['cu_user' => $cu_user ,'curso_materias'=> $curso_materias, 'periodos' => $periodos, 'periodo' => $periodo]);
    }
}
