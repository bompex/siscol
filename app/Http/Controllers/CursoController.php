<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateCursoRequest;
use App\Http\Requests\UpdateCursoRequest;
use App\Repositories\CursoRepository;
use App\Repositories\AlumnoRepository;
use App\Repositories\ProfesorRepository;
use App\Repositories\MateriaRepository;
use App\Repositories\CursoMateriaRepository;
use App\Repositories\CursoMateriaHorarioRepository;
use App\Repositories\CursoAlumnoRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

use App\Models\Curso;
use App\Models\CursoMateriaHorario;
use App\Models\Alumnos;

use Flash;
use Response;

class CursoController extends AppBaseController
{
    /** @var  CursoRepository */
    private $cursoRepository;
    private $materiaRepository;
    private $profesorRepository;
    private $cursoMateriaRepository;
    private $cursoMateriaHorarioRepository;
    private $alumnoRepository;
    private $cursoAlumnoRepository;

    public function __construct(CursoRepository $cursoRepo, MateriaRepository $materiaRepo, ProfesorRepository $profesorRepo, CursoMateriaRepository $cursoMateriaRepo, AlumnoRepository $alumnoRepo, CursoMateriaHorarioRepository $cursoMateriaHorarioRepo, CursoAlumnoRepository $cursoAlumnoRepo)
    {
        $this->cursoRepository = $cursoRepo;
        $this->materiaRepository = $materiaRepo;
        $this->profesorRepository = $profesorRepo;
        $this->cursoMateriaRepository = $cursoMateriaRepo;
        $this->cursoMateriaHorarioRepository = $cursoMateriaHorarioRepo;
        $this->alumnoRepository = $alumnoRepo;
        $this->cursoAlumnoRepository = $cursoAlumnoRepo;
    }

    /**
     * Display a listing of the Curso.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $nivel = $request->input('nivel');
        $periodo = $request->input('periodo');

        if(!$periodo){
            $periodo = date('Y',time());
        }
        if($nivel){
            $curso_nivel = array_flip(Curso::$nivel);
            $cursos = $this->cursoRepository->getByNivel($curso_nivel[$nivel], $periodo);
            $periodos = $this->cursoRepository->getPeriodos($curso_nivel[$nivel]);
        }else{
            $cursos = $this->cursoRepository->getByNivel(false, $periodo);
            $periodos = $this->cursoRepository->getPeriodos(false);
        }  

        $periodos[date('Y',time())] = date('Y',time());      
        
        return view('curso.index')
            ->with(['cursos'=> $cursos, 'nivel' => $nivel, 'periodos' => $periodos, 'periodo' => $periodo]);
    }

    /**
     * Show the form for creating a new Curso.
     *
     * @return Response
     */
    public function create(Request $request)
    {
        $nivel = $request->input('nivel');
        if($nivel){
            $curso_nivel = array_flip(Curso::$nivel);
            $preset_nivel = $curso_nivel[$nivel];
        }else{
            $preset_nivel = false;
        }

        $cursos_last = $this->cursoRepository->getByNivel(false, date('Y',strtotime('-1 year')));

        $curso = new Curso();

        $turnos = Curso::$turnos;

        $nivel = Curso::$nivel;

        return view('curso.create')->with(['curso' => $curso, 'turnos'=>$turnos, 'nivel' => $nivel, 'preset_nivel' => $preset_nivel, 'cursos_last' => $cursos_last]);
    }

    /**
     * Store a newly created Curso in storage.
     *
     * @param CreateCursoRequest $request
     *
     * @return Response
     */
    public function store(CreateCursoRequest $request)
    {
        $input = $request->all();

        

        try {

            $curso = $this->cursoRepository->create($input);

            if($input['accion'] == 1){
                $curso = $this->cursoRepository->copyConfigFromExist($curso, $input['curso_exist']);
            }
            Flash::success(trans('page.form.action.message.success', ['model' => trans('page.model.curso')]));

        } catch (\Exception $e) {
            Log::error($e->getMessage());
            Flash::error(trans('page.form.action.message.not_save', ['model' => trans('page.model.curso')]));
        }




        return redirect(route('curso.index',['nivel'=> Curso::$nivel[$curso->primario]]));
    }



    /**
     * Show the form for add Materias and Profesores in Curso.
     *
     * @return Response
     */
    public function addMateria($id)
    {
        $curso = $this->cursoRepository->find($id);

        $materias = $this->materiaRepository->selectArray($curso->primario);
        $profesores = $this->profesorRepository->selectArray($curso->primario);
        
        $profesores_sup = $profesores;

        $turnos = Curso::$turnos;

        return view('curso.add_materia')->with(['curso' => $curso, 'materias'=>$materias, 'profesores'=>$profesores, 'profesores_sup'=>$profesores_sup]);
    }


    /**
     * Store the materias added to created Curso in storage.
     *
     * @param Request $request
     *
     * @return Response
     */

    public function storeMaterias($id, Request $request)
    {
        $input = $request->all();

        $cm = [];

        $curso = $this->cursoRepository->find($id);

        try {

            foreach($input['materia_id'] as $index => $val){

                $cm = ['curso_id' => $id,
                        'materia_id' => $input['materia_id'][$index],
                        'profesor_id' =>  $input['profesor_id'][$index],
                        'profesor_suplente_id' =>  $input['profesor_suplente_id'][$index]];
                if($input['curso_materia_id'][$index] == "-"){          //Crea nuevo cursoMateria
                    $this->cursoMateriaRepository->create($cm);
                }else{
                    if($input['curso_materia_del_id'][$index] == "-"){  //Modifica cursoMateria
                        $this->cursoMateriaRepository->update($cm, $input['curso_materia_id'][$index]);
                    }else{                                              //Elimina cursoMateria
                        $this->cursoMateriaRepository->delete($input['curso_materia_del_id'][$index]);
                    }
                }

            }

            Flash::success(trans('page.form.action.message.success', ['model' => trans('page.model.curso')]));

        } catch (\Exception $e) {
            Log::error($e->getMessage());
            Flash::error(trans('page.form.action.message.not_save', ['model' => trans('page.model.curso')]));
        }
        return redirect(route('curso.index',['nivel'=> Curso::$nivel[$curso->primario]]));
    }


     public function addAlumnos($id)
    {
        $curso = $this->cursoRepository->find($id);

        $alumno = $this->alumnoRepository->getAll();

        $current_alumnos = [];
        foreach($curso->cursoAlumnos as $cursoAlumno){
            $current_alumnos[] = $cursoAlumno->alumno_id;
        }

        return view('curso.add_alumno')->with(['curso' => $curso, 'alumnos'=>$alumno, 'current_alumnos'=>$current_alumnos]);
    }

    public function storeAlumnos($id, Request $request)
    {
        $input = $request->all();

        $alumnos_id = [];

        $curso = $this->cursoRepository->find($id);
        
        if($input['alumnos_id']){
            $alumnos_id = explode(',', $input['alumnos_id']);
        }
        
        try {

            $curso = $this->cursoRepository->storeAlumnos($id, $alumnos_id);
            Flash::success(trans('page.form.action.message.success', ['model' => trans('page.model.curso')]));

        } catch (\Exception $e) {
            Log::error($e->getMessage());
            Flash::error(trans('page.form.action.message.not_save', ['model' => trans('page.model.curso')]));
        }

        return redirect(route('curso.index',['nivel'=> Curso::$nivel[$curso->primario]]));
    }


    public function addCursoMateriaHorario($id){

        $curso = $this->cursoRepository->find($id);

        $dias = CursoMateriaHorario::$dias;

        $hs_limit = Curso::$horario_turno[$curso->turno];

        for($i=$hs_limit['from']; $i <= $hs_limit['to'] ; $i++) { 
            $horas[$i] = $i;
        }



        return view('curso.add_materia_horario')->with([
                'curso' => $curso,
                'dias' => $dias,
                'horamin' => $hs_limit['from'],
                'horamax' => $hs_limit['to'],
                            ]);
    }


    public function storeCursoMateriaHorario($id, Request $request)
    {
        $input = $request->all();

        $curso = $this->cursoRepository->find($id);
        
        try {

            foreach($input['dia'] as $curso_materia_id => $dato){

                foreach($input['dia'][$curso_materia_id] as $index => $val){
                    $cmh = ['curso_materia_id' => $curso_materia_id,
                            'dia' => $input['dia'][$curso_materia_id][$index],
                            'hora_inicio' =>  $input['hora_inicio'][$curso_materia_id][$index],
                            'hora_fin' =>  $input['hora_fin'][$curso_materia_id][$index]];
               
                    if($input['curso_materia_horario_id'][$curso_materia_id][$index] == "-"){          //Crea nuevo cursoMateriaHorario
                        $this->cursoMateriaHorarioRepository->create($cmh);
                    }else{
                        if($input['curso_materia_horario_del_id'][$curso_materia_id][$index] == "-"){  //Modifica cursoMateriaHorario
                            $this->cursoMateriaHorarioRepository->update($cmh, $input['curso_materia_horario_id'][$curso_materia_id][$index]);
                        }else{                                              //Elimina cursoMateriaHorario
                            $this->cursoMateriaHorarioRepository->delete($input['curso_materia_horario_del_id'][$curso_materia_id][$index]);
                        }
                    }
                }

            }
            Flash::success(trans('page.form.action.message.success', ['model' => trans('page.model.curso')]));

        } catch (\Exception $e) {
            Log::error($e->getMessage());
            Flash::error(trans('page.form.action.message.not_save', ['model' => trans('page.model.curso')]));
        }

        return redirect(route('curso.index',['nivel'=> Curso::$nivel[$curso->primario]]));
    }




    /**
     * Display the specified Curso.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $curso = $this->cursoRepository->find($id);

        if (empty($curso)) {
            Flash::error(trans('page.form.action.message.not_found'));

             return redirect(route('curso.index'));
        }

        $alumnos = $curso->Alumnos()->orderBy('lastname', 'asc')->orderBy('firstname', 'asc')->get();

        return view('curso.show')->with(['curso'=> $curso, 'alumnos' => $alumnos]);
    }

    /**
     * Show the form for editing the specified Curso.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $curso = $this->cursoRepository->find($id);


        if (empty($curso)) {
            Flash::error(trans('page.form.action.message.not_found'));

            return redirect(route('curso.index'));
        }

        $turnos = Curso::$turnos;

        $nivel = Curso::$nivel;

        return view('curso.edit')->with(['curso' => $curso, 'turnos'=>$turnos, 'nivel' => $nivel, 'preset_nivel' => $curso->primario]);
    }

    /**
     * Update the specified Curso in storage.
     *
     * @param int $id
     * @param UpdateCursoRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateCursoRequest $request)
    {
        $curso = $this->cursoRepository->find($id);

        if (empty($curso)) {
            Flash::error(trans('page.form.action.message.not_found'));

            return redirect(route('curso.index'));
        }

        


        try {

            $curso = $this->cursoRepository->update($request->all(), $id);
            Flash::success(trans('page.form.action.message.update', ['model' => trans('page.model.curso')]));

        } catch (\Exception $e) {
            Log::error($e->getMessage());
            Flash::error(trans('page.form.action.message.not_save', ['model' => trans('page.model.curso')]));
        }

        return redirect(route('curso.index',['nivel'=> Curso::$nivel[$curso->primario]]));
    }

    /**
     * Remove the specified Curso from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $curso = $this->cursoRepository->find($id);

        if (empty($curso)) {
            Flash::error(trans('page.form.action.message.not_found'));

            return redirect(route('curso.index'));
        }

        $this->cursoRepository->delete($id);

        Flash::success('Curso deleted successfully.');

        return redirect(route('curso.index',['nivel'=> Curso::$nivel[$curso->primario]]));
    }



    public function createPromote($id){
        $curso_actual = $this->cursoRepository->find($id);

        $cursos_current = $this->cursoRepository->getByNivel(false, date('Y',time()));

        $cursos_last = $this->cursoRepository->getByNivel(false, date('Y',strtotime('-1 year')));

        $alumnos = [];

        foreach($curso_actual->alumnos as $alumno){
            $aux = $alumno->id;
            $aux2= $alumno->curso_actual;
            if($alumno->curso_ultimo && $curso_actual->id == $alumno->curso_ultimo->id){
                $alumnos[] = $alumno;
            }
        }
        

        $accion = [
            '0' => 'Seleccione Opción',
            'old' => 'Crear Curso del Periodo Anterior',
            'exist' => 'Usar Curso Existente',
            'new' => 'Crear Nuevo Curso'
        ];

        $nivel = Curso::$nivel;
        $turnos = Curso::$turnos;
        $curso = new Curso();

        return view('curso.promote')->with(['curso_actual' => $curso_actual, 'accion' => $accion, 'cursos_current'=> $cursos_current, 'cursos_last'=> $cursos_last, 'alumnos' => $alumnos, 'preset_nivel' => false, 'nivel'=>$nivel, 'turnos' => $turnos, 'curso' => $curso]);

    }

    public function storePromote(Request $request)
    {
        $input = $request->all();



        try {
            switch ($input['accion']) {
                case 'old':
                    $curso = $this->cursoRepository->createFromExist($input['curso_old']);

                    break;

                case 'exist':
                    $curso =$this->cursoRepository->find($input['curso_exist']);
                    
                    break;
                
                case 'new':

                    $data = [
                        'name' => $input['name'],
                        'date_from' => $input['date_from'],
                        'date_to' => $input['date_to'],
                        'primario' => $input['primario'],
                        'turno' => $input['turno'],
                        'active' => $input['active'],
                    ];

                    $curso = $this->cursoRepository->create($data);
                    
                    break;

                
                default:
                    
                    break;
            }

            $this->cursoAlumnoRepository->promote($curso, $input['alumno_select']);

            $curso_old = $this->cursoRepository->find($input['old']);
            $curso_old->active = 0;
            $curso_old->save();

            Flash::success('Se han efectuado los cambios con éxito');

        } catch (\Exception $e) {
            Log::error($e->getMessage());
            Flash::error('Ocurrio un error al guardar.');
            return redirect(route('curso.index'));
        }

        return redirect(route('curso.index',['nivel'=> Curso::$nivel[$curso->primario]]));
    }
}
