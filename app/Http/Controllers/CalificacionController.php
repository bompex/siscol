<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateCalificacionRequest;
use App\Http\Requests\UpdateCalificacionRequest;
use App\Repositories\CalificacionRepository;
use App\Repositories\EvaluacionRepository;
use App\Repositories\AlumnoRepository;
use App\Repositories\CursoMateriaRepository;

use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use App\Models\Calificacion;
use Flash;
use Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;

class CalificacionController extends AppBaseController
{
    /** @var  MateriaRepository */
    private $evaluacionRepository;
    private $calificacionRepository;
    private $alumnoRepository;
    private $cursoMateriaRepository;

    public function __construct(CalificacionRepository $calificacionRepo, EvaluacionRepository $evaluacionRepo, AlumnoRepository $alumnoRepo, CursoMateriaRepository $cursoMateriaRepo)
    {
        $this->evaluacionRepository = $evaluacionRepo;
        $this->calificacionRepository = $calificacionRepo;
        $this->alumnoRepository = $alumnoRepo;
        $this->cursoMateriaRepository = $cursoMateriaRepo;
    }


    


    public function create($evaluacion_id, $alumno_id)
    {


        $alumno = $this->alumnoRepository->find($alumno_id);

        $evaluacion = $this->evaluacionRepository->find($evaluacion_id);

        $calificacion = new Calificacion();
        return view('calificacion.create_modal')
            ->with(['alumno'=> $alumno, 'cursoMateria' => $evaluacion->cursoMateria, 'evaluacion' => $evaluacion, 'calificacion' => $calificacion]);
    }


    public function store($evaluacion_id, $alumno_id, CreateCalificacionRequest $request)
    {
        $input = $request->all();

        $evaluacion = $this->evaluacionRepository->find($evaluacion_id);

        $cu_user = Auth::user();
        $profesor = $cu_user->profesor;

        $input['curso_materia_id'] = $evaluacion->cursoMateria->id;
        $input['alumno_id'] = $alumno_id;
        $input['profesor_id'] =  $profesor->id;
        $input['evaluacion_id'] = $evaluacion->id;
        $input['status'] = 1;


        $calificacion = $this->calificacionRepository->create($input);

        Flash::success(trans('page.form.action.message.success', ['model' => trans('page.model.calificacion')]));

        return redirect(route('evaluacion.show', $evaluacion->id));
    }

    public function create_concepto($cursoMateria_id, $alumno_id)
    {


        $alumno = $this->alumnoRepository->find($alumno_id);

        $cursoMateria = $this->cursoMateriaRepository->find($cursoMateria_id);

        $calificacion = new Calificacion();
        return view('calificacion.create_modal')
            ->with(['alumno'=> $alumno, 'cursoMateria' => $cursoMateria, 'evaluacion' => null, 'calificacion' => $calificacion]);
    }


    public function store_concepto($cursoMateria_id, $alumno_id, CreateCalificacionRequest $request)
    {
        $input = $request->all();

        $cursoMateria = $this->cursoMateriaRepository->find($cursoMateria_id);

        $cu_user = Auth::user();
        $profesor = $cu_user->profesor;

        $input['curso_materia_id'] = $cursoMateria->id;
        $input['alumno_id'] = $alumno_id;
        $input['profesor_id'] =  $profesor->id;
        $input['evaluacion_id'] = null;
        $input['status'] = 1;


        $calificacion = $this->calificacionRepository->create($input);

        Flash::success(trans('page.form.action.message.success', ['model' => trans('page.model.calificacion')]));

        return redirect(route('profesor_curso_materia.index',$cursoMateria->id));
    }


    public function edit($calificacion_id)
    {
        $calificacion = $this->calificacionRepository->find($calificacion_id);

        return view('calificacion.edit_modal')
            ->with(['alumno'=> $calificacion->alumno, 'cursoMateria' => $calificacion->cursoMateria, 'evaluacion' => $calificacion->evaluacion, 'calificacion' => $calificacion]);
    }


    public function update($calificacion_id, UpdateCalificacionRequest $request)
    {
        $input = $request->all();

        $cu_user = Auth::user();
        $profesor = $cu_user->profesor;

        $calificacion = $this->calificacionRepository->update($input,$calificacion_id);

        Flash::success(trans('page.form.action.message.success', ['model' => trans('page.model.calificacion')]));

        if($calificacion->evaluacion){
            return redirect(route('evaluacion.show', $calificacion->evaluacion->id));
        }else{
            return redirect(route('profesor_curso_materia.index',$calificacion->cursoMateria->id));
        }
        
    }

    public function publishAll($evaluacion_id)
    {
        $cu_user = Auth::user();
        $profesor = $cu_user->profesor;

        $evaluacion = $this->evaluacionRepository->find($evaluacion_id);

        foreach($evaluacion->calificaciones as $calificacion){
            $calificacion->publish();
        }

        Flash::success(trans('page.form.action.message.success', ['model' => trans('page.model.calificacion')]));

        return redirect(route('evaluacion.show', $evaluacion->id));
        
    }

}
