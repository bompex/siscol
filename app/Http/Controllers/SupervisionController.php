<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateTopicRequest;
use App\Http\Requests\UpdateTopicRequest;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

use App\Repositories\CursoRepository;
use App\Repositories\AlumnoRepository;
use App\Repositories\ProfesorRepository;
use App\Repositories\MateriaRepository;
use App\Repositories\CursoMateriaRepository;
use App\Repositories\CursoMateriaHorarioRepository;
use App\Repositories\TopicRepository;
use App\Repositories\TopicCursoMateriaRepository;
use App\Repositories\FileRepository;
use App\Repositories\EvaluacionRepository;

use App\Models\Curso;
use App\Models\CursoMateria;
use App\Models\CursoMateriaHorario;
use App\Models\CursoMateriaAlumnoComment;
use App\Models\Topic;
use App\Models\Evaluacion;
use App\Models\TopicFile;

use Illuminate\Support\Facades\Auth;

use Flash;
use Response;

class SupervisionController extends AppBaseController
{
    /** @var  CursoRepository */
    private $cursoRepository;
    private $materiaRepository;
    private $profesorRepository;
    private $cursoMateriaRepository;
    private $cursoMateriaHorarioRepository;
    private $alumnoRepository;
    private $topicRepository;
    private $topicCursoMateriaRepository;
    private $fileRepository;
    private $evaluacionRepository;

    public function __construct(CursoRepository $cursoRepo, MateriaRepository $materiaRepo, ProfesorRepository $profesorRepo, CursoMateriaRepository $cursoMateriaRepo, AlumnoRepository $alumnoRepo, CursoMateriaHorarioRepository $cursoMateriaHorarioRepo, TopicRepository $topicRepo, TopicCursoMateriaRepository $topicCursoMateriaRepo, FileRepository $fileRepo, EvaluacionRepository $evaluacionRepo)
    {
        $this->cursoRepository = $cursoRepo;
        $this->materiaRepository = $materiaRepo;
        $this->profesorRepository = $profesorRepo;
        $this->cursoMateriaRepository = $cursoMateriaRepo;
        $this->cursoMateriaHorarioRepository = $cursoMateriaHorarioRepo;
        $this->alumnoRepository = $alumnoRepo;
        $this->topicRepository = $topicRepo;
        $this->topicCursoMateriaRepository = $topicCursoMateriaRepo;
        $this->fileRepository = $fileRepo;
        $this->evaluacionRepository = $evaluacionRepo;
    }


    public function index(request $request)
    {
        $today = date('Y-m-d H:i:s',time());
        $cu_user = Auth::user();
        $periodo = $request->input('periodo');

        if(!$periodo){
            $periodo = date('Y',time());
        }

        $cursos = $this->cursoRepository->getByNivel(false, $periodo);
        $periodos = $this->cursoRepository->getPeriodos(false);

        $periodos[date('Y',time())] = date('Y',time());      
        
        return view('supervision.index')
            ->with(['cu_user' => $cu_user ,'cursos'=> $cursos, 'periodos' => $periodos, 'periodo' => $periodo]);
    }

    public function curso($curso_id){
        $today = date('Y-m-d H:i:s',time());
        $cu_user = Auth::user();

        $curso  = $this->cursoRepository->find($curso_id);

        $anuncios = $curso->getAnuncios();
        $calendar = $curso->getCalendar(false,true);


        return view('supervision.curso')
            ->with(['cu_user' => $cu_user, 'curso' => $curso, 'anuncios'=>$anuncios, 'calendar' => $calendar]);
    }

    public function showNota($cusro_materia_horario_id){
        $cu_user = Auth::user();

        $cursoMateriaHorario = $this->cursoMateriaHorarioRepository->find($cusro_materia_horario_id);
        $cursoMateria = $cursoMateriaHorario->cursoMateria;

        return view('supervision.curso_materia_horario_nota')
            ->with(['cursoMateriaHorario' => $cursoMateriaHorario ,'cursoMateria'=> $cursoMateria]);
    }

    public function cursoMateria($id)
    {
        $today = date('Y-m-d H:i:s',time());
        $week_past = date('Y-m-d H:i:s',time()-604800);
        $cu_user = Auth::user();

        $cursoMateria = $this->cursoMateriaRepository->find($id);

        $alumnos_qry = $cursoMateria->curso->alumnos()->whereNull('curso_alumno.deleted_at')->orderBy('lastname', 'asc')->orderBy('firstname', 'asc');
        if($cursoMateria->materia->gender != "A"){
            $alumnos_qry = $alumnos_qry->where('alumnos.gender',$cursoMateria->materia->gender);
        }
        if(strtotime($cursoMateria->curso->date_to) >= time()){
            $alumnos_qry = $alumnos_qry->where('curso_alumno.status','1');
        }

        $alumnos = $alumnos_qry->get();
        
        $topics = $cursoMateria->topics()->where('active',1)->orderBy('sort', 'asc')->orderBy('created_at', 'desc')->get();

        $evaluaciones = $cursoMateria->evaluaciones()->where('fecha_fin','>=', $today)->orderBy('created_at', 'desc')->get();

        $evaluaciones_prev = $cursoMateria->evaluaciones()->where('fecha_fin','<', $today)->orderBy('created_at', 'desc')->get();

        $evaluacion_types = Evaluacion::$types;

        $anuncios = $cursoMateria->anuncios()->where('fecha','<', $today)->orderBy('fecha', 'desc')->get();

        return view('supervision.curso_materia')
            ->with(['cu_user' => $cu_user ,'cursoMateria'=> $cursoMateria, 'alumnos' => $alumnos, 'topics'=>$topics, 'evaluaciones' => $evaluaciones, 'evaluaciones_prev' => $evaluaciones_prev, 'evaluacion_types' => $evaluacion_types, 'anuncios' => $anuncios]);
    }

    public function CursoMateriaAlumnoNota($curso_materia_id, $alumno_id){


        $CursoMateriaAlumnoComment = CursoMateriaAlumnoComment::where('curso_materia_id',$curso_materia_id)->where('alumno_id',$alumno_id)->first();

        $cursoMateria = $this->cursoMateriaRepository->find($curso_materia_id);

        $alumno = $this->alumnoRepository->find($alumno_id);
        
        if(!$CursoMateriaAlumnoComment){
            $CursoMateriaAlumnoComment = new CursoMateriaAlumnoComment();
        }

        return view('supervision.curso_materia_alumno_nota')
            ->with(['CursoMateriaAlumnoComment' => $CursoMateriaAlumnoComment ,'cursoMateria'=> $cursoMateria, 'alumno'=> $alumno]);
    }


    //TABLON

    public function indexTablon($id)
    {
        $cu_user = Auth::user();
        $cursoMateria = $this->cursoMateriaRepository->find($id);
        $topics = $cursoMateria->topics()->orderBy('sort', 'asc')->orderBy('created_at', 'desc')->get();   


        return view('supervision.tablon_index')
            ->with(['cu_user'=>$cu_user, 'cursoMateria'=> $cursoMateria, 'topics' => $topics]);
    }

    public function showTopic($id){
        $cu_user = Auth::user();

        $topic = $this->topicRepository->find($id);

        $comments = $topic->comments()->whereNull('comment_parent_id')->orderBy('created_at', 'DESC')->get();

        $cursoMateria = $topic->cursoMateria[0];

        return view('supervision.tablon_show_topic')
            ->with(['cu_user' => $cu_user ,'cursoMateria'=> $cursoMateria, 'topic' => $topic, 'comments' => $comments]);
    }



    //EVALUACION


    public function indexEvaluacion($curso_materia_id)
    {
        $today = date('Y-m-d H:i:s',time());
        $cursoMateria = $this->cursoMateriaRepository->find($curso_materia_id);

        $next_evaluaciones = $cursoMateria->evaluaciones()->where('fecha_inicio','>=', $today)->orderBy('created_at', 'desc')->get();
        $prev_evaluaciones = $cursoMateria->evaluaciones()->where('fecha_fin','<=', $today)->orderBy('created_at', 'desc')->get();

        $evaluacion_types = Evaluacion::$types;

        return view('supervision.evaluacion_index')
            ->with(['cursoMateria'=> $cursoMateria,'evaluacion_types' => $evaluacion_types, 'next_evaluaciones' => $next_evaluaciones, 'prev_evaluaciones' => $prev_evaluaciones]);
    }

    public function showEvaluacion($id)
    {
        $cu_user = Auth::user();
        $evaluacion = $this->evaluacionRepository->find($id);
        $types = Evaluacion::$types;

        $comments = $evaluacion->comments()->orderBy('created_at', 'DESC')->get();

        if($evaluacion->cursoMateria->materia->gender == "A"){
            $alumnos = $evaluacion->cursoMateria->curso->alumnos()->whereNull('curso_alumno.deleted_at')->where('curso_alumno.status','1')->orderBy('lastname', 'asc')->orderBy('firstname', 'asc')->get();
        }else{
            $alumnos = $evaluacion->cursoMateria->curso->alumnos()->whereNull('curso_alumno.deleted_at')->where('curso_alumno.status','1')->where('alumnos.gender',$evaluacion->cursoMateria->materia->gender)->orderBy('lastname', 'asc')->orderBy('firstname', 'asc')->get();

        }
        
        if (empty($evaluacion)) {
            Flash::error(trans('page.form.action.message.not_found'));

            return redirect(route('supervision.index'));
        }

        return view('supervision.evaluacion_show')->with(['cu_user'=>$cu_user, 'evaluacion'=> $evaluacion, 'alumnos' => $alumnos, 'comments' => $comments]);
    }
    public function showEntrega($evaluacion_id, $alumno_id){

        $alumno = $this->alumnoRepository->find($alumno_id);
        $evaluacion = $this->evaluacionRepository->find($evaluacion_id);

        $entregas = $alumno->entrega($evaluacion->id);


        return view('supervision.evaluacion_show_entrega')
            ->with(['alumno' => $alumno, 'evaluacion' => $evaluacion, 'entregas'=> $entregas ]);
    }

}