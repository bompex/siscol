<?php

namespace App\Http\Controllers;

use App\Repositories\AsistenciaRepository;
use App\Repositories\AlumnoRepository;
use App\Repositories\CursoMateriaRepository;
use App\Repositories\CursoRepository;

use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use App\Models\Asistencia;
use App\Models\AsistenciaAlumno;
use App\Models\Curso;
use Flash;
use Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;

class AsistenciaController extends AppBaseController
{
    /** @var  MateriaRepository */
    private $asistenciaRepository;
    private $alumnoRepository;
    private $cursoMateriaRepository;
    private $cursoRepository;

    public function __construct(AsistenciaRepository $asistenciaRepo, AlumnoRepository $alumnoRepo, CursoMateriaRepository $cursoMateriaRepo, CursoRepository $cursoRepo)
    {
        $this->asistenciaRepository = $asistenciaRepo;
        $this->alumnoRepository = $alumnoRepo;
        $this->cursoMateriaRepository = $cursoMateriaRepo;
        $this->cursoRepository = $cursoRepo;
    }

    public function index(Request $request)
    {
        $nivel = $request->input('nivel');

        if($nivel){
            $curso_nivel = array_flip(Curso::$nivel);
            $cursos = $this->cursoRepository->getByNivel($curso_nivel[$nivel]);
        }else{
            $cursos = $this->cursoRepository->all();
        }   

        return view('asistencia.index')
            ->with(['cursos'=> $cursos, 'nivel' => $nivel]);
    }

    public function list($curso_id){



        $curso = $this->cursoRepository->find($curso_id);
        $asistencias = $this->asistenciaRepository->getAll($curso_id);

        if (empty($curso)) {
            Flash::error('No se encontro el Curso');

            return redirect(route('home.admin'));
        }

        return view('asistencia.list')->with(['asistencias'=> $asistencias, 'curso' => $curso]);
    }

    public function create($curso_id){

        $curso = $this->cursoRepository->find($curso_id);

        $alumnos = $curso->alumnos;

        $asistencia = new Asistencia();

        return view('asistencia.create')->with(['asistencia'=> $asistencia, 'curso' => $curso, 'alumnos' => $alumnos]);
    }

    public function store($curso_id, Request $request){

        $input = $request->all();

        $cu_user = Auth::user();

        $curso = $this->cursoRepository->find($curso_id);
        
        $data = [];
        $data['user_id'] = $cu_user->id;
        $data['curso_id'] = $curso->id;
        $data['curso_materia_id'] = null;
        $data['observacion'] = $input['observacion'];
        $data['fecha'] = $input['fecha'];

        

        try {

            $asistencia = $this->asistenciaRepository->create($data);
            if($asistencia){
                foreach($curso->alumnos as $alumno){
                    $asist_alumn = new AsistenciaAlumno();
                    $asist_alumn->asistencia_id =$asistencia->id;
                    $asist_alumn->alumno_id =$alumno->id;
                    $asist_alumn->status =(isset($input['presente'][$alumno->id]) && $input['presente'][$alumno->id] == 1)? 1 : 0;
                    $asist_alumn->save();
                }
            }

            Flash::success('Se guardaron las Asistencia con éxito');

        } catch (\Exception $e) {
            Log::error($e->getMessage());
            Flash::error('Error al guardar Asistencias');
        }
        return redirect(route('asistencia.index',['nivel'=>$curso->nivel]));
    }


}