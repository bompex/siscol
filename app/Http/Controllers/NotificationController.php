<?php

namespace App\Http\Controllers;

use App\Http\Controllers\AppBaseController;
use Flash;
use Response;
use Illuminate\Support\Facades\Auth;
use App\Utils\NotificationHelper;

class NotificationController extends AppBaseController
{
    

    public function __construct()
    {
    }

    public function read_notify(){
        NotificationHelper::read_notifications();
    }

}