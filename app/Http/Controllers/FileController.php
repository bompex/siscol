<?php

namespace App\Http\Controllers;

use App\Models\File;
use Illuminate\Http\Request;
use App\Utils\UploadHandler;
use App\Repositories\FileRepository;
use Flash;
use Response;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;

class FileController extends AppBaseController
{

    private $fileRepository;

    public function __construct(FileRepository $fileRepo)
    {
        $this->fileRepository = $fileRepo;
    }

    /**
     * Display a listing of the Materia.
     *
     * @param Request $request
     *
     * @return Response
     */
    
    public function upload() {

        $cu_user = Auth::user();

        try {

            $upload_handler = new UploadHandler([
                'param_name' => 'file_uploader',
                'upload_dir' => public_path() . '/uploads/files/',
                'upload_url' => url('/uploads/files') . "/",
                'image_file_types' => '/\.(jpe?g|png)$/i',
                'accept_file_types' => '/.+$/i',
                'print_response' => false]);
            if(isset($upload_handler->response['file_uploader']['0'])){

                if(isset($upload_handler->response['file_uploader']['0']->error)){
                    $data['error'] = $upload_handler->response['file_uploader']['0']->error;
                    echo json_encode($data);
                }else{
                    $uploaded = $upload_handler->response['file_uploader']['0'];

                    $data['user_id'] = $cu_user->id ;
                    $data['name'] = $uploaded->name;
                    $data['description'] = $uploaded->size;
                    $expl = explode('.',$uploaded->url);
                    $data['extension'] = end($expl);
                    $data['path'] = $uploaded->url;
                    $data['file_type'] = $uploaded->type;

                    $file = $this->fileRepository->create($data);
               
                    $data['file_id'] = $file->id;

                    echo json_encode($data);
                }

                
            }



        } catch (\Exception $e) {
            Log::error($e->getMessage());
        }

    }
}
