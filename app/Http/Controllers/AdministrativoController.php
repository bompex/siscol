<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateAdministrativoRequest;
use App\Http\Requests\UpdateAdministrativoRequest;
use App\Repositories\AdministrativoRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use App\Models\Administrativo;
use App\Models\Profesor;
use App\Models\User;
use Flash;
use Response;

class AdministrativoController extends AppBaseController
{
    /** @var  AdministrativoRepository */
    private $administrativoRepository;

    public function __construct(AdministrativoRepository $administrativoRepo)
    {
        $this->administrativoRepository = $administrativoRepo;
    }

    /**
     * Display a listing of the Administrativo.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $administrativo = $this->administrativoRepository->all();

        return view('administrativo.index')
            ->with('administrativo', $administrativo);
    }

    /**
     * Show the form for creating a new Administrativo.
     *
     * @return Response
     */
    public function create()
    {
        $administrativo = new Administrativo();
        $administrativo->user = new User();
        $roles = Administrativo::$roles;
        return view('administrativo.create')->with(['administrativo' => $administrativo, 'roles' => $roles]);
    }

    /**
     * Store a newly created Administrativo in storage.
     *
     * @param CreateAdministrativoRequest $request
     *
     * @return Response
     */
    public function store(CreateAdministrativoRequest $request)
    {
        $input = $request->all();
        

        try {

            $administrativo = $this->administrativoRepository->create($input);
            Flash::success(trans('page.form.action.message.success', ['model' => trans('page.model.administrativo')]));

        } catch (\Exception $e) {
            Log::error($e->getMessage());
            Flash::error(trans('page.form.action.message.not_save', ['model' => trans('page.model.administrativo')]));
        }

        return redirect(route('administrativo.index'));
    }

    /**
     * Display the specified Administrativo.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $administrativo = $this->administrativoRepository->find($id);

        if (empty($administrativo)) {
            Flash::error(trans('page.form.action.message.not_found'));

            return redirect(route('administrativo.index'));
        }

        return view('administrativo.show')->with('administrativo', $administrativo);
    }

    /**
     * Show the form for editing the specified Administrativo.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $administrativo = $this->administrativoRepository->find($id);
        $roles = Administrativo::$roles;

        if (empty($administrativo)) {
            Flash::error(trans('page.form.action.message.not_found'));

            return redirect(route('administrativo.index'));
        }

        return view('administrativo.edit')->with(['administrativo'=> $administrativo, 'roles' => $roles]);
    }

    /**
     * Update the specified Administrativo in storage.
     *
     * @param int $id
     * @param UpdateAdministrativoRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateAdministrativoRequest $request)
    {
        $administrativo = $this->administrativoRepository->find($id);

        if (empty($administrativo)) {
            Flash::error('Administrativo not found');

            return redirect(route('administrativo.index'));
        }

        try {

            $administrativo = $this->administrativoRepository->update($request->all(), $id);
            Flash::success(trans('page.form.action.message.update', ['model' => trans('page.model.administrativo')]));

        } catch (\Exception $e) {
            Log::error($e->getMessage());
            Flash::error(trans('page.form.action.message.not_save', ['model' => trans('page.model.administrativo')]));
        }

        return redirect(route('administrativo.index'));

    }

    
    public function store_profesor($id){
        $administrativo = $this->administrativoRepository->find($id);

        $profesor = Profesor::where('user_id',$administrativo->user_id)->withTrashed()->first();

        if(isset($profesor->id)){
            $profesor->deleted_at = null;
            $profesor->save();
        }else{
            $profesor = Profesor::firstOrCreate([
                'user_id' => $administrativo->user_id,
                'firstname' =>$administrativo->firstname,
                'lastname' => $administrativo->lastname,
                'dni' => $administrativo->dni,
                'birthdate' => $administrativo->birthdate,
                'telefono' => $administrativo->telefono
            ]);
        }

        $administrativo->user->roles()->attach(3);
       

        return redirect(route('profesor.edit',$profesor->id));
    }


    public function destroy($id)
    {
        $administrativo = $this->administrativoRepository->find($id);

        if (empty($administrativo)) {
            Flash::error(trans('page.form.action.message.not_found'));

            return redirect(route('administrativo.index'));
        }
        $administrativo->user->roles()->detach(1);
        $administrativo->user->roles()->detach(4);
        $administrativo->user->roles()->detach(5);
        $administrativo->user->roles()->detach(6);
        $administrativo->user->roles()->detach(7);
        $this->administrativoRepository->delete($id);

        Flash::success('Administrativo deleted successfully.');

        return redirect(route('administrativo.index'));
    }
}
