<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateEvaluacionRequest;
use App\Http\Requests\UpdateEvaluacionRequest;
use App\Http\Requests\CreateEvaluacionCommentRequest;

use App\Repositories\EvaluacionRepository;
use App\Repositories\CursoMateriaRepository;
use App\Repositories\EntregaRepository;
use App\Repositories\AlumnoRepository;
use App\Repositories\FileRepository;
use App\Repositories\EvaluacionCommentRepository;

use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use App\Models\Evaluacion;
use App\Models\Entrega;
use App\Models\EvaluacionFile;
use Flash;
use Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use App\Utils\NotificationHelper;

class EvaluacionController extends AppBaseController
{
    /** @var  MateriaRepository */
    private $evaluacionRepository;
    private $cursoMateriaRepository;
    private $entregaRepository;
    private $alumnoRepository;
    private $fileRepository;
    private $evaluacionCommentRepository;

    public function __construct(EvaluacionRepository $evaluacionRepo, CursoMateriaRepository $cursoMateriaRepo, EntregaRepository $entregaRepo, AlumnoRepository $alumnoRepo, FileRepository $fileRepo, EvaluacionCommentRepository $evaluacionCommentRepo)
    {
        $this->evaluacionRepository = $evaluacionRepo;
        $this->cursoMateriaRepository = $cursoMateriaRepo;
        $this->entregaRepository = $entregaRepo;
        $this->alumnoRepository = $alumnoRepo;
        $this->fileRepository = $fileRepo;
        $this->evaluacionCommentRepository = $evaluacionCommentRepo;

    }


    public function index($curso_materia_id)
    {
        $today = date('Y-m-d H:i:s',time());
        $cursoMateria = $this->cursoMateriaRepository->find($curso_materia_id);

        $next_evaluaciones = $cursoMateria->evaluaciones()->where('fecha_inicio','>=', $today)->orderBy('created_at', 'desc')->get();
        $prev_evaluaciones = $cursoMateria->evaluaciones()->where('fecha_fin','<=', $today)->orderBy('created_at', 'desc')->get();

        return view('evaluacion.index')
            ->with(['cursoMateria'=> $cursoMateria, 'next_evaluaciones' => $next_evaluaciones, 'prev_evaluaciones' => $prev_evaluaciones]);
    }


    public function create($type,$curso_materia_id)
    {
        $cursoMateria = $this->cursoMateriaRepository->find($curso_materia_id);
        $evaluacion = new Evaluacion();

        $type_sel = ($type == 'tp')? 2 : 1;
        

        $types = Evaluacion::$types;
        return view('evaluacion.create')
            ->with(['cursoMateria'=> $cursoMateria, 'evaluacion' => $evaluacion, 'types' => $types, 'type' => $type_sel]);
    }


    public function store($curso_materia_id, CreateEvaluacionRequest $request)
    {
        $input = $request->all();

        $cu_user = Auth::user();
        $profesor = $cu_user->profesor;

        $input['fecha_inicio'] = $input['fecha_inicio_date']." ".$input['fecha_inicio_time'].":00";
        $input['fecha_fin'] = $input['fecha_fin_date']." ".$input['fecha_fin_time'].":00";
        $input['fecha_public'] = $input['fecha_public_date']." ".$input['fecha_public_time'].":00";

        $input['profesor_id'] =  $profesor->id;
        $input['curso_materia_id'] = $curso_materia_id;
        $input['status'] = 1;

        

        try {

            if(isset($input['files_del']) && count($input['files_del']) > 0){
                foreach($input['files_del'] as $file_del_id){
                    $this->fileRepository->delete($file_del_id);
                }
            }

            $evaluacion = $this->evaluacionRepository->create($input);
            $cursoMateria = $this->cursoMateriaRepository->find($curso_materia_id);

            if(time() > strtotime($input['fecha_public'])){
               NotificationHelper::nofity_curso($cursoMateria->curso, trans('notify.nueva_evaluacion',['materia'=>$cursoMateria->materia->name]), route('evaluacion_alumno.show', $evaluacion->id)); 
            }
            

            Flash::success(trans('page.form.action.message.success', ['model' => trans('page.model.evaluacion')]));

        } catch (\Exception $e) {
            Log::error($e->getMessage());
            Flash::error(trans('page.form.action.message.not_save', ['model' => trans('page.model.evaluacion')]));
        }

        return redirect(route('profesor_curso_materia.index', $curso_materia_id));
    }


    public function show($id)
    {
        $cu_user = Auth::user();
        $evaluacion = $this->evaluacionRepository->find($id);
        $types = Evaluacion::$types;

        $comments = $evaluacion->comments()->orderBy('created_at', 'DESC')->get();

        if($evaluacion->cursoMateria->materia->gender == "A"){
            $alumnos = $evaluacion->cursoMateria->curso->alumnos()->whereNull('curso_alumno.deleted_at')->where('curso_alumno.status','1')->orderBy('lastname', 'asc')->orderBy('firstname', 'asc')->get();
        }else{
            $alumnos = $evaluacion->cursoMateria->curso->alumnos()->whereNull('curso_alumno.deleted_at')->where('curso_alumno.status','1')->where('alumnos.gender',$evaluacion->cursoMateria->materia->gender)->orderBy('lastname', 'asc')->orderBy('firstname', 'asc')->get();

        }
        
        if (empty($evaluacion)) {
            Flash::error(trans('page.form.action.message.not_found'));

            return redirect(route('home.profesor'));
        }

        return view('evaluacion.show')->with(['cu_user'=>$cu_user, 'evaluacion'=> $evaluacion, 'alumnos' => $alumnos, 'comments' => $comments]);
    }

    /**
     * Show the form for editing the specified Materia.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($curso_materia_id, $id)
    {
        $cursoMateria = $this->cursoMateriaRepository->find($curso_materia_id);
        $evaluacion =$this->evaluacionRepository->find($id);

        $types = Evaluacion::$types;

        return view('evaluacion.edit')
            ->with(['cursoMateria'=> $cursoMateria, 'evaluacion' => $evaluacion, 'types' => $types, 'type' => $evaluacion->type_id]);
    }

    /**
     * Update the specified Materia in storage.
     *
     * @param int $id
     * @param UpdateMateriaRequest $request
     *
     * @return Response
     */
    public function update($curso_materia_id, $id, UpdateEvaluacionRequest $request)
    {
        $input = $request->all();

        $cu_user = Auth::user();
        $profesor = $cu_user->profesor;

        $input['fecha_inicio'] = $input['fecha_inicio_date']." ".$input['fecha_inicio_time'].":00";
        $input['fecha_fin'] = $input['fecha_fin_date']." ".$input['fecha_fin_time'].":00";
        $input['fecha_public'] = $input['fecha_public_date']." ".$input['fecha_public_time'].":00";

        $input['profesor_id'] =  $profesor->id;

        

        try {

            

            $evaluacion = $this->evaluacionRepository->update($input, $id);

            if(isset($input['files_del']) && count($input['files_del']) > 0){
                foreach($input['files_del'] as $file_del_id){
                    $evaluacionFile = EvaluacionFile::where('file_id', $file_del_id)->where('evaluacion_id', $evaluacion->id)->first();
                    if($evaluacionFile){
                        $evaluacionFile->delete();
                    }
                    $this->fileRepository->delete($file_del_id);
                }
            }

            $cursoMateria = $this->cursoMateriaRepository->find($curso_materia_id);

            if(time() > strtotime($input['fecha_public'])){
                NotificationHelper::nofity_curso($cursoMateria->curso, trans('notify.edit_evaluacion',['evaluacion'=>$evaluacion->subject]), route('evaluacion_alumno.show', $evaluacion->id));
            }
            Flash::success(trans('page.form.action.message.success', ['model' => trans('page.model.evaluacion')]));

        } catch (\Exception $e) {
            Log::error($e->getMessage());
            Flash::error(trans('page.form.action.message.not_save', ['model' => trans('page.model.evaluacion')]));
        }

        return redirect(route('evaluacion.show', $evaluacion->id));
    }



    public function show_entrega($evaluacion_id, $alumno_id){

        $alumno = $this->alumnoRepository->find($alumno_id);
        $evaluacion = $this->evaluacionRepository->find($evaluacion_id);

        $entregas = $alumno->entrega($evaluacion->id);


        return view('evaluacion.show_entrega_modal')
            ->with(['alumno' => $alumno, 'evaluacion' => $evaluacion, 'entregas'=> $entregas ]);
    }


    public function index_alumno($curso_materia_id)
    {
        $today = date('Y-m-d H:i:s',time());
        $cu_user = Auth::user();
        $alumno = $cu_user->alumno;
        $cursoMateria = $this->cursoMateriaRepository->find($curso_materia_id);

        $next_evaluaciones = $cursoMateria->evaluaciones()->where('fecha_public','<=', $today)->where('fecha_fin','>=', $today)->orderBy('created_at', 'desc')->get();
        $prev_evaluaciones = $cursoMateria->evaluaciones()->where('fecha_fin','<=', $today)->orderBy('created_at', 'desc')->get();

        return view('evaluacion.index_alumno')
            ->with(['cursoMateria'=> $cursoMateria, 'next_evaluaciones' => $next_evaluaciones, 'prev_evaluaciones' => $prev_evaluaciones]);
    }

    public function show_alumno($id)
    {
        $cu_user = Auth::user();
        $alumno = $cu_user->alumno;

        $evaluacion = $this->evaluacionRepository->find($id);
        $types = Evaluacion::$types;

        $comments = $evaluacion->comments()->orderBy('created_at', 'DESC')->get();
        
        if (empty($evaluacion)) {
            Flash::error(trans('page.form.action.message.not_found'));

            return redirect(route('home.alumno'));
        }

        return view('evaluacion.show_alumno')->with(['cu_user'=>$cu_user, 'evaluacion'=> $evaluacion, 'alumno' => $alumno, 'comments' => $comments]);
    }


    public function create_entrega_alumno($evaluacion_id)
    {
        $cu_user = Auth::user();
        $alumno = $cu_user->alumno;

        $evaluacion = $this->evaluacionRepository->find($evaluacion_id);

        $entrega = new Entrega();

        return view('evaluacion.entrega_modal')
            ->with(['alumno'=> $alumno, 'evaluacion' => $evaluacion, 'entrega' => $entrega]);
    }


    public function store_entrega_alumno($evaluacion_id, Request $request)
    {
        $cu_user = Auth::user();
        $alumno = $cu_user->alumno;

        $input = $request->all();

        $evaluacion = $this->evaluacionRepository->find($evaluacion_id);

        $cu_user = Auth::user();
        $profesor = $cu_user->profesor;

        $input['alumno_id'] = $alumno->id;
        $input['evaluacion_id'] = $evaluacion->id;
        $input['status'] = 1;

        if(isset($input['files_del']) && count($input['files_del']) > 0){
            foreach($input['files_del'] as $file_del_id){
                $this->fileRepository->delete($file_del_id);
            }
        }

        $entrega = $this->entregaRepository->create($input);

        NotificationHelper::nofity_profesor($evaluacion->cursoMateria->profesor, trans('notify.nueva_entrega',['evaluacion'=>$evaluacion->subject, 'usuario'=> $alumno->nombre_curso]), route('evaluacion.show', $evaluacion->id));

        Flash::success(trans('page.form.action.message.success', ['model' => trans('page.model.calificacion')]));

        return redirect(route('evaluacion_alumno.show', $evaluacion->id));
    }

    public function corregir_entrega($entrega_id){
        $entrega = $this->entregaRepository->find($entrega_id);

        $file_alumno = false;
        $file_profesor = false;

        if($entrega->files){
            foreach($entrega->files as $file){
                if($file->pivot->devolucion == 0){
                    $file_alumno[] = $file;
                }else{
                    $file_profesor[] = $file;
                }
            }
        }

        return view('evaluacion.corregir_entrega_modal')
            ->with(['alumno' => $entrega->alumno, 'evaluacion' => $entrega->evaluacion, 'entrega'=> $entrega, 'file_alumno' => $file_alumno, 'file_profesor' => $file_profesor ]);
    }

    public function update_corregir_entrega($entrega_id, Request $request){

        $input = $request->all();

        $entrega = $this->entregaRepository->update($input, $entrega_id);


        try {

            if(isset($input['files_del']) && count($input['files_del']) > 0){
                foreach($input['files_del'] as $file_del_id){
                    $entrega->files()->detach($file_del_id);
                    $this->fileRepository->delete($file_del_id);
                }
            }

            if(isset($input['files_id']) && count($input['files_id']) > 0){
                foreach($input['files_id'] as $file_id){
                    $entrega->files()->syncWithoutDetaching([$file_id =>['devolucion'=>1]]);
                }
            }

            NotificationHelper::nofity_alumno($entrega->alumno, trans('notify.nueva_correccion',['evaluacion'=>$entrega->evaluacion->subject]), route('evaluacion_alumno.show', $entrega->evaluacion->id));

            Flash::success(trans('page.form.action.message.success', ['model' => 'Corrección']));

        } catch (\Exception $e) {
            Log::error($e->getMessage());
            Flash::error(trans('page.form.action.message.not_save', ['model' => 'Corrección']));
        }

        return redirect(route('evaluacion.show', [$entrega->evaluacion->id]));

    }

    public function store_comment($evaluacion_id, CreateEvaluacionCommentRequest $request){
        $cu_user = Auth::user();

        $input = $request->all();

        $evaluacion = $this->evaluacionRepository->find($evaluacion_id);

        $input['user_id'] = $cu_user->id;
        $input['evaluacion_id'] = $evaluacion_id;
        $input['active'] = 1;


        $this->evaluacionCommentRepository->create($input);
        
        if($cu_user->isProfesor()){
            NotificationHelper::nofity_curso($evaluacion->cursoMateria->curso, trans('notify.nuevo_comentario_evaluacion',['usuario'=> $cu_user->profesor->nombre, 'evaluacion'=>$evaluacion->subject]), route('evaluacion_alumno.show', $evaluacion->id));
        }else{
            NotificationHelper::nofity_profesor($evaluacion->cursoMateria->profesor, trans('notify.nuevo_comentario_evaluacion',['usuario'=> $cu_user->alumno->nombre_curso, 'evaluacion'=>$evaluacion->subject]), route('evaluacion.show', $evaluacion->id));
        }
        
        if($cu_user->isProfesor()){
            return redirect(route('evaluacion.show', $evaluacion_id));
        }else{
            return redirect(route('evaluacion_alumno.show', $evaluacion_id));
        }
        
        
    }

    public function destroy_comment($evaluacion_id, $id)
    {
        $evaluacionComment = $this->evaluacionCommentRepository->find($id);

        if (empty($evaluacionComment)) {
            Flash::error(trans('page.form.action.message.not_found'));

            return redirect(route('evaluacion.show', $evaluacion_id));
        }

        $evaluacionComment->disable();

        Flash::success('El Comentario se ha ocultado.');

        return redirect(route('evaluacion.show', $evaluacion_id));
    }

    public function destroy_evaluacion($curso_materia_id, $evaluacion_id)
    {
        $evaluacion = $this->evaluacionRepository->find($evaluacion_id);

        if (empty($evaluacion)) {
            Flash::error(trans('page.form.action.message.not_found'));

            return redirect(route('evaluacion.index',$curso_materia_id));
        }

        $evaluacion->disable();

        Flash::success('La Actividad se ha eliminado.');

        return redirect(route('evaluacion.index', $curso_materia_id));
    }
}
