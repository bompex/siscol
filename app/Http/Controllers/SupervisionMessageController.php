<?php

namespace App\Http\Controllers;

use App\Models\Message;
use Illuminate\Http\Request;
use App\Repositories\UserRepository;
use App\Repositories\MessageRepository;
use App\Repositories\FileRepository;
use Flash;
use Response;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;

use App\Utils\NotificationHelper;

class SupervisionMessageController extends AppBaseController
{

    private $userRepository;
    private $messageRepository;
    private $fileRepository;

    public function __construct(UserRepository $userRepo, MessageRepository $messageRepo, FileRepository $fileRepo)
    {
        $this->userRepository = $userRepo;
        $this->messageRepository = $messageRepo;
        $this->fileRepository = $fileRepo;
    }

    public function index(){
        $all_users = $this->userRepository->all();

        $users=[];
        $users[0] = "Seleccionar Usuario";
        foreach($all_users as $user){
            if($user->administrativo || $user->profesor || $user->alumno){
                $name = $user->lastname.", ".$user->firstname;
                if($user->isAlumno()){
                    $name .= "(Alumno)";
                }else{
                    if($user->isProfesor()){
                        $name .= "(Profesor)";
                    }
                }
                $users[$user->id] = $name;
            }
        }

        return view('supervision_message.index')
            ->with(['users' => $users]);

    }

    public function show($user1_id,$user2_id){

        $messages = $this->messageRepository->getMessagesByConversation($user1_id, $user2_id);

        $user1 = $this->userRepository->find($user1_id);
        $user2 = $this->userRepository->find($user2_id);

        return view('supervision_message.show_modal')
            ->with(['messages' => $messages, 'user1' => $user1, 'user2' => $user2]);
        
    }

}
