<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\API\ApiBaseController;
use Illuminate\Http\Request;
use App\Models\User;
use Exception;

class AuthController extends ApiBaseController
{

    public function __construct() {
        $this->middleware('jwt.auth', ['except' => ['authenticate']]);
    }

    public function authenticate(Request $request) {
        $credentials = $request->only('email', 'password');
        try {
            $token = User::authenticate($credentials);
            return $this->sendResponse(['token' => 'Bearer '.$token],'User authenticated.');
        }
        catch(Exception $e) {
            return $this->sendError($e->getMessage(), 400);
        }
    }

    public function show() {
        try {
            $user = User::getUserFromToken();
            return $this->sendResponse($user->toArray());
        }
        catch (Exception $ex) {
            $this->sendError($ex->getMessage(), $code = 404);
        }
    }

    public function getToken() {
        try {
            $token = User::getToken();
            return $this->sendResponse(['token' => 'Bearer '.$token],'Token updated.');
        }
        catch (Exception $e) {
            return $this->sendError($e->getMessage(), 404);
        }
    }

}
