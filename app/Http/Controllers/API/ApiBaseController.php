<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use Response;
use Tymon\JWTAuth\Facades\JWTAuth;
use App\Http\Controllers\Controller;

if (version_compare(PHP_VERSION, '7.2.0', '>=')) {
    // Ignores notices and reports all other kinds... and warnings
    error_reporting(E_ALL ^ E_NOTICE ^ E_WARNING);
    // error_reporting(E_ALL ^ E_WARNING); // Maybe this is enough
}

class ApiBaseController extends Controller
{

    public function sendResponse($result, $message = "")
    {
        return Response::json(['result'=>$result, 'message'=>$message]);
    }

    public function sendError($error, $code = 404)
    {
        return Response::json(['message'=>$error],$code);
    }

    protected function objectNotFound($modelName, $id)
    {
        return Response::json(['message'=>$modelName . " with id " . $id . " not found"]);
    }

    protected function getUser()
    {
        return JWTAuth::parseToken()->authenticate();
    }

}
