<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\WithDrawings;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\BeforeSheet;
use Maatwebsite\Excel\Events\BeforeExport;
use Maatwebsite\Excel\Events\AfterSheet;
use PhpOffice\PhpSpreadsheet\Style\Alignment;
use Illuminate\Support\Facades\Auth;

class AbstractBaseExport implements ShouldAutoSize, WithEvents {
    
    protected $footer = null;


    protected $columns = [
        1 => 'A',
        2 => 'B',
        3 => 'C',
        4 => 'D',
        5 => 'E',
        6 => 'F',
        7 => 'G',
        8 => 'H',
        9 => 'I',
        10 => 'J',
        11 => 'K',
        12 => 'L',
        13 => 'M',
        14 => 'N',
        15 => 'O',
        16 => 'P',
        17 => 'R'
        
    ];

    public function __construct($footer = null) {
        if($footer) $this->footer = $footer;
    }

    public function registerEvents(): array {
        return [
            BeforeSheet::class => function(BeforeSheet $event) {
                $sheet = $event->sheet->getDelegate();

                //Insert Logo
                $drawing = new \PhpOffice\PhpSpreadsheet\Worksheet\Drawing();
                $drawing->setName('Logo');
                $drawing->setDescription('Logo');
                $drawing->setPath(public_path('img/logo@2x.png'));
                $drawing->setCoordinates('A1');
                $sheet->fromArray($this->getHeaderReport(), null, 'A6', false, false);
                $sheet->setShowGridlines(False);
                $drawing->setWorksheet($sheet);
            },
            AfterSheet::class => function(AfterSheet $event) {
                
                $rows = $event->sheet->getDelegate()->toArray();
                
                $cols = 0;
                if(count($rows) > 0){
                    $cols = count($rows[0]);
                    $rows = count($rows);
                }
                
                $sheet = $event->sheet->getDelegate();
                $cellRange = 'A8:' . $this->columns[$cols] . '8';
                $sheet->getStyle($cellRange)->getFont()->setSize(10);
                
                /* Header Styles */
                $header_style = [
                    'font' => [
                        'bold' => false,
                        'color' => ['argb' => 'FFFFFFFF'],
                    ],
                    'fill' => [
                        'fillType' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID,
                        'color' => [
                            'argb' => 'FF4A86E8',
                        ]
                    ],
                    'borders' => [
                        'allBorders' => [
                            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_NONE,
                        ],
                    ],
                    'alignment' => [
                        'horizontal' => Alignment::HORIZONTAL_CENTER,
                        'vertical' => Alignment::VERTICAL_CENTER,
                        'wrapText' => true,
                    ]
                ];

                $sheet->getStyle($cellRange)->applyFromArray($header_style);
                $sheet->setAutoFilter($cellRange);
                
                /* Rows Styles */
                $rowsRange = 'A9:' . $this->columns[$cols] . $rows;
                $rows_style = [
                    'borders' => [
                        'allBorders' => [
                            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                        ],
                    ]
                ];
                $sheet->getStyle($rowsRange)->applyFromArray($rows_style);
                
                if($this->footer){
                    $sheet->fromArray($this->footer, null, 'A' . ($rows + 1), false, false);
                    $range = 'A' . ($rows + 1) . ':' . $this->columns[count($this->footer)] . ($rows + 1);
                    $sheet->getStyle($range)->getFont()->setSize(14);
                }
            },
            BeforeExport::class => function(BeforeExport $event) {
                $event->getWriter()->getDelegate()
                        ->getProperties()
                        ->setCreator("Colegio")
                        ->setLastModifiedBy(\Auth::user()->name . ' ' . \Auth::user()->last_name);
            }
        ];
    }

    private function getHeaderReport() {
        $user = \Auth::user();

        return [
            ["Generado Por:", $user->administrativo->lastname . ', ' . $user->administrativo->firstname,
            "Generado el: ", date('d/m/Y')],
            [" ", " "]
        ];
    }

}
