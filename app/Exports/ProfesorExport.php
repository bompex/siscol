<?php

namespace App\Exports;

use App\Exports\AbstractBaseExport;
use App\Repositories\ProfesorRepository;
use App\Repositories\CursoRepository;
use App\Models\Profesor;
use App\Models\Curso;
use Maatwebsite\Excel\Concerns\FromCollection;
use Illuminate\Contracts\Support\Responsable;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\Exportable;

class ProfesorExport extends AbstractBaseExport implements FromCollection, Responsable, WithMapping, WithHeadings{

    use Exportable;

    protected $profesorRepository;

    public function __construct(ProfesorRepository $profesorRepo) {
        $this->profesorRepository = $profesorRepo;

    }

    public function collection() {
        return $this->profesorRepository->all();
    }

    public function headings(): array {
        return [
            trans('page.label.user.name'),
            trans('page.label.user.lastname'),
            trans('page.label.user.dni'),
            trans('page.label.user.phone'),
            trans('page.label.user.email'),
            trans('page.label.curso.name'),
            trans('page.label.suplente'),
            
        ];
    }

    public function map($row): array {

        $curso = [];
        $suplente = [];

        foreach($row->cursoMaterias as $cm){
            $curso[] =  $cm->materia->name.'('.$cm->curso->name.')';
        }

        foreach($row->cursoMateriasSuplente as $cm){
            $suplente[] =  $cm->materia->name.'('.$cm->curso->name.')';
        }


            

        return [
            $row->firstname,
            $row->lastname,
            $row->dni,
            $row->telefono,
            $row->user->email,
            implode(', ',$curso),
            implode(', ',$suplente),
        ];
    }

}
