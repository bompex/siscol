<?php

namespace App\Exports;

use App\Exports\AbstractBaseExport;
use App\Repositories\AlumnoRepository;
use App\Repositories\CursoRepository;
use App\Models\Alumno;
use App\Models\Curso;
use Maatwebsite\Excel\Concerns\FromCollection;
use Illuminate\Contracts\Support\Responsable;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\Exportable;

class AlumnoExport extends AbstractBaseExport implements FromCollection, Responsable, WithMapping, WithHeadings{

    use Exportable;

    protected $alumnoRepository;
    protected $curso_id;
    protected $orderBy;
    protected $sortType;

    public function __construct(AlumnoRepository $alumnoRepo, $curso_id,$orderBy = '', $sortType = '') {
        $this->alumnoRepository = $alumnoRepo;
        $this->curso_id = $curso_id;
        $this->orderBy = $orderBy;
        $this->sortType = $sortType;

    }

    public function collection() {
        return $this->alumnoRepository->getAll($this->curso_id, $this->orderBy, $this->sortType);
    }

    public function headings(): array {
        return [
            trans('page.label.user.name'),
            trans('page.label.user.lastname'),
            trans('page.label.user.dni'),
            trans('page.label.curso.name'),
            trans('page.label.curso.turno'),
            trans('page.label.user.phone'),
            trans('page.label.user.email'),
        ];
    }

    public function map($row): array {

        $curso_name = (isset($row->curso_actual->name))? $row->curso_actual->name : '--';
        $curso_turno = (isset($row->curso_actual->turno))? Curso::$turnos[$row->curso_actual->turno] : '--';

        return [
            $row->firstname,
            $row->lastname,
            $row->dni,
            $curso_name,
            $curso_turno,
            $row->telefono,
            $row->user->email
        ];
    }

}
