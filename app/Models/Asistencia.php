<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model as Model;



class Asistencia extends AbstractBaseModel
{

    public $table = 'asistencias';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    public static $status = [ '0' => 'Ausente', '1' => 'Presente', '2' => 'Media Falta'];

    public $fillable = [
        'user_id',
        'curso_id',
        'curso_materia_id',
        'observacion',
        'fecha',
    ];

	public function asistenciaAlumno()
    {
        return $this->hasMany(\App\Models\AsistenciaAlumno::class, 'asistencia_id');
    }
    
    public function user()
    {
        return $this->belongsTo(\App\Models\User::class, 'user_id');
    }

    public function curso()
    {
        return $this->belongsTo(\App\Models\Curso::class, 'curso_id');
    }
    public function cursoMateria()
    {
        return $this->belongsTo(\App\Models\CursoMateria::class, 'curso_materia_id');
    }

    
}