<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model as Model;

/**
 * Class CursoMateria
 * @package App\Models
 * @version April 25, 2020, 9:14 pm UTC
 *
 * @property \App\Models\Curso curso
 * @property \App\Models\Materia materia
 * @property \App\Models\Profesore profesor
 * @property \Illuminate\Database\Eloquent\Collection cursoMateriaHorarios
 * @property integer curso_id
 * @property integer profesor_id
 * @property integer materia_id
 * @property integer created_by
 * @property integer updated_by
 */
class CursoMateria extends AbstractBaseModel
{

    public $table = 'curso_materia';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';




    public $fillable = [
        'curso_id',
        'profesor_id',
        'profesor_suplente_id',
        'materia_id',
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'curso_id' => 'integer',
        'profesor_id' => 'integer',
        'profesor_suplente_id' => 'integer',
        'materia_id' => 'integer',
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'curso_id' => 'required',
        'profesor_id' => 'required',
        'materia_id' => 'required'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function curso()
    {
        return $this->belongsTo(\App\Models\Curso::class, 'curso_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function materia()
    {
        return $this->belongsTo(\App\Models\Materia::class, 'materia_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function profesor()
    {
        return $this->belongsTo(\App\Models\Profesor::class, 'profesor_id')->withTrashed();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function profesorSuplente()
    {
        return $this->belongsTo(\App\Models\Profesor::class, 'profesor_suplente_id')->withTrashed();
    }

    public function topics()
    {
        return $this->belongsToMany(\App\Models\Topic::class, 'topic_curso_materia');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function cursoMateriaHorarios()
    {
       return $this->hasMany(CursoMateriaHorario::class, 'curso_materia_id');
       
    }

    public function evaluaciones()
    {
       return $this->hasMany(Evaluacion::class, 'curso_materia_id');
       
    }

    public function anuncios()
    {
       return $this->hasMany(Anuncio::class, 'curso_materia_id');
       
    }

    public function getAnuncios()
    {
       return $this->anuncios()->where('fecha','<=',date('Y-m-d H:i:s',time()))->orderBy('fecha', 'desc')->get();
       
    }

    public function getSchedule(){
        $schedule = [];

        foreach($this->cursoMateriaHorarios as $cursoMateriaHorario){

            $has_note = ($cursoMateriaHorario->note && $cursoMateriaHorario->note != '')? 1 : 0;

            $schedule[$cursoMateriaHorario->dia][] = [date('H:i',strtotime($cursoMateriaHorario->hora_inicio)), date('H:i',strtotime($cursoMateriaHorario->hora_fin)), $cursoMateriaHorario->id, $has_note];

        }

        return $schedule;
    }

    public function getFullSchedule(){
        $schedule =[];

        $schedule[$this->id]['name'] = $this->materia->name;
        $schedule[$this->id]['color'] = $this->materia->color;
        $schedule[$this->id]['schedule'] = $this->getSchedule();

        return $schedule;
    }


    public function getCalendar($is_admin){
        $out = [];
        $evaluacion_types = Evaluacion::$types;
        $anuncios = $this->getAnuncios();
        foreach($anuncios as $anuncio){
            if($anuncio->fecha_inicio){
                $out[] = array(
                    'id' => 'a'.$anuncio->id,
                    'title' => $this->materia->name.": ".$anuncio->subject,
                    'url' => route('anuncio.show', [$anuncio->id]),
                    'class' => 'event-special open-in-modal',
                    'start' => strtotime($anuncio->fecha_inicio) . '000',
                    'end' => strtotime($anuncio->fecha_fin) .'000'
                );
            }
            
        }
        $evaluaciones = $this->evaluaciones()->where('fecha_public','<=', date('Y-m-d H:i:s',time()))->get();
        foreach($evaluaciones as $evaluacion){
            if($evaluacion->fecha_inicio){
                $class = ($evaluacion->type_id == 1)? 'event-info' : 'event-warning';
                $out[] = array(
                    'id' => 'e'.$evaluacion->id,
                    'title' => $this->materia->name." - ".$evaluacion_types[$evaluacion->type_id].": ".$evaluacion->subject,
                    'url' => ($is_admin)? route('supervision.show_evaluacion', $evaluacion->id) : route('evaluacion_alumno.show', $evaluacion->id),
                    'start' => strtotime($evaluacion->fecha_inicio) . '000',
                    'end' => strtotime($evaluacion->fecha_fin) .'000',
                    'class' => $class
                );
            }
            
        }

        return $out;
    }
}
