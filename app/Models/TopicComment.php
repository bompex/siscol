<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model as Model;

/**
 * Class Materia
 * @package App\Models
 * @version April 16, 2020, 8:46 pm UTC
 *
 * @property \Illuminate\Database\Eloquent\Collection cursoMateria
 * @property string name
 */
class TopicComment extends AbstractBaseModel
{

    public $table = 'topic_comment';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    public $fillable = [
        'user_id',
        'topic_id',
        'comment_parent_id',
        'body',
        'active',
        'private',
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'user_id' => 'integer',
        'topic_id' => 'integer',
        'body' => 'string',
        'active' => 'boolean',
        'private' => 'boolean',
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'body' => 'required'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function user()
    {
        return $this->belongsTo(\App\Models\User::class, 'user_id');
    }


    public function Topic()
    {
        return $this->belongsTo(\App\Models\Topic::class, 'topic_id');
    }

    public function Parent()
    {
        return $this->belongsTo(\App\Models\TopicComment::class, 'comment_parent_id');
    }

    public function childrens(){
        return $this->hasMany(\App\Models\TopicComment::class, 'comment_parent_id');
    }

    public function disable()
    {
        $this->deleted_at = date('Y-m-d H:i:s',time());
        $this->save();
    }

   


    
}