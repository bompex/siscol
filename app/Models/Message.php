<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model as Model;

use Illuminate\Support\Facades\Auth;



class Message extends AbstractBaseModel
{

    public $table = 'messages';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';




    public $fillable = [
        'parent_message_id',
        'sender_user_id',
        'receiver_user_id',
        'body',
        'viewed',
    ];

	public function childrens_message()
    {
        return $this->hasMany(\App\Models\Message::class, 'parent_message_id');
    }
    
    
    public function parent_message()
    {
        return $this->belongsTo(\App\Models\Message::class, 'parent_message_id');
    }
    

    public function sender_user()
    {
        return $this->belongsTo(\App\Models\User::class, 'sender_user_id');
    }

    public function receiver_user()
    {
        return $this->belongsTo(\App\Models\User::class, 'receiver_user_id');
    }

    public function files()
    {
        return $this->belongsToMany(\App\Models\File::class, 'message_files');
    }


    public function is_own($user_id)
    {
        if($user_id == $this->sender_user_id){
            return true;
        }else{
            return false;
        }

    }

    public function mark_readed(){
        if($this->viewed == 0){
            if($this->receiver_user_id == Auth::user()->id){
                $this->viewed = 1;
                $this->save();
            }
        }
    }



    public static function getReplyMessage($reply, $reply_id, $receptor){
        $message = "<p></p><div class='quote-message' style='font-size: 11px;background-color: #e0e0e0;padding: 10px;'>";
        switch ($reply) {
            case 'topic':
                    $topic = Topic::find($reply_id);
                    $message .= "<i>En referencia al tablon:</i> <a href='".route('curso_materia_foro.show_topic', $topic->id)."' target='blank'><i>".$topic->subject."</i></a><br />";
                break;

            case 'topic_comment':
                    $topic_comment = TopicComment::find($reply_id);
                    $message .= "<i>En referencia al comentario:</i> ".$topic_comment->body."<i>En el tablon: </i> <a href='".route('curso_materia_foro.show_topic', $topic_comment->topic->id)."' target='blank'><i>".$topic_comment->topic->subject."</i></a>";
                break;

            case 'evaluacion':
                    $evaluacion = Evaluacion::find($reply_id);

                    if($receptor->isProfesor()){
                        $route = route('evaluacion.show', $evaluacion->id);
                    }elseif($receptor->isAlumno()){
                        $route = route('evaluacion_alumno.show', $evaluacion->id);
                    }else{
                        $route = '#'; 
                    }
                    $message .= "<i>En referencia a la actividad:</i> <a href='".$route."' target='blank'><i>".$evaluacion->subject."</i></a><br />";
                break;

            case 'evaluacion_comment':
                    $evaluacionComment = EvaluacionComment::find($reply_id);

                    if($receptor->isProfesor()){
                        $route = route('evaluacion.show', $evaluacionComment->evaluacion->id);
                    }elseif($receptor->isAlumno()){
                        $route = route('evaluacion_alumno.show', $evaluacionComment->evaluacion->id);
                    }else{
                        $route = '#'; 
                    }
                    $message .= "<i>En referencia al comentario:</i> ".$evaluacionComment->body."<i>En la Actividad: </i> <a href='".$route."' target='blank'><i>".$evaluacionComment->evaluacion->subject."</i></a>";
                break;
            
            default:
                # code...
                break;
        }

        $message .= '</div><p><br/></p>';

        return $message;
    }

    
}