<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model as Model;

/**
 * Class Profesor
 * @package App\Models
 * @version April 16, 2020, 8:45 pm UTC
 *
 * @property \App\Models\User user
 * @property \Illuminate\Database\Eloquent\Collection cursoMateria
 * @property integer user_id
 * @property string firstname
 * @property string lastname
 * @property string dni
 * @property string|\Carbon\Carbon birthdate
 * @property string telefono
 * @property string email
 * @property integer created_by
 * @property integer updated_by
 */
class Profesor extends AbstractBaseModel
{

    public $table = 'profesores';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';




    public $fillable = [
        'user_id',
        'firstname',
        'lastname',
        'dni',
        'birthdate',
        'telefono',
        'primario',
        'secundario',
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'firstname' => 'string',
        'lastname' => 'string',
        'dni' => 'string',
        'birthdate' => 'datetime',
        'telefono' => 'string',
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'firstname' => 'required',
        'lastname' => 'required',
        'dni' => 'required',
    ];

    protected $appends = ["nombre"];
    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function user()
    {
        return $this->belongsTo(\App\Models\User::class, 'user_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function cursoMaterias()
    {
        return $this->hasMany(\App\Models\CursoMateria::class, 'profesor_id')
                    ->select('curso_materia.*')
                    ->join('cursos', 'cursos.id', '=', 'curso_materia.curso_id')
                    ->where('cursos.active', 1)
                    ///////////
                    ->where('cursos.date_from', '<=', date('Y-m-d',time()))
                    ->where('cursos.date_to', '>=', date('Y-m-d',time()))
                    ///////////
                    ->whereNull('cursos.deleted_at');
    }

    public function cursoMateriasSuplente()
    {
        return $this->hasMany(\App\Models\CursoMateria::class, 'profesor_suplente_id')
                    ->select('curso_materia.*')
                    ->join('cursos', 'cursos.id', '=', 'curso_materia.curso_id')
                    ->where('cursos.active', 1)
                    ///////////
                    ->where('cursos.date_from', '<=', date('Y-m-d',time()))
                    ->where('cursos.date_to', '>=', date('Y-m-d',time()))
                    ///////////
                    ->whereNull('cursos.deleted_at');
    }

    public function cursoMateriasHistory($periodo)
    {
        $result = CursoMateria::where('profesor_id', $this->id)
                    ->select('curso_materia.*')
                    ->join('cursos', 'cursos.id', '=', 'curso_materia.curso_id')
                    ->whereBetween('date_from', [$periodo.'-01-01', $periodo.'-12-31'])
                    ->whereNull('cursos.deleted_at')->get();
        return $result;
    }

    

    public function getFullSchedule($turno = 'mañana'){
        $schedule = [];

        foreach($this->cursoMaterias as $cursoMateria){
            if($cursoMateria->curso->turno == $turno ){
                $schedule[$cursoMateria->id]['name'] = $cursoMateria->curso->name."<br />".$cursoMateria->materia->name;
                $schedule[$cursoMateria->id]['color'] = $cursoMateria->materia->color;
                $schedule[$cursoMateria->id]['schedule'] = $cursoMateria->getSchedule();
            }      
        }
        foreach($this->cursoMateriasSuplente as $cursoMateria){
            if($cursoMateria->curso->turno == $turno ){
                $schedule[$cursoMateria->id]['name'] = $cursoMateria->curso->name."<br />".$cursoMateria->materia->name;
                $schedule[$cursoMateria->id]['color'] = $cursoMateria->materia->color;
                $schedule[$cursoMateria->id]['schedule'] = $cursoMateria->getSchedule();
            }      
        }
        if(empty($schedule)){
            return false;
        }
        return $schedule;
    }

    public function getNombreAttribute(){

        return $this->lastname.", ".$this->firstname;

    }
}
