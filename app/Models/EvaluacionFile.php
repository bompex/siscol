<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model as Model;

/**
 * Class Materia
 * @package App\Models
 * @version April 16, 2020, 8:46 pm UTC
 *
 * @property \Illuminate\Database\Eloquent\Collection cursoMateria
 * @property string name
 */
class EvaluacionFile extends AbstractBaseModel
{

    public $table = 'evaluacion_files';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    public $fillable = [
        'evaluacion_id',
        'file_id',
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'evaluacion_id',
        'file_id',
    ];

    
    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function evaluacion()
    {
        return $this->belongsTo(\App\Models\Evaluacion::class, 'evaluacion_id');
    }

    public function file()
    {
        return $this->belongsTo(\App\Models\File::class, 'File_id');
    }

        
}