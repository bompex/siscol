<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Contracts\Auditable;

abstract class AbstractBaseModel extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;
    use SoftDeletes;

    protected $dates = ['deleted_at'];

     public static function boot() {
        parent::boot();
        
        static::creating(function($model) {
            if(Auth::user()){
                $user = Auth::user();
                $model->created_by = $user->id;
                $model->updated_by = $user->id;
            }
        });
        
        static::updating(function($model) {
            $user = Auth::user();
            $model->updated_by = $user->id;
        });
    }
}