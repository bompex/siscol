<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model as Model;



class Notification extends Model
{

    public $table = 'notifications';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';




    public $fillable = [
        'user_id',
        'message',
        'url',
        'viewed',
        'class',
    ];

    
    

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function user()
    {
        return $this->belongsTo(\App\Models\User::class, 'user_id');
    }

    
}
