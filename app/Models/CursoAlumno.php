<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model as Model;

/**
 * Class CursoAlumno
 * @package App\Models
 * @version April 26, 2020, 8:49 pm UTC
 *
 * @property \App\Models\Alumno alumno
 * @property \App\Models\Curso curso
 * @property integer curso_id
 * @property integer alumno_id
 * @property string|\Carbon\Carbon registration_date
 * @property integer status
 * @property integer created_by
 * @property integer updated_by
 */
class CursoAlumno extends AbstractBaseModel
{

    public $table = 'curso_alumno';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';




    public $fillable = [
        'curso_id',
        'alumno_id',
        'registration_date',
        'status',
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'curso_id' => 'integer',
        'alumno_id' => 'integer',
        'registration_date' => 'datetime',
        'status' => 'integer',
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'curso_id' => 'required',
        'alumno_id' => 'required',
        'registration_date' => 'required',
        'status' => 'required'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function alumno()
    {
        return $this->belongsTo(\App\Models\Alumno::class, 'alumno_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function curso()
    {
        return $this->belongsTo(\App\Models\Curso::class, 'curso_id');
    }
}
