<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model as Model;

/**
 * Class Materia
 * @package App\Models
 * @version April 16, 2020, 8:46 pm UTC
 *
 * @property \Illuminate\Database\Eloquent\Collection cursoMateria
 * @property string name
 */
class EntregaFile extends AbstractBaseModel
{

    public $table = 'entrega_files';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    public $fillable = [
        'entrega_id',
        'file_id',
        'devolucion',
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'entrega_id',
        'file_id',
        'devolucion',
    ];

    
    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function entrega()
    {
        return $this->belongsTo(\App\Models\Entrega::class, 'entrega_id');
    }

    public function file()
    {
        return $this->belongsTo(\App\Models\File::class, 'File_id');
    }

        
}