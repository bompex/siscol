<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model as Model;

/**
 * Class Materia
 * @package App\Models
 * @version April 16, 2020, 8:46 pm UTC
 *
 * @property \Illuminate\Database\Eloquent\Collection cursoMateria
 * @property string name
 */
class Materia extends Model
{

    public $table = 'materias';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    public static $nivel = [ '0' => 'Secundario',
                             '1' => 'Primario',
                                ];

    public static $gender = [ 'A' => 'Ambos',
                            'm' => 'Masculino',
                            'f' => 'Femenino',
                                ];


    public $fillable = [
        'name',
        'color',
        'primario',
        'gender'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'name' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'name' => 'required'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function cursoMateria()
    {
        return $this->hasMany(\App\Models\CursoMateria::class, 'materia_id');
    }
}
