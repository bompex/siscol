<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model as Model;

/**
 * Class Administrativo
 * @package App\Models
 * @version April 16, 2020, 8:45 pm UTC
 *
 * @property \App\Models\User user
 * @property integer user_id
 * @property string firstname
 * @property string lastname
 * @property string dni
 * @property string|\Carbon\Carbon birthdate
 * @property string telefono
 * @property string email
 * @property integer created_by
 * @property integer updated_by
 */
class Administrativo extends AbstractBaseModel
{

    public $table = 'administrativos';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    public static $roles = [ 'administrativo' => 'Administrativo', 'preceptor' => 'Preceptor', 'secretario' => 'Secretario', 'director'=> 'Director'];




    public $fillable = [
        'user_id',
        'firstname',
        'lastname',
        'dni',
        'birthdate',
        'telefono',
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'firstname' => 'string',
        'lastname' => 'string',
        'dni' => 'string',
        'birthdate' => 'datetime',
        'telefono' => 'string',
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'firstname' => 'required',
        'lastname' => 'required',
        'dni' => 'required',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function user()
    {
        return $this->belongsTo(\App\Models\User::class, 'user_id');
    }
}
