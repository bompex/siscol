<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model as Model;

/**
 * Class Materia
 * @package App\Models
 * @version April 16, 2020, 8:46 pm UTC
 *
 * @property \Illuminate\Database\Eloquent\Collection cursoMateria
 * @property string name
 */
class Calificacion extends AbstractBaseModel
{

    public $table = 'calificaciones';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    public $fillable = [
        'curso_materia_id',
        'alumno_id',
        'profesor_id',
        'evaluacion_id',
        'status',
        'publica',
        'nota',
        'observacion',
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'nota' => 'required',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function alumno()
    {
        return $this->belongsTo(\App\Models\Alumno::class, 'alumno_id');
    }

    public function profesor()
    {
        return $this->belongsTo(\App\Models\Profesor::class, 'profesor_id');
    }


    public function evaluacion()
    {
        return $this->belongsTo(\App\Models\Evaluacion::class, 'evaluacion_id');
    }

    public function cursoMateria()
    {
        return $this->belongsTo(\App\Models\CursoMateria::class, 'curso_materia_id');
    }

    public function publish()
    {
        $this->publica = 1;
        $this->save();
    }
    
}