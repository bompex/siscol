<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model as Model;



class Anuncio extends AbstractBaseModel
{

    public $table = 'anuncios';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    public $fillable = [
        'curso_materia_id',
        'subject',
        'body',
        'fecha',
        'fecha_inicio',
        'fecha_fin',

    ];

     public static $rules = [
        'subject' => 'required',
        'body' => 'required'
    ];

    public function files()
    {
        return $this->belongsToMany(\App\Models\File::class, 'anuncio_files');
    }

	public function cursoMateria()
    {
        return $this->belongsTo(\App\Models\CursoMateria::class, 'curso_materia_id');
    }

    public function disable()
    {
        $this->deleted_at = date('Y-m-d H:i:s',time());
        $this->save();
    }


    
}