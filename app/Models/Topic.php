<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model as Model;

/**
 * Class Materia
 * @package App\Models
 * @version April 16, 2020, 8:46 pm UTC
 *
 * @property \Illuminate\Database\Eloquent\Collection cursoMateria
 * @property string name
 */
class Topic extends AbstractBaseModel
{

    public $table = 'topics';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    public $fillable = [
        'user_id',
        'topic_parent_id',
        'subject',
        'body',
        'active',
        'private',
        'allow_comments',
        'sort'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'user_id' => 'integer',
        'topic_parent_id' => 'integer',
        'subject' => 'string',
        'body' => 'string',
        'active' => 'boolean',
        'private' => 'boolean',
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'subject' => 'required',
        'body' => 'required'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function user()
    {
        return $this->belongsTo(\App\Models\User::class, 'user_id');
    }


    public function cursoMateria()
    {
        return $this->belongsToMany(\App\Models\CursoMateria::class, 'topic_curso_materia');
    }

    // public function parentTopic()
    // {
    //     return $this->belongsTo(\App\Models\Topic::class, 'topic_parent_id');
    // }

    public function comments(){
        return $this->hasMany(\App\Models\TopicComment::class, 'topic_id');
    }

    public function files()
    {
        return $this->belongsToMany(\App\Models\File::class, 'topic_files');
    }

    public function disable()
    {
        $this->deleted_at = date('Y-m-d H:i:s',time());
        $this->save();
    }

    
}