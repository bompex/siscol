<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model as Model;

/**
 * 
 * Class ActivityLog
 * @package App\Models
 * @version September 24, 2019, 3:34 pm UTC
 *
 */
class ActivityLog extends Model {

    public $table = 'activity_logs';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    protected $request;
    protected $user;
    protected $agent;


    public $fillable = [
        'user_id',
        'route',
        'method',
        'data',
        'device_type',
        'device',
        'browser',
        'platform'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'user_id' => 'integer',
        'route' => 'string',
        'method' => 'string',
        'data' => 'string',
        'device_type' => 'string',
        'device' => 'string',
        'browser' => 'string',
        'platform' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function user()
    {
        return $this->belongsTo(\App\Models\User::class);
    }

    public function __construct($request, $user, $agent) {
        $this->user = $user;
        $this->request = $request;
        $this->agent = $agent;
    }

    public function process() {

        $browser = $this->agent->browser();
        $bversion = $this->agent->version($browser);
        
        $platform = $this->agent->platform();
        $pversion = $this->agent->version($platform);

        $data = $this->request->all();
        unset($data['password']);

        $this->device = $this->agent->device();
        $this->browser = $browser . " " . $bversion;
        $this->platform = $platform . " " . $pversion;     
        $this->ip = $this->getClientIp();   



        if($this->agent->isPhone()) {
            $this->device_type = 'mobile';
        }
        else if($this->agent->isTablet()) {
            $this->device_type = 'tablet';    
        }
        else if($this->agent->isDesktop()) {
            $this->device_type = 'desktop';
        }
        else if ($this->agent->isRobot()) {
            $this->device_type = 'robot';
        }
        else {
            $this->device_type = 'n/a';
        }

        if($this->user) {
            $this->user_id = $this->user->id;    
        }
        else {
            $this->user_id = null;
        }
        $this->route = $this->request->path();
        $this->method = $this->request->method();
        $this->data = json_encode($data);

        $this->save();
    }

    function getClientIp() {
        if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
            $ip = $_SERVER['HTTP_CLIENT_IP'];
        } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
            $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
        } else {
            $ip = $_SERVER['REMOTE_ADDR'];
        }
        $ip_arr = explode(',',$ip);
        if(is_array($ip_arr) && count($ip_arr) >1)
        {
            $ip = $ip_arr[0];
        }
        return $ip;
    }

}
