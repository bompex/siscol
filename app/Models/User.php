<?php

namespace App\Models;

use Zizaco\Entrust\Traits\EntrustUserTrait;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Tymon\JWTAuth\Contracts\JWTSubject;
use Illuminate\Database\Eloquent\SoftDeletes;
use JWTAuth;
use Exception;

class User extends Authenticatable implements JWTSubject
{
    use Notifiable;
    use EntrustUserTrait { restore as private restoreA; }
    use SoftDeletes { restore as private restoreB; }

    protected $dates = ['deleted_at'];

    public static $rules = [
        'name' => 'required',   
        'email' => 'required|email|unique:users',
        'password' => 'required',
        'roles' => 'required|array|min:1'
    ];

    protected $fillable = [
        'name', 'email', 'password'
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    protected $appends = ["firstname, lastname"];

    public function restore() {
        $this->restoreA();
        $this->restoreB();
    }

    public function sendPasswordResetNotification($token) {
        $this->notify(new \App\Models\PasswordReset($token));
    }  

    public function isAdmin() {
        return $this->hasRole('admin');
    }

    public function isProfesor() {
        return $this->hasRole('profesor');
    }
    public function isAlumno() {
        return $this->hasRole('alumno');
    }
    public function isAdministrativo() {
        return $this->hasRole('administrativo');
    }
    public function isDirector() {
        return $this->hasRole('director');
    }
    public function isSecretario() {
        return $this->hasRole('secretario');
    }
    public function isPreceptor() {
        return $this->hasRole('preceptor');
    }

    public function __toString() {
        $text =  $this->name;
        return $text;
    }  

    public function getDescriptionAttribute() {
        $text =  $this->name;
        return $text;
    }


    public function getJWTIdentifier() {
        return $this->getKey();
    }

    public function getJWTCustomClaims() {
        return [];
    }

    public static function authenticate($credentials) {
        if (!isset($credentials['email']) || !isset($credentials['password'])) {
            throw new Exception('Datos inválidos', 500);
        }
        try {
            // verify the credentials and create a token for the user
            if (!$token = JWTAuth::attempt($credentials)) {
                throw new Exception('Credenciales inválidas.', 401);
            }
        } catch (JWTException $e) {
            // something went wrong
            throw new Exception('No se pudo autenticar al usuario.', 500);
        }
        return $token;
    }

    public static function getUserFromToken() {
        try {
            if (!$user = JWTAuth::parseToken()->authenticate()) {
                throw new Exception('user_not_found', 404);
            }
        } catch (Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {
            throw new Exception($e->getMessage(), $e->getStatusCode());
        } catch (Tymon\JWTAuth\Exceptions\TokenInvalidException $e) {
            throw new Exception('token_invalid', $e->getStatusCode());
        } catch (Tymon\JWTAuth\Exceptions\JWTException $e) {
            throw new Exception('token_absent', $e->getStatusCode());
        }
        return $user;
    }

    public static function getToken() {
        $token = JWTAuth::getToken();
        if (!$token) {
            throw new Exception("Invalid token");
        }
        try {
            $refreshToken = JWTAuth::refresh($token);
            return $refreshToken;
        } catch (JWTException $ex) {
            throw new Exception("Something went wrong");
        }
    }

    public function setPasswordAttribute($pass) {
        if(strlen($pass) == 0) return;
        $this->attributes['password'] = bcrypt($pass);
    }


    public function administrativo()
    {
        return $this->hasOne(\App\Models\Administrativo::class);
    }

    public function profesor()
    {
        return $this->hasOne(\App\Models\Profesor::class);
    }

    public function alumno()
    {
        return $this->hasOne(\App\Models\Alumno::class);
    }

    public function getFirstnameAttribute(){
        if($this->alumno){
            return $this->alumno->firstname;
        }
        if($this->profesor){
            return $this->profesor->firstname;
        }
        return $this->administrativo->firstname;
    }
    public function getLastnameAttribute(){
        if($this->alumno){
            return $this->alumno->lastname;
        }
        if($this->profesor){
            return $this->profesor->lastname;
        }
        return $this->administrativo->lastname;
    }
}
