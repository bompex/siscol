<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model as Model;

/**
 * Class Materia
 * @package App\Models
 * @version April 16, 2020, 8:46 pm UTC
 *
 * @property \Illuminate\Database\Eloquent\Collection cursoMateria
 * @property string name
 */
class MessageFile extends AbstractBaseModel
{

    public $table = 'message_files';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    public $fillable = [
        'message_id',
        'file_id',
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'message_id',
        'file_id',
    ];

    
    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function message()
    {
        return $this->belongsTo(\App\Models\Message::class, 'message_id');
    }

    public function file()
    {
        return $this->belongsTo(\App\Models\File::class, 'File_id');
    }

        
}