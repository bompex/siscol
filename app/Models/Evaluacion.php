<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model as Model;

/**
 * Class Materia
 * @package App\Models
 * @version April 16, 2020, 8:46 pm UTC
 *
 * @property \Illuminate\Database\Eloquent\Collection cursoMateria
 * @property string name
 */
class Evaluacion extends AbstractBaseModel
{

    public $table = 'evaluaciones';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    public static $types = [ '1' => 'Examen', '2' => 'Trabajo Práctico'];

    public $fillable = [
        'profesor_id',
        'curso_materia_id',
        'type_id',
        'status',
        'subject',
        'body',
        'fecha_public',
        'fecha_inicio',
        'fecha_fin',
        'allow_comments',
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'profesor_id' => 'integer',
        'curso_materia_id' => 'integer',
        'type_id' => 'integer',
        'status' => 'integer',
        'subject' => 'string',
        'body' => 'string',
        'fecha_inicio' => 'datetime',
        'fecha_fin' => 'datetime',
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'subject' => 'required',
    ];

    protected $appends = ["type"];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function profesor()
    {
        return $this->belongsTo(\App\Models\Profesor::class, 'profesor_id');
    }


    public function cursoMateria()
    {
        return $this->belongsTo(\App\Models\CursoMateria::class, 'curso_materia_id');
    }

    public function comments(){
        return $this->hasMany(\App\Models\EvaluacionComment::class, 'evaluacion_id');
    }

    public function files()
    {
        return $this->belongsToMany(\App\Models\File::class, 'evaluacion_files');
    }

    public function calificaciones(){
        return $this->hasMany(\App\Models\Calificacion::class, 'evaluacion_id');
    }

    public function getTypeAttribute(){
        return self::$types[$this->type_id];
    }

    public function disable()
    {
        $this->deleted_at = date('Y-m-d H:i:s',time());
        $this->save();
    }
}