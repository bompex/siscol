<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model as Model;



class AsistenciaAlumno extends AbstractBaseModel
{

    public $table = 'asistencia_alumno';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    public $fillable = [
        'asistencia_id',
        'alumno_id',
        'status',
    ];

    public function asistencia()
    {
        return $this->belongsTo(\App\Models\Asistencia::class, 'asistencia_id');
    }

    public function alumno()
    {
        return $this->belongsTo(\App\Models\Alumno::class, 'alumno_id');
    }

    
}