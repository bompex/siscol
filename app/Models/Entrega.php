<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model as Model;

/**
 * Class Materia
 * @package App\Models
 * @version April 16, 2020, 8:46 pm UTC
 *
 * @property \Illuminate\Database\Eloquent\Collection cursoMateria
 * @property string name
 */
class Entrega extends AbstractBaseModel
{

    public $table = 'entregas';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    public $fillable = [
        'alumno_id',
        'evaluacion_id',
        'observacion',
        'status',
        'profesor_observacion',
        'profesor_file_id',
    ];


    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'alumno_id' => 'integer',
        'evaluacion_id' => 'integer',
        'status' => 'integer',
        'observacion' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function alumno()
    {
        return $this->belongsTo(\App\Models\Alumno::class, 'alumno_id');
    }


    public function evaluacion()
    {
        return $this->belongsTo(\App\Models\Evaluacion::class, 'evaluacion_id');
    }

    public function files()
    {
        return $this->belongsToMany(\App\Models\File::class, 'entrega_files')->withPivot('devolucion');
    }

    public function profesor_file()
    {
        return $this->belongsTo(\App\Models\File::class, 'profesor_file_id');
    }
    
}