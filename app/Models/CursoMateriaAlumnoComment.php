<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model as Model;

/**
 * Class CursoMateria
 * @package App\Models
 * @version April 25, 2020, 9:14 pm UTC
 *
 * @property \App\Models\Curso curso
 * @property \App\Models\Materia materia
 * @property \App\Models\Profesore profesor
 * @property \Illuminate\Database\Eloquent\Collection cursoMateriaHorarios
 * @property integer curso_id
 * @property integer profesor_id
 * @property integer materia_id
 * @property integer created_by
 * @property integer updated_by
 */
class CursoMateriaAlumnoComment extends AbstractBaseModel
{

    public $table = 'curso_materia_alumno_comments';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    public $fillable = [
        'curso_materia_id',
        'profesor_id',
        'alumno_id',
        'body',
        'status',
    ];


    protected $appends = ["dianame"];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function cursoMateria()
    {
        return $this->belongsTo(\App\Models\CursoMateria::class, 'curso_materia_id');
    }

    public function Alumno()
    {
        return $this->belongsTo(\App\Models\Alumno::class, 'alumno_id');
    }

    public function Profesor()
    {
        return $this->belongsTo(\App\Models\Profesor::class, 'profesor_id');
    }

}
