<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model as Model;

/**
 * Class Curso
 * @package App\Models
 * @version April 16, 2020, 8:46 pm UTC
 *
 * @property \Illuminate\Database\Eloquent\Collection cursoAlumnos
 * @property \Illuminate\Database\Eloquent\Collection cursoMateria
 * @property string name
 * @property string turno
 * @property string date_from
 * @property string date_to
 * @property boolean active
 * @property integer created_by
 * @property integer updated_by
 */
class Curso extends AbstractBaseModel
{

    public $table = 'cursos';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    public static $turnos = [ 'mañana' => 'Mañana', 'tarde' => 'Tarde', 'noche' => 'Noche'];

    public static $horario_turno = [ 'mañana' => ['from' => '7', 'to' => '17'],
                                    'tarde' => ['from' => '12', 'to' => '18'],
                                    'noche' => ['from' => '19', 'to' => '23'],
                                ];

    public static $nivel = [ '0' => 'Secundario',
                             '1' => 'Primario',
                                ];

    Public static $hora_fraction = 15;

    public $fillable = [
        'name',
        'turno',
        'date_from',
        'date_to',
        'active',
        'primario',
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'name' => 'string',
        'turno' => 'string',
        'date_from' => 'date',
        'date_to' => 'date',
        'active' => 'boolean',
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'name' => 'required',
        'turno' => 'required',
        'date_from' => 'required',
        'date_to' => 'required',
        'active' => 'required'
    ];

    protected $appends = ["nivel", "asistenciaHoy"];

    public function getNivelAttribute(){
        return self::$nivel[$this->primario];
    }
    public function getasistenciaHoyAttribute(){
        $asist = $this->asistencias()->where('fecha',date('Y-m-d',time()))->whereNull('curso_materia_id')->first();
        if($asist){
            return $asist;
        }
        else{
            return false;
        }

    }
    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function cursoAlumnos()
    {
        $return = $this->hasMany(\App\Models\CursoAlumno::class, 'curso_id')
                    ->select('curso_alumno.*')
                    ->join('alumnos', 'alumnos.id', '=', 'curso_alumno.alumno_id')
                    ->whereNull('alumnos.deleted_at')
                    ->orderBy('alumnos.lastname', 'ASC')
                    ->orderBy('alumnos.firstname', 'ASC');



        
        return $return;
    }

    public function alumnos()
    {
        return $this->belongsToMany(\App\Models\Alumno::class, 'curso_alumno')->whereNull('curso_alumno.deleted_at');
    }

    public function cursoMaterias()
    {
        return $this->hasMany(\App\Models\CursoMateria::class, 'curso_id');
    }

    public function cursoAlumnosGenero($gender = False)
    {
        if(!$gender || $gender == "A"){
            return $this->cursoAlumnos;
        }

        $cursoAlumnos = [];
        foreach($this->cursoAlumnos as $cursoAlumno){
        
            if($cursoAlumno->alumno->gender == $gender){
                $cursoAlumnos[] = $cursoAlumno;
            }
        }
        return $cursoAlumnos;
    }

    public function cursoMateriasGenero($gender = False)
    {
        if(!$gender){
            return $this->cursoMaterias;
        }

        $cursoMaterias = [];
        foreach($this->cursoMaterias as $cursoMateria){
        
            if($cursoMateria->materia->gender == $gender || $cursoMateria->materia->gender == 'A'){
                $cursoMaterias[] = $cursoMateria;
            }
        }
        return $cursoMaterias;
    }

    public function asistencias()
    {
        return $this->hasMany(\App\Models\Asistencia::class, 'curso_id');
    }

    public function getFullSchedule($gender = false){
        $schedule = [];

        foreach($this->cursoMateriasGenero($gender) as $cursoMateria){
            $schedule[$cursoMateria->id]['name'] = $cursoMateria->materia->name;
            $schedule[$cursoMateria->id]['color'] = $cursoMateria->materia->color;
            $schedule[$cursoMateria->id]['schedule'] = $cursoMateria->getSchedule();
        }

        return $schedule;
    }

    public function getAnuncios($gender = false){
        $cm = $this->cursoMateriasGenero($gender);
        $cm_ids = [];
        foreach($cm as $item){
            $cm_ids[] = $item->id;
        }
        $anuncios = Anuncio::whereIn('curso_materia_id', $cm_ids)->where('fecha','<=',date('Y-m-d H:i:s',time()))->orderBy('fecha', 'desc')->get();
        return $anuncios;
    }

    public function getCalendar($gender = false, $is_admin=false){
        $out = [];
        
        foreach($this->cursoMateriasGenero($gender) as $cursoMateria){
            $cal = $cursoMateria->getCalendar($is_admin);
            $out = array_merge($cal, $out);
        }

        return $out;
    }

}
