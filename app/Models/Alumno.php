<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model as Model;

/**
 * Class Alumno
 * @package App\Models
 * @version April 16, 2020, 8:24 pm UTC
 *
 * @property \App\Models\User user
 * @property \Illuminate\Database\Eloquent\Collection cursoAlumno
 * @property integer user_id
 * @property string firstname
 * @property string lastname
 * @property string dni
 * @property string|\Carbon\Carbon birthdate
 * @property string telefono
 * @property string email
 * @property integer created_by
 * @property integer updated_by
 */
class Alumno extends AbstractBaseModel
{

    public $table = 'alumnos';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    public static $gender = [ 'm' => 'Masculino', 'f' => 'Femenino'];




    public $fillable = [
        'user_id',
        'firstname',
        'lastname',
        'dni',
        'birthdate',
        'telefono',
        'gender',
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'firstname' => 'string',
        'lastname' => 'string',
        'dni' => 'string',
        'birthdate' => 'datetime',
        'telefono' => 'string',
        'gender' => 'string',
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'firstname' => 'required',
        'lastname' => 'required',
        'dni' => 'required',
        'birthdate' => 'required',
        'gender' => 'required'
    ];


    protected $appends = ["curso_actual", "curso_ultimo", "curso", "materias", "nombre_curso","curso_previo"];
    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function user()
    {
        return $this->belongsTo(\App\Models\User::class, 'user_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function cursoAlumno() 
    {
        return $this->hasMany(\App\Models\CursoAlumno::class, 'alumno_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function getCursoAttribute()
    {
        $curso_alumno =  $this->cursoAlumno()->where('status',1)->orderBy('created_at', 'desc')->get();

        if(!$curso_alumno || empty($curso_alumno) || count($curso_alumno) == 0)
            return false;

        return $curso_alumno[0]->curso;
    }

    public function getCursoActualAttribute(){
        $curso = null;
        foreach($this->cursoAlumno as $cursoAlumno){
            if($cursoAlumno->status == 1 && $cursoAlumno->curso && $cursoAlumno->curso->active == 1){
                
                $curso = $cursoAlumno->curso;
            }            
        }
        if($curso){
            return $curso;
        }else{
            return false;
        }
    }
    public function getCursoUltimoAttribute(){
        $curso = null;
        foreach($this->cursoAlumno as $cursoAlumno){
            if($cursoAlumno->status == 1){
                
                $curso = $cursoAlumno->curso;
            }            
        }
        if($curso){
            return $curso;
        }else{
            return false;
        }
    }

    public function getCursoPrevioAttribute(){
        $curso = null;
        foreach($this->cursoAlumno as $cursoAlumno){
            $c = $cursoAlumno->curso;

            if($c && $cursoAlumno->status == 1  && date('Y',strtotime($c->date_from)) == date("Y", strtotime('-1 Year')) ) {
                $curso = $cursoAlumno->curso;
            }            
        }
        if($curso){
            return $curso;
        }else{
            return false;
        }
    }

    public function getMateriasAttribute()
    {
        return $this->curso->cursoMateriasGenero($this->gender);
    }

    public function getNombreCursoAttribute(){
        $curso = $this->curso;

        return $this->lastname.", ".$this->firstname." (".$curso->name.")";

    }


    public function entregas(){
        return $this->hasMany(\App\Models\Entrega::class, 'alumno_id');
    }

    public function calificaciones(){
        return $this->hasMany(\App\Models\Calificacion::class, 'alumno_id');
    }

    public function asistencias(){
        return $this->hasMany(\App\Models\AsistenciasAlumno::class, 'alumno_id');
    }

    public function entrega($evaluacion_id){
        $entrega = $this->entregas()->where('evaluacion_id',$evaluacion_id)->orderBy('created_at', 'ASC')->get();

        if(!$entrega || count($entrega) == 0){
            return [];
        }
        
        return $entrega;
    }

    public function calificacion($evaluacion_id, $only_publish = false){
        
        $query = $this->calificaciones()->where('evaluacion_id',$evaluacion_id);

        if($only_publish){
            $query = $query->where('publica',1);
        }

        $calificacion = $query->orderBy('created_at', 'desc')->get();

        if(!$calificacion || count($calificacion) == 0){
            return false;
        }
        return $calificacion[0];
    }

    public function getCalificaciones($curso_materia_id, $type = null){
        $calificacion = [];

        if(!$type){
            $calificacion = $this->calificaciones()->where('calificaciones.curso_materia_id',$curso_materia_id)->whereNull('evaluacion_id')->orderBy('created_at', 'asc')->get();
        }else{
            $calificacion = $this->calificaciones()->join('evaluaciones', 'calificaciones.evaluacion_id', '=', 'evaluaciones.id')->where('calificaciones.curso_materia_id',$curso_materia_id)->where('evaluaciones.type_id',$type)->orderBy('evaluaciones.created_at', 'asc')->get();
        }
        
        return $calificacion;

    }

    public function getPromedioPeriodo($curso_materia_id){
        $calificacion = $this->calificaciones()->where('calificaciones.curso_materia_id',$curso_materia_id)->orderBy('created_at', 'asc')->get();
        $count = 0;
        $subtotal = 0;
        foreach($calificacion as $calif){
            $subtotal +=  $calif->nota;
            $count++;
        }
        if($count > 0){
            return number_format(($subtotal / $count), 2);
        }
        return 0;
    }

    public function getAsistencia($asistencia_id){
        $asistencia = $this->asistencias()->where('asistencias_alumno.asistencia_id',$asistencia_id)->first();

        return $asistencia;
    }

    public function cursoMateriaAlumnoComments(){
        return $this->hasMany(\App\Models\CursoMateriaAlumnoComment::class, 'alumno_id');
    }

    public function cursoMateriaAlumnoComment($curso_materia_id){
        return $this->cursoMateriaAlumnoComments()->where('curso_materia_id',$curso_materia_id)->first();
    }
}
