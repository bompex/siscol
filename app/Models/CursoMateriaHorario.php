<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model as Model;

/**
 * Class CursoMateria
 * @package App\Models
 * @version April 25, 2020, 9:14 pm UTC
 *
 * @property \App\Models\Curso curso
 * @property \App\Models\Materia materia
 * @property \App\Models\Profesore profesor
 * @property \Illuminate\Database\Eloquent\Collection cursoMateriaHorarios
 * @property integer curso_id
 * @property integer profesor_id
 * @property integer materia_id
 * @property integer created_by
 * @property integer updated_by
 */
class CursoMateriaHorario extends AbstractBaseModel
{

    public $table = 'curso_materia_horario';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    public static $dias = [ 
                            '1' => 'Lunes',
                            '2' => 'Martes',
                            '3' => 'Miércoles',
                            '4' => 'Jueves',
                            '5' => 'Viernes',
    ];


    public $fillable = [
        'curso_materia_id',
        'dia',
        'hora_inicio',
        'hora_fin',
        'note',
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'curso_materia_id' => 'integer',
        'hora_inicio' => 'time',
        'hora_fin' => 'time',
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'curso_materia_id' => 'required',
        'hora_inicio' => 'required',
        'hora_fin' => 'required',
    ];

    protected $appends = ["dianame"];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function cursoMateria()
    {
        return $this->belongsTo(\App\Models\CursoMateria::class, 'curso_materia_id');
    }

    public function getDianameAttribute(){
        return self::$dias[$this->dia];
    }

}
