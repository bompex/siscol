<?php 

namespace App\Models;

use Zizaco\Entrust\EntrustRole;

class Role extends EntrustRole {

	public function __toString() {
		return $this->display_name;
	}
	
}