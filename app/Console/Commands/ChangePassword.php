<?php 
namespace App\Console\Commands;

use Log;
use Illuminate\Console\Command;
use App\Models\User;

class ChangePassword extends Command
{

    protected $signature = 'temp:changePassword';

    protected $description = 'cambiar password prof. y alumnos';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        $users = User::all();

        foreach($users as $user){
            if($user->isAlumno()){
                if(isset($user->alumno->dni)){
                    $user->password = $user->alumno->dni;
                    $user->save();
                }
            }
            if($user->isProfesor()){
                if(isset($user->profesor->dni)){
                    $user->password = $user->profesor->dni;
                    $user->save();
                }
            }
        }
    }
}
