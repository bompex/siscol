<?php

namespace App\Utils;
use DB;
use App\Models\Message;
use App\Models\User;
use Illuminate\Support\Facades\Auth;

class MessageHelper 
{

	

    static public function getMyMessages(){
    	$cu_user = Auth::user();

    	$rows = DB::table('messages')->select(DB::raw('IF(sender_user_id = '.$cu_user->id.', receiver_user_id, sender_user_id) as user_id, max(created_at) as created_at, min(if(sender_user_id = '.$cu_user->id.', 1, viewed)) as viewed'))
                        ->where('created_at','>',date('Y-01-01', time()))
                        ->where(function ($query) use ($cu_user){
                            $query->where('sender_user_id', $cu_user->id)
                                ->orWhere('receiver_user_id', $cu_user->id );
                        })
						->groupBy('user_id')
						->orderBy('created_at', 'DESC')->get();
    	$notif = [];
    	foreach($rows as $row){
    		$n['user'] = User::find($row->user_id);
    		$n['created_at'] = $row->created_at;
    		$n['viewed'] =   $row->viewed;
    		$notif[] = $n;
    	}				

    	return $notif;
    }
}