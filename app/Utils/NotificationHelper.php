<?php

namespace App\Utils;
use App\Models\Notification;
use Illuminate\Support\Facades\Auth;

class NotificationHelper 
{

	static public function isUnreadNotification(){
    	$cu_user = Auth::user();

    	$notif = Notification::where('user_id',$cu_user->id)->where('viewed', 0)->first();

    	if($notif){
    		return true;
    	}
    	return false;
    }

    static public function getMyNotifications(){
    	$cu_user = Auth::user();

    	$notif = Notification::where('user_id',$cu_user->id)
    					->where('created_at','>',date('Y-01-01', time()))
    					->where(function ($query) {
			        		$query->where('viewed', 0)
			            		  ->orWhere('updated_at','>', date('Y-m-d H:i:s', time() - 886400) );
						})
						->orderBy('created_at', 'DESC')->get();
    	return $notif;
    }


    static public function humanTiming($time)
	{

	    $time = time() - $time; // to get the time since that moment
	    $time = ($time<1)? 1 : $time;
	    $tokens = array (
	        31536000 => 'año',
	        2592000 => 'mes',
	        604800 => 'semana',
	        86400 => 'día',
	        3600 => 'hora',
	        60 => 'minuto',
	        1 => 'segundo'
	    );

	    foreach ($tokens as $unit => $text) {
	        if ($time < $unit) continue;
	        $numberOfUnits = floor($time / $unit);
	        if($numberOfUnits > 1){
	        	if($text=='mes'){
	        		$text = $text.'es';
	        	}else{
	        		$text = $text.'s';
	        	}
	        }
	        return $numberOfUnits.' '.$text;
	    }

	}

	static public function read_notifications(){
		$notifications = self::getMyNotifications();
		foreach($notifications as $notif){
			if($notif->viewed == 0){
				$notif->viewed = 1;
				$notif->save();
			}
		}
	}

	static public function nofity($user, $message, $url = null, $class=''){
		$notif = new Notification();
		$notif->user_id = $user->id;
		$notif->message = $message;
		$notif->url = $url;
		$notif->class = $class;
		
		$notif->save();
	}

	static public function nofity_curso($curso, $message, $url = null, $class=''){
		foreach($curso->alumnos as $alumno){
			self::nofity($alumno->user, $message, $url, $class);
		}
	}

	static public function nofity_alumno($alumno, $message, $url = null, $class=''){
		self::nofity($alumno->user, $message, $url, $class);
	}

	static public function nofity_profesor($profesor, $message, $url = null, $class=''){
		self::nofity($profesor->user, $message, $url, $class);
	}

}