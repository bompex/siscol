<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();


Route::group(['middleware' => [ 'auth' ]], function(){

	Route::get('/', 'HomeController@index')->name('home');
	Route::get('/home', 'HomeController@index')->name('home');

	Route::post('file/upload',['as' => 'file.upload', 'uses' => 'FileController@upload']);
	Route::patch('file/upload',['as' => 'file.upload', 'uses' => 'FileController@upload']);

	
	Route::get('/foro/{curso_materia_id}', 'CursoMateriaForoController@index')->name('curso_materia_foro.index');
	Route::get('/foro/topic/show/{topic_id}', 'CursoMateriaForoController@showTopic')->name('curso_materia_foro.show_topic');
	Route::get('/foro/comentario/create/{topic_id}', 'CursoMateriaForoController@create_comment')->name('foro.create_comment');
	Route::post('/foro/comentario/store/{topic_id}', 'CursoMateriaForoController@store_comment')->name('foro.store_comment');
	Route::post('/evaluacion/comentario/store/{evaluacion_id}', 'EvaluacionController@store_comment')->name('evaluacion.store_comment');

	Route::get('/change_password', 'UserController@changePassword')->name('user.change_password');
	Route::post('/store_password', 'UserController@storePassword')->name('user.store_password');


	Route::post('/read_notif', 'NotificationController@read_notify')->name('notif.read');


	Route::get('/message/show/{user_id}', 'MessageController@show')->name('message.show');
	Route::post('/message/store/{user_id}', 'MessageController@store')->name('message.store');
	Route::post('/message/store_modal/{user_id}', 'MessageController@store_modal')->name('message.store_modal');

	Route::get('/anuncio/{id}', 'AnuncioController@show')->name('anuncio.show');


	Route::group(['middleware' => [ 'role:profesor'  ]], function(){	
		Route::get('/profesor_dashboard', 'HomeController@indexProfesor')->name('home.profesor');
		Route::get('/profesor_dashboard/curso_materia/{curso_materia_id}', 'ProfesorCursoMateriaController@index')->name('profesor_curso_materia.index');

		//TOPICS
		Route::get('/profesor_dashboard/curso_materia/topic/create/{curso_materia_id}', 'ProfesorCursoMateriaController@createTopic')->name('profesor_curso_materia.create_topic');
		Route::post('/profesor_dashboard/curso_materia/topic/store/{curso_materia_id}', 'ProfesorCursoMateriaController@storeTopic')->name('profesor_curso_materia.store_topic');
		Route::get('/profesor_dashboard/curso_materia/topic/edit/{curso_materia_id}/{id}', 'ProfesorCursoMateriaController@editTopic')->name('profesor_curso_materia.edit_topic');
		Route::patch('/profesor_dashboard/curso_materia/topic/update/{curso_materia_id}/{id}', 'ProfesorCursoMateriaController@updateTopic')->name('profesor_curso_materia.update_topic');
		Route::delete('/profesor_dashboard/curso_materia/topic/comment/remove/{topic_id}/{id}', 'CursoMateriaForoController@destroy_comment')->name('foro.remove_comment');
		Route::get('/profesor_dashboard/curso_materia/archive_topic/{curso_materia_id}/{id}', 'ProfesorCursoMateriaController@archiveTopic')->name('profesor_curso_materia.archive_topic');
		Route::get('/profesor_dashboard/curso_materia/publish_topic/edit/{curso_materia_id}/{id}', 'ProfesorCursoMateriaController@publishTopic')->name('profesor_curso_materia.publish_topic');
		Route::delete('/profesor_dashboard/curso_materia/topic/remove/{curso_materia_id}/{id}', 'ProfesorCursoMateriaController@destroyTopic')->name('profesor_curso_materia.remove_topic');
		Route::patch('/profesor_dashboard/curso_materia/topic/sort/{curso_materia_id}', 'ProfesorCursoMateriaController@sortTopic')->name('profesor_curso_materia.sort_topic');

		//EVALUACION
		Route::get('/profesor_dashboard/evaluacion/{curso_materia_id}', 'EvaluacionController@index')->name('evaluacion.index');
		Route::get('/profesor_dashboard/evaluacion/create/{type}/{curso_materia_id}', 'EvaluacionController@create')->name('evaluacion.create');
		Route::post('/profesor_dashboard/evaluacion/store/{curso_materia_id}', 'EvaluacionController@store')->name('evaluacion.store');
		Route::get('/profesor_dashboard/evaluacion/show/{id}', 'EvaluacionController@show')->name('evaluacion.show');
		Route::get('/profesor_dashboard/evaluacion/edit/{curso_materia_id}/{id}', 'EvaluacionController@edit')->name('evaluacion.edit');
		Route::patch('/profesor_dashboard/evaluacion/update/{curso_materia_id}/{id}', 'EvaluacionController@update')->name('evaluacion.update');
		Route::delete('/profesor_dashboard/evaluacion/remove/{curso_materia_id}/{id}', 'EvaluacionController@destroy_evaluacion')->name('evaluacion.remove');

		//ENTREGA
		Route::get('/profesor_dashboard/evaluacion/entrega/show/{evaluacion_id}/{alumno_id}', 'EvaluacionController@show_entrega')->name('evaluacion.show_entrega');
		Route::get('/profesor_dashboard/evaluacion/entrega/correccion/{entrega_id}', 'EvaluacionController@corregir_entrega')->name('evaluacion.corregir_entrega');
		Route::post('/profesor_dashboard/evaluacion/entrega/correccion/update/{entrega_id}', 'EvaluacionController@update_corregir_entrega')->name('evaluacion.update_corregir_entrega');

		//EVALUACION COMMENT
		Route::delete('/profesor_dashboard/evaluacion/comment/remove/{evaluacion_id}/{id}', 'EvaluacionController@destroy_comment')->name('evaluacion.remove_comment');

		//ANUNCIOS
		Route::get('/profesor_dashboard/anuncio/{curso_materia_id}', 'AnuncioController@index')->name('anuncio.index');
		Route::get('/profesor_dashboard/anuncio/create/{curso_materia_id}', 'AnuncioController@create')->name('anuncio.create');
		Route::post('/profesor_dashboard/anuncio/store/{curso_materia_id}', 'AnuncioController@store')->name('anuncio.store');
		Route::get('/profesor_dashboard/anuncio/edit/{curso_materia_id}/{id}', 'AnuncioController@edit')->name('anuncio.edit');
		Route::patch('/profesor_dashboard/anuncio/update/{curso_materia_id}/{id}', 'AnuncioController@update')->name('anuncio.update');
		Route::delete('/profesor_dashboard/anuncio/remove/{curso_materia_id}/{id}', 'AnuncioController@destroy')->name('anuncio.remove');

		//CALIFICACIONES
		Route::get('/profesor_dashboard/calificacion/create/{evaluacion_id}/{alumno_id}', 'CalificacionController@create')->name('calificacion.create');
		Route::post('/profesor_dashboard/calificacion/store/{evaluacion_id}/{alumno_id}', 'CalificacionController@store')->name('calificacion.store');
		Route::get('/profesor_dashboard/calificacion/create_concepto/{curso_materia_id}/{alumno_id}', 'CalificacionController@create_concepto')->name('calificacion.create_concepto');
		Route::post('/profesor_dashboard/calificacion/store_concepto/{curso_materia_id}/{alumno_id}', 'CalificacionController@store_concepto')->name('calificacion.store_concepto');
		Route::get('/profesor_dashboard/calificacion/edit/{calificacion_id}', 'CalificacionController@edit')->name('calificacion.edit');
		Route::patch('/profesor_dashboard/calificacion/update/{calificacion_id}', 'CalificacionController@update')->name('calificacion.update');
		Route::patch('/profesor_dashboard/calificacion/publish_all/{evaluacion_id}', 'CalificacionController@publishAll')->name('calificacion.publish_all');



		Route::get('/profesor_dashboard/curso_materia/nota_horario/create/{id}', 'ProfesorCursoMateriaController@addCursoMateriaNota')->name('profesor_curso_materia.create_horario_note');
		Route::post('/profesor_dashboard/curso_materia/nota_horario/store/{id}', 'ProfesorCursoMateriaController@storeCursoMateriaNota')->name('profesor_curso_materia.store_horario_note');

		Route::get('/profesor_dashboard/curso_materia/nota_alumno/create/{curso_materia_id}/{alumno_id}', 'ProfesorCursoMateriaController@addCursoMateriaAlumnoNota')->name('profesor_curso_materia.create_alumno_note');
		Route::post('/profesor_dashboard/curso_materia/nota_alumno/store/{curso_materia_id}/{alumno_id}', 'ProfesorCursoMateriaController@storeCursoMateriaAlumnoNota')->name('profesor_curso_materia.store_alumno_note');


		Route::get('/profesor_dashboard/curso_materia/history/cursos/', 'ProfesorCursoMateriaController@history')->name('profesor_curso_materia.history');
	});	

	Route::group(['middleware' => [ 'role:alumno'  ]], function(){	
		Route::get('/dashboard', 'HomeAlumnoController@index')->name('home.alumno');
		Route::get('/dashboard/curso_previo', 'HomeAlumnoController@curso_previo')->name('home_alumno.previo');

		Route::get('/dashboard/materia/nota_horario/{id}', 'HomeAlumnoController@showNota')->name('home_alumno.show_horario_note');


		Route::get('/dashboard/materia/{id}', 'HomeAlumnoController@indexMateria')->name('home_alumno.index_materia');

		Route::get('/dashboard/evaluacion/{curso_materia_id}', 'EvaluacionController@index_alumno')->name('evaluacion_alumno.index');
		Route::get('/dashboard/evaluacion/show/{id}', 'EvaluacionController@show_alumno')->name('evaluacion_alumno.show');

		Route::get('/dashboard/evaluacion/entrega/create/{evaluacion_id}', 'EvaluacionController@create_entrega_alumno')->name('evaluacion_alumno.create_entrega');
		Route::post('/dashboard/evaluacion/entrega/store/{evaluacion_id}', 'EvaluacionController@store_entrega_alumno')->name('evaluacion_alumno.store_entrega');
	});	
	

	if (config('app.env') === 'production') {
		\Illuminate\Support\Facades\URL::forceScheme('https');
	}

	

	Route::group(['middleware' => [ 'role:admin|administrativo|director|secretario|preceptor'  ]], function(){	

		Route::get('/select_role', 'HomeController@selectRole')->name('home.select_role');

		Route::get('/admin_dashboard', 'HomeController@indexAdmin')->name('home.admin');
		Route::get('/asistencias/', 'AsistenciaController@index')->name('asistencia.index');
		Route::get('/asistencias/list/{curso_id}', 'AsistenciaController@list')->name('asistencia.list');
		Route::get('/asistencias/show/{asistencia_id}', 'AsistenciaController@list')->name('asistencia.show'); /////////
		Route::get('/asistencias/create/{curso_id}', 'AsistenciaController@create')->name('asistencia.create');
		Route::post('/asistencias/store/{curso_id}', 'AsistenciaController@store')->name('asistencia.store');
		Route::get('/asistencias/edit/{asistencia_id}', 'AsistenciaController@create')->name('asistencia.edit');
		Route::patch('/asistencias/update/{asistencia_id}', 'AsistenciaController@store')->name('asistencia.update');

		Route::get('/supervision/', 'SupervisionController@index')->name('supervision.index');
		Route::get('/supervision/curso/{curso_id}', 'SupervisionController@curso')->name('supervision.curso');
		Route::get('/supervision/curso_materia/{curso_materia_id}', 'SupervisionController@cursoMateria')->name('supervision.curso_materia');
		Route::get('/supervision/materia/nota_horario/{id}', 'SupervisionController@showNota')->name('supervision.show_horario_note');
		Route::get('/supervision/alumno_nota/{curso_materia_id}/{alumno_id}', 'SupervisionController@CursoMateriaAlumnoNota')->name('supervision.alumno_nota_show');

		Route::get('/supervision/tablon/{curso_materia_id}', 'SupervisionController@indexTablon')->name('supervision.index_tablon');
		Route::get('/supervision/tablon/topic/{topic_id}', 'SupervisionController@showTopic')->name('supervision.show_topic');

		Route::get('/supervision/evaluacion/{curso_materia_id}', 'SupervisionController@indexEvaluacion')->name('supervision.index_evaluacion');
		Route::get('/supervision/evaluacion/show/{evaluacion_id}', 'SupervisionController@showEvaluacion')->name('supervision.show_evaluacion');
		Route::get('/supervision/evaluacion/entrega/show/{evaluacion_id}/{alumno_id}', 'SupervisionController@showEntrega')->name('supervision.show_entrega');
		
		Route::get('/supervision_message/', 'SupervisionMessageController@index')->name('supervision_message.index');
		Route::get('/supervision_message/show/{user1_id}/{user2_id}', 'SupervisionMessageController@show')->name('supervision_message.show');
	});
	

	Route::group(['middleware' => [ 'role:admin|administrativo|director|secretario'  ]], function(){	

		Route::resource('alumno', 'AlumnoController');

		Route::resource('profesor', 'ProfesorController');
		Route::get('/profesor/store_admin/{id}', 'ProfesorController@store_admin')->name('profesor.store_admin');

		Route::resource('administrativo', 'AdministrativoController');
		Route::get('/administrativo/store_profesor/{id}', 'AdministrativoController@store_profesor')->name('administrativo.store_profesor');

		Route::resource('materia', 'MateriaController');

		Route::resource('curso', 'CursoController');
		Route::group(['prefix' => 'curso'], function() {
			Route::get('/add_materias/{curso_id}', 'CursoController@addMateria')->name('curso.add_materias');
			Route::post('/store_materias/{curso_id}', 'CursoController@storeMaterias')->name('curso.store_materias');
			Route::get('/add_alumnos/{curso_id}', 'CursoController@addAlumnos')->name('curso.add_alumnos');
			Route::post('/store_alumnos/{curso_id}', 'CursoController@storeAlumnos')->name('curso.store_alumnos');
			Route::get('/add_materias_horario/{curso_id}', 'CursoController@addCursoMateriaHorario')->name('curso.add_materias_horario');
			Route::post('/store_materias_horario/{curso_id}', 'CursoController@storeCursoMateriaHorario')->name('curso.store_materias_horario');
			Route::get('/promote/{curso_id}', 'CursoController@createPromote')->name('curso.promote');
			Route::post('/store_promote/', 'CursoController@storePromote')->name('curso.store_promote');
			// 
		});

		

	});	

	Route::group(['middleware' => [ 'role:admin' ]], function(){	
		Route::resource('users', 'UserController');
	});	

	
	

});
