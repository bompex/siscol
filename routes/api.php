<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::post('/login', 'AuthController@authenticate');

// Route::get('/show', 'AuthController@show');

Route::group(['prefix' => 'campaign', 'middleware' => ['jwt.auth']], function () {
    Route::get('details', 'ReportController@getCampaignDetails')->name('campaign.details');
});

Route::group(['prefix' => 'audiences', 'middleware' => ['jwt.auth']], function () {
	Route::post('store', 'AudienceController@store')->name('audiences.store');
});

Route::get('/send_emails','ApiBaseController@sendEmails');	
Route::get('/attribution/update','AttributionReportController@update');

Route::post('/upload','AudienceController@uploadImg');