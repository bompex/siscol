
function initUploadConfig() {
    config = {
        type: 'POST',
        url: file_upload_url,
        dataType: 'json',

        add: function (e, data) {
            $('.progress .progress-bar').css('width', '0%');
            $('.progress').removeClass('hidden');
            data.submit();
        },
        done: function (e, data) {

            if(data.result.error){
                $('#upload_file_message').html(data.result.error);
            }else{
                var file_row = "<div class='file-row'><div><a href='"+data.result.path+"' target='blank'>"+data.result.name+"</a></div><input type='hidden' name='files[]' value='"+data.result.file_id+"' class='input-file' /><a href='#' onclick='delete_file(this)'><i class='glyphicon glyphicon-trash'></i></a></div>"
                $('.file-list').append(file_row);
            }
            $('.progress').addClass('hidden');
        },
        progressall: function (e, data) {
            $('#upload_file_message').html('');
            var progress = parseInt(data.loaded / data.total * 100, 10);
            $('.progress .progress-bar').css('width', progress + '%');
        }
    };
    return config;
}



$(function(){

  $('#ventana-modal').on("submit",'#message-form-modal',function(e){
    e.preventDefault();

    $('#message-form-modal #body').val(CKEDITOR.instances.body.getData());
    var formData = $(this).serialize();
    var actionURL = $(this).attr('action');

    $.ajax({
      url:actionURL+"?mode=modal",
      method:"POST",
      data:formData,
      success:function(data){
        $('#ventana-modal').html(data);
        $('.file_uploader').fileupload(initUploadConfig()).prop('disabled', !$.support.fileInput);
        CKEDITOR.replace( 'body',{plugins:'emoji,wysiwygarea,toolbar',height: 60}); 

          var chat = $('.chat-wrapper');
          chat.scrollTop(chat[0].scrollHeight);  
      },
      error:function(error){
      console.log(error);
      }
    });
  });

  $('.open_message_modal').on( "click", function() {
    var href = $(this).data('href');
      $.ajax({
          url: href,
          type : 'GET',
        success : function(data) {
          $('#ventana-modal').html(data);
          $('#ventana-modal').modal('show');  
          $('.file_uploader').fileupload(initUploadConfig()).prop('disabled', !$.support.fileInput);
          CKEDITOR.replace( 'body',{plugins:'emoji,link,basicstyles,div,wysiwygarea,toolbar',height: 70});

          setTimeout(function(){
            var chat = $('.chat-wrapper');
            chat.scrollTop(chat[0].scrollHeight);  
          }, 500);
          
        },

        // código a ejecutar si la petición falla;
         error : function(xhr, status) {
          console.log(status);
              alert('Disculpe, existió un problema');
          },
      });
  })

  $(document).on( "click",'a.open-in-modal', function(event) {
    event.preventDefault();
    event.stopPropagation();
    var href = $(this).attr('href');
      $.ajax({
          url: href,
          type : 'GET',
        success : function(data) {
          $('#ventana-modal').html(data);
          $('#ventana-modal').modal('show');  
          
        },

        // código a ejecutar si la petición falla;
         error : function(xhr, status) {
          console.log(status);
              alert('Disculpe, existió un problema');
          },
      });
  })

});



function mark_as_readed(elem){
  $(elem).find('.new-message-dot').remove();
}



